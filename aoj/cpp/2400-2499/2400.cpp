#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define cn first
#define pn second.first
#define tn second.second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int t, p, r; cin >> t >> p >> r, t;){
        vector<vi> wc = vector<vi>(t, vi(p, 0));

        vector< pair<int, pii> > res(t);
        rep(i, t) res[i].tn = i;

        rep(i, r){
            int tid, pid, tm; string msg; cin >> tid >> pid >> tm >> msg;
            tid--, pid--;

            if(msg == "CORRECT"){
                res[tid].cn--;
                res[tid].pn += wc[tid][pid] * 1200 + tm;
            }
            else{
                wc[tid][pid]++;
            }
        }

        sort(all(res));
        rep(i, t){
            cout << res[i].tn + 1 << " "<< res[i].cn * -1 << " " << res[i].pn << endl;
        }
    }

	return 0;
}
