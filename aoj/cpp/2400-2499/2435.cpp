#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using pii = pair<int, int>;
using vi = vector<int>;

int main(void){
    int m; cin >> m;
    map<string, pii> s2d;
    rep(i, m){
        string name; cin >> name;
        cin >> s2d[name].first >> s2d[name].second;
    }

    int n; cin >> n;
    stack<vi> stk;
    rep(loop, n){
        string op; cin >> op;
        
        if(op == "+" or op == "-" or op == "*" or op == "/"){
            if(stk.size() < 2){
                cout << "error" << endl;
                return 0;
            }
            vi b = stk.top(); stk.pop();
            vi a = stk.top(); stk.pop();

            vi c(256);
            rep(i, 256){
                rep(j, 256){
                    if(a[i] and b[j]){
                        int res;
                        if(op == "+") res = (i + j) & 255;
                        if(op == "-") res = (i - j) & 255;
                        if(op == "*") res = (i * j) & 255;
                        if(op == "/"){
                            if(j == 0){
                                cout << "error" << endl;
                                return 0;
                            }
                            res = (i / j) & 255;
                        }
                        c[res] = true;
                    }
                }
            }
            stk.push(c);
        }
        else {
            vi a(256);
            if(s2d.find(op) != end(s2d)){
                range(i, s2d[op].first, s2d[op].second + 1){
                    a[i] = true;
                }
            }
            else {
                stringstream ss(op);
                int num; ss >> num;
                a[num] = true;
            }

            stk.push(a);
        }
    }

    if(stk.size() != 1){
        cout << "error" << endl;
        return 0;
    }

    cout << "correct" << endl;

	return 0;
}
