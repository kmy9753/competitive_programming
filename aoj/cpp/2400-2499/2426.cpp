#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 30;

void compress(vi const& vec, map<int, int>& zip, vi& unzip){
    unzip = vec;

    sort(all(unzip));
    unzip.erase(unique(all(unzip)), unzip.end());
    unzip.pb(inf);

    rep(i, unzip.size()) zip[unzip[i]] = i;
}

int main(void){
    for(int n, m; cin >> n >> m;){
        vi xs(n), ys(n);
        rep(i, n) cin >> xs[i] >> ys[i];

        vi unzip_x, unzip_y;
        map<int, int> zip_x, zip_y;

        compress(xs, zip_x, unzip_x);
        compress(ys, zip_y, unzip_y);

        vvi sum(n + 2, vi(n + 2));

        rep(i, n) sum[zip_y[ys[i]] + 1][zip_x[xs[i]] + 1]++;
        rep(i, n + 1) rep(j, n + 1) sum[i + 1][j + 1] += sum[i + 1][j] + sum[i][j + 1] - sum[i][j];

        rep(i, m){
            int lux, luy, rdx, rdy; cin >> lux >> luy >> rdx >> rdy;

            lux = zip_x[*lower_bound(all(unzip_x), lux)];
            luy = zip_y[*lower_bound(all(unzip_y), luy)];
            rdx = zip_x[*upper_bound(all(unzip_x), rdx)];
            rdy = zip_y[*upper_bound(all(unzip_y), rdy)];

            cout << sum[rdy][rdx] - sum[rdy][lux] - sum[luy][rdx] + sum[luy][lux] << endl;
        }
    }

	return 0;
}
