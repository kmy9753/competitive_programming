#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using ll = long long;
using vi = vector<int>;
using vvi = vector<vi>;

int n;
vector<ll> adj;
vi power, sum_rest;

int res;

void dfs(int v, int sum, ll used, ll disabled){
    if(v == n){
        res = max(res, sum);
        return;
    }
    if(sum + sum_rest[v] < res) return;

    int nv = v + 1;

    dfs(nv, sum, used, disabled);

    if(not(disabled & (1LL << v))){
        dfs(nv, sum + power[v], used & (1LL << v), disabled | adj[v]);
    }
}

int main(void){
    for(; cin >> n, n;){
        res = 0;

        power = sum_rest = vi(n);
        adj = vector<ll>(n);

        vector<vector<string>> d_str(n);
        map<string, int> s2i;

        int sum = 0;
        rep(i, n){
            string name;
            int p, m; cin >> name >> p >> m;
            d_str[i] = vector<string>(m);
            for(auto & ee : d_str[i]) cin >> ee;

            s2i[name] = i;
            power[i] = p;

            sum += p;
        }

        sum_rest[0] = sum;
        rep(i, n - 1){
            sum_rest[i + 1] = sum_rest[i] - power[i];
        }

        rep(i, n){
            ll vec = 0;
            for(auto & e : d_str[i]){
                int idx = s2i[e];
                vec |= (1LL << idx);
            }
            adj[i] = vec;
        }

        dfs(1, power[0], 1LL, adj[0]);
        cout << res << endl;
    }

	return 0;
}
