#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = 1 << 30;
const ll mod=1000000007LL;
const int dx[]={0, 1, 1, 0, -1, -1, 0};
const int dy[2][7]={{1, 0, -1, -1, -1, 0, 0},
                    {1, 1,  0, -1,  0, 1, 0}};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

// (x, y)
using P = tuple<int, int>;
using State = tuple<int, int, int, int>;

const int N = 300;
const int offset = 102;

int main(void){
    int sx, sy, gx, gy; cin >> sx >> sy >> gx >> gy;
    int n; cin >> n;

    vi xs(n), ys(n);
    rep(i, n) cin >> xs[i] >> ys[i];

    int lx, ly; cin >> lx >> ly;
    map<P, bool> ng;
    rep(x, -lx - 1, lx + 2){
        rep(y, -ly - 1, ly + 2){
            ng[P(x, y)] = false;
        }
    }
    rep(i, n){
        int x = xs[i], y = ys[i];
        ng[P(x, y)] = true;
    }
    rep(x, -lx - 1, lx + 2){
        ng[P(x, -ly - 1)] = true;
        ng[P(x,  ly + 1)] = true;
    }
    rep(y, -ly - 1, ly + 2){
        ng[P(-lx - 1, y)] = true;
        ng[P( lx + 1, y)] = true;
    }

    priority_queue<State, vector<State>, greater<State>> q;
    q.push(State(0, sx, sy, 0));
    int min_cost[N][N][6];
    rep(i, N) rep(j, N) rep(k, 6) min_cost[i][j][k] = inf;

    int res = -1;
    while(q.size()){
        State cur = q.top(); q.pop();
        int cost, x, y, t; tie(cost, x, y, t) = cur;

        if(ng[P(x, y)]) continue;
        if(not chmin(min_cost[x + offset][y + offset][t], cost)) continue;

        // cerr << cost << ": (" << x << ", " << y << "), " << t << endl;

        if(x == gx and y == gy){
            res = cost;
            break;
        }

        int one_i = abs(x * y * t) % 6;

        rep(i, 7){
            int nx = x + dx[i], ny = y + dy[(x + 200) % 2][i];
            if(ng[P(nx, ny)]) continue;

            int nt = (t + 1) % 6;

            int ncost = cost;
            if(i != one_i) ncost++;

            if(min_cost[nx + offset][ny + offset][nt] != inf) continue;
            q.push(State(ncost, nx, ny, nt));
        }
    }

    cout << res << endl;

    return 0;
}
