#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int pos;
const string kVars = "abcdefghijk";
string formula1, formula2, res;

int mlt(int lhs, int rhs){
    return lhs && rhs;
}

int pls(int lhs, int rhs){
    return lhs || rhs;
}

int mns(int val){
    return !val;
}

int imp(int lhs, int rhs){
    return !(lhs && !rhs);
}

int operate(const string& formula, int& pos){
    int ret;

    if(formula.at(pos) == '('){
        pos++;

        int lhs = operate(formula, pos);
        char op = formula.at(pos++); if(op == '-') pos++;
        int rhs = operate(formula, pos);

        switch(op){
        case '*': { ret = mlt(lhs, rhs); break; }
        case '+': { ret = pls(lhs, rhs); break; }
        case '-': { ret = imp(lhs, rhs); break; }
        default : break; 
        }

        pos++;
    }

    else if(formula.at(pos) == '-'){
        bool isMns = true;
        while(formula.at(++pos) == '-') isMns = !isMns;

        ret = operate(formula, pos);
        if(isMns) ret = mns(ret);
    }

    else {
        ret = formula.at(pos) - '0';

        pos++;
    }

    return ret;
}

void dfs(int p){
    if(p == kVars.size()){
        if(operate(formula1, pos = 0) != operate(formula2, pos = 0)){
            res = "NO";
        }

        return;
    }

    rep(i, 2){
        string tmp1 = formula1, tmp2 = formula2;

        replace(all(formula1), kVars.at(p), (char)(i + '0'));
        replace(all(formula2), kVars.at(p), (char)(i + '0'));

        dfs(p + 1);

        formula1 = tmp1, formula2 = tmp2;
    }
}

int main(void){
    for(string in; cin >> in, in != "#"; cout << res << endl){
        res = "YES";

        replace(all(in), 'T', '1');
        replace(all(in), 'F', '0');
        replace(all(in), '=', ' ');

        stringstream ss(in);
        ss >> formula1;
        ss >> formula2;

        dfs(0);
    }

	return 0;
}
