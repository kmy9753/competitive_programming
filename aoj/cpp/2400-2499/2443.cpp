#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
map<vector<int>, int> memo[2];
int main(void){
    int n; cin >> n;

    vector<int> a(n);
    for(auto & e : a) cin >> e;

    int nn = (n - 1) / 2;

    int res = n - 1;
    rep(i, 2){
        queue<vector<int>> q;
        q.push(a);
        memo[i][a] = 0;

        while(q.size()){
            vector<int> b;
            b = q.front(); q.pop();

            if(i == 1 and memo[0].find(b) != end(memo[0])){
                res = min(res, memo[0][b] + memo[1][b]);
                continue;
            }

            if(memo[i][b] >= nn) continue;

            rep(j, n){
                range(k, j + 1, n){
                    vector<int> nb = b;
                    reverse(begin(nb) + j, begin(nb) + k + 1);

                    if(memo[i].find(nb) != end(memo[i])) continue;

                    memo[i][nb] = memo[i][b] + 1;
                    q.push(nb);
                }
            }
        }

        sort(begin(a), end(a));
    }

    cout << res << endl;

	return 0;
}
