#include <bits/stdc++.h>

typedef long long ll;
#define int ll

#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

// -----------------------
namespace SegmentTrees{
    template<typename T>
    class RMQ{
    private:
        int Pow2Fit(int _n){
            int d = 1;
            while((d << 1) <= _n) d <<= 1;
            return d;
        }

    public:
        vector<T> dat;
        int n, size;

        RMQ(int _n){
            n = Pow2Fit(_n) << 1;
            size = 2 * n - 1;
            dat = vector<T>(size, 0);
        }

        // node v := a (0-indexed)
        void set(int v, T a){

            // leaf
            v += n - 1;
            dat[v]=a;

            // update toward root
            while(v > 0){
                int parent = v = (v - 1) / 2;
                int chl = parent * 2 + 1, chr = parent * 2 + 2;
                dat[parent] = max(dat[chl], dat[chr]);
            }
        }

        T get(int v){ // v (0-indexed)
            return dat[v + n - 1];
        }

        T query(int a, int b){ // [a,b)
            return query(0, a, b, 0, n);
        }

    private:
        T query(int v, int a, int b, int l, int r){ // [a,b)
            if(r <= a || b <= l) return 0; // out range
            if(a <= l && r <= b) return dat[v]; // covered
 
            T vl = query(v * 2 + 1, a, b, l, (l + r) / 2),
              vr = query(v * 2 + 2, a, b, (l + r) / 2, r);

            return max(vl, vr);
        }
    };
}

using namespace SegmentTrees;
// -----------------------

signed main(){
    int n; cin >> n;
    RMQ<int> rmq(n);

    rep(i, n){
        int x; cin >> x;
        rmq.set(x - 1, rmq.query(0, x) + x);
    }

    cout << n * (n + 1) / 2 - rmq.query(0, n) << endl;
}
