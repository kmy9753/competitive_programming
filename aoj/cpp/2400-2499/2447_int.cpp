#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1LL << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
 
using vs = vector<string>;
using vvi = vector<vi>;
using vb = vector<bool>;
using vvb = vector<vb>;
using State = tuple<int, int, int, char>;

int main(void){
    int w, h; cin >> w >> h;

    vs field(h);
    vvi sw_idx(h, vi(w, -1));
    int sx, sy, tx, ty;
    rep(y, h){
        cin >> field[y];
        rep(x, w){
            if(field[y][x] == '%'){
                sx = x, sy = y;
                field[y][x] = '_';
            }
            if(field[y][x] == '&'){
                tx = x, ty = y;
                field[y][x] = '_';
            }
            if(isupper(field[y][x])){
                sw_idx[y][x] = field[y][x] - 'A';
                field[y][x] = '^';
            }
            if(islower(field[y][x])){
                sw_idx[y][x] = field[y][x] - 'a';
                field[y][x] = '_';
            }
        }
    }

    int n; cin >> n;
    vvi sw_field(h, vi(w));

    rep(i, n){
        rep(y, h){
            rep(x, w){
                char c; cin >> c;
                if(c == '*') sw_field[y][x] |= (1 << i);
            }
        }
    }

    queue<State> q;
    q.push(State(sx, sy, 0, '_'));

    vector<vector<vvi>> min_cost(h, vector<vvi>(w, vvi(1 << n, vi(2, inf))));
    min_cost[sy][sx][0][0] = 0;

    while(q.size()){
        int cx, cy, cb; char cf;
        tie(cx, cy, cb, cf) = q.front();
        q.pop();

        int cfi = (cf == '^');

        if(cx == tx and cy == ty){
            cout << min_cost[cy][cx][cb][cfi] << endl;
            return 0;
        }

        int nc = min_cost[cy][cx][cb][cfi] + 1;

        if(sw_idx[cy][cx] != -1){
            int idx = sw_idx[cy][cx];

            int nb = cb ^ (1 << idx);
            char nf = cf;
            if(sw_field[cy][cx] & (1 << idx)){
                nf = (cf == '^' ? '_' : '^');
            }
            int nfi = (nf == '^');
            if(chmin(min_cost[cy][cx][nb][nfi], nc)){
                q.push(State(cx, cy, nb, nf));
            }
        }

        if(field[cy][cx] == '|'){
            char nf = (cf == '^' ? '_' : '^');
            int nfi = (nf == '^');

            if(chmin(min_cost[cy][cx][cb][nfi], nc)){
                q.push(State(cx, cy, cb, nf));
            }
        }

        rep(i, 4){
            int nx = cx + dx[i], ny = cy + dy[i];

            if(field[ny][nx] == '#') continue;

            if(field[ny][nx] == '|' and chmin(min_cost[ny][nx][cb][cfi], nc)){
                q.push(State(nx, ny, cb, cf));
                continue;
            }

            bool nrev = false;
            rep(j, n){
                if((sw_field[ny][nx] & (1 << j)) and (cb & (1 << j))){
                    nrev ^= true;
                }
            }

            char nf;
            if(nrev) nf = (field[ny][nx] == '^' ? '_':'^');
            else     nf =  field[ny][nx];
            int nfi = (nf == '^');

            if(cf == nf and chmin(min_cost[ny][nx][cb][nfi], nc)){
                q.push(State(nx, ny, cb, nf));
            }
        }
    }

    cout << -1 << endl;

    return 0;
}
