#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using W = ll;
using edge = struct {int to, rev; W cap, flow, cost;};
using G = vector<vector<edge>>;

void add_edge(G &graph, int from, int to, W cap, W cost) {
    graph[from].push_back({to, int(graph[to].size()) , cap , 0 , cost});
    graph[to].push_back({from, int(graph[from].size()) - 1, 0 , 0, -cost});
}

W primal_dual(G &graph, int s, int t, int f) {
    const W inf = 1LL << 50;
    W res = 0;
    while (f) {
        int n = graph.size(), update;
        vector<W> dist(n, inf);
        vector<int> pv(n, 0), pe(n, 0);
        dist[s] = 0;

        rep(loop, n) {
            update = false;
            rep(v, n)rep(i, graph[v].size()) {
                edge &e = graph[v][i];
                if (e.cap > e.flow and chmin(dist[e.to], dist[v] + e.cost)) {
                    pv[e.to] = v, pe[e.to] = i;
                    update = true;

                }
            }
            if (!update) break;
        }

        if (dist[t] == inf) return -1;

        W d = f;

        for (int v = t; v != s; v = pv[v]){
            chmin(d, graph[pv[v]][pe[v]].cap - graph[pv[v]][pe[v]].flow);
        }

        f -= d, res += d * dist[t];

        for (int v = t; v != s; v = pv[v]) {
            edge &e = graph[pv[v]][pe[v]];
            e.flow += d;
            graph[v][e.rev].flow -= d;
        }
    }
    return res;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    for(int n; cin >> n, n;){
        vector<vi> dolls(n, vi(3));
        vi V(n);
        rep(i, n){
            auto& e = dolls[i];
            cin >> e[0] >> e[1] >> e[2];
            sort(_all(e));
            V[i] = e[0] * e[1] * e[2];
        }
        G g(2 * n + 2);

        rep(i, n){
            rep(j, n){
                bool ok = true;
                rep(k, 3){
                    if(dolls[i][k] >= dolls[j][k]) ok = false;
                }
                if(ok){
                    add_edge(g, 2 * i, 2 * j + 1, n, 0);
                }
            }
        }
        int s = 2 * n, t = s + 1;
        rep(i, n){
            add_edge(g, s, 2 * i, 1, 0);
            add_edge(g, 2 * i + 1, t, 1, 0);
            add_edge(g, 2 * i, t, n, V[i]);
        }

        cout << primal_dual(g, s, t, n) << endl;
    }

    return 0;
}
