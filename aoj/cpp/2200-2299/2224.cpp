#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using pii = pair<int, int>;
#define fst first
#define snd second

using Edge  = tuple<int, double>;
using State = tuple<double, int>;

constexpr double inf = 1e20;
constexpr double ub  = 1e6;

int main(void){
    int n, m; cin >> n >> m;

    vector<pii> pos(n);
    for(auto & e : pos) cin >> e.fst >> e.snd;

    vector<vector<Edge>> edge(n);
    double res = 0.0;
    rep(_, m){
        int a, b; cin >> a >> b;
        a--, b--;
        double dist = sqrt(pow(pos[a].snd - pos[b].snd, 2) + pow(pos[a].fst - pos[b].fst, 2));
        res += dist;

        edge[a].push_back(Edge(b, ub - dist));
        edge[b].push_back(Edge(a, ub - dist));
    }

    int start = 0;
    vector<bool> used(n);
    while(1){
        while(start < n and (edge[start].size() == 0 or used[start])) start++;
        if(start == n) break;

        priority_queue<State, vector<State>, greater<State>> q;
        q.push(State(ub, start));

        while(q.size()){
            double d; int v;
            tie(d, v) = q.top(); q.pop();

            if(used[v]) continue;
            used[v] = true;

            res -= (ub - d);

            for(auto & e : edge[v]){
                int nv; double nd;
                tie(nv, nd) = e;

                if(used[nv]) continue;
                q.push(State(nd, nv));
            }
        }
    }

    cout.precision(12);
    cout << res << endl;

    return 0;
}
