#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define X first
#define Y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int N = 200;
const int D = 100;

int dx[] = { 1, 1, 0,-1,-1, 0};
int dy[] = { 0, 1, 1, 0,-1,-1};

int main(void){
    for(int t, n, res; cin >> t >> n, t; cout << res << endl){
        res = 0;

        vector< vector<bool> > trap(N, vector<bool>(N, false));
        rep(i, n){
            int x, y; cin >> x >> y;
            trap[y + D][x + D] = true;
        }

        pii s; cin >> s.X >> s.Y;

        queue< pair<pii, int> > q;
        q.push(mp(mp(s.X + D, s.Y + D), 0));

        while(!q.empty()){
            pii p = q.front().first;
            int turn = q.front().second;
            q.pop();

            if(turn == t + 1) continue;
            if(trap[p.Y][p.X]) continue;

            res++;
            trap[p.Y][p.X] = true;

            rep(i, 6){
                q.push(mp(mp(p.X + dx[i], p.Y + dy[i]), turn + 1));
            }
        }
    }

	return 0;
}
