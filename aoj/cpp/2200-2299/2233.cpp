#include  <bits/stdc++.h>
#define range(i,a,b) for(int (i)=(a);(i)<(b);(i)++)
#define rep(i,n) range(i,0,n)
using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;
using State = tuple<double, int, int, int>;

int n;
double R, theta;

vector<vector<double>> dist, angle;
vector<vvi> min_dist;

const double pi = acos(-1);

int main(void){
    cin >> n;
    cin >> R >> theta;
    theta *= pi / 180.0;

    vector<int> x(n), y(n);
    rep(i, n) cin >> x[i] >> y[i];

    dist = angle = vector<vector<double>>(n, vector<double>(n));
    rep(i, n){
        rep(j, n){
            if(i == j) continue;

            int dx = x[j] - x[i], dy = y[j] - y[i];
            dist[i][j]  = sqrt(pow(dx, 2) + pow(dy, 2));
            angle[i][j] = atan2(dy, dx);
        }
    }

    vector<vvi> edge(n + 1, vvi(n + 1));
    rep(i, n){
        rep(j, n){
            if(i == j) continue;
            rep(k, n){
                if(j == k) continue;

                double dtheta = angle[j][k] - angle[i][j];
                while(dtheta < 0)      dtheta += 2 * pi;
                while(dtheta >= pi) dtheta -= 2 * pi;

                if(fabs(dtheta) <= theta){
                    edge[i][j].push_back(k);
                }
            }
        }
    }
    range(i, 1, n) edge[n][0].push_back(i);

    priority_queue<State, vector<State>, greater<State>> q;
    q.push(State(0.0, n, 0, 0));

    const int inf = R + 1234;
    min_dist = vector<vvi>(n + 1, vvi(n + 1, vi(10001, inf)));

    int res = 0;
    while(q.size()){
        double d;
        int prev, v, num;
        tie(d, prev, v, num) = q.top();
        q.pop();

        if(d > R or min_dist[prev][v][num] != inf) continue;
        min_dist[prev][v][num] = d;
        res = max(res, num);

        for(auto & nv : edge[prev][v]){
            if(min_dist[v][nv][num + 1] != inf) continue;
            q.push(State(d + dist[v][nv], v, nv, num + 1));
        }
    }

    cout << res << endl;

	return 0;
}

