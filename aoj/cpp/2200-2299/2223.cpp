#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,x,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
 
int h, w;

using State = tuple<int, int, int>;

// R U L D
vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

int total;
string res;

string i2c = "RULD";
map<char, int> c2i;

vector<string> field;

bool dfs(State cur, int cnt, string op){
    int y, x, dir_i;
    tie(y, x, dir_i) = cur;

    if(cnt == total){
        res = op;
        return true;
    }

    vi ndirs_i = { (dir_i + 1) % 4, dir_i, (dir_i + 3) % 4 };
    for(auto & ndir_i : ndirs_i){
        int ny = -1, nx = -1;
        rep(k, 100){
            int yy = y + k * dy[ndir_i], xx = x + k * dx[ndir_i];

            if(yy < 0 or h <= yy or
               xx < 0 or w <= xx){
                break;
            }

            if(field[yy][xx] == 'o'){
                ny = yy, nx = xx;
                break;
            }
        }

        if(ny == -1) continue;

        field[ny][nx] = '.';
        if(dfs(tie(ny, nx, ndir_i), cnt + 1, op + i2c[ndir_i])){
            return true;
        }
        field[ny][nx] = 'o';
    }

    return false;
}

int main(void){
    rep(i, 4) c2i[i2c[i]] = i;

    cin >> h >> w;
    
    field = vector<string>(h);
    State start;
    rep(y, h){
        cin >> field[y];

        rep(x, w){
            if(field[y][x] != 'o' and field[y][x] != '.'){
                int dir = c2i[field[y][x]];
                start = tie(y, x, dir);

                field[y][x] = '.';
            }
            if(field[y][x] == 'o'){
                total++;
            }
        }
    }

    dfs(start, 0, "");
    
    cout << res << endl;

    return 0;
}
