#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define L first
#define R second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int S[] = {-1, 0, 1, 2, 0, -1, 2, 0, 1, 2};

int main(void){
    int res;
    for(string steps; cin >> steps, steps != "#"; cout << res << endl){
        res = inf;
        pii foot = mp(1, 1);
     
        rep(z, 2){
            int cnt = 0;
            bool isR = !z;

            rep(i, steps.size()){
                int next = S[steps[i] - '0'];
                if(i == 0){
                    if(isR) foot.R = next;
                    else    foot.L = next;
                    isR = !isR;
                    continue;
                }
                if(isR){
                    if(foot.L <= next){
                        foot.R = next;
                        isR = !isR;
                    }
                    else{
                        foot.L = next;
                        cnt++;
                    }
                }
                else{
                    if(next <= foot.R){
                        foot.L = next;
                        isR = !isR;
                    }
                    else{
                        foot.R = next;
                        cnt++;
                    }
                }
            }
            res = min(res, cnt);
        }
    }

	return 0;
}
