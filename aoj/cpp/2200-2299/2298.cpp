#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, k, t, u, v, l; cin >> n >> k >> t >> u >> v >> l;){
        vi d(n);
        rep(i, n) cin >> d[i];

        double res = 0.;
        double default_tpm = 1. / u, speedup_tpm = 1. / v;
        int numD = 0;
        bool isSpeedup = false;
        int speedup_m = t * v, spend_m = 0;
        int ind = 0;

        range(i, 1, l + 1){
            res += (isSpeedup ? speedup_tpm:default_tpm);
            if(isSpeedup) spend_m++;

            if(ind < d.size() && i == d[ind]){
                if(isSpeedup){
                    if(numD == k){
                        spend_m = 0;
                    }
                    else{
                        numD++;
                    }
                }
                else{
                    isSpeedup = true;
                    spend_m = 0;
                }
                ind++;
            }

            if(!isSpeedup) continue;

            if(spend_m == speedup_m){
                if(numD > 0){
                    numD--;
                    spend_m = 0;
                }
                else{
                    isSpeedup = false;
                }
            }
        }
        
        printf("%.9f\n", res);
    }

	return 0;
}
