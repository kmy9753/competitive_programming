#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    vll h, t, p2e, e2p, speed, a, d;
    bool ok = true;
    ll sum = 0LL;
    rep(i, n + 1){
        ll hh, aa, dd, ss; cin >> hh >> aa >> dd >> ss;
        ll _e2p, _p2e, tt; _e2p = _p2e = tt = 0LL;
        if(i >= 1){
            _e2p = max(0LL, aa - d[0]);
            _p2e = max(0LL, a[0] - dd);

            if(_e2p == 0LL) continue;
            if(_p2e == 0LL){
                if(_e2p > 0LL){
                    cout << -1 << endl;
                    return 0;
                }
                continue;
            }
            tt = (hh + _p2e - 1) / _p2e;
        }

        h.emplace_back(hh);
        a.emplace_back(aa);
        d.emplace_back(dd);
        t.emplace_back(tt);
        e2p.emplace_back(_e2p);
        p2e.emplace_back(_p2e);
        speed.emplace_back(ss);
        sum += _e2p;
    }
    ll h_org = h[0];

    n = h.size(); 

    using R = double;
    vector<pair<R, int>> table(n - 1);
    rep(i, 1, n){
        if(speed[i] > speed[0]){
            h[0] -= e2p[i];
        }

        table[i - 1] = pair<R, int>(1.0 * t[i] / e2p[i], i);
    }
    sort(_all(table));

    ll turn = 0LL;
    for(auto& e : table){
        int i = e.second;

        h[0] -= sum * (t[i] - 1);
        sum  -= e2p[i];
        h[0] -= sum;

        if(h[0] <= 0LL) break;
    }

    if(h[0] <= 0LL){
        cout << -1 << endl;
    }
    else {
        cout << h_org - h[0] << endl;
    }

    return 0;
}
