#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define X first
#define Y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int dx[] = { 1, 1, 0,-1,-1, 0};
int dy[] = { 0, 1, 1, 0,-1,-1};

const int kOffset = 100;

int main(void){
    for(int n, t, res; cin >> t >> n, t; cout << res << endl){
        res = 0;

        vvi used, hasTrap;
        used = hasTrap = vvi(2 * kOffset, vi(2 * kOffset));

        rep(i, n){
            int x, y; cin >> x >> y;
            x += kOffset; y += kOffset;

            hasTrap[y][x] = true;
        }

        pii s; cin >> s.X >> s.Y;
        s.X += kOffset; s.Y += kOffset;

        queue< pair<pii, int> > q;
        q.push(mp(s, 0));

        while(!q.empty()){
            pii cur_pos = q.front().first;
            int cur_turn = q.front().second;
            q.pop();

            if(used[cur_pos.Y][cur_pos.X] || hasTrap[cur_pos.Y][cur_pos.X] || t < cur_turn) continue;

            used[cur_pos.Y][cur_pos.X] = true;
            res++;

            rep(i, 6){
                pair<pii, int> next(mp(cur_pos.X + dx[i], cur_pos.Y + dy[i]), cur_turn + 1);
                q.push(next);
            }
        }
    }

	return 0;
}
