#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=int(a);i<int(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=int(a)-1;i>=int(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = 1 << 29;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,x,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
 
/*--------------------------*/
using vvi = vector<vi>;
const int kMax_n = 5001; //$BD:E@?t$N:GBgCM(B

int n; //$BD:E@?t(B
vvi G(kMax_n);
vi match(kMax_n); //$B%^%C%A%s%0$N%Z%"(B
vi used(kMax_n);

// u$B$H(Bv$B$r7k$VJU$r%0%i%U$KDI2C$9$k(B
void add_edge(int u, int v){
    G[u].push_back(v);
    G[v].push_back(u);
}

// $BA}2C%Q%9$r(BDFS$B$GC5$9(B
bool dfs(int v){
    used[v] = true;
    
    for(auto u : G[v]){
        int w = match[u];

        if(w < 0 || (!used[w] && dfs(w))){
            match[v] = u;
            match[u] = v;

            return true;
        }
    }

    return false;
}

//$BFsJ,%0%i%U$N:GBg%^%C%A%s%0$r5a$a$k(B
int b_match(void){
    int res = 0;

    fill(_all(match), -1);

    rep(i, n){
        if(match[i] < 0){
            fill(_all(used), false);

            if(dfs(i)) res++;
        }
    }

    return res;
}

void init_G(void){
    for(auto & g : G) g.clear();
}
/*--------------------------*/

int main(void){
    for(int N, m, L; cin >> N >> m >> L, N;){
        vvi dist(N, vi(N, inf));
        rep(i, N) dist[i][i] = 0;
        rep(loop, m){
            int u, v, d; cin >> u >> v >> d;

            dist[u][v] = dist[v][u] = d;
        }

        vector<pair<int, int>> tasks(L);
        for(auto & e : tasks){
            cin >> e.first >> e.second;
        }

        rep(k, N){
            rep(i, N){
                rep(j, N){
                    chmin(dist[i][j], dist[i][k] + dist[k][j]);
                }
            }
        }

        n = 2 * L;
        init_G();
        rep(i, L){
            rep(j, L){
                if(i == j) continue;

                if(tasks[i].second + dist[tasks[i].first][tasks[j].first] > tasks[j].second){
                    continue;
                }

                int a = i, b = L + j;
                add_edge(a, b);
            }
        }

        cout << L - b_match() << endl;
    }

    return 0;
}
