#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define F first
#define S second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int dx[] = { 1, 0,-1, 0};
int dy[] = { 0, 1, 0,-1};

int main(void){
    for(int w, h, res; cin >> h >> w; cout << res << endl){
        res = 0;

        map<char, vector<pii> > m;

        vs field(h);
        rep(y, h){
            cin >> field[y];
            rep(x, w) if(field[y][x] != '.') m[field[y][x]].pb(mp(y, x));
        }

        bool end = false;
        while(!end){
            end = true;

            rep(y, h){
                rep(x, w){
                    if(field[y][x] != '.') continue;

                    set<char> s;

                    rep(i, 4){
                        range(k, 1, 1000){
                            pii next = mp(y + k * dy[i], x + k * dx[i]);
                            if(next.F < 0 || h <= next.F || next.S < 0 || w <= next.S) break;

                            char c = field[next.F][next.S];
                            if(c != '.'){
                                if(!s.insert(c).S){
                                    rep(j, 2) field[m[c][j].F][m[c][j].S] = '.';
                                    res += 2;
                                    end = false;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

	return 0;
}
