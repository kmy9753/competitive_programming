#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    string res;
    for(int h, w, n; cin >> h >> w >> n; cout << res << endl){
        res = "NO";
        vs orim(h);
        rep(y, h){
            cin >> orim[y];
        }

        rep(y, h){
            range(x, 1, w){
                vs m = orim;
                if(m[y][x] == '.' && m[y][x - 1] == '.') continue;

                swap(m[y][x], m[y][x - 1]);

                bool ok;
                do{
                    for(int y = h - 1; 0 <= y; y--){
                        rep(x, w){
                            if(m[y][x] == '.'){
                                int yy = y;
                                while(0 <= --yy){
                                    if(m[yy][x] != '.'){
                                        m[y][x] = m[yy][x];
                                        m[yy][x] = '.';
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    ok = false;
                    vvi del(h, vi(w));

                    rep(y, h){
                        rep(x, w){
                            if(m[y][x] == '.') continue;

                            char c = m[y][x];
                            int cnt = 1;
                            for(int sign = -1; sign < 2; sign += 2){
                                int xx = x;

                                while(0 <= (xx += sign) && xx < w && m[y][xx] == c) cnt++;
                            }
                            if(n <= cnt){
                                for(int sign = -1; sign < 2; sign += 2){
                                    int xx = x;

                                    while(0 <= (xx += sign) && xx < w && m[y][xx] == c) del[y][xx] = true;
                                }
                            }

                            cnt = 1;
                            for(int sign = -1; sign < 2; sign += 2){
                                int yy = y;

                                while(0 <= (yy += sign) && yy < h && m[yy][x] == c) cnt++;
                            }
                            if(n <= cnt){
                                for(int sign = -1; sign < 2; sign += 2){
                                    int yy = y;

                                    while(0 <= (yy += sign) && yy < h && m[yy][x] == c) del[yy][x] = true;
                                }
                            }
                        }
                    }

                    rep(y, h){
                        rep(x, w){
                            if(del[y][x]){
                                ok = true;
                                m[y][x] = '.';
                            }
                        }
                    }
                } while(ok);

                ok = true;
                rep(y, h){
                    rep(x, w){
                        if(m[y][x] != '.') ok = false;
                    }
                }

                if(ok){
                    res = "YES";
                }
            }
        }
    }

    return 0;
}
