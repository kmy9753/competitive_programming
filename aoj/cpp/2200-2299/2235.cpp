#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

vector<set<int>> G, T;
vi used;
set<int> ac_vs;

void dfs(int v, int pv){
    used[v] = true;

    for(auto& nv : G[v]){
        int tmp = pv;
        if(nv != pv and ac_vs.find(v) != end(ac_vs)){
            T[v].insert(pv);
            T[pv].insert(v);
            pv = nv;
        }

        if(not used[nv]) dfs(nv, pv);
        pv = tmp;
    }
}

bool dfs2(int v, int t){
    used[v] = true;

    if(v == t) return true;

    for(auto& nv : T[v]){
        if(used[nv]) continue;
        if(dfs2(nv, t)) return true;
    }
    return false;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, Q; cin >> n >> Q;
    G = T = vector<set<int>>(n);
    int sq = sqrt(Q);
    vi ops, us, vs;
    ops = us = vs = vi(Q);
    rep(i, Q) cin >> ops[i] >> us[i] >> vs[i];
    rep(it, Q){
        if(it % sq == 0){
            rep(v, n) T[v].clear();
            ac_vs.clear();
            int i = it;
            do {
                int u = us[i], v = vs[i];
                ac_vs.insert(u);
                ac_vs.insert(v);
                i++;
            } while(i % sq != 0 and i < Q);

            used = vi(n);
            rep(rv, n){
                if(ac_vs.find(rv) != end(ac_vs) and not used[rv]){
                    dfs(rv, rv);
                }
            }
        }

        int op = ops[it], u = us[it], v = vs[it];

        // construct
        if(op == 1){
            G[v].insert(u); G[u].insert(v);
            T[v].insert(u); T[u].insert(v);
        }

        // destruct
        else if(op == 2){
            G[v].erase(u); G[u].erase(v);
            T[v].erase(u); T[u].erase(v);
        }

        // ?
        else {
            for(auto& e : ac_vs) used[e] = false;
            cout << (dfs2(u, v) ? "YES":"NO") << endl;
        }
    }

    return 0;
}
