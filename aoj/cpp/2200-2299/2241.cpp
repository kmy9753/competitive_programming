#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

enum {kRabbit = 0, kCat};
enum {kMigishita = 0, kMigiue};

#define F first
#define S second

int main(void){
    vi t(2);
    for(int n, m; cin >> n >> t[kRabbit] >> t[kCat] >> m;){
        vector<vvi> mats(2, vvi(n, vi(n)));

        //(num) -> (y, x)
        vector<map<int, pii>> dic(2);

        rep(i, 2){
            rep(y, n){
                rep(x, n){
                    int num;
                    cin >> num;

                    mats[i][y][x] = num;
                    dic[i][num] = mp(y, x);
                }
            }
        }

        vvi rowCnt(2, vi(n));
        vvi colCnt(2, vi(n));
        vvi nanameCnt(2, vi(2));

        string res = "UNKNOWN";
        rep(i, m){
            int num; cin >> num;

            rep(j, 2){
                if(dic[j].find(num) == dic[j].end()) continue;

                int y = dic[j][num].F, 
                    x = dic[j][num].S;

                rowCnt[j][y]++;
                colCnt[j][x]++;

                int delta = 0;
                if(y == x){
                    if(++nanameCnt[j][0] == n) delta++;
                }
                if(y == n - 1 - x){
                    if(++nanameCnt[j][1] == n) delta++;
                }

                if(rowCnt[j][y] == n) delta++;
                if(colCnt[j][x] == n) delta++;

                if(delta >= 1 && n == 1) delta = 1;
                t[j] -= delta;
            }

            if(res != "UNKNOWN") continue;

            if(t[0] <= 0){
                if(t[1] <= 0){
                    res = "DRAW";
                }
                else res = "USAGI";
            }
            else if(t[1] <= 0) res = "NEKO";
        }

        if(res == "UNKNOWN") res = "DRAW";

        cout << res << endl;
    }

	return 0;
}
