#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

using Edge  = tuple<int, int, int>;
using State = tuple<int, int, int>;

int main(void){
    for(int n, m; cin >> n >> m, n;){
        vector<vector<Edge>> edge(n);
        rep(_, m){
            int u, v, d, c; cin >> u >> v >> d >> c;
            u--, v--;

            edge[u].push_back(Edge(v, d, c));
            edge[v].push_back(Edge(u, d, c));
        }

        vector<int> used(n);
        priority_queue<State, vector<State>, greater<State>> q;
        q.push(State(0, 0, 0));

        int res = 0;
        while(q.size()){
            int v, diff_c, sum_d;
            tie(sum_d, diff_c, v) = q.top();
            q.pop();

            if(used[v]) continue;
            used[v] = true;

            res += diff_c;

            for(auto & e : edge[v]){
                int nv, d, c;
                tie(nv, d, c) = e;

                if(used[nv]) continue;

                q.push(State(sum_d + d, c, nv));
            }
        }

        cout << res << endl;
    }

    return 0;
}
