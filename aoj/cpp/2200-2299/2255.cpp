#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

void dfs(vector<set<int>>& nums, string ops, set<int>& ret){
    int len = ops.size();

    if(len == 0){
        for(auto & e : nums[0]){
            ret.insert(e);
        }
        return;
    }

    rep(i, len){
        char op = ops[i];

        set<int> next;
        for(auto & l : nums[i]){
            for(auto & r : nums[i + 1]){
                switch(op){
                    case '+': { next.insert(l + r); break; }
                    case '-': { next.insert(l - r); break; }
                    case '*': { next.insert(l * r); break; }
                    case '/': { if(r == 0) break; next.insert(l / r); break; }
                }
            }
        }

        if(next.size() == 0){
            continue;
        }

        vector<set<int>> nnums;
        string nops = "";
        rep(j, len + 1){
            if(j == i){
                nnums.push_back(next);
                continue;
            }

            if(j < len) nops.push_back(ops[j]);
            if(j != i + 1) nnums.push_back(nums[j]);
        }

        dfs(nnums, nops, ret);
    }
}

set<int> parse(string& str, int& idx){
    vector<set<int>> nums;
    string ops;

    int len = str.size();
    while(true){
        if(str[idx] == '('){
            idx++;
            nums.push_back(parse(str, idx));
        }
        else {
            int cur = 0;
            while(idx < len and '0' <= str[idx] and str[idx] <= '9'){
                cur = 10 * cur + (str[idx] - '0');
                idx++;
            }
            nums.push_back({cur});
        }

        if(idx == len){
            break;
        }
        if(str[idx] == ')'){
            idx++;
            break;
        }

        ops.push_back(str[idx]);
        idx++;
    }

    set<int> s;
    dfs(nums, ops, s);

    return s;
}

int main(void){
    for(string in; cin >> in, in != "#";){
        int idx = 0;
        cout << parse(in, idx).size() << endl;
    }

    return 0;
}
