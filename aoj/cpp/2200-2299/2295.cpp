#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 101;
string dp[N][N][2];

const string inf_s = "9";
const string imp_s = "8";

string rec(int rest0, int rest1, int cur){
    string& ret = dp[rest0][rest1][cur];
    if(ret != inf_s) return ret;

    if(rest0 == 0 and rest1 == 0){
        if(cur == 0){
            ret = imp_s;
        }
        else {
            ret = "";
        }
        return ret;
    }

    ret = imp_s;
    rep(next, 2){
        if(rest0 != 0){
            if((cur == 0 and next == 1) or (cur == 1 and next == 0)){
                string diff = rec(rest0 - 1, rest1, next);
                if(diff != imp_s){
                    chmin(ret, "0" + diff);
                }
            }
        }
        if(rest1 != 0){
            if(cur == 1){
                string diff = rec(rest0, rest1 - 1, next);
                if(diff != imp_s){
                    chmin(ret, "1" + diff);
                }
            }
        }
    }

    return ret;
}

int main(void){
    int n; cin >> n;
    vi a(n); for(auto & e : a)cin >> e;
    int cnt0 = 0, cnt1 = 0;
    for(auto & e : a){
        if(e == 0) cnt0++;
        if(e == 1) cnt1++;
    }

    rep(i, N) rep(j, N) rep(k, 2) dp[i][j][k] = inf_s;

    sort(_all(a));

    vi rest;
    for(auto & e : a){
        if(e != 0 and e != 1){
            rest.push_back(e);
        }
    }
    int len = rest.size();

    int ex = 0;
    vi exnums;
    string suf;
    while((suf = rec(cnt0, cnt1 + ex, 1)) == imp_s){
        if(len != 0){
            exnums.push_back(rest[0]);
            ex++;
            rest.erase(begin(rest), begin(rest) + 1);
            len--;
        }
        else {
            suf = string(cnt0, '0') + string(cnt1, '1');
            break;
        }
    }

    bool found = false;
    if(len >= 2){
        if(rest[len - 2] == 2 and rest[len - 1] == 3){
            found = true;
            rest.erase(begin(rest) + len - 2);
            len--;
        }
    }

    for(auto & e : rest){
        cout << e << endl;
    }
    if(found){
        cout << 2 << endl;
    }
    int ei = 0;
    int cnt = 0;
    for(auto & c : suf){
        if(c == '1'){
            cnt++;
            if(cnt > cnt1){
                cout << exnums[ei++] << endl;
            }
            else {
                cout << c << endl;
            }
        }
        else {
            cout << c << endl;
        }
    }

    return 0;
}
