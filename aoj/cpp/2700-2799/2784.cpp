#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using pll = pair<ll, ll>;
map<pll, int> dic;
vector<vi> graph;

ll mod1 = 1e9+7LL;
int base1 = 10007;
ll mod2 = 1e9+9LL;
int base2 = 1009;

pll dfs(int v, int pv, int d){
    ll ret1 = 0LL, ret2 = 0LL;
    for(int nv : graph[v]){
        if(nv == pv) continue;
        pll nxt = dfs(nv, v, d + 1);
        ret1 = ADD(ret1, nxt.first , mod1);
        ret2 = ADD(ret2, nxt.second, mod2);
    }
    ret1 = MUL(ret1, base1, mod1);
    ret2 = MUL(ret2, base2, mod2);
    ret1 = ADD(ret1, 1LL, mod1);
    ret2 = ADD(ret2, 1LL, mod2);

    dic[pll(ret1, ret2)]++;
    return pll(ret1, ret2);
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    graph = vector<vi>(n);

    rep(i, n - 1){
        int a, b; cin >> a >> b; a--, b--;
        graph[a].emplace_back(b);
        graph[b].emplace_back(a);
    }
    dfs(0, -1, 0);

    long long res = 0LL;
    for(auto& e : dic){
        ll num = e.second;
        res += num * (num - 1) / 2;
    }
    cout << res << endl;

    return 0;
}
