#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int n;
int len;
int idx;
string in;

void Print(vi& tbl){
    rep(i, n){
        rep(j, n){
            cerr << tbl[i * n + j] << " ";
        }
        cerr << endl;
    }
    cerr << "-----" << endl;
}

void calc(vi& tbl, vi& dst){
    vi ndst = dst;
    rep(i, n * n){
        ndst[i] = dst[tbl[i]];
    }
    dst = ndst;
}

void power(vi& tbl, int m, vi& dst){
    vi cur = tbl;
    while(m >= 1){
        if(m % 2 == 1){
            calc(cur, dst);
        }
        calc(cur, cur);
        m /= 2;
    }
}

void operate(char op, int k, vi& dst){
    k--;
    vi tbl(n * n);
    rep(i, n * n) tbl[i] = i;

    int tmp;
    switch(op){
        case 'L': 
            tmp = tbl[k * n];
            rep(i, n - 1){
                tbl[k * n + i] = tbl[k * n + i + 1];
            }
            tbl[(k + 1) * n - 1] = tmp;
            break;
        case 'R': 
            tmp = tbl[(k + 1) * n - 1];
            rrep(i, n, 1){
                tbl[k * n + i] = tbl[k * n + i - 1];
            }
            tbl[k * n] = tmp;
            break;
        case 'U':
            tmp = tbl[k];
            rep(i, n - 1){
                tbl[n * i + k] = tbl[n * (i + 1) + k];
            }
            tbl[n * (n - 1) + k] = tmp;
            break;
        case 'D':
            tmp = tbl[n * (n - 1) + k];
            rrep(i, n, 1){
                tbl[n * i + k] = tbl[n * (i - 1) + k];
            }
            tbl[k] = tmp;
            break;
        default:
            assert(false);
            break;
    }

    power(tbl, 1, dst);
}

vi parse(){
    vi ret(n * n);
    rep(i, n * n) ret[i] = i;

    while(idx < len){
        if(in[idx] == ')'){
            break;
        }
        else if(in[idx] == '('){
            assert(in[idx++] == '(');
            vi tbl = parse();
            assert(in[idx++] == ')');

            int times = 0;
            while(isdigit(in[idx])){
                times = 10 * times + (int)(in[idx] - '0');
                idx++;
            }
            power(tbl, times, ret);
        }
        else {
            char op = in[idx]; idx++;
            int  k  = 0;
            while(isdigit(in[idx])){
                k = 10 * k + (int)(in[idx] - '0');
                idx++;
            }
            operate(op, k, ret);
        }
    }

    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> n >> len;
    cin >> in;
    if(in[0] != '(') in = "(" + in + ")1";

    auto tbl = parse();

    vi res(n * n);
    rep(i, n * n){
        res[i] = i + 1;
    }
    calc(tbl, res);

    rep(i, n){
        rep(j, n){
            cout << (j ? " ":"") << res[i * n + j];
        }
        cout << endl;
    }

    return 0;
}
