#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

struct Node{
    int i;
    int num;
    int li;
    int ri;

    Node() : i(-1), num(0), li(-1), ri(-1) {}
    Node(int _i, int _num, int _li, int _ri) : i(_i), num(_num), li(_li), ri(_ri) {}
    Node(const Node &rhs) : i(rhs.i), num(rhs.num), li(rhs.li), ri(rhs.ri) {}
};

int parse(vector<Node>& nodes, string in, int& idx){
    nodes.push_back(Node());
    Node node;
    node.i = nodes.size() - 1;

    // left child
    if(in.substr(idx, 2) != "()"){
        assert(in[idx++] == '(');
        node.li = parse(nodes, in, idx);
        assert(in[idx++] == ')');
    }
    else idx += 2;

    // [ num ]
    assert(in[idx++] == '[');
    int& num = node.num = 0;
    while(isdigit(in[idx])){
        num = 10 * num + (in[idx] - '0');
        idx++;
    }
    assert(in[idx++] == ']');

    // right child
    if(in.substr(idx, 2) != "()"){
        assert(in[idx++] == '(');
        node.ri = parse(nodes, in, idx);
        assert(in[idx++] == ')');
    }
    else idx += 2;

    nodes[node.i] = node;
    return node.i;
}

string merge(vector<Node>& a, vector<Node>& b, int va, int vb){
    string ret;
    if(a[va].li != -1 and b[vb].li != -1){
        ret += "(" + merge(a, b, a[va].li, b[vb].li) + ")";
    }
    else ret += "()";

    stringstream ss; ss << a[va].num + b[vb].num;
    ret += "[" + ss.str() + "]";
    if(a[va].ri != -1 and b[vb].ri != -1){
        ret += "(" + merge(a, b, a[va].ri, b[vb].ri) + ")";
    }
    else ret += "()";

    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    string A, B; cin >> A >> B;
    vector<Node> a, b;
    int idx;
    idx = 0; parse(a, A, idx);
    idx = 0; parse(b, B, idx);

    cout << merge(a, b, 0, 0) << endl;

    return 0;
}
