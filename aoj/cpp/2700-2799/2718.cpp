#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    vi x(n), y(n);
    map<int, map<int, char>> d;
    rep(i, n){
        char c;
        cin >> x[i] >> y[i] >> c;
        d[y[i]][x[i]] = c;
    }

    map<int, set<int>> x2y_, y2x_, x2ny_, y2nx_;
    rep(i, n){
        int xx = x[i], yy = y[i];
        x2y_ [xx].insert( yy);
        x2ny_[xx].insert(-yy);
        y2x_ [yy].insert( xx);
        y2nx_[yy].insert(-xx);
    }

    int res = 0;
    rep(i, n){
        auto x2y = x2y_, y2x = y2x_, x2ny = x2ny_, y2nx = y2nx_;

        int cx = x[i], cy = y[i];
        int cur = 0;
        while(true){
            cur++;
            int nx = cx, ny = cy;
            switch(d[cy][cx]){
                case 'v': { auto ni = x2y [cx].upper_bound( cy); if(ni != end(x2y [cx])){ ny =   *ni ; } break; }
                case '^': { auto ni = x2ny[cx].upper_bound(-cy); if(ni != end(x2ny[cx])){ ny = -(*ni); } break; }
                case '>': { auto ni = y2x [cy].upper_bound( cx); if(ni != end(y2x [cy])){ nx =   *ni ; } break; }
                case '<': { auto ni = y2nx[cy].upper_bound(-cx); if(ni != end(y2nx[cy])){ nx = -(*ni); } break; }
            }
            x2y[cx].erase(cy); x2ny[cx].erase(-cy);
            y2x[cy].erase(cx); y2nx[cy].erase(-cx);

            if(nx == cx and ny == cy) break;
            cx = nx, cy = ny;
        }
        chmax(res, cur);
    }

    cout << res << endl;

    return 0;
}
