#include <bits/stdc++.h>
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)

using namespace std;
using vi = vector<int>;

const int M = 40010 * 2;
const int offset = 20001 * 2;

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    for(int n; cin >> n, n;){
        vi xs(n), ys(n);
        rep(i, n) cin >> xs[i] >> ys[i], xs[i] *= 2, ys[i] *= 2, xs[i] += offset, ys[i] += offset;
        vi cx(4), cy(4); rep(i, 4) cin >> cx[i] >> cy[i], cx[i] *= 2, cy[i] *= 2, cx[i] += offset, cy[i] += offset;
        int cx1 = cx[0], cx2 = cx[0], cy1 = cy[0], cy2 = cy[0];
        rep(i, 4){
            cx1 = min(cx1, cx[i]); cx2 = max(cx2, cx[i]);
            cy1 = min(cy1, cy[i]); cy2 = max(cy2, cy[i]);
        }

        vector<set<int>> xmap(M), ymap(M);
        rep(i, n){
            int x = xs[i], y = ys[i];
            xmap[x].insert(y);
            ymap[y].insert(x);
        }

        vi used(M, -1);
        rep(y, M){
            int pre = -1;
            for(auto x : ymap[y]){
                if(pre == -1){
                    pre = x;
                }
                else {
                    rep(diff, x - pre + 1){
                        int nx = pre + diff;
                        if(cy1 < y and y < cy2 and cx1 < nx and nx < cx2){
                        }
                        else {
                            xmap[nx].insert(y);
                        }
                    }
                    pre = -1;
                }
            }
        }
        rep(k, 2){
            int cy = cy1;
            if(k == 1) cy = cy2;

            rep(diff, cx2 - cx1 + 1){
                int x = cx1 + diff;

                int pre = -1;
                bool ok = false;
                for(auto y : xmap[x]){
                    if(xmap[x + 1].find(y) == end(xmap[x + 1])) continue;
                    if(pre == -1){
                        pre = y;
                    }
                    else {
                        if(pre <= cy and cy <= y){
                            ok = true;
                            break;
                        }
                    }
                }

                if(ok){
                    xmap[x].insert(cy);
                }
            }
            
        }

        int res = 0;
        rep(x, M - 1){
            int pre = -1;
            for(auto y : xmap[x]){
                if(xmap[x + 1].find(y) == end(xmap[x + 1])) continue;
                if(pre == -1){
                    pre = y;
                }
                else {
                    res += (y - pre);
                    pre = -1;
                }
            }
        }
        cout << res / 2 << endl;
    }

    return 0;
}
