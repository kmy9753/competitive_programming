#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 28;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using pii = pair<int, int>;

struct SegTree {
    int n;
    vi data;
    vector<pii> lazy;

    SegTree(int sz){
        n = 1; while(n < sz) n *= 2;
        data = vi(n * 2 - 1);
        lazy = vector<pii>(n * 2 - 1, pii(-1, -1));
    }

    int c1(int k){ return 2 * k + 1; }
    int c2(int k){ return 2 * k + 2; }

    void setLazy(int k, pii x){
        pii& cur = lazy[k];
        if(cur == pii(-1, -1)){
            cur = x;
        }
        else if(cur.second + 1 == x.first){
            cur.second = x.second;
        }
        else {
            cur = pii(inf, inf);
        }
    }

    void updateall(int k = 0, int l = 0, int r = -1){
        if(r < 0) r = n;
        update(k, l, r);
        if(r - l > 1){
            updateall(c1(k), l, (l + r) / 2);
            updateall(c2(k), (l + r) / 2, r);
        }
    }
    void update(int k, int l, int r){
        pii& cur = lazy[k];
        if(cur == pii(-1, -1)) return;

        if(data[k] == cur.first - 1){
            data[k] = cur.second;
        }
        else {
            data[k] = -1;
        }

        if(r - l > 1){
            for(int c : {c1(k), c2(k)}){
                setLazy(c, cur);
            }
        }

        cur = pii(-1, -1);
    }

    void query(int a, int b, int x, int k = 0, int l = 0, int r = -1){
        if(r < 0) r = n;
        update(k, l, r);

        if(a >= r or  l >= b) return;
        if(a <= l and r <= b){
            setLazy(k, pii(x, x));
            update(k, l, r);
            return;
        }

        query(a, b, x, c1(k), l, (l + r) / 2);
        query(a, b, x, c2(k), (l + r) / 2, r);
    }
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, K; cin >> n >> K;
    int T; cin >> T;
    SegTree seg(n);
    rep(loop, T){
        int l, r, x; cin >> l >> r >> x;
        l--;
        seg.query(l, r, x);
    }

    int res = 0;
    seg.updateall();
    rep(i, n){
        if(seg.data[seg.n - 1 + i] == K) res++;
    }
    cout << res << endl;

    return 0;
}
