#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = (int)7e8;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 51;
int n, m;
vi a, b;
int memo[2][2][N][N][N][N];

int rec(int turn, int pcnt, int ai, int bi, int sa, int sb){
    int& ret = memo[turn][pcnt][ai][bi][sa][sb];
    int sign = (turn == 0 ? +1:-1);

    if(ret != -inf){
        return sign * ret;
    }

    // pass
    if(pcnt == 1){
        ret = 0;
    }
    else {
        int da = 0, db = 0;
        int ct = turn ^ (sa != sb);
        rep(i, sa + sb){
            int j = i / 2;
            if(ct == 0){
                assert(ai - sa + j >= 0 and ai - sa + j < n);
                int card = a[ai - sa + j];
                if(card == -1) db = 0;
                else da += card;
            }
            else {
                assert(bi - sb + j >= 0 and bi - sb + j < m);
                int card = b[bi - sb + j];
                if(card == -1) da = 0;
                else db += card;
            }
            ct ^= 1;
        }
        
        int npcnt = 0;
        if(sa == 0 and sb == 0) npcnt = 1;
        chmax(ret, sign * ((da - db) + rec(turn ^ 1, npcnt, ai, bi, 0, 0)));
    }

    // play
    int da = (turn == 0 ? 1:0);
    int db = (turn == 1 ? 1:0);
    if(ai + da <= n and bi + db <= m){
        chmax(ret, sign * rec(turn ^ 1, 0, ai + da, bi + db, sa + da, sb + db));
    }

    // cerr << turn << " " << pcnt << " "  << ai << " " << bi << " " << sa << " " << sb  << ": " << sign * ret << endl;
    return sign * ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> n >> m;
    a = vi(n), b = vi(m);
    for(auto& e : a) cin >> e;
    for(auto& e : b) cin >> e;

    rep(i, 2) rep(j, 2) rep(ai, n + 1) rep(bi, m + 1) rep(sa, n + 1) rep(sb, m + 1) memo[i][j][ai][bi][sa][sb] = -inf;
    cout << rec(0, 0, 0, 0, 0, 0) << endl;

    return 0;
}
