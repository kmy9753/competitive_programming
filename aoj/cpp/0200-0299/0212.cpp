#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <utility>
#include <cstring>
#include <string>
#include <sstream>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;
typedef vector<vector<int> > table;

int n;
const int INF = (int)1e7;
const int N = 100;
const int C = 11;
table cost(N);
vector<vector<bool> > used(11);

int dijkstra(int, int, int);

int main(void){
    rep(i, N) cost[i].resize(N);
    rep(i, C) used[i].resize(N);

    for(int c, m, s, g; cin >> c >> n >> m >> s >> g, n; cout << dijkstra(s, g, c) << endl){
        rep(i, N) fill(ALL(cost[i]), INF);
        rep(i, C) fill(ALL(used[i]), false);

        while(m --){
            int a, b, f; cin >> a >> b >> f;
            
            cost[a - 1][b - 1] = cost[b - 1][a - 1] = f;
        }
    }
    
    return 0;
}

int dijkstra(int s, int g, int c){
    table d(c + 1);
    rep(i, c + 1) d[i].resize(n), fill(ALL(d[i]), INF);
    
    d[c][s - 1] = 0;

    while(1){
        int vn = -1, vc = -1;

        rep(i, c + 1) rep(j, n) if(!used[i][j] && (vc == -1 || d[i][j] < d[vc][vn])) vc = i, vn = j;

        if(vc == -1) break;
        used[vc][vn] = true;

        rep(u, n){
            d[vc][u] = min(d[vc][u], d[vc][vn] + cost[vn][u]);
            if(vc) d[vc - 1][u] = min(d[vc - 1][u], d[vc][vn] + cost[vn][u] / 2);
        }
    }

    int ret = INF;
    rep(i, c) ret = min(ret, d[i][g - 1]);
    return ret;
}
