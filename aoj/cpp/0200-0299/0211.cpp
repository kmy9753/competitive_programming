#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

ll gcd(ll x, ll y){
    //cout << "gcd now x = " << x << "y = " << y<< endl;
    return y ? gcd(y, x % y):x;
}

ll lcm(ll x, ll y){
    //cout << "lcm now" << endl;
    return x * y / gcd(x, y);
}

int main(void){
    for(int n; cin >> n, n;){
        vi d(n), v(n);
        rep(i, n) cin >> d[i] >> v[i];

        rep(i, n){
            int res = 1;
            COUNT(j, n - 1){
                int ind = (i + j) % n;
                res = lcm(res, v[i] * d[ind] / gcd(v[ind] * d[i], v[i] * d[ind]));
            }
            cout << res << endl;
        }
    }

    return 0;
}
