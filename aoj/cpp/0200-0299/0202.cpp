#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 1000001;

int main(void){
    vector<bool> isP(N);
    fill(ALL(isP), true);
    rep(i, 2) isP[i] = false;
    for(int i = 2; i * i < N; i ++) if(isP[i]) for(int j = i * i; j < N; j += i) isP[j] = false;

    for(int n, x; cin >> n >> x, n;){
        vector<bool> dp(x + 1);
        fill(ALL(dp), false);
        dp[0] = true;
        while(n --){
            int m; cin >> m;

            rep(i, x - m + 1) if(dp[i]) dp[i + m] = true;
        }
        while(~x && !(dp[x] && isP[x])) x --;
     
        if(~x) cout << x << endl;
        else cout << "NA" << endl;
    }
 
    return 0;
}
