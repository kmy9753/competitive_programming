#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

int main(void){
    for(int n; cin >> n, n;)
        for(int x1, y1, z1, w1, x2, y2, z2, w2; n -- && cin >> x1 >> y1 >> z1 >> w1 >> x2 >> y2 >> z2 >> w2; cout << x1 * x2 - y1 * y2 - z1 * z2 - w1 * w2 << ' ' << x1 * y2 + y1 * x2 + z1 * w2 - w1 * z2 << ' ' << x1 * z2 - y1 * w2 + z1 * x2 + w1 * y2 << ' ' << x1 * w2 + y1 * z2 - z1 * y2 + w1 * x2 << endl);
 
    return 0;
}
