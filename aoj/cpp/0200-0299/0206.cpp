#include <iostream>

using namespace std;

int main(void){
	for(int cost; cin >> cost, cost;){
		int result = 0;
		for(int i = 1, in, out; i <= 12; i ++){
			cin >> in >> out;
			cost -= (in - out);
			if(cost <= 0 && !result) result = i;
		}
		if(!result) cout << "NA" << endl;
		else cout << result << endl;
	}

	return 0;
}
