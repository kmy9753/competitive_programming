#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const string DRAW = "33333";
const int N = 5;

int main(void){
    while(1){
        set<char> comb;
        string in;
        rep(i, N){
            char c; cin >> c;
            if(c == '0') return 0;
            comb.insert(c);
            in.pb(c);
        }
        string res = DRAW;

        if(comb.size() == 2){
            char w;
            COUNT(i, 3) if(comb.insert('0' + i).second) w = '1' + i % 3;
            rep(i, N) res[i] = in[i] == w ? '1':'2';
        }

        rep(i, N) cout << res.at(i) << endl;
    }
 
    return 0;
}
