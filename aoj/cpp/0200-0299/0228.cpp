#include <iostream>

#define rep(i, n) for(int i = 0; i < n; i ++)

using namespace std;

static const bool PATTERN[11][7] =
{
	0, 1, 1, 1, 1, 1, 1,
	0, 0, 0, 0, 1, 1, 0,
	1, 0, 1, 1, 0, 1, 1,
	1, 0, 0, 1, 1, 1, 1,
	1, 1, 0, 0, 1, 1, 0,
	1, 1, 0, 1, 1, 0, 1,
	1, 1, 1, 1, 1, 0, 1,
	0, 1, 0, 0, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1,
	1, 1, 0, 1, 1, 1, 1,
	0, 0, 0, 0, 0, 0, 0
};

static const int DEFAULT = 10;

int main(void){
	for(int n; cin >> n, n != -1;){
		int preNum = DEFAULT;
		while(n --){
			int num; cin >> num;
			rep(i, 7) cout << (PATTERN[preNum][i] ^ PATTERN[num][i]);
			cout << endl;
			preNum = num; 
		}
	}

	return 0;
}
