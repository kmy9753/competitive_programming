#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 1 << 17; // ~~ 1.3 * 10^5
/**
 * 0-indexed, [l, r)
 * update(l,r,v) := [l,r)$B$N6h4V$KBP$7$F(Bv$B$r0lMM$KB-$9(B
 * query (l,r)   := [l,r)$B$N6h4V$NAmOB$r5a$a$k(B
 **/

struct Node {
    int sum;  // $B99?7$5$l$?CM(B. $B$3$NCM$r;2>H$9$k;~$OI>2A$,40A4$K40N;$7$F$$$k$h$&$K$9$k(B.
    int lazy; // $BCY1d$5$l$F$$$kCM(B
    Node(){
        sum  = 0;
        lazy = 0;
    }
};

Node seg[2 * N];

// $B%N!<%I(Bk$B$rI>2A$7!$;R$KEAGE$9$k(B
// a, b: $B%N!<%I(Bk$B$NI=$96h4V(B[a, b)
inline void lazy_evaluate_node(int k, int a, int b){
    seg[k].sum += seg[k].lazy * (b - a);

    // $BMU$G$J$1$l$PEAGE(B
    if(k < N - 1){
        seg[2 * k + 1].lazy += seg[k].lazy;
        seg[2 * k + 2].lazy += seg[k].lazy;
    }

    seg[k].lazy = 0;
}

// $B%N!<%I(Bk$B$NCM$r99?7(B
// (k$B$N;R$O$9$G$KI>2A$5$l$F$$$k(B(lazy$B$,(B0)) and (k$B$OMU$G$J$$(B) $B$,A0Ds(B
inline void update_node(int k){
    seg[k].sum = seg[2 * k + 1].sum + seg[2 * k + 2].sum;
}

// update(l, r, v) := [l, r)$B$r99?7$9$k(B
void update(int l, int r, int v, int k = 0, int a = 0, int b = N - 1){
    lazy_evaluate_node(k, a, b);

    if(b <= l or r <= a) return;

    if(l <= a and b <= r){
        seg[k].lazy += v;
        lazy_evaluate_node(k, a, b);
        return;
    }

    int m = (a + b) / 2;
    update(l, r, v, 2 * k + 1, a, m);
    update(l, r, v, 2 * k + 2, m, b);
    update_node(k);
}

// get(l, r) := [l, r)$B$KBP$9$k%/%(%j$NEz$($rF@$k(B
int query(int l, int r, int k = 0, int a = 0, int b = N - 1){
    lazy_evaluate_node(k, a, b);

    if(b <= l or r <= a) return 0;

    if(l <= a and b <= r){
        return seg[k].sum;
    }

    int m = (a + b) / 2;
    int vl = query(l, r, 2 * k + 1, a, m);
    int vr = query(l, r, 2 * k + 2, m, b);
    update_node(k);

    return vl + vr;
}

int main(void){
    int n, Q; cin >> n >> Q;

    rep(loop, Q){
        int com; cin >> com;

        if(com == 0){
            int i, x; cin >> i >> x; i--;
            update(i, i + 1, x);
        }
        else {
            int l, r; cin >> l >> r; l--;
            cout << query(l, r) << endl;
        }
    }

    return 0;
}
