#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

inline int par(int k){ return (k - 1) / 2; }
inline int c1(int k){ return 2 * k + 1; }
inline int c2(int k){ return 2 * k + 2; }

struct SegTree {
    int n;
    vll node, lazy;

    SegTree(int sz){
        n = 1; while(n < sz) n *= 2;
        node = vll(2 * n - 1);
        lazy = vll(2 * n - 1);
    }

    void eval(int k, int l, int r){
        ll& cur = lazy[k];
        if(cur == 0) return;
        node[k] += cur;
        if(r - l > 1){
            lazy[c1(k)] += cur / 2;
            lazy[c2(k)] += cur / 2;
        }
        cur = 0;
    }

    void update(int a, int b, int x, int k = 0, int l = 0, int r = -1){
        if(r < 0) r = n;

        eval(k, l, r);
        if(a >= r or l >= b) return;
        if(a <= l and r <= b){
            lazy[k] = (r - l) * x;
            eval(k, l, r);
            return;
        }

        update(a, b, x, c1(k), l, (l + r) / 2);
        update(a, b, x, c2(k), (l + r) / 2, r);
        node[k] = node[c1(k)] + node[c2(k)];
    }

    int getsum(int a, int b, int k = 0, int l = 0, int r = -1){
        if(r < 0) r = n;

        eval(k, l, r);
        if(a >= r or l >= b)  return 0;
        if(a <= l and r <= b) return node[k];

        return getsum(a, b, c1(k), l, (l + r) / 2)
             + getsum(a, b, c2(k), (l + r) / 2, r);
    }
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, Q; cin >> n >> Q;
    SegTree seg(n);
    rep(loop, Q){
        int op; cin >> op;
        if(op == 0){
            int s, t, x; cin >> s >> t >> x; s--;
            seg.update(s, t, x);
        }
        else {
            int i; cin >> i; i--;
            cout << seg.getsum(i, i + 1) << endl;
        }
    }

    return 0;
}
