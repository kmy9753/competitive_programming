#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

struct SegTree {
    int n;
    vi node, lazy;

    SegTree(int sz){
        n = 1; while(n < sz) n *= 2;
        node = vi(2 * n - 1, inf);
        lazy = vi(2 * n - 1, -1);
    }

    void eval(int k, int l, int r){
        int& cur = lazy[k];
        if(cur == -1) return;
        node[k] = cur;
        if(r - l > 1){
            lazy[2 * k + 1] = cur;
            lazy[2 * k + 2] = cur;
        }
        cur = -1;
    }

    // node[a:b] := x
    void update(int a, int b, int x, int k = 0, int l = 0, int r = -1){
        if(r < 0) r = n;

        eval(k, l, r);
        if(a >= r or l >= b) return;
        if(a <= l and r <= b){
            lazy[k] = x;
            eval(k, l, r);
            return;
        }

        update(a, b, x, 2 * k + 1, l, (l + r) / 2);
        update(a, b, x, 2 * k + 2, (l + r) / 2, r);
        node[k] = min(node[2 * k + 1], node[2 * k + 2]);
    }

    int getmin(int a, int b, int k = 0, int l = 0, int r = -1){
        if(r < 0) r = n;
        
        eval(k, l, r);
        if(a >= r or  l >= b) return inf;
        if(a <= l and r <= b) return node[k];

        return min(getmin(a, b, 2 * k + 1, l, (l + r) / 2), 
                   getmin(a, b, 2 * k + 2, (l + r) / 2, r));
    }
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, Q; cin >> n >> Q;
    SegTree seg(n);
    rep(loop, Q){
        int op; cin >> op;
        if(op == 0){
            int s, t, x; cin >> s >> t >> x;
            seg.update(s, t + 1, x);
        }
        else {
            int i; cin >> i;
            cout << seg.getmin(i, i + 1) << endl;
        }
    }

    return 0;
}
