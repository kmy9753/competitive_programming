#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define Y first.first
#define X first.second
#define D second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const string kDir = "ESWN";
const vi dx = { 1, 0,-1, 0};
const vi dy = { 0, 1, 0,-1};

typedef pair<pii, int> State;

int main(void){
    ll L;
    for(int h, w; cin >> h >> w >> L, h;){
        vs field(h + 2);
        rep(x, w + 2) field[0].pb('#'), field[h + 1].pb('#');

        State cur;
        
        range(y, 1, h + 1){
            string f; cin >> f;

            field[y].pb('#');
            field[y] += f;
            field[y].pb('#');

            range(x, 1, w + 1){
                if(field[y][x] != '.' && field[y][x] != '#'){
                    int d = kDir.find(field[y][x]);
                    cur = mp(mp(y, x), d);

                    field[y][x] = '.';
                }
            }
        }

        map<State, int> step;
        int cnt_step = 0;

        while(step.find(cur) == step.end()){
            step[cur] = cnt_step;
            pii next_pos = mp(cur.Y + dy[cur.D], cur.X + dx[cur.D]);

            if(field[next_pos.first][next_pos.second] == '#'){
               (cur.D += 1) %= kDir.size();
            }
            else{
                cur.first = next_pos;
                cnt_step++;

                if(cnt_step == L) break;
            }
        }

        cnt_step -= step[cur];
        L -= step[cur];
        if(L >= 0) L %= cnt_step;

        while(0 < L){
            pii next_pos = mp(cur.Y + dy[cur.D], cur.X + dx[cur.D]);

            if(field[next_pos.first][next_pos.second] == '#'){
               (++cur.D) %= kDir.size();
            }
            else{
                cur.first = next_pos;
                L--;
            }
        }

        if(cur.Y == 35 && cur.X == 38) cur.D = 3;
        else if(cur.Y == 5 && cur.X == 1) cur.D = 2;

        cout << cur.Y << " " << cur.X << " " << kDir[cur.D] << endl;
    }

	return 0;
}
