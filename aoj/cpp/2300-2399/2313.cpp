#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

class FF{
	public :
		void init(int _n){
			n=_n;
			G.resize(n);

            rep(a, n){
                G[a] = vector<edge>(2 * n);
            }
            rep(a, n){
                rep(b, n){
                    G[a][b]     = {b, 0, n + a};
                    G[b][n + a] = {a, 0, b};
                }
            }
		}

		void add_edge(int from,int to ,ll cap){
			G[from][to].cap = cap;
		}
	
		void add_edge_both(int from,int to ,ll cap){
			add_edge(from,to,cap);
			add_edge(to,from,cap);
		}
	
		ll max_flow(int s,int t){
			ll flow=0;
			for(;;){
                used = vi(n);
                ll f = dfs(s, t, inf);
                if(f == 0) return flow;
                flow += f;
			}
		}

        ll connect(int a, int b){
            G[a][b].cap = G[b][a].cap = 1;
            used = vi(n);
            ll ret = dfs(0, n-1, 1);
            return ret;
        }

        ll disconnect(int a, int b){
            ll ret = 0;

            rep(loop, 2){
                if(G[a][b].cap > 0){ // not used a->b
                    G[a][b].cap = 0;
                }
                else if(G[a][b].cap == 0 and G[b][n+a].cap > 0){ // used
                    used = vi(n);
                    if(dfs(a, b, 1) == 0){
                        used2 = vi(n); dfs2(n-1, b);
                        used2 = vi(n); dfs2(a, 0);
                        ret -= 1;
                    }
                    G[b][n+a].cap = 0;
                }
                swap(a, b);
            }

            return ret;
        }
	private:
	
		int n;
		struct edge{int to; ll cap; int rev;};
		vector< vector<edge> > G;
        vi used, used2;

        bool dfs2(int v, int z){
            used2[v] = true;
            if(v == z){
                return true;
            }
            for(auto& e : G[v]){
                if(e.cap == 0 or used2[e.to]) continue;
                if(dfs2(e.to, z)){
                    e.cap = 0;
                    G[e.to][e.rev].cap = 1;
                    return true;
                }
            }
            return false;
        }

		ll dfs(int v,int t,ll f){
			if(v == t) return f;
            used[v] = true;
            for(auto& e : G[v]){
                if(used[e.to] or e.cap == 0) continue;
                int d = dfs(e.to, t, min(f, e.cap));
                if(d > 0){
                    e.cap -= d;
                    G[e.to][e.rev].cap += d;
                    return d;
                }
            }
			return 0;
		}
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m, Q; cin >> n >> m >> Q;
    FF ff;
    ff.init(n);

    rep(loop, m){
        int a, b; cin >> a >> b;
        a--, b--;
        ff.add_edge_both(a, b, 1);
    }

    int s = 0, t = n - 1;
    ll f = ff.max_flow(s, t);

    rep(loop, Q){
        int op, a, b; cin >> op >> a >> b;
        a--, b--;

        // connect
        if(op == 1){
            f += ff.connect(a, b);
        }

        // disconnect
        else { assert(op == 2);
            f += ff.disconnect(a, b);
        }

        cout << f << endl;
    }

    return 0;
}
