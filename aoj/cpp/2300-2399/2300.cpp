#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

vector< vector<double> > d;
double res;
int n, m;
vi used;

void dfs(int cn, int rest, double sum);

int main(void){
    for(; cin >> n >> m; printf("%.10f\n", res)){
        vector<double> l(n), a(n), b(n);
        rep(i, n) cin >> l[i] >> a[i] >> b[i];

        d = vector< vector<double> >(n, vector<double>(n));
        used = vi(n);

        rep(i, n) rep(j, n){
            d[i][j] = pow(l[i] - l[j], 2.) + pow(a[i] - a[j], 2.) + pow(b[i] - b[j], 2.);
        }

        res = 0.;
        dfs(0, m, 0.);
    }

	return 0;
}

void dfs(int cn, int rest, double sum){
    if(rest == 0){
        res = max(res, sum);
        return;
    }

    range(i, cn, n){
        double s = 0.;
        rep(j, i){
            s += used[j] * d[i][j];
        }

        used[i] = 1;
        dfs(i + 1, rest - 1, sum + s);
        used[i] = 0;
    }
}
