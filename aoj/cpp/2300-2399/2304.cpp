#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int inf =1 << 29;
struct edge{int to,cap,rev,idx;};

class dinic{
	public :
		vector< vector<edge> > G;

		void init(int _n){
			n=_n;
			G.resize(n);
			iter.resize(n);
			level.resize(n);
		}

		void add_edge(int from,int to ,int cap, int i){
			G[from].push_back((edge){to,cap,(int)G[to].size(),i});
			G[to].push_back((edge){from,0,(int)G[from].size()-1,-1});
		}
	
		void add_edge_both(int from,int to ,int cap, int i){
			add_edge(from,to,cap,i);
			add_edge(to,from,cap,i);
		}
	
		int max_flow(int s,int t){
			int flow=0;
			for(;;){
				bfs(s);
				if(level[t]<0) return flow;
				iter.assign(n,0);
				int f;
				while((f=dfs(s,t,DINIC_INF))>0){
					flow+=f;
				}
			}
		}
	private:
		int n;
		static const int DINIC_INF = inf;
		vi level;
		vi iter;
	
		void bfs(int s){
			level.assign(n,-1);
			queue<int> que;
			level[s]=0;
			que.push(s);
			while(!que.empty()){
				int v=que.front();que.pop();
				for(int i=0;i< (int)G[v].size(); i++){
					edge &e=G[v][i];
					if(e.cap>0 && level[e.to] <0){
						level[e.to]=level[v]+1;
						que.push(e.to);
					}
				}
			}
		}

		int dfs(int v,int t,int f){
			if(v==t) return f;
			for(int &i=iter[v];i<(int)G[v].size();i++){
				edge &e= G[v][i];
				if(e.cap>0 && level[v]<level[e.to]){
					int d=dfs(e.to,t,min(f,e.cap));
					if(d>0){
						e.cap -=d;
						G[e.to][e.rev].cap+=d;
						return d;
					}
				}
			}
			return 0;
		}	
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;
    dinic d; d.init(n);
    using Ein = tuple<int, int>;
    vector<Ein> edges(m);
    rep(i, m){
        int a, b; cin >> a >> b;
        a--, b--;
        edges[i] = Ein(a, b);
        d.add_edge_both(a, b, 1, i);
    }
    int S, T; cin >> S >> T; S--, T--;

    int res = d.max_flow(S, T);
    set<int> s;
    rep(v, n){
        for(auto& e : d.G[v]){
            int i = e.idx;
            if(i == -1) continue;
            int a, b; tie(a, b) = edges[i];
            if(e.cap == 0 and v != a){
                s.insert(i);
            }
        }
    }

    cout << res << endl;
    cout << s.size() << endl;
    for(auto& e : s){
        cout << e + 1 << endl;
    }

    return 0;
}
