#include <bits/stdc++.h>
#define int long long 
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

const int inf = 1LL << 50;

int n, m, L;
vector<int> K, S, sum;
vector<vector<int>> memo;

int calc(int a, int b){
    return (sum[K[b] + 1] - sum[K[a]]) / L;
}

int rec(int li, int ri){
    if(memo[li][ri] != inf) return memo[li][ri];
    
    int ni = max(li, ri) + 1;

    if(ni == n - 1){
        return memo[li][ri] = calc(li, ni) + calc(ri, ni);
    }

    return memo[li][ri] = min(rec(li, ni) + calc(ri, ni), rec(ni, ri) + calc(li, ni));
}

signed main(void){
    cin >> n >> m >> L;
    K = vector<int>(n), S = vector<int>(m);
    for(auto & e : K) cin >> e, e--;
    for(auto & e : S) cin >> e;

    sort(begin(K), end(K));

    sum = vector<int>(m + 1);
    sum[0] = 0;
    rep(i, m){
        sum[i + 1] = sum[i] + S[i];
    }

    memo = vector<vector<int>>(n, vector<int>(n, inf));

    cout << rec(0, 0) << endl;

	return 0;
}
