#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define x first 
#define y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int N = 8;

int dx[] = {1, 1, 0,-1,-1,-1, 0, 1};
int dy[] = {0, 1, 1, 1, 0,-1,-1,-1};

int main(void){
    vs b(N);

    for(; cin >> b[0];){
        range(i, 1, N) cin >> b[i];

        bool mamiT = true;
        rep(turncnt, N * N){
            char mine = mamiT ? 'o':'x', enemy = mamiT ? 'x':'o';

            vector< vector< pair<int, vi> > > stat(N, vector< pair<int, vi> >(N));
            rep(y, N){
                rep(x, N){
                    stat[y][x].second = vi(8, 0);
                    if(b[y][x] != '.') continue;

                    rep(i, 8){
                        bool ok = false;

                        range(j, 1, N){
                            pii next = mp(x + dx[i] * j, y + dy[i] * j);
                            if(next.x < 0 || N <= next.x ||
                               next.y < 0 || N <= next.y) break;
                            if(b[next.y][next.x] == enemy){
                                stat[y][x].first++;
                                stat[y][x].second[i]++;
                            }
                            else if(b[next.y][next.x] == mine){
                                ok = true;
                                break;
                            }
                            else break;
                        }
                        
                        if(!ok){
                            stat[y][x].first -= stat[y][x].second[i];
                            stat[y][x].second[i] = 0;
                        }
                    }
                }
            }

            pii p;
            int maxs = 0;
            rep(y, N){
                rep(x, N){
                    if((mamiT && maxs < stat[y][x].first) ||
                      (!mamiT && maxs <=stat[y][x].first)){
                        p = mp(x, y);
                        maxs = stat[y][x].first;
                    }
                }
            }

            if(maxs != 0){
                rep(i, 8){
                    if(stat[p.y][p.x].first == 0) continue;
                    do{
                        int k = stat[p.y][p.x].second[i];
                        b[p.y + k * dy[i]][p.x + k * dx[i]] = mine;
                    } while(0 <= --stat[p.y][p.x].second[i]);
                }
            }

            mamiT = !mamiT;
        }

        rep(y, N){
            cout << b[y] << endl;
        }
    }

	return 0;
}
