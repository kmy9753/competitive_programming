#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,-1,0,1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using State = tuple<int, int, int, int, int>;
const int N = 101;
int min_q[N][N][4][11];

int h, w, n;
vector<string> field;

bool is_p[4][4];

inline bool check(int x, int y){
    return 0 <= x and x < w and 0 <= y and y < h and field[y][x] != '#';
}

int main(void){
    cin >> h >> w >> n;
    field = vector<string>(h);
    for(auto & e : field) cin >> e;

    int sx, sy, tx, ty;
    rep(y, h){
        rep(x, w){
            if(field[y][x] == 'S') sx = x, sy = y, field[y][x] = '.';
            if(field[y][x] == 'G') tx = x, ty = y, field[y][x] = '.';
        }
    }

    rep(i, h) rep(j, w) rep(k, 4) rep(l, n + 1) min_q[i][j][k][l] = inf;
    priority_queue<State, vector<State>, greater<State>> q;
    rep(i, 4){
        if(i != 3) continue;
        int nx = sx + dx[i], ny = sy + dy[i];
        if(not check(nx, ny)) continue;
        q.push(State(0, nx, ny, i, 0));
        min_q[ny][nx][i][0] = 0;
    }

    is_p[0][3] = true;
    is_p[1][2] = true;
    is_p[2][1] = true;
    is_p[3][0] = true;

    while(q.size()){
        int x, y, di, up, uq;
        tie(uq, x, y, di, up) = q.top(); q.pop();

        if(x == sx and y == sy and di % 2 == 1) continue;
        
        rep(ndi, 4){
            int nx = x + dx[ndi], ny = y + dy[ndi];
            if(not check(nx, ny)) continue;

            int nup = up, nuq = uq;

            if(ndi != di){
                if(x == sx and y == sy) continue;
                if((di + 2) % 2 == ndi) continue;
                else if(is_p[di][ndi]){
                    if(up == n) continue;
                    nup++;
                }
                else {
                    if(uq == n) continue;
                    nuq++;
                }
            }

            assert(field[ny][nx] != '#'); 
            if(not chmin(min_q[ny][nx][ndi][nup], nuq)) continue;
            q.push(State(nuq, nx, ny, ndi, nup));
        }
    }

    int res = inf;
    rep(di, 4){
        rep(up, n + 1){
            chmin(res, up + min_q[ty][tx][di][up]);
        }
    }
    if(res == inf){
        res = -1;
    }

    cout << res << endl;

    return 0;
}
