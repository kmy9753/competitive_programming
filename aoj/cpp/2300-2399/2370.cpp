#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,x,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
 
vector<vi> edges;
vi color;
map<int, int> d2n;

using pii = pair<int, int>;
#define fst first
#define snd second

bool ok = true;

pii dfs(int v, bool b){
    color[v] = b ? 1:0;

    pii ret(b ? 1:0, b ? 0:1);
    for(auto & nv : edges[v]){
        if(color[nv] == b) ok = false;
        if(color[nv] != -1) continue;

        pii nret = dfs(nv, !b);
        ret.fst += nret.fst; ret.snd += nret.snd;
    }

    return ret;
}

int main(void){
    int n, m; cin >> n >> m;
    edges = vector<vi>(n);

    rep(loop, m){
        int a, b; cin >> a >> b;
        a--, b--;

        edges[a].push_back(b);
        edges[b].push_back(a);
    }

    color = vi(n, -1);
    int min_b = 0;
    rep(i, n){
        if(color[i] != -1) continue;

        pair<int, int> ret = dfs(i, true);
        if(ret.fst > ret.snd) swap(ret.fst, ret.snd);

        d2n[ret.snd - ret.fst]++;
        min_b += ret.fst;
    }

    if(not ok){
        cout << -1 << endl;
        return 0;
    }

    int opt = n / 2 - min_b;
    vi dp(opt + 1, -1);
    dp[0] = 0;

    for(auto & e : d2n){
        rep(i, opt + 1){
            if(dp[i] >= 0){
                dp[i] = e.snd;
            }
            else if(i - e.fst < 0 or dp[i - e.fst] <= 0){
                dp[i] = -1;
            }
            else {
                dp[i] = dp[i - e.fst] - 1;
            }
        }
    }

    rrep(i, opt + 1){
        if(dp[i] >= 0){
            int num_b = min_b + i;
            cout << 1LL * num_b * (n - num_b) - m << endl;
            break;
        }
    }

    return 0;
}
