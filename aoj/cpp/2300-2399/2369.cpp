#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    string in; cin >> in;

    vector< vector<bool> > dp = vector< vector<bool> >(in.size(), vector<bool>(in.size() + 1, false));

    rep(i, in.size()) dp[i][i] = true;

    for(int i = 3; i < in.size() + 1; i += 3){
        rep(j, in.size() - i + 1){
            if(in.at(j) == 'm' && in.at(j + i - 1) == 'w'){
                range(k, 1, i - 1){
                    if(in.at(j + k) == 'e' && dp[j + 1][j + k] && dp[j + k + 1][j + i - 1]){
                        dp[j][j + i] = true;
                    }
                }
            }
        }
    }

    if(dp[0][in.size()]) cout << "Cat" << endl;
    else cout << "Rabbit" << endl;
    
	return 0;
}
