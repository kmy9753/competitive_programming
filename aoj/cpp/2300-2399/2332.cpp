#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int kLoop = -1;
const int kYet = -2;

int main(void){
    for(int n; cin >> n;){
        vi p(n);
        rep(i, n){
            cin >> p[i];
        }

        vi to(n, kYet);
        rep(i, n){
            if(to[i] != kYet) continue;
            
            int pos = i, to_pos = kLoop;
            set<int> s;

            while(to[pos] != kLoop && s.insert(pos).second){
                if(p[pos] == 0){
                    to_pos = pos;
                    break;
                }
                pos += p[pos];
            }

            each(itr, s) to[*itr] = to_pos;
        }

        vi numTurn(n, inf);
        numTurn[0] = 0;
        bool end = false;

        while(!end){
            end = true;

            rep(i, n){
                if(numTurn[i] == kYet) continue;

                range(j, 1, 7){
                    int next_pos = to[min(n - 1, i + j)];

                    if(next_pos != kLoop && numTurn[i] + 1 < numTurn[next_pos]){
                        numTurn[next_pos] = numTurn[i] + 1;
                        end = false;
                    }
                }
            }
        }

        cout << numTurn[n - 1] << endl;
    }

	return 0;
}
