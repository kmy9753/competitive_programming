#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define int ll

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

vs field;
int w, h;

//((cost, (y, x)) 
typedef pair<int, pii> State;

#define F first
#define S second

vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

void bfs(char tar, vvi & minDist){
    minDist = vvi(h, vi(w, inf));

    priority_queue<State, vector<State>, greater<State>> q;
    rep(y, h){
        rep(x, w){
            if(field[y][x] != tar) continue;
            q.push(mp(0, mp(y, x)));
        }
    }

    while(!q.empty()){
        int cur_cost = q.top().F;
        pii cur_pos = q.top().S;
        q.pop();

        if(minDist[cur_pos.F][cur_pos.S] != inf) continue;

        if(field[cur_pos.F][cur_pos.S] == '.'){
            minDist[cur_pos.F][cur_pos.S] = cur_cost;
        }

        int next_cost = cur_cost + 1;
        rep(i, 4){
            pii next_pos = mp(cur_pos.F + dy[i], cur_pos.S + dx[i]);

            if(field[next_pos.F][next_pos.S] == '#' ||
               field[next_pos.F][next_pos.S] == 'g' ||
               field[next_pos.F][next_pos.S] == '*' ||
               minDist[next_pos.F][next_pos.S] != inf)
                continue;
            
            q.push(mp(next_cost, next_pos));
        }
    }
}

signed main(void){
    for(; cin >> w >> h;){
        field = vs(h);
        pii s;
        int num_f = 0;
        rep(y, h){
            cin >> field[y];
            
            rep(x, w){
                if(field[y][x] == 's'){
                    s = mp(y, x);
                    field[y][x] = '.';
                }
                if(field[y][x] == '.'){
                    num_f++;
                }
            }
        }

        vvi minDist_s, minDist_g;
        bfs('*', minDist_s);
        bfs('g', minDist_g);

//        rep(y, h){
//            rep(x, w){
//                printf("%4d", (minDist_s[y][x] == inf ? -1:minDist_s[y][x]));
//            }
//            cout << endl;
//        }
//        cout << endl;
//        rep(y, h){
//            rep(x, w){
//                printf("%4d", (minDist_g[y][x] == inf ? -1:minDist_g[y][x]));
//            }
//            cout << endl;
//        }
//        cout << "----------" <<endl;
        
        vector<pair<int, pii>> deltaSG;
        int sum_dist = 0, num_tos = 0;
        rep(y, h){
            rep(x, w){
                if(field[y][x] != '.') continue;

                if(minDist_g[y][x] != inf){
                    sum_dist += minDist_g[y][x];

                    if(minDist_s[y][x] != inf){
                        deltaSG.pb(mp(minDist_g[y][x] - minDist_s[y][x], mp(y, x)));
                    }
                }
                else{
                    sum_dist += minDist_s[y][x];
                    num_tos++;
                }
            }
        }
        sort(all(deltaSG), greater<pair<int, pii>>());
        
#define F first
#define S second

        long double e = 1.0 / (num_f - num_tos) * sum_dist;
        for(auto state : deltaSG){
            pii pos = state.S;

            num_tos++;
            sum_dist = sum_dist - minDist_g[pos.F][pos.S] + minDist_s[pos.F][pos.S];

            if(num_f == num_tos) break;
            long double cur = 1.0 / (num_f - num_tos) * sum_dist;

            if(cur < e) e = cur;
            else break;
        }
        long double res = minDist_s[s.F][s.S] + e;
        if(minDist_g[s.F][s.S] != inf) res = min(res, (long double)minDist_g[s.F][s.S]);
        printf("%.12Lf\n", res);
    }

    return 0;
}
