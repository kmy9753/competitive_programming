#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int n;
vector<vi> adj;

vi used;
void dfs(int v, int label){
    used[v] = label;
    rep(nv, n){
        if(not used[nv] and adj[v][nv]){
            dfs(nv, label);
        }
    }
}

const int N = 1001;
int memo[N][N][2];

int rec(int odd, int even, int unuse){
    int& ret = memo[odd][even][unuse];
    if(ret != -1){
        return ret;
    }

    if(odd + even == 2){
        ret = (unuse == 1);
        return ret;
    }

    ret = false;

    // (0, 0)
    if(even >= 2){
        ret |= !rec(odd, even - 1, unuse ^ 1);
    }

    // (1, 1)
    if(odd >= 2){
        ret |= !rec(odd - 2, even + 1, unuse);
    }
    
    // (0, 1)
    if(even >= 1 and odd >= 1){
        ret |= !rec(odd, even - 1, unuse ^ 1);
    }

    // cerr << "odd = " << odd  << ", even = " << even << ", unuse = " << unuse << ": " << ret << endl;
    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> n;
    adj = vector<vi>(n, vi(n));
    rep(i, n){
        string in; cin >> in;
        rep(j, n){
            if(in[j] == 'Y'){
                adj[i][j] = true;
            }
        }
    }

    int odd = 0, even = 0, unuse = 0;
    int label = 1;
    used = vi(n);
    rep(v, n){
        if(used[v]) continue;
        dfs(v, label);

        vi s;
        rep(i, n) if(used[i] == label) s.emplace_back(i);
        
        int sz = s.size();
        int cur = sz * (sz - 1) / 2;
        rep(i, sz){
            rep(j, i + 1, sz){
                if(adj[s[i]][s[j]]){
                    cur--;
                }
            }
        }
        if(sz % 2 == 1) odd++;
        else even++;
        unuse = (unuse + cur) % 2;

        label++;
    }

    rep(i, n + 1) rep(j, n + 1) rep(k, 2) memo[i][j][k] = -1;
    cout << (rec(odd, even, unuse) ? "Taro":"Hanako") << endl;


    return 0;
}
