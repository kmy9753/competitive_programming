#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

ll mod;
vi sum;

// [l, r)
inline ll calc(int l, int r){
    return SUB(sum[l], sum[r], mod);
}

int main(void){
    for(ll a, b; cin >> a >> b >> mod, a;){
        ll n = b - a;
        vi dp(n + 1);
        sum = vi(n + 2);

        dp[n] = sum[n] = 1 % mod;
        rrep(i, n){
            ll ca = a + i;
            ll upper = 1;
            while(upper <= ca) upper *= 10;

            dp[i] = 1;

            ll p = 1;
            rep(loop, 10){
                if(loop != 0) p *= 10;

                if(ca % (p * 10) / p == 9 or ca / p == 0) continue;
                ll na = (ca + p) / p * p;
                ll nb = (ca + 10 * p) / (10 * p) * (10 * p);

                if(na > b) continue;
                na -= a; nb -= a;
                chmin(nb, n + 1);

                dp[i] = ADD(dp[i], calc(na, nb), mod);
            }

            p = 10;
            rep(loop, 10){
                ll na = ca * p;
                ll nb = upper * p;

                if(na > b) continue;
                na -= a; nb -= a;
                chmin(nb, n + 1);

                dp[i] = ADD(dp[i], calc(na, nb), mod);

                p *= 10;
            }

            sum[i] = ADD(sum[i + 1], dp[i], mod);
        }

        cout << sum[0] << endl;
    }

    return 0;
}
