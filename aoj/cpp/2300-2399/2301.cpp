#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

double P, E, T;

double dfs(int rest, double lb, double ub){
    double H = (lb + ub) / 2.0;

    if(rest == 0){
        if(abs(T - H) < E) return 1.0;
        else return 0.0;
    }

    if(T - E <= lb && ub <= T + E) return 1.0;
    else if(ub < T - E || T + E < lb) return 0.0;

    if(H <= T){
        return P * dfs(rest - 1, lb, H) + (1.0 - P) * dfs(rest - 1, H, ub);
    }
    else{
        return P * dfs(rest - 1, H, ub) + (1.0 - P) * dfs(rest - 1, lb, H);
    }
}

int main(void){
    for(int K, L, R; cin >> K >> L >> R >> P >> E >> T; printf("%f\n", dfs(K, L, R)));

	return 0;
}
