#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,-1,0,1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 1000;
int dp[N][N][4];

vector<string> field;
set<int> next_idx[4];
vi dirs;

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    while(true){
    int h, w, n; cin >> h >> w >> n;
    if(h == 0) break;

    string commands; cin >> commands;
    vector<string> field(h); rep(y, h) cin >> field[y];
    int sx, sy, gx, gy;
    rep(y, h){
        rep(x, w){
            if(field[y][x] == 'S'){
                sx = x, sy = y;
                field[y][x] = '.';
            }
            if(field[y][x] == 'G'){
                gx = x, gy = y;
                field[y][x] = '.';
            }
        }
    }

    rep(i, 4) next_idx[i].clear();
    int d = 0; next_idx[0].insert(0);
    dirs = vi(n + 1); dirs[0] = 0;
    rep(i, n){
        char c = commands[i];
        if(c == 'L'){
            (d += 1) %= 4;
        }
        else {
            (d += 3) %= 4;
        }
        next_idx[d].insert(i + 1);
        dirs[i + 1] = d;
    }

    rep(y, N) rep(x, N) rep(d, 4) dp[y][x][d] = inf;
    using Elem = tuple<int, int, int, int>;
    priority_queue<Elem, vector<Elem>, greater<Elem>> q;
    q.push(Elem(0, sx, sy, 1));
    dp[sy][sx][1] = 0;

    bool ok = false;
    while(q.size()){
        int x, y, d, idx; tie(idx, x, y, d) = q.top(); q.pop();
        if(idx != dp[y][x][d]) continue;

        if(x == gx and y == gy){
            ok = true;
            break;
        }

        rep(nd, 4){
            int nx = x + dx[nd];
            int ny = y + dy[nd];
            if(nx < 0 or w <= nx or ny < 0 or h <= ny or field[y][x] == '#') continue;

            int nidx = idx;
            if(nd != d){
                int ang = (nd - d + dirs[idx] + 4) % 4;
                auto itr = next_idx[ang].lower_bound(idx);
                if(itr == end(next_idx[ang])) continue;
                nidx = *itr;
            }
            if(chmin(dp[ny][nx][nd], nidx)){
                q.push(Elem(nidx, nx, ny, nd));
            }
        }
    }

    cout << (ok ? "Yes":"No") << endl;
}

    return 0;
}
