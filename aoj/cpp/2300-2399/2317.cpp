#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
#define int ll

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1e9;

signed main(void){
    for(int n, m; cin >> n >> m;){
        vi s(n), t(n);
        rep(i, n) cin >> s[i] >> t[i];

        m += 2;
        vi p(m);
        p[0] = 0, p[m - 1] = inf;
        range(i, 1, m - 1) cin >> p[i];

        sort(all(p));
        
        int res = 0;
        vi cnt_lb(m, 0), cnt_ub(m, 0);

        rep(i, n){
            auto p_lb = p.begin(), p_ub = p.begin();

            if(s[i] < t[i]){
                p_lb = lower_bound(all(p), s[i]) - 1;
                p_ub = lower_bound(all(p), t[i]);

                if((p_ub - p_lb - 1) % 2 == 1){
                    p_ub--;
                }
                else {
                    res -= (*p_ub - t[i]);
                }
                
                res -= (s[i] - *p_lb);
            }
            else {
                p_lb = lower_bound(all(p), t[i]) - 1;
                p_ub = lower_bound(all(p), s[i]);

                if((p_ub - p_lb - 1) % 2 == 1){
                    p_lb++;
                }
                else {
                    res -= (t[i] - *p_lb);
                }

                res -= (*p_ub - s[i]);
            }

            cnt_lb[p_lb - p.begin()]++;
            cnt_ub[p_ub - p.begin()]++;
        }

        int cur_cnt = cnt_lb[0], pre_cnt = 0;

        range(i, 1, m){
            res += cur_cnt * (p[i] - p[i - 1]);

            cur_cnt -= cnt_ub[i];
            swap(cur_cnt, pre_cnt);
            cur_cnt += cnt_lb[i];
        }

        cout << res << endl;
    }

	return 0;
}
