#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;
#define S first
#define E second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, res; cin >> n, n; cout << res << endl){
        vi dates(n);
        vi sat(n);

        rep(i, n){
            int m, l; cin >> m >> l;
            sat[i] = l;

            rep(j, m){
                int s, e; cin >> s >> e;
                s -= 6, e -= 6;

                range(k, s, e){
                    dates[i] |= (1 << k);
                }
            }
        }

        vvi dp(2, vi(1 << 16, -1));
        dp[0][0] = 0;

        rep(i, n){
            dp[1] = dp[0];

            rep(j, 1 << 16){
                if(dp[0][j] == -1) continue;

                if((j & dates[i]) != 0){
                    continue;
                }
                int next = j | dates[i];

                dp[1][next] = max(dp[1][next],
                                  dp[0][j] + sat[i]);
            }
            dp[0] = dp[1];
        }

        res = *max_element(all(dp[1]));
    }

	return 0;
}
