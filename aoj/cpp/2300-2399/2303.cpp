#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, m, l; cin >> n >> m >> l;){
        vector<double> p(n);
        vi t(n), v(n);
        rep(i, n){
            cin >> p[i] >> t[i] >> v[i];
            p[i] /= 100.0;
        }

        // [human][location][num_rest]
        vector<vector<vector<double>>> dp(n, vector<vector<double>>(m + 1, vector<double>(m + 1, 0.0)));
        rep(i, n) dp[i][0][0] = 1.0;
        rep(i, n){
            rep(j, m){
                rep(k, m){
                    dp[i][j + 1][k + 1] += dp[i][j][k] * p[i];
                    dp[i][j + 1][k]     += dp[i][j][k] * (1.0 - p[i]);
                }
            }
        }

        // [human][num_rest]
        vector<vector<double>> sum_time(n, vector<double>(m + 1, (double)inf));
        rep(i, n){
            if(v[i] == 0) continue;
            rep(j, m + 1){
                sum_time[i][j] = (double)l / v[i] + (double)t[i] * j;
            }
        }

        vector<double> res(n, 0.0);
        rep(i, n){
            if(v[i] == 0) continue;
            rep(a, m + 1){
                double prov = dp[i][m][a];

                rep(j, n){
                    if(i == j) continue;
                    
                    double q = 0.0;
                    rep(b, m + 1){
                        if(sum_time[i][a] < sum_time[j][b]) q += dp[j][m][b];
                    }

                    prov *= q;
                }

                res[i] += prov;
            }
        }

        rep(i, n) printf("%.9f\n", res[i]);
    }

	return 0;
}
