#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define repn(i, m, n) for(int i = m; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 6;
const int M = 10;

int n, x, m, sum = -1;
int l[M], r[M], s[M], res[N], cur[N];

void dfs(int);

int main(void){
    cin >> n >> x >> m;
    rep(i, m) cin >> l[i] >> r[i] >> s[i], l[i]--, r[i]--;
    
    dfs(0);
    if(~sum) rep(i, n) cout << (i ? " ":"") << res[i];
    else cout << sum;
    cout << endl;

    return 0;
}

void dfs(int ind){
    if(ind == n){
        rep(i, m){
            int ss = 0;
            repn(j, l[i], r[i] + 1) ss += cur[j];
            if(ss != s[i]) return;
        }

        int ss = 0;
        rep(i, n) ss += cur[i];
        if(ss > sum){
            sum = ss;
            rep(i, n) res[i] = cur[i];
        }
        
        return;
    }

    rep(i, x + 1) cur[ind] = i, dfs(ind + 1); 
}
