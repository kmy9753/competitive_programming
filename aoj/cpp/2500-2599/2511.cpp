#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

struct UnionFind {
    vector<int> data;
    UnionFind(int size) : data(size, -1) { }
    bool unionSet(int x, int y) {
        x = root(x); y = root(y);
        if (x != y) {
            if (data[y] < data[x]) swap(x, y);
            data[x] += data[y]; data[y] = x;
        }
        return x != y;
    }
    bool findSet(int x, int y) {
        return root(x) == root(y);
    }
    int root(int x) {
        return data[x] < 0 ? x : data[x] = root(data[x]);
    }
    int size(int x) {
        return -data[root(x)];
    }
};

using HV = tuple<int, int>;
using Edge = tuple<int, int, int>;

const int N = 201;
int costs[N][N];
vi used;

void dfs(int v, int n){
    used[v] = true;

    rep(nv, n){
        if(costs[v][nv] == -1 or used[nv]) continue;
        dfs(nv, n);
    }
}

int main(void){
    for(int n, m; cin >> n >> m, n;){
        rep(i, n) rep(j, n) costs[i][j] = -1;

        vector<HV> hv(n);
        vi h(n);
        rep(i, n){
            cin >> h[i];
            hv[i] = HV(h[i], i);
        }
        sort(_all(hv));

        int preh = get<0>(hv[0]);
        vector<vi> h2vs = {{}, {get<1>(hv[0])}};
        int hmax = 1;
        for(auto & e : hv){
            int h, v; tie(h, v) = e;
            if(h != preh) h2vs.push_back(vi()), preh = h, hmax++;
            h2vs.back().push_back(v);
        }

        rep(loop, m){
            int a, b, c; cin >> a >> b >> c;
            a--, b--;
            costs[a][b] = costs[b][a] = c;
        }

        int hend = hmax + 1;
        [&]{
            rep(i, hmax + 1){
                used = vi(n);

                rep(j, i + 1){
                    for(auto & v : h2vs[j]){
                        used[v] = true;
                    }
                }

                rep(v, n){
                    if(used[v]) continue;
                    dfs(v, n);
                    break;
                }
                rep(v, n) if(not used[v]){
                    hend = i;
                    return;
                }
            }
        }();

        if(hend == 0){
            cout << 0 << endl;
            continue;
        }
        used = vi(n);
        UnionFind uf(n);
        priority_queue<Edge, vector<Edge>, greater<Edge>> q;
        int res = 0;
        rrep(i, hmax + 1){
            for(auto & v : h2vs[i]){
                rep(nv, n){
                    if(used[nv] and costs[v][nv] != -1){
                        q.push(Edge(costs[v][nv], v, nv));
                    }
                }
                for(auto & nv : h2vs[i]){
                    if(v >= nv) continue;
                    if(costs[v][nv] != -1){
                        q.push(Edge(costs[v][nv], v, nv));
                    }
                }
            }

            for(auto & v : h2vs[i]) used[v] = true;

            if(i > hend) continue;

            while(q.size()){
                int cost, a, b; tie(cost, a, b) = q.top(); q.pop();
                if(uf.findSet(a, b)) continue;

                uf.unionSet(a, b);
                res += cost;
            }
            q = priority_queue<Edge, vector<Edge>, greater<Edge>>();
        }

        cout << res << endl;
    }

    return 0;
}
