#include <bits/stdc++.h>

using namespace std;

#define int long long

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

vs str;
int len;

vi vals;

#define F first
#define S second

pii dfs(int pos, int offset, int num){
    if(pos == 3){
        if(vals[2] == vals[0] + vals[1] + offset){
            return mp(1, 0);
        }
        else if(vals[2] == (vals[0] + vals[1] + offset) % 10){
            return mp(0, 1);
        }
        else return mp(0, 0);
    }

    pii ret = mp(0, 0);

    if(str[pos][num] != '?'){
        vals[pos] = str[pos][num] - '0';

        pii diff = dfs(pos + 1, offset, num);
        ret.F += diff.F;
        ret.S += diff.S;
    }
    else{
        rep(i, 10){
            if(num == 0 && i == 0) continue;

            vals[pos] = i;

            pii diff = dfs(pos + 1, offset, num);
            ret.F += diff.F;
            ret.S += diff.S;
        }
    }

    return ret;
}

constexpr int MOD = 1000000007;

signed main(void){
    vals = vi(3);
    for(str = vs(3); cin >> str[0], str[0] != "0";){
        len = str[0].size();
        cin >> str[1] >> str[2];

        vvi dp(len + 1, vi(2));
        dp[len][0] = 1;

        for(int i = len - 1; i >= 0; i--){
            rep(j, 2){
                pii diff = dfs(0, j, i);

                (dp[i][0] += dp[i + 1][j] * diff.F) %= MOD;
                (dp[i][1] += dp[i + 1][j] * diff.S) %= MOD;
            }
        }

        cout << dp[0][0] << endl;
    }

	return 0;
}
