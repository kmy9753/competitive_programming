#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

inline bool isAlpha(char c){ return 'A' <= c && c <= 'Z'; }

string parse(string & str, int & pos){
    string ret;

    while(pos < str.size() && str.at(pos) != ']'){
        if(str.at(pos) == '['){
            pos++;

            string diff = parse(str, pos);
            reverse(all(diff));
            ret += diff;

            pos++;
        }
        else {
            ret.pb(str.at(pos));
            pos++;
        }
    }

    return ret;
}

vs strs;

void dfs(string str){
    bool ok = true;

    for(char & c : str){
        if(c != '?') continue;

        ok = false;
        rep(i, 26){
            c = 'A' + i;

            dfs(str);
        }
    }

    if(!ok) return;

    string s;
    int delta = 0;
    for(char c : str){
        if(c == '+') delta++;
        else if(c == '-') delta--;
        else {
            if(isAlpha(c)) s.pb((c - 'A' + delta + 26 * 10) % 26 + 'A');
            else s.pb(c);

            delta = 0;
        }
    }

    strs.pb(s);
}

int main(void){
    for(string str; cin >> str, str != ".";){
        
        //$B%"%k%U%!%Y%C%H$rKd$a$FA5C5:w(B
        strs = vs();
        dfs(str);

        string res;
        rep(i, 1000) res.pb('Z');
        
        for(auto cur : strs){
            int pos = 0;
            res = min(res, parse(cur, pos));
        }

        cout << res << endl;
    }

	return 0;
}
