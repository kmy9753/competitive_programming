#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

int n, m;
int used[200], lb[200];

int main(void){
    cin >> m >> n;

    vector<vector<pair<int, int>>> rng(m, vector<pair<int, int>>(n, make_pair(-100, 200)));
    rep(i, m){
        int K; cin >> K;
        rep(loop, K){
            int s, t; string cond; cin >> s >> cond >> t;
            s--;

            if(cond == ">="){
                rng[i][s].first = max(rng[i][s].first, t);
            }
            else {
                rng[i][s].second = min(rng[i][s].second, t);
            }
        }

        rep(j, n){
            if(rng[i][j].first > rng[i][j].second){
                cout << "No" << endl;
                return 0;
            }
        }
    }

    int cnt = 0;
    while(cnt < m){
        bool ok1 = false;
        [&]{
            rep(a, m){
                if(used[a]) continue;
                bool ok2 = true;
                [&]{
                    rep(b, m){
                        if(a == b or used[b]) continue;
                        rep(i, n){
                            int cur = max(lb[i], rng[a][i].first);
                            if(rng[b][i].second < cur){
                                ok2 = false;
                                return;
                            }
                        }
                    }
                }();
                if(ok2){
                    cnt++;
                    used[a] = true;
                    ok1 = true;
                    rep(i, n){
                        lb[i] = max(lb[i], rng[a][i].first);
                    }
                    return;
                }
            }
        }();
        
        if(not ok1) break;
    }

    cout << (cnt == m ? "Yes":"No") << endl;

    return 0;
}
