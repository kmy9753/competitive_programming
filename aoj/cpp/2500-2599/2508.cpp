#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
 
using namespace std;
 
typedef pair<int, int> pii;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef vector<string> vs;

const char LIST[53] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"};

int main(void){
    string str;
    for(int n; cin >> n, n; cout << str << endl){
        vi ope(n);
        rep(i, n) cin >> ope[i];
        cin >> str;

        rep(i, str.size()){
            int diff = ((str[i] <= 'Z') ? str[i] - 'A':str[i] - 'a' + 26) - ope[i % n];
            str[i] = LIST[(0 <= diff) ? diff:diff + 52];
        }
    }
 
    return 0;
}
