#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 30;

int main(void){
    for(int n; cin >> n, n;){
        vs signal(n);
        bool out = false, nox = true;
        rep(i, n){
            cin >> signal[i];
            if(i != 0 && signal[i - 1] == signal[i]){
                out = true;
            }
            if(signal[i] == "x") nox = false;
        }
        rep(i, n){
            if(i % 2 && signal[i] != "x"){
                int left = signal[i - 1] != "x" ? toInt(signal[i - 1]):-inf;
                int right = i + 1 < signal.size() ? signal[i + 1] != "x" ? toInt(signal[i + 1]):-inf:-inf;
                if(left < toInt(signal[i]) && right < toInt(signal[i]));
                else out = true;
            }
        }
        if(out){
            cout << "none" << endl;
            continue;
        }
        if(nox){
            cout << "ambiguous" << endl;
            continue;
        }

        int lb = -inf, ub = inf;
        rep(i, n){
            if(signal[i] == "x"){

                if(i % 2){
                    int left = 0 <= i - 1 ? (toInt(signal[i - 1]) + 1):-inf;
                    int right = i + 1 < signal.size() ? (toInt(signal[i + 1]) + 1):-inf;
                    lb = max(lb, max(left, right));
                }
                else{
                    int left = 0 <= i - 1 ? (toInt(signal[i - 1]) - 1):inf;
                    int right = i + 1 < signal.size() ? (toInt(signal[i + 1]) - 1):inf;
                    ub = min(ub, min(left, right));
                }
            }
        }

        if(ub == lb){
            cout << lb << endl;
        }
        else if(lb < ub){
            cout << "ambiguous" << endl;
        }
        else{
            cout << "none" << endl;
        }
    }

	return 0;
}
