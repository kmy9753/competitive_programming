#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 26;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 301;
const int L = 17;
int dist[N][N];
int min_cost[L][1 << L];

using Elem = tuple<int, int, int>;

int main(void){
    for(int n, m, l, sv, t; cin >> n >> m >> l >> sv >> t, n;){
        sv--;
        l++;

        rep(i, n) rep(j, n) dist[i][j] = inf;
        rep(i, n) dist[i][i] = 0;
        rep(loop, m){
            int a, b, c; cin >> a >> b >> c;
            a--, b--;
            dist[a][b] = dist[b][a] = c;
        }

        vi jiro(n, -1);
        vi j2v(l);
        j2v[0] = sv; jiro[sv] = 0;
        vi eat_time(l);
        rep(i, 1, l){
            int v, e; cin >> v >> e;
            v--;
            jiro[v] = i;
            j2v[i] = v;
            eat_time[i] = e;
        }

        rep(k, n) rep(i, n) rep(j, n) chmin(dist[i][j], dist[i][k] + dist[k][j]);

        rep(i, l) rep(j, 1 << l) min_cost[i][j] = inf;
        priority_queue<Elem, vector<Elem>, greater<Elem>> q;
        q.push(Elem(0, 0, 1));

        int res = 0;
        while(q.size()){
            int cost, ji, visited;
            tie(cost, ji, visited) = q.top(); q.pop();
            int v = j2v[ji];

            if(not chmin(min_cost[ji][visited], cost)) continue;
            chmax(res, __builtin_popcount(visited) - 1);

            rep(nji, 1, l){
                if(visited & (1 << nji)) continue;

                int nv = j2v[nji];

                int ncost = cost + dist[v][nv] + eat_time[nji];
                if(ncost + dist[nv][sv] > t) continue;

                int nvisited = (visited | (1 << nji));

                if(min_cost[nji][nvisited] == inf){
                    q.push(Elem(ncost, nji, nvisited));
                }
            }
        }

        cout << res << endl;
    }

    return 0;
}
