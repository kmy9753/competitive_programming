#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[2]={1,0};
const int dy[2]={0,1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 50;

// y1, x1, y2, x2 ( [y1, y2), [x1, x2) )
int memo[N][N][N + 1][N + 1];

int h, w;
vector<string> field;

vi next_ys[N][N];
vi next_xs[N][N];

int ok[N + 1][N + 1][N + 1][N + 1];

inline bool is_valid(int y1, int x1, int y2, int x2){
    return ok[y1][x1][y2][x2];
}

int rec(int y1, int x1, int y2, int x2){
    int& ret = memo[y1][x1][y2][x2];
    if(ret != -2){
        return ret;
    }
    if(y1 + 1 == y2 and x1 + 1 == x2){
        return (ret = 0);
    }

    ret = -1;

    // not pick up
    if(is_valid(y1 + 1, x1, y2, x2)){
        chmax(ret, rec(y1 + 1, x1, y2, x2));
    }
    if(is_valid(y1, x1 + 1, y2, x2)){
        chmax(ret, rec(y1, x1 + 1, y2, x2));
    }

    // pick up
    rep(i, (int)next_ys[y1][x1].size()){
        assert(islower(field[y1][x1]));
        int yy = next_ys[y1][x1][i];
        int xx = next_xs[y1][x1][i];
        assert(isupper(field[yy][xx]));
        if(yy >= y2 or xx >= x2) continue;

        int cur = 1;
        if(is_valid(y1 + 1, x1, yy, xx + 1)){ // vv
            chmax(cur, 1 + rec(y1 + 1, x1, yy, xx + 1));
        }
        if(is_valid(y1 + 1, x1, yy + 1, xx)){ // v>
            chmax(cur, 1 + rec(y1 + 1, x1, yy + 1, xx));
        }
        if(is_valid(y1, x1 + 1, yy, xx + 1)){ // >v
            chmax(cur, 1 + rec(y1, x1 + 1, yy, xx + 1));
        }
        if(is_valid(y1, x1 + 1, yy + 1, xx)){ // >>
            chmax(cur, 1 + rec(y1, x1 + 1, yy + 1, xx));
        }

        if(is_valid(yy, xx, y2, x2)) chmax(ret, cur + rec(yy, xx, y2, x2)); // ??v
    }

    return ret;
}

vector<vi> used;
void dfs(int y, int x, int oy, int ox){
    used[y][x] = true;
    ok[oy][ox][y + 1][x + 1] = true;
    if(islower(field[oy][ox]) and field[y][x] - 'A' + 'a' == field[oy][ox]){
        next_ys[oy][ox].emplace_back(y);
        next_xs[oy][ox].emplace_back(x);
    }

    rep(i, 2){
        int ny = y + dy[i];
        int nx = x + dx[i];

        if(0 <= ny and ny < h and 0 <= nx and nx < w and field[ny][nx] != '#' and not used[ny][nx]) dfs(ny, nx, oy, ox);
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    while(true){
        cin >> h >> w;
        if(h == 0 and w == 0) break;

        field = vector<string>(h);
        for(auto& e : field) cin >> e;

        rep(y1, N) rep(x1, N) rep(y2, N + 1) rep(x2, N + 1) memo[y1][x1][y2][x2] = -2;
        rep(y1, N + 1) rep(x1, N + 1) rep(y2, N + 1) rep(x2, N + 1) ok[y1][x1][y2][x2] = false;
        rep(y, h) rep(x, w){
            next_xs[y][x] = vi(); next_ys[y][x] = vi();
            if(field[y][x] == '#') continue;
            used = vector<vi>(h, vi(w));
            dfs(y, x, y, x);
        }
        cout << rec(0, 0, h, w) << endl;
    }

    return 0;
}
