#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int kNumAlphabets = 26;

vi used;
vvi edge;
string res;

void dfs(int v, set<int> from){
    if(used[v]){
        if(from.find(v) != from.end()) res = "no";
        return;
    }

    used[v] = true;
    from.insert(v);

    rep(u, kNumAlphabets){
        if(edge[v][u]) dfs(u, from);
    }
}

int main(void){
    for(int n; cin >> n, n; cout << res << endl){
        res = "yes";
        edge = vvi(kNumAlphabets, vi(kNumAlphabets));

        string pre; cin >> pre;
        rep(i, n - 1){
            string cur; cin >> cur;

            bool same = true;
            rep(j, min(pre.size(), cur.size())){
                if(pre.at(j) != cur.at(j)){
                    edge[pre.at(j) - 'a'][cur.at(j) - 'a'] = true;
                    same = false;
                    break;
                }
            }
            if(same && cur.size() < pre.size()) res = "no";

            pre = cur;
        }

        rep(u, kNumAlphabets){
            used = vi(kNumAlphabets);
            dfs(u, set<int>());
        }
    }

	return 0;
}
