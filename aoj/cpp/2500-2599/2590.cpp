#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

string o = "0123456789ABCD";
int main(void){
    int n, m, q; 

    while(cin >> n >> m >> q, n){
        vs _s(q), b(q), s(q);
        rep(i, q){
            cin >> _s[i] >> b[i];
            s[i] = _s[i];
        }
        rep(i, q){
            if(i == 0) continue;
            rep(j, n){
                if(_s[i][j] == '1'){
                    s[i][j] = s[i - 1][j] == '1' ? '0':'1';
                }
                else s[i][j] = s[i - 1][j];
            }
        }

        vector< vector<bool> > tbl = vector< vector<bool> >(m, vector<bool>(n, true));

        rep(i, q){
            rep(j, m){
                rep(k, n){
                    if((s[i][k] == '1' && b[i][j] == '0') || (s[i][k] == '0' && b[i][j] == '1'))
                        tbl[j][k] = false;
                }
            }
        }

        rep(i, m){
            char res;
            int cnt = 0;
            rep(j, n) if(tbl[i][j]) res = j < 10 ? '0' + j:'A' + j - 10, cnt++;

            if(cnt == 1) cout << res;
            else cout << '?';
        }
        cout << endl;
    }


	return 0;
}
