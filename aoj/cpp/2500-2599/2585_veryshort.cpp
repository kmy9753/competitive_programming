#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> pii;
#define all(a)  (a).begin(),(a).end()
#define pb push_back
#define mp make_pair
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
const int inf =1 << 24;
typedef tuple<int, int, int, int> Edge;
typedef tuple<int, int, int> State;
int main(void){
    for(int n, m, h, com; cin >> n >> m >> h >> com, n;){
        vector<vector<Edge>> edges(n);
        rep(i, m){
            int a, b, c, hh, r; cin >> a >> b >> c >> hh >> r;
            a--, b--, r--;
            edges[a].pb(make_tuple(b, c, hh, r));
            edges[b].pb(make_tuple(a, c, hh, r));
        }
        int s, z; cin >> s >> z;
        s--, z--;
        vi costTicket(1 << com, inf);
        costTicket[0] = 0;

        int p; cin >> p;
        rep(i, p){
            int l, d; cin >> l >> d;
            int kaisha = 0;
            rep(j, l){
                int kk; cin >> kk;
                kk--;
                kaisha |= (1 << kk);
            }
            costTicket[kaisha] = d;
        }
        rep(i, 1 << com){
            if(costTicket[i] == inf) continue;
            rep(j, 1 << com){
                if(costTicket[j] == inf) continue;
                int k = (i | j);
                costTicket[k] = min(costTicket[k], costTicket[i] + costTicket[j]);
            }
        }
        int res = inf;
        rep(i, 1 << com){
            if(costTicket[i] == inf) continue;
            priority_queue<State, vector<State>, greater<State>> q;
            q.push(make_tuple(0, 0, s));
            vvi minCost(h + 1, vi(n, -1));
            while(q.size()){
                int cur_c, cur_t, cur_p;
                tie(cur_c, cur_t, cur_p) = q.top();
                q.pop();
                if(minCost[cur_t][cur_p] != -1) continue;
                minCost[cur_t][cur_p] = cur_c;
                if(cur_p == z){
                    res = min(res, cur_c + costTicket[i]);
                    break;
                }
                for(auto && edge : edges[cur_p]){
                    int next_p, cost, t, kk; 
                    tie(next_p, cost, t, kk) = edge;

                    if((i & (1 << kk)) != 0) cost = 0;

                    int next_c = cur_c + cost, next_t = cur_t + t;
                    if(next_t > h) continue;
                    if(minCost[next_t][next_p] != -1) continue;

                    q.push(make_tuple(next_c, next_t, next_p));
                }
            }
        }
        if(res == inf) res = -1;
        cout << res << endl;
    }
	return 0;
}
