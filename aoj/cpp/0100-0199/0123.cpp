#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 7;
const string res[N + 1] = {"AAA", "AA", "A", "B", "C", "D", "E", "NA"};
const double t[N][2] = {35.50, 71.00, 37.50, 77.00, 40.00, 83.00, 43.00, 89.00, 50.00, 105.00, 55.00, 116.00, 70.00, 148.00};

int main(void){
    for(vector<double> in(2); cin >> in[0] >> in[1];){
        int ind = N;
        for(int i = N - 1; 0 <= i; i --) rep(j, 2){
            if(t[i][j] <= in[j]) break;
            if(j) ind = i;
        }
        cout << res[ind] << endl;
    }
 
    return 0;
}
