#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

int main(void){
    int res;
    for(string in; getline(cin, in), in.at(0) != '0'; cout << res << endl){
        stringstream ss;
        ss << in;
        
        bool one = false;
        res = 0;
        while(ss >> in){
            int cur = in.size() > 1 ? 10:in.at(0) - '0';
            if(cur == 1) one = true;

            res += cur;
        }
        if(one && res <= 11) res += 10;
        if(21 < res) res = 0;
    }

    return 0;
}
