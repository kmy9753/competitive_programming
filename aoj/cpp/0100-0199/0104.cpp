#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define X first
#define Y second

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const string p = "<v>^";
const int dx[4] = {-1,  0,  1,  0};
const int dy[4] = { 0,  1,  0, -1};

char m[101][101];

int main(void){
    map<char, pii> dir;
    rep(i, 4) dir[p.at(i)] = mp(dx[i], dy[i]);

    for(int h, w; cin >> h >> w, h;){
        rep(y, h) cin >> m[y];

        pii curP = mp(0, 0);
        char cur; int cnt = 0;
        while((cur = m[curP.Y][curP.X]) != '.'){
            curP.X += dir[cur].X;
            curP.Y += dir[cur].Y;

            if(w * h < ++cnt){
                cout << "LOOP" << endl;
                cnt = -1;
                break;
            }
        }
        if(~cnt) cout << curP.X << ' ' << curP.Y << endl;
    }
 
    return 0;
}
