#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 20;
int n, d[N], cost[N][N];
bool used[N];

void dijkstra(int);

int main(void){
    rep(i, N) fill(cost[i], cost[i] + N, INF);

    char sp;
    int m; cin >> n >> m;
    while(m--){
        int a, b, c1, c2; cin >> a >> sp >> b >> sp >> c1 >> sp >> c2;
        a--, b--;
        cost[a][b] = c1;
        cost[b][a] = c2;
    }
    int x1, x2, y1, y2; cin >> x1 >> sp >> x2 >> sp >> y1 >> sp >> y2;
    int res = y1 - y2;

    x1--, x2--;
    dijkstra(x1), res -= d[x2];
    dijkstra(x2), res -= d[x1];

    cout << res << endl;

    return 0;
}

void dijkstra(int s){
    fill(used, used + N, false);
    fill(d, d + N, INF);

    d[s] = 0;

    while(1){
        int v = -1;
        rep(u, n){
            if(!used[u] && (v == -1 || d[u] < d[v])) v = u;
        }
        if(v == -1) break;
        used[v] = true;

        rep(u, n){
            d[u] = min(d[u], d[v] + cost[v][u]);
        }
    }
}
