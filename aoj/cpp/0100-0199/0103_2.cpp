#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int H = 4;

int main(void){
    int n; cin >> n;

    while(n --){
        int out = 0;
        vi t(5);
        while(out != 3){
            t[0] = 1;
            string in; cin >> in;
            
            if(in == "OUT")
                out ++;
            else
                rep(i, 4){
                    int next = in == "HIT" ? 4 - i:H;
                    if(t[3 - i]) t[next] ++, t[3 - i] = 0;
                }
        }
        cout << t[H] << endl;
    }
 
    return 0;
}
