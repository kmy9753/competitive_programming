#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

int main(void){
    for(int n, cnt; cnt = -1, cin >> n, n; cout << endl){
        vi p(n); rep(i, n) cin >> p[i];
        vi pre;

        do{
            pre = p;
            rep(i, n) p[i] = count(ALL(pre), pre[i]);
            cnt ++;
        }
        while(p != pre);

        cout << cnt << endl;
        rep(i, n) cout << (i ? " ":"") << p[i];
    }
 
    return 0;
}
