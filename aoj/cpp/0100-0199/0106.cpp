#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

int main(void){
    for(int n, res; res = INF, cin >> n, n /= 100; cout << res << endl)
        for(int c = 0; c <= n; c += 5)
            for(int b = 0, a; (b + c) <= n; b += 3)
                if(!((a = n - b - c) % 2))
                    res = min(res, int((c / 5 * 0.88+ c / 5 % 3 * 0.12) * 850
                                     + (b / 3 * 0.85+ b / 3 % 4 * 0.15) * 550
                                     + (a / 2 * 0.8 + a / 2 % 5 * 0.20) * 380));

    return 0;
}
