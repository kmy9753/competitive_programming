#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const double BODER[4] = {1.1, 0.6, 0.2, 0.0};

int main(void){
    int res[4][2], cnt = 0;
    MEMSET(res, 0);
    
    for(double in; cin >> in; cnt ++)
        rep(i, 4) if(BODER[i] <= in){
            res[i][cnt % 2] ++;
            break;
        }

    rep(i, 4){ rep(j, 2) cout << (j ? " ":"") << res[i][j]; cout << endl; }
    
    return 0;
}
