#include <iostream>
#include <string>

using namespace std;

char META_CHARACTER[] = "z.?! ";

int main(void){
	for(string in, out; cin >> in; cout << out << endl){
		out.clear();
		if(in.length() % 2){ out = "NA"; continue; }

		for(int i = 0; i < in.length(); i += 2){
			int col = in.at(i) - '1';
			int row = in.at(i + 1) - '1';

			if(col < 0 || 5 < col ||
			   row < 0 || 4 < row){ out = "NA"; break; }

			if(col == 5){ out.push_back(META_CHARACTER[row]); }
			else{ out.push_back('a' + 5 * col + row); }
		}
	}
		
		
	return 0;
}
