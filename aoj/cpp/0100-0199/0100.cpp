#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(T) __typeof((T).begin())
#define each(T, it) for(ITER(T) it =(T).begin(); it!=(T).end(); it++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 4001;
const int B = (int)1e6;

int main(void){
    for(int n, f; cin >> n, n; cout << (f?"":"NA\n")){
        vector<ll> in(N);
        vi p;

        while(f = n --){
            int id; ll v, m; cin >> id >> v >> m;
            
            if((in[id] += (ll)v * m) >= B) in[id] = B;
            if(find(ALL(p), id) == p.end()) p.pb(id);
        }
        each(p, it) if(in[*it] == B) cout << *it << endl, f = 1;
    }
 
    return 0;
}
