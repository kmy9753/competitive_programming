#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
 
int main(void){
    for(int n; cin >> n, n;){
        vector<vi> res(n + 1);
        rep(y, n + 1) res[y].resize(n + 1);
        rep(y, n) rep(x, n) cin >> res[y][x];

        rep(y, n) rep(x, n) res[y][n] += res[y][x], res[n][x] += res[y][x];
        rep(y, n) res[n][n] += res[y][n];

        rep(y, n + 1){ rep(x, n + 1) printf("%5d", res[y][x]); cout << endl; }
    }
 
    return 0;
}
