#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 10000;

int main(void){
    bool isP[N + 1];
    MEMSET(isP, true);
    rep(i, 2) isP[i] = false;
    COUNT(i, N) if(isP[i]) for(int j = i; i * j <= N; j ++) isP[i * j] = false;

    vi res(N + 1);
    COUNT(i, N - 2) if(isP[i] & isP[i + 2]) fill(res.begin() + i + 2, res.end(), i + 2);
    
    for(int n; cin >> n, n; cout << res[n] - 2 << ' ' << res[n] << endl);
 
    return 0;
}
