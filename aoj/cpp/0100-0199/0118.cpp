#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define X first
#define Y second

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 101;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, -1, 0, 1};

int w, h;
char m[N][N];

void dfs(pii, char);

int main(void){
    for(int res; cin >> h >> w, h; cout << res << endl){
        rep(y, h) cin >> m[y];

        res = 0;
        rep(y, h) rep(x, w) if(m[y][x] != '.') dfs(mp(x, y), m[y][x]), res++;
    }

    return 0;
}

void dfs(pii p, char f){
    if(p.X < 0 || w <= p.X ||
       p.Y < 0 || h <= p.Y ||
       m[p.Y][p.X] != f) return;

    m[p.Y][p.X] = '.';

    rep(i, 4) dfs(mp(p.X + dx[i], p.Y + dy[i]), f);
}
