#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define S first
#define I second

using namespace std; 

typedef pair<int, int> pii;
typedef pair<string, int> psi;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
 
int main(void){
    vector<psi> t;
    for(psi in; cin >> in.S >> in.I; t.pb(in));
   
    sort(ALL(t));

    string pre;
    each(t, it){
        if(it->S != pre) cout << (it != t.begin() ? "\n":"") << it->S << endl;
        cout << (pre == it->S ? " ":"") << it->I;
        pre = it->S;
    }
    cout << endl;
 
    return 0;
}
