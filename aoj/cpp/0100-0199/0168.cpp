#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <utility>
#include <cstring>
#include <string>
#include <sstream>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int Y = 365;

int main(void){
    int dp[30];
    for(int n, cnt; cin >> n, n; cnt = dp[n - 1] / 10 + (!!dp[n - 1] % 10), cout <<  cnt / Y + (!!cnt % Y) << endl)
        rep(i, n) dp[i] = i < 3 ? 1 << i:dp[i - 3] + dp[i - 2] + dp[i - 1];

    return 0;
}
