#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;

class BaseballSimulation {
public:
	BaseballSimulation() {
		score = 0;
		for(int i = 0; i < BASE_NUM; i ++){
			exist_onBase.push_back(false);
		}
		outCount = 0;
	}
	void action(string str){
		/*
		typedef map<string, void(*)()> funcs_type;

		funcs_type funcs;
		funcs.insert(make_pair("HOMERUN", homerun));
		funcs.insert(make_pair("HIT", hit));
		funcs.insert(make_pair("OUT", out));
		
		funcs_type::iterator it = funcs.find(str);
		if(it != funcs.end()){
			it->second();
		}
		*/
		if(str == "HOMERUN") homerun();
		else if(str == "HIT") hit();
		else if(str == "OUT") out();
	}

	int getScore(void){
		return score;
	}
	int getOutCount(void){
		return outCount;
	}
private:
	static const int BASE_NUM = 3;
	int score;
	vector<bool> exist_onBase;
	int outCount;

	void out(void){
		outCount ++;
	}
	void hit(void){
		score += (exist_onBase.back()) ? 1:0;
		for(int i = BASE_NUM - 1; i >= 1; i --){
			exist_onBase[i] = exist_onBase[i-1];
		}
		exist_onBase[0] = true;
	}
	void homerun(void){
		score ++;
		for(int i = 0; i <BASE_NUM; i ++){
			score += (exist_onBase[i]) ? 1:0;
			exist_onBase[i] = false;
		}
	}
};

int main(void){
	int n; cin >> n;
	while(n -- != 0){
		BaseballSimulation baseballSim;
		while(baseballSim.getOutCount() != 3){
			string str; cin >> str;
			baseballSim.action(str);
		}
		cout << baseballSim.getScore() << endl;
	}
	return 0;
}
