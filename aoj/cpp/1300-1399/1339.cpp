#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 3;
int start[N][N], goal[N][N];

using State = tuple<int, string>;

int main(void){
    for(int cx, cy; cin >> cx >> cy, cx;){
        rep(i, N) rep(j, N) cin >> start[i][j];
        rep(i, N) rep(j, N) cin >>  goal[i][j];

        auto vec2s = [&](int hoge[N][N]){
            string ret;
            rep(i, N){
                rep(j, N){
                    ret += to_string(hoge[i][j]);
                }
            }
            return ret;
        };

        string s = vec2s(start);
        string z = vec2s(goal);

        priority_queue<State, vector<State>, greater<State>> q;
        q.push(State(0, s));

        unordered_map<string, int> min_cost;

        int res = -1;
        while(q.size()){
            int cost; string f;
            tie(cost, f) = q.top(), q.pop();

            if(min_cost.find(f) != end(min_cost)) continue;
            min_cost[f] = cost;

            if(f == z){
                res = cost;
                break;
            }

            int bx = -1, by = -1;
            rep(i, N * N){
                if(f[i] == '0'){
                    bx = i % 3, by = i / 3;
                    break;
                }
            }
            assert(bx != -1 and by != -1);

            rep(i, 4){
                int tx = bx + dx[i], ty = (by + dy[i] + N) % N;

                if(tx < 0){
                    tx = 2;
                    ty = (by + 2) % N;
                }
                else if(tx >= N){
                    tx = 0;
                    ty = (by + 1) % N;
                }

                assert(not (tx < 0 or N <= tx or ty < 0 or N <= ty));

                string nf = f;
                swap(nf[ty * 3 + tx], nf[by * 3 + bx]);
                int ncost = cost + (i % 2 == 0 ? cx : cy);
                if(min_cost.find(nf) == end(min_cost)){
                    q.push(State(ncost, nf));
                }
            }
        }

        cout << res << endl;
    }

    return 0;
}
