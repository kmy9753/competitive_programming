#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[8]={1,0,-1,0,1,1,-1,-1};
const int dy[8]={0,1,0,-1,1,-1,1,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

// (y, x, field)
using State = tuple<int, int, int>;

int n;

inline vector<vi> s2f(int fs){
    vector<vi> field(n, vi(n));

    rep(y, n){
        rep(x, n){
            if((fs >> (y * n + x)) & 1){
                field[y][x] = 1;
            }
        }
    }

    return field;
}

inline int f2s(vector<vi> field){
    int fs = 0;

    rep(y, n){
        rep(x, n){
            if(field[y][x]){
                fs |= (1 << (y * n + x));
            }
        }
    }

    return fs;
}

inline bool in(int y, int x){
    return 0 <= y and y < n and 0 <= x and x < n;
}

inline vector<vi> calc(vector<vi>& cur, int vy, int vx){
    vector<vi> next(n, vi(n));

    rep(y, n){
        rep(x, n){
            if(y == vy and x == vx) continue;

            int cnt = 0;
            rep(i, 8){
                int ny = y + dy[i], nx = x + dx[i];
                if(not in(ny, nx)) continue;
                cnt += ((cur[ny][nx] or (ny == vy and nx == vx)) ? 1 : 0); 
            }

            if(cur[y][x]){
                if(cnt == 2 or cnt == 3){
                    next[y][x] = 1;
                }
            }
            else {
                if(cnt == 3){
                    next[y][x] = 1;
                }
            }
        }
    }

    return next;
}

int main(void){
    for(; cin >> n, n;){
//        cerr << "hoge-------" << endl;
        vector<vi> sfield(n, vi(n));
        int sy, sx;
        rep(y, n){
            string in; cin >> in;
            rep(x, n){
                if(in[x] == '#'){
                    sfield[y][x] = 1;
                }
                if(in[x] == '@'){
                    sy = y;
                    sx = x;
                }
            }
        }
        int sfs = f2s(sfield);

        queue<State> q;
        State s = State(sy, sx, sfs);
        q.push(s);
        map<State, int> min_cost;
        min_cost[s] = 0;

        int res = -1;
        while(q.size()){
            int cost = min_cost[q.front()];
            int y, x, fs; tie(y, x, fs) = q.front(); q.pop();

            if(fs == 0){
                res = cost;
                break;
            }

            auto field = s2f(fs);

//            cerr << "-------" << endl;
//            cerr << cost << endl;
//            rep(yy, n){
//                rep(xx, n){
//                    if(yy == y and xx == x){
//                        cerr << "@ ";
//                    }
//                    else {
//                        cerr << (field[yy][xx] ? '#' : '.') << " ";
//                    }
//                }
//                cerr << endl;
//            }

            rep(i, 8){
                int ny = y + dy[i], nx = x + dx[i];
                if(not in(ny, nx) or field[ny][nx]) continue;

                auto nfield = calc(field, ny, nx);
                int nfs = f2s(nfield);

                State ns = State(ny, nx, nfs);
                if(min_cost.find(ns) == end(min_cost)){
                    min_cost[ns] = cost + 1;
                    q.push(ns);
                }
            }
        }

        cout << res << endl;
    }

    return 0;
}
