#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

string f;
int idx, len;

int number();
int factor();
int term();
int expression();

int number(){
    char c = f[idx++];
    if(c != '0' and c != '1') throw 1;

    if(c == '0') return 0;

    int ret = 1;
    while(idx < len and (f[idx] == '0' or f[idx] == '1')){
        ret *= 2;
        char c = f[idx++];
        if(c == '1') ret += 1;
    }
    return ret;
}

int factor(){
    int mcnt = 0;
    while(idx < len and f[idx] == '-'){
        mcnt++;
        idx++;
    }
    if(idx >= len) throw 1;

    int sign = (mcnt % 2 == 0 ? +1:-1);
    if(f[idx] != '(') return sign * number();
    idx++;

    int ret = sign * expression();
    if(idx >= len or f[idx] != ')') throw 1;
    idx++;

    return ret;
}

int term(){
    int ret = factor();
    while(idx < len and f[idx] == '*'){
        idx++;
        int rhs = factor();
        ret *= rhs;
    }
    return ret;
}

int expression(){
    int ret = term();
    while(idx < len and (f[idx] == '+' or f[idx] == '-')){
        char op = f[idx++];
        int rhs = term();
        switch(op){
            case '+': { ret += rhs; break; }
            case '-': { ret -= rhs; break; }
        }
    }
    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    /*
    while(cin >> f){
        len = f.size(), idx = 0;
        int ret = expression();
        cerr << ret << ", idx = " << idx << ", len = " << len << endl;
    }
    return 0;
    //*/

    string in; cin >> in;
    string T = "01+-*()="; sort(_all(T));

    set<string> s;
    do {
        map<char, bool> used;
        map<char, char> c2t;
        int j = 0;
        f = in;
        rep(i, in.size()){
            char c = in[i];
            if(not isupper(c) and not islower(c)) continue;

            if(not used[c]){
                used[c] = true;
                if(j >= T.size()){
                    cout << 0 << endl;
                    return 0;
                }
                c2t[c] = T[j++];
            }
            f[i] = c2t[c];
        }

        string equation = f;
        string l, r;
        rep(i, in.size()){
            if(f[i] == '='){
                l = f.substr(0, i);
                r = f.substr(i + 1);
                break;
            }
        }
        if(l == "" or r == "") continue;

        try {
            idx = 0; f = l; len = f.size();
            int lv = expression();
            if(idx != len) continue;

            idx = 0; f = r; len = f.size();
            int rv = expression();
            if(idx != len) continue;

            if(lv != rv) continue;
        }
        catch(int e){
            continue;
        }
        s.insert(equation);
    } while(next_permutation(_all(T)));

    // for(auto& e : s){
    //     cerr << e << endl;
    // }
    cout << s.size() << endl;

    return 0;
}
