#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 53;
const int offset = 1;

bool graph[4][N][N];

int main(void){
    int n, sx, sy, T; cin >> n >> sx >> sy >> T;
    sx += offset, sy += offset;
    rep(loop, n){
        int x1, y1, x2, y2; cin >> x1 >> y1 >> x2 >> y2;
        x1 += offset, x2 += offset;
        y1 += offset, y2 += offset;

        int dir;
        if(x1 == x2){
            if(y1 > y2) swap(y1, y2);
            dir = 1;
            rep(y, y1, y2) graph[dir][y][x1] = true;
            rep(y, y1 + 1, y2 + 1) graph[(dir + 2) % 4][y][x1] = true;
        }
        else {
            if(x1 > x2) swap(x1, x2);
            dir = 0;
            rep(x, x1, x2) graph[dir][y1][x] = true;
            rep(x, x1 + 1, x2 + 1) graph[(dir + 2) % 4][y1][x] = true;
        }
    }
    vi dists(T), dirs(T);
    rep(t, T){
        int d; char c; cin >> d >> c;
        dists[t] = d;
        switch(c){
            case 'E': { dirs[t] = 0; break; };
            case 'N': { dirs[t] = 1; break; };
            case 'W': { dirs[t] = 2; break; };
            case 'S': { dirs[t] = 3; break; };
        }
    }

    vector<vector<vi>> emp(4, vector<vi>(N, vi(N)));
    auto cur = emp;
    rep(i, 4){
        if(not graph[i][sy][sx]) continue;
        cur[i][sy][sx] = true;
    }

    int ct = 0, dd = 0;
    while(1){
//        cerr << "---" << endl;
        auto next = emp;
        auto predir = emp;

        rep(i, 4){
            rep(y, N){
                rep(x, N){
                    if(not cur[i][y][x] or not graph[i][y][x]) continue;
//                    cerr << i << ", (" << x - offset << ", " << y - offset << ")" << endl;

                    int ny = y + dy[i], nx = x + dx[i];

                    next[i][ny][nx] = true;
                    rep(ni, 4){
                        if(ni == (i + 2) % 4 or not graph[ni][ny][nx]) continue;
                        next[ni][ny][nx] = true;
//                    cerr << ">> " << ni << ", (" << nx - offset << ", " << ny - offset << ")" << endl;
                        predir[ni][ny][nx] |= (1 << i);
                    }
                }
            }
        }
        cur = next;

        dd++;
        if(dd == dists[ct]){
            if(ct + 1 == T) break;

            int dir = dirs[ct];
            rep(i, 4){
                rep(y, N){
                    rep(x, N){
                        if(i == dir or (predir[i][y][x] & (1 << dir))) continue;
                        cur[i][y][x] = false;
                    }
                }
            }
            dd = 0;
            ct++;
        }
    }

    using P = tuple<int, int>;
    set<P> s;
    rep(y, N){
        rep(x, N){
            if(cur[dirs.back()][y][x]){
                s.insert(P(x - offset, y - offset));
            }
        }
    }
    for(auto & e : s){
        int x, y; tie(x, y) = e;
        cout << x << " " << y << endl;
    }

    return 0;
}
