#include <bits/stdc++.h>

#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)

#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)

#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))

// #define DEBUG

#ifdef DEBUG
#define dump(...) fprintf(stderr, __VA_ARGS__)
#else
#define dump(...)
#endif

template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}

using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;

const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};


ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}

random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int p, r, T;
vector<vi> num;
vi l;
vi in_P, in_R;

bool check(int border){
    int cnt = 0;
    vi ended(p);
    vi rest_n = l;
    vector<vi> use_r(p, vi(r));
    rep(t, border){
        int P = in_P[t], R = in_R[t];

        rest_n[R]--;
        use_r[P][R]++;

        bool ok = true;
        rep(i, r){
            ok &= (use_r[P][i] == num[P][i]);
        }

        // release
        if(ok){
            rep(i, r){
                rest_n[i] += num[P][i];
            }
            ended[P] = true; cnt++;
        }
    }

    /*
    cerr << "-----" << endl;
    cerr << "border = " << border << endl;
    cerr << "rest_n: ";
    for(auto& e : rest_n) cerr << e << " "; cerr << endl;

    cerr << "use_r: " << endl;
    rep(i, p){
        rep(j, r){
            cerr << use_r[i][j] << " ";
        }
        cerr << endl;
    }
    //*/

    while(true){
        int pcnt = cnt;
        rep(i, p){
            if(ended[i]) continue;

            bool ok = true;
            rep(j, r){
                if(rest_n[j] < num[i][j] - use_r[i][j]){
                    ok = false;
                }
            }

            // release
            if(ok){
                rep(j, r){
                    rest_n[j] += use_r[i][j];
                }
                ended[i] = true; cnt++;
            }
        }
        if(pcnt == cnt) break;
    }

    return cnt == p;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> p >> r >> T;
    l = vi(r); for(auto& e : l) cin >> e;
    num = vector<vi>(p, vi(r));
    rep(i, p) rep(j, r) cin >> num[i][j];

    in_P = vi(T), in_R = vi(T);
    rep(t, T) cin >> in_P[t] >> in_R[t], in_P[t]--, in_R[t]--;

    int lb = 0, ub = T + 1;
    bool deadlock = false;
    while(ub - lb > 1){
        int mid = (lb + ub) / 2;

        if(check(mid)){
            lb = mid;
        }
        else {
            deadlock = true;
            ub = mid;
        }
    }

    if(deadlock){
        cout << ub << endl;
    }
    else {
        cout << -1 << endl;
    }

    return 0;
}
