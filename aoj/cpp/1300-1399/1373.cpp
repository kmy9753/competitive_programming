#include <bits/stdc++.h>

#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)

#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)

#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))

// #define DEBUG

#ifdef DEBUG
#define dump(...) fprintf(stderr, __VA_ARGS__)
#else
#define dump(...)
#endif

template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}

using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;

const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};


ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}

random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

template <typename T>
class Bit {
    public:
        Bit(int size) : size_(size) {
            data_.resize(size + 1);
        }

        // 0-indexed
        void add(int index, T value) {
            if (index < 0 || index >= size_) return;
            ++index;
            while (index <= size_) {
                data_[index] += value;
                index += index & -index;
            }
        }

        // [0, index)
        T sum(int index) {
            index--;
            if (index < 0 || index >= size_) return 0;
            ++index;
            T ret = 0;
            while (index > 0) {
                ret += data_[index];
                index -= index & -index;
            }
            return ret;
        }

        // [l, r)
        T sum(int l, int r){
            return sum(r) - sum(l);
        }

    private:
        std::vector<T> data_;
        int size_;
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    const int M = 1e6;
    vi x(n); for(auto& e : x) cin >> e;

    Bit<int> bit(M);
    bool has_big = false;
    for(auto& e : x){
        bool ok = false;

        if(bit.sum(1) == 1); // full
        else if(e >= M){
            ok = true;
            has_big = true;
        }
        else if(bit.sum(e, e + 1) == 0){
            ok = true;
            bit.add(e, 1);
        }
        else { // already 1
            if(bit.sum(e) <= e - 1){
                if(bit.sum(e) != e - 1 or (bit.sum(e + 1, M) == 0 and not has_big)){
                    ok = true;

                    int i = e;
                    while(bit.sum(i, i + 1) == 1){
                        bit.add(i, -1);
                        i--;
                    }
                    bit.add(i, +1);
                }
            }
        }

        cout << (ok ? "Yes":"No") << endl;
    }

    return 0;
}
