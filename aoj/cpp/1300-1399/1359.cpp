#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using In = tuple<int, int, char>;
using BA = tuple<int, int>;
using P = tuple<int, int>;

int n, w, h;

int calc(P p){
    int x, y;
    tie(x, y) = p;

    int xx = 0;
    if(x == 0){
        xx = (h - y);
    }
    else if(y == 0){
        xx = h + x;
    }
    else if(x == w - 1){
        xx = h + w + y;
    }
    else {
        xx = h + w + h + (w - x);
    }

    return xx;
}

int main(void){
    cin >> n >> w >> h;

    vector<In> in(n);
    for(auto & e : in){
        int x, y; char c; cin >> x >> y >> c;
        e = In(x, y, c);
    }

    vector<BA> ba;

    const int L = 2 * w + 2 * h;
    for(auto & e : in){
        int x, y; char f;
        tie(x, y, f) = e;

        vi idxs = {
            (y - x + L) % L,
            (y + x + L) % L,
            (w + h + (h - (y + (w - x))) + L) % L,
            (w + h + (h - (y - (w - x))) + L) % L
        };

        int a, b;
        switch(f){
            case 'S': { a = idxs[3], b = idxs[0]; break; }
            case 'E': { a = idxs[2], b = idxs[3]; break; }
            case 'N': { a = idxs[1], b = idxs[2]; break; }
            case 'W': { a = idxs[0], b = idxs[1]; break; }
        }
        
        ba.push_back(BA(b, a));
    }

    int res = n;
    rep(rev, 2){
        if(rev){
            rep(i, n){
                int a, b; tie(b, a) = ba[i];
                b = L - b;
                a = L - a;
                swap(a, b);
                ba[i] = BA(b, a);
            }
        }
        auto ba_origin = ba;

        rep(tar, n){
            int base; tie(ignore, base) = ba[tar];
            rep(i, n){
                int a, b; tie(b, a) = ba[i];
                a = (a - base + L) % L;
                b = (b - base + L) % L;
                if(a > b) a = 0;
                ba[i] = BA(b, a);
            }

            sort(_all(ba));

            int ri = -1;
            int cnt = 0;
            rep(i, n){
                int a, b; tie(b, a) = ba[i];
                if(cnt == 0 or a > ri){
                    cnt++;
                    ri = b;
                }
            }

            chmin(res, cnt);

            ba = ba_origin;
        }
    }

    cout << res << endl;

    return 0;
}
