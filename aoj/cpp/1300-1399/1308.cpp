#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using Vec = vector<int>;
using Mat = vector<Vec>;

// $B%,%&%9$N>C5nK!$GO"N)<0$r$H$/(B
bool gauss_jordan(const Mat & A, const Vec & b, Vec & res){
    int n = A.size();
    Mat B(n, Vec(n + 1));
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            B[i][j] = A[i][j];
    for(int i = 0; i < n; i++)
        B[i][n] = b[i];

    // $B3FJQ?t$KCmL\(B
    for(int i = 0; i < n; i++){

        // $B:GBg$N78?t$N@dBPCM$r$b$D$b$N$r(Bi$BHVL\$K;}$C$F$/$k(B
        int piv = -1;
        int val = -1;
        for(int j = i; j < n; j++){
            if(val < abs(B[j][i])){
                piv = j;
                val = abs(B[j][i]);
            }
        }

        // $B1&JU$,(B0$B$K$J$k$+$I$&$+D4$Y$k(B
        if(piv == -1 || val <= 0)
            continue;
        swap(B[piv], B[i]);
        for(int j = 0; j < n; j++){
            if(i == j) continue;
            int mulnum = B[j][i];
            if(mulnum != 0){
                for(int k = i; k <= n; k++)
                    B[j][k] = (B[j][k] + B[i][k]) % 2;
            }
        }
    }
    res = Vec(n);

    // $B$b$7:8JU$,(B0$B$K$J$C$F$$$k<0$,$"$l$P!"1&JU$b(B0$B$K$J$C$F$$$k$+D4$Y$k(B
    // $B$J$C$F$$$J$1$l$P!"2r$OB8:_$7$J$$(B
    // $B1&JU$b(B0$B$J$i!"B>$N:8JU$,(B0$B$N<0$b1&JU$,(B0$B$K$J$k$H$-$K8B$j!"J#?t2rB8:_(B
    // $B$=$NB>$N>l9g$O!"2r$O0l$D$@$1B8:_(B
    for(int i = 0; i < n; i++){
        res[i] = B[i][n];
        if(B[i][i] == 0){
            if(B[i][n] != 0) return false;
        }
    }
    return true;
}

int main(void){
    for(int w, h, d; cin >> w >> h >> d, w;){
        vector<vi> field(h, vi(w));
        rep(i, h) rep(j, w) cin >> field[i][j];

        Mat a(w * h, Vec(w * h));
        Vec b(w * h);
        rep(i, w * h){
            int y = i / w, x = i % w;
            b[i] = field[y][x];

            a[i][i] = 1;
            rep(ni, w * h){
                int ny = ni / w, nx = ni % w;
                if(abs(y - ny) + abs(x - nx) == d){
                    a[i][ni] = 1;
                }
            }
        }

        Vec c;
        cout << (gauss_jordan(a, b, c) ? 1:0) << endl;
    }

    return 0;
}
