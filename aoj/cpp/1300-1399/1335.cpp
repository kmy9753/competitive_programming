#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    for(int n, k, s; cin >> n >> k >> s, n;){
        vector<vector<int>> dp(k + 1, vector<int>(s + 1));
        dp[0][0] = 1;
        range(i, 1, n + 1){
            for(int j = k; j > 0; j--){
                for(int l = s; l >= i; l--){
                    dp[j][l] += dp[j - 1][l - i];
                }
            }
        }

        cout << dp[k][s] << endl;
    }

    return 0;
}
