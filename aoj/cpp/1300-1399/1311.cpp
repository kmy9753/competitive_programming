#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using State = tuple<int, int, int>;
using Edge  = tuple<int, int>;

constexpr int inf = 1 << 29;

int main(void){
    for(int n, m, C; cin >> n >> m >> C, n;){
        vector<vector<Edge>> edge(n);
        rep(_, m){
            int a, b, c; cin >> a >> b >> c;
            a--, b--;
            edge[a].push_back(Edge(b, c));
        }

        priority_queue<State, vector<State>, greater<State>> q;
        q.push(State(0, 0, 0));

        vector<vector<int>> minCost(n, vector<int>(n, inf));

        while(q.size()){
            int cost, v, num_zero;
            tie(cost, v, num_zero) = q.top();
            q.pop();

            if(minCost[v][num_zero] != inf){
                continue;
            }
            minCost[v][num_zero] = cost;

            for(auto & e : edge[v]){
                int nv, diff_cost;
                tie(nv, diff_cost) = e;

                if(minCost[nv][num_zero] == inf){
                    q.push(State(cost + diff_cost, nv, num_zero));
                }
                if(num_zero + 1 < n and minCost[nv][num_zero + 1] == inf){
                    q.push(State(cost, nv, num_zero + 1));
                }
            }
        }

        int res;
        rep(i, n){
            if(minCost[n - 1][i] <= C){
                res = i;
                break;
            }
        }

        cout << res << endl;
    }

    return 0;
}
