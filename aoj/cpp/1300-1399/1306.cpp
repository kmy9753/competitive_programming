#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int N = 4;

int main(void){
    for(int n; cin >> n, n;){
        vvi dp(n + 1, vi(N, inf));
        dp[0][0] = 0;

        int prep = 0, pret = 0;
        int stop = inf;

        vector<pii> in(n);
        rep(i, n){
            cin >> in[i].first >> in[i].second;
        }

        bool ok;
        range(i, 1, n + 1){
            int p = in[i - 1].first, t = in[i - 1].second;

            rep(j, N){
                int dist = max(p, prep) - min(p, prep);
                if(j + 1 < N && dist * (j + 1) <= (t - pret)){
                    dp[i][j + 1] = min(dp[i][j + 1], dp[i - 1][j] + dist);
                }
                if((double)prep * (j + 1) + p <= (t - pret)){
                    dp[i][1] = min(dp[i][1], dp[i - 1][j] + prep + p);
                }
            }

            ok = false;
            rep(j, N){
                if(dp[i][j] < inf) ok = true;
            }
            if(!ok){
                cout << "NG " << i << endl;
                break;
            }
            prep = p; pret = t;
        }
        int res = *min_element(all(dp[n])) + prep;
        if(ok) cout << "OK " << res << endl;
    }

	return 0;
}
