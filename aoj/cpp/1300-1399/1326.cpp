#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;
#define round first
#define curly second.first
#define square second.second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int N = 21;

int main(void){
    for(int p, q; cin >> p >> q, p;){
        vi indent(p);
        vs P(p), Q(q);

        rep(i, p){
            cin >> P[i];

            indent[i] = 0;
            while(indent[i] < P[i].size() && P[i].at(indent[i]) == '.') indent[i]++;
        }

        rep(i, q) cin >> Q[i];

        vi ans(q, -2);

        range(R, 1, N){
            range(C, 1, N){
                range(S, 1, N){
                    int r = 0, c = 0, s = 0;
                    bool ok = true;

                    rep(i, p){
                        if(R * r + C * c + S * s != indent[i]){
                            ok = false;
                        }

                        rep(j, P[i].size()){
                            switch(P[i].at(j)){
                                case '(': r++; break;
                                case '{': c++; break;
                                case '[': s++; break;
                                case ')': r--; break;
                                case '}': c--; break;
                                case ']': s--; break;
                            }
                        }
                    }
                    if(!ok) continue;

                    r = c = s = 0;
                    rep(i, q){
                        int ind = R * r + C * c + S * s;
                        if(ans[i] == -2) ans[i] = ind;
                        else if(ans[i] != ind) ans[i] = -1;

                        rep(j, Q[i].size()){
                            switch(Q[i].at(j)){
                                case '(': r++; break;
                                case '{': c++; break;
                                case '[': s++; break;
                                case ')': r--; break;
                                case '}': c--; break;
                                case ']': s--; break;
                            }
                        }
                    }
                }
            }
        }
        rep(i, q){
            cout << (i ? " ":"") << ans[i];
        }
        cout << endl;
    }

	return 0;
}
