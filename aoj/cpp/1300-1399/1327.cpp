#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int MOD, n;
vi S;

void maltiple(vvi& mat){
    vi nextS(n);

    rep(i, n){
        rep(j, n){
             (nextS[i] += mat[i][j] * S[j]) %= MOD;
        }
    }

    S = nextS;
        //pow(mat);
}

void mul(vvi& a, vvi& b){
    vvi res(n, vi(n));

    rep(i, n){
        rep(j, n){
            rep(k, n){
                (res[i][j] += a[i][k] * b[k][j]) %= MOD;
            }
        }
    }

    a = res;
}

int main(void){
    for(int a, b, c, t; cin >> n >> MOD >> a >> b >> c >> t, n;){
        S = vi(n, 0);

        rep(i, n){
            cin >> S[i];
        }

        vvi mat(n, vi(n, 0));
        rep(i, n){
            mat[i][i] = b;
            if(i != n - 1){
                mat[i + 1][i] = a;
                mat[i][i + 1] = c;
            }
        }
        vvi D;

        bool ok = t != 0;
        rep(i, 30){
            bool run = t & 1;
            t >>= 1;

            if(run){
                if(D.size() == 0) D = mat;
                else mul(D, mat);
            }

            mul(mat, mat);
            if(t == 0) break;
        }
        if(ok) maltiple(D);

        rep(i, n) cout << (i ? " ":"") << S[i];
        cout << endl;
    }

	return 0;
}
