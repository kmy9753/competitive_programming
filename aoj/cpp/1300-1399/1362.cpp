#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

ll K;
string in, rev;
int n, optlen;
vector<vi> dp1;
vector<vll> dp2;

ll addOcc(ll a, ll b){
    return min(K, a + b);
}

ll rec(int r, int c){
    ll& ret = dp2[r][c];
    if(ret != -1){
        return ret;
    }

    int len = dp1[r][c];
    ret = 0;
    if(r < n and c < n and in[r] == rev[c]){
        assert(dp1[r + 1][c + 1] == len + 1);
        ret = rec(r + 1, c + 1);
    }
    else {
        if(r < n and dp1[r + 1][c] == len + 1){
            ret = addOcc(ret, rec(r + 1, c));
        }
        if(c < n and dp1[r][c + 1] == len + 1){
            ret = addOcc(ret, rec(r, c + 1));
        }
    }

    if(len >= (optlen + 1) / 2){
        chmin(ret, 1LL);
    }

    return ret;
}

string res;
int cnt = 0;
void explore(int r, int c, ll rest){
    if(cnt == (optlen + 1) / 2) return;
    if(cnt != dp1[r][c]){
        cerr << res << endl;
        cerr << in.substr(r - 1, 3) << ", " << rev.substr(c - 1, 3) << endl;
        assert(false);
    }
    cnt++;

    assert(rest >= 1LL);

    // stringstream ss; ss << (min(rest, 99LL)); res += ss.str();

    int len = dp1[r][c];

    // cerr << res << endl;
    if(in[r] == rev[c]){
        assert(dp1[r + 1][c + 1] == len + 1);
        assert(dp2[r][c] == dp2[r + 1][c + 1]);
        res += in[r];
        explore(r + 1, c + 1, rest);
        return;
    }
    
    if(r < n and dp1[r + 1][c] != len + 1) dp2[r + 1][c] = 0;
    if(c < n and dp1[r][c + 1] != len + 1) dp2[r][c + 1] = 0;
    // assert(dp2[r][c] == addOcc(dp2[r + 1][c], dp2[r][c + 1]));
    /*
    if(rest == 1){
        if(r < n and dp2[r + 1][c] >= 1){
            res += in[r];
            explore(r + 1, c, rest);
        }
        else {
            res += rev[c];
            explore(r, c + 1, rest);
        }
        return;
    }
    */

    if(in[r] < rev[c]){
        if(dp2[r + 1][c] >= rest){
            res += in[r];
            explore(r + 1, c, rest);
        }
        else {
            res += rev[c];
            explore(r, c + 1, rest - dp2[r + 1][c]);
        }
    }
    else {
        if(dp2[r][c + 1] >= rest){
            res += rev[c];
            explore(r, c + 1, rest);
        }
        else {
            res += in[r];
            explore(r + 1, c, rest - dp2[r][c + 1]);
        }
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> in;
    cin >> K;
    n = (int)in.size();
    rev = in; reverse(_all(rev));

    dp1 = vector<vi>(n + 1, vi(n + 1));
    rep(i, n + 1) dp1[i][0] = dp1[0][i] = i;
    rep(i, 1, n + 1){
        rep(j, 1, n + 1){
            int& cur = dp1[i][j];
            if(in[i - 1] == rev[j - 1]){
                cur = dp1[i - 1][j - 1] + 1;
            }
            else {
                cur = min(dp1[i - 1][j] + 1, dp1[i][j - 1] + 1);
            }
        }
    }
    optlen = dp1[n][n];

    /*
    cerr << "dp1: " << endl;
    rep(i, n + 1){
        rep(j, n + 1){
            cerr << dp1[i][j] << " ";
        }
        cerr << endl;
    }
    cerr << endl;
    */

    dp2 = vector<vll>(n + 1, vll(n + 1, -1));
    dp2[n][n] = 1;
    rec(0, 0);
    rep(i, n + 1) rep(j, n + 1) if(dp2[i][j] == -1) dp2[i][j] = 0;

    /*
    cerr << "dp2: " << endl;
    rep(i, n + 1){
        rep(j, n + 1){
            cerr << dp2[i][j] << " ";
        }
        cerr << endl;
    }
    cerr << endl;
    //*/

    if(dp2[0][0] < K){
        cout << "NONE" << endl;
        return 0;
    }
    explore(0, 0, K);

    string rres = res;
    reverse(_all(rres));

    if(optlen % 2 == 1){
        cout << res + rres.substr(1) << endl;
    }
    else {
        cout << res + rres << endl;
    }

    return 0;
}
