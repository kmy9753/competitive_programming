#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

map<char, int> dic;
vector<vi> matched;

inline vector<vi> b2f(int w, int h, vector<string>& in){
    vector<vi> field(h, vi(w));
    rep(y, h){
        rep(x, w){
            int val = dic[in[y][x / 6]] % (1 << (6 - x % 6));
            field[y][x] = val / (1 << (5 - x % 6));
        }
    }

    return field;
}

void match(vector<vll>& field, ll hash, int p){
    int w = field[0].size(), h = field.size();

    rep(y, h - p + 1){
        rep(x, w - p + 1){
            matched[y][x] |= (field[y][x] == hash);
        }
    }
}

vector<vll> calc_hash(vector<vi>& field, int w, int h, int p){
    
    vector<vll> hash(h, vll(w));
    vector<vll> tmp(h, vll(w));

    // --
    const ll B1 = 1007, B2 = 1009, mod1 = 1e9+7, mod2 = 1e9+7;

    ll t1 = 1; rep(i, p) (t1 *= B1) %= mod1;
    rep(y, h){
        ll e = 0;
        rep(x, p) e = (e * B1 + field[y][x]) % mod1;

        rep(x, w - p + 1){
            tmp[y][x] = e;
            if(x + p < w){
                e = (e * B1 - t1 * field[y][x] + field[y][x + p] + t1 * mod1) % mod1;
                assert(e * B1 - t1 * field[y][x] + field[y][x + p] + t1 * mod1 >= 0);
            }
        }
    }

    // |
    ll t2 = 1; rep(i, p) (t2 *= B2) %= mod2;
    rep(x, w - p + 1){
        ll e = 0;
        rep(y, p) e = (e * B2 + tmp[y][x]) % mod2;

        rep(y, h - p + 1){
            hash[y][x] = e;
            if(y + p < h){
                assert(e * B2 - t2 * tmp[y][x] + tmp[y + p][x] + t2 * mod2 >= 0);
                e = (e * B2 - t2 * tmp[y][x] + tmp[y + p][x] + t2 * mod2) % mod2;
            }
        }
    }

    return hash;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    {
        const int M = 'z' - 'a' + 1;
        int idx = 0;
        rep(i, M) dic[(char)('A' + i)] = idx++;
        rep(i, M) dic[(char)('a' + i)] = idx++;
        rep(i, 10) dic[(char)('0' + i)] = idx++;
        dic['+'] = idx++, dic['/'] = idx++;
        assert(idx == 64);
    }

    for(int w, h, p; cin >> w >> h >> p, w;){
        vector<string> in(h);
        for(auto& e : in) cin >> e;
        vector<string> in_pat(p);
        for(auto& e : in_pat) cin >> e;

        if(w < p or h < p){
            cout << 0 << endl;
            continue;
        }

        auto field = b2f(w, h, in);
        auto hash_field = calc_hash(field, w, h, p);
        auto pattern = b2f(p, p, in_pat);

        // rep(y, h){
        //     rep(x, w){
        //         cerr << field[y][x] << " ";
        //     }
        //     cerr << endl;
        // }
        // cerr << endl;

        // rep(y, p){
        //     rep(x, p){
        //         cerr << pattern[y][x] << " ";
        //     }
        //     cerr << endl;
        // }
        // cerr << "----" << endl;

        matched = vector<vi>(h, vi(w));
        rep(i, 2){
            rep(j, 4){
                vector<vi> next_p(p, vi(p));
                rep(y, p){
                    rep(x, p){
                        next_p[p - x - 1][y] = pattern[y][x];
                    }
                }
                pattern = next_p;

                // rep(y, p){
                //     rep(x, p){
                //         cerr << pattern[y][x] << " ";
                //     }
                //     cerr << endl;
                // }
                // cerr << endl;

                ll hash = calc_hash(pattern, p, p, p)[0][0];
                match(hash_field, hash, p);
            }
            rep(y, p) reverse(_all(pattern[y]));
        }

        int res = 0;
        rep(y, h - p + 1){
            rep(x, w - p + 1){
                res += (matched[y][x] ? 1:0);
            }
        }

        cout << res << endl;
    }

    return 0;
}
