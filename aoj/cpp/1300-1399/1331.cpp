#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=int(a);i<int(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=int(a)-1;i>=int(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(arg),key)-begin(arg)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
using namespace std;
 
template<class T>void reg(vector<T> &ary,const T &elem){ary.emplace_back(elem);}
template<class T>bool chmin(T &a, const T &b) {return (b<a)?(a=b,1):0;}
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
 
using R=long double; // __float128
const R EPS = 1E-11; // [-1000:1000]->EPS=1e-8 [-10000:10000]->EPS=1e-7
const R INF = 1E40;
constexpr R PI = acos(R(-1));
inline int sgn(const R& r){ return (r > EPS) - (r < -EPS);}
inline int sgn(const R& a, const R &b){ return sgn(a-b); }
inline R sq(R x){return sqrt(max<R>(x,0.0));}
 
struct P{
    R x, y, z;
    P():x(0),y(0),z(0){}
    P(R x, R y, R z):x(x),y(y),z(z){}
    P operator+(const P &p) const{ return P(x+p.x, y+p.y, z+p.z);}
    P operator-(const P &p) const{ return P(x-p.x, y-p.y, z-p.z);}
    P operator*(const R &d) const{ return P(x*d, y*d, z*d);}
    // P operator/(const R &d) const{ return P(x/d, y/d, z/d);}
};

using VP=vector<P>;
using L=struct{P s,t;};
using VL=vector<L>;
using C=struct{P c;R r;};
using VC=vector<C>;
 
const P O = P(0,0,0);
istream& operator >> (istream& is,P& p){ R x,y,z;is >> x >> y >> z; p=P(x,y,z); return is;}
ostream& operator << (ostream& os,P& p){ os << p.x << " " << p.y << " " << p.z; return os;}
 
namespace std{
    // bool operator <  (const P& a,const P& b){ return sgn((a-b).x)?real(a-b)<0:sgn(imag(a-b))<0;}
    // bool operator == (const P& a,const P& b){ return sgn(real(a-b))==0 && sgn(imag(a-b))==0;}
}
 
// bool operator <  (const L& a,const L& b){ return a.s==b.s?a.t<b.t:a.s<b.s;}
// bool operator == (const L& a,const L& b){ return a.s==b.s&&a.t==b.t;}
 
//内積 dot 外積 det
inline R dot(P o,P a,P b){a=a-o,b=b-o; return a.x * b.x + a.y * b.y + a.z * b.z;}
inline R norm(P a){return dot(O, a, a);}
inline R abs(P a){return sqrt(norm(a));}
inline R det(P o,P a,P b){a=a-o,b=b-o; return abs(P(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x));}
// inline P vec(L l){return l.t-l.s;}

// bool operator <  (const C& a,const C& b){ return a.c==b.c?sgn(a.r-b.r)<0:a.c<b.c;}
// bool operator == (const C& a,const C& b){ return a.c==b.c&&sgn(a.r-b.r)==0;}
 
inline R dsp(L s,P p){
    if(sgn(dot(s.s,s.t,p))<=0) return abs(p-s.s);
    if(sgn(dot(s.t,s.s,p))<=0) return abs(p-s.t);
    return abs(det(s.s,s.t,p))/abs(s.t-s.s);
}

inline P proj(const L &l, const P &p){
    R t = dot(O, p - l.s, l.s - l.t) / norm(l.s - l.t);
    return l.s + (l.s - l.t) * t;
}
inline bool ics(const C &c, const L &l){
    P a = l.s - c.c, b = l.t - c.c, p = proj(l, c.c) - c.c;
    if(abs(a) < c.r - EPS and abs(b) < c.r - EPS and abs(p) < c.r - EPS) return 0; // contain
    if(abs(p) > c.r + EPS) return 0; // far
    if(abs(a) < c.r - EPS or abs(b) < c.r - EPS) return 1; // intersect
    if(abs(a - p) + abs(b - p) < abs(a - b) + EPS) return 1; // intersect
    return 0;
}

VC balloons;
VP lights;
vector<R> scores;
P goal;

int n, m, r_num;

int hit[2000][15];

// pat... 1: used
R calc(int pat){
    R ret = 0;
    int cnt = 0;
    rep(j, n){
        rep(i, m){
            if(not ((pat >> i) & 1)) continue;

            if(hit[j][i]){
                cnt++;
                break;
            }
        }
        if(cnt > r_num) return -1;
    }
    rep(i, m){
        if(not ((pat >> i) & 1)) continue;
        ret += scores[i] / norm(lights[i] - goal);
    }

    return ret;
}

int main(void){
    for(; cin >> n >> m >> r_num, n;){
        balloons = VC(n); for(auto& e : balloons){ R r; P c; cin >> c >> r; e = {c, r}; }
        scores = vector<R>(m); lights = VP(m);
        rep(i, m){ cin >> lights[i] >> scores[i]; }
        cin >> goal;

        rep(i, m){
            L seg = {lights[i], goal};
            rep(j, n){
                hit[j][i] = false;
                if(ics(balloons[j], seg)){
                    hit[j][i] = true;
                }
            }
        }

        R res = 0;
        rep(i, 1 << m){
            chmax(res, calc(i));
        }

        cout.precision(20);
        cout << fixed << res << endl;
    }

    return 0;
}

