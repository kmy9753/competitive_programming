#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n; cin >> n, n;){
        vs str(n);
        rep(i, n) cin >> str[i];

        set<int> del;
        rep(i, n){
            rep(j, n){
                if(i == j || str[j].size() < str[i].size()) continue;

                rep(k, str[j].size() - str[i].size()){
                    if(str[j].substr(k, str[i].size()) == str[i]){
                        del.insert(i);
                    }
                }
            }
        }
        for(auto itr = del.rbegin(); itr != del.rend(); itr++){
            str.erase(str.begin() + *itr);
        }
        n -= del.size();

        vector<vs> sub(n, vs(n));
        rep(i, n){
            rep(j, n){
                if(i == j) continue;

                sub[i][j] = str[j];
                for(int k = min(str[i].size(), str[j].size()); k > 0; k--){
                    if(str[j].substr(0, k) != str[i].substr(str[i].size() - k, k)) continue;

                    sub[i][j].erase(0, k);
                    break;
                }
            }
        }


        vector<vs> dp(1 << n, vs(n));

        rep(i, 1 << n){
            rep(j, n){
                if(dp[i][j] == "" && i != 0) continue;

                rep(k, n){
                    if(i & (1 << k)) continue;
                    int next_idx = i | (1 << k);

                    string next_str = dp[i][j] + (i == 0 ? str[k]:sub[j][k]);

                    if(dp[next_idx][k] == ""){
                        dp[next_idx][k] = next_str;
                        continue;
                    }
                    
                    if(dp[next_idx][k].size() > next_str.size()) dp[next_idx][k] = next_str;
                    else if(dp[next_idx][k].size() == next_str.size()) dp[next_idx][k] = min(dp[next_idx][k], next_str);
                }
            }
        }

        string res = dp[(1 << n) - 1][0];
        range(i, 1, n){
            if(res.size() > dp[(1 << n) - 1][i].size()) res = dp[(1 << n) - 1][i];
            else if(res.size() == dp[(1 << n) - 1][i].size()) res = min(res, dp[(1 << n) - 1][i]);
        }

        cout << res.size() << endl;
    }

	return 0;
}
