#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1LL << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

map<string, int> dic;
int n = 0;

inline int s2i(string s){
    if(dic.find(s) != end(dic)) return dic[s];
    return (dic[s] = n++);
}

using Edge = tuple<int, int>;
using State = tuple<int, int, int>;

signed main(void){
    for(int m, Q, cap; cin >> m >> Q >> cap, m;){
        cap *= 10;
        n = 0;
        dic = map<string, int>();

        int s, t;
        {
            string S, T; cin >> S >> T;
            s = s2i(S), t = s2i(T);
        }

        const int N = 6000;

        vector<vector<Edge>> graph(N);
        rep(loop, m){
            string A, B; cin >> A >> B;
            int dist; cin >> dist;

            int a = s2i(A), b = s2i(B);
            graph[a].push_back(Edge(b, dist));
            graph[b].push_back(Edge(a, dist));
        }

        vi hasStation(N);
        rep(loop, Q){
            string S; cin >> S;
            hasStation[s2i(S)] = true;
        }

        priority_queue<State, vector<State>, greater<State>> q;
        q.push(State(0, s, cap));
        vector<vi> min_dist(n, vi(cap + 1, inf));

        int res = inf;
        while(q.size()){
            State cur = q.top(); q.pop();
            int dist, v, rest; tie(dist, v, rest) = cur;

            if(not chmin(min_dist[v][rest], dist)) continue;

            if(v == t){
                res = dist;
                break;
            }

            for(auto & e : graph[v]){
                int nv, ddist;
                tie(nv, ddist) = e;
                
                int ndist = dist + ddist;
                int nrest = rest - ddist;
                if(nrest < 0) continue;

                if(hasStation[nv]) nrest = cap;
                if(min_dist[nv][nrest] != inf) continue;

                q.push(State(ndist, nv, nrest));
            }
        }

        if(res == inf) res = -1;
        cout << res << endl;
    }

    return 0;
}
