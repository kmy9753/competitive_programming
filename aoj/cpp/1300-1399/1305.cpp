#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n; cin >> n, n;){
        map<string, set<string> > s;

        string group1;
        rep(i, n){
            string in; cin >> in;

            int pos = in.find(':');
            string group = in.substr(0, pos++);
            if(!i) group1 = group;

            int z;
            while((z = in.find(',', pos)) != -1){
                string member = in.substr(pos, z - pos);
                s[group].insert(member);

                pos = z + 1;
            }
            s[group].insert(in.substr(pos, in.size() - 1 - pos));
        }

        rep(i, n){
            set<string> next = s[group1];
            each(itr, s[group1]){
                string mem = *itr;
                if(mem.size() > 20) continue;
                if(s.find(mem) != s.end()){
                    each(jtr, s[mem]){
                        next.insert(*jtr);
                    }
                    next.erase(mem);
                }
            }
            s[group1] = next;
        }

        cout << s[group1].size() << endl;
    }

	return 0;
}
