#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
struct Ant {
    int pos, idx;
    char dir;
};

int main(void){
    for(int n, l; cin >> n >> l, n;){
        vector<Ant> ant(n);
        for(auto & e : ant) cin >> e.dir >> e.pos;
        rep(i, n) ant[i].idx = i + 1;

        int rest = 0, resi;
        set<int> s;
        rep(i, n) s.insert(i + 1);
        while(s.size() != 0){
            rep(i, n){
                rep(j, n){
                    if(ant[j].pos - ant[i].pos == 2 and
                            ant[i].dir == 'R' and
                            ant[j].dir == 'L'){
                        swap(ant[i].idx, ant[j].idx);
                    }
                }
            }

            bool left = false;
            rep(i, n){
                int dx = ant[i].dir == 'R' ? 1:-1;
                ant[i].pos += dx;
                if(ant[i].pos == 0 or ant[i].pos == l){
                    if(not left or ant[i].pos == 0){
                        resi = ant[i].idx;
                        left = true;
                    }
                    s.erase(ant[i].idx);
                }
            }

            rest++;
        }

        cout << rest << " " << resi << endl;
    }

	return 0;
}
