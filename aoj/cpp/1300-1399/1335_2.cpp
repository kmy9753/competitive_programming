#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int n, k, s;
int dfs(int d, int pnum, int cs){
    if(cs + pnum >= s) return 0;
    if(d == k - 1){
        int cnum = s - cs;
        if(pnum < cnum and cnum <= n){
            return 1;
        }
    }

    int ret = 0;
    range(i, pnum + 1, n + 1){
        ret += dfs(d + 1, i, cs + i);
    }
    return ret;
}
int main(void){
    for(; cin >> n >> k >> s, n;){
        cout << dfs(0, 0, 0) << endl;
    }

    return 0;
}
