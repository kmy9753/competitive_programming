#include <iostream>

#define N 30

using namespace std;

int main(void){
	bool submitted[N];
	for(int i = 0; i < N; i ++) submitted[i] = false;
	for(int num; cin >> num; submitted[num - 1] = true) ;
	for(int i = 0; i < N; i ++) if(!submitted[i]) cout << i + 1 << endl;

	return 0;
}
