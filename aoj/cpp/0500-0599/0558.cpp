#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define X first
#define Y second
#define P first
#define C second

using namespace std; 

typedef pair<int, int> pii;
typedef pair<pii, int> mouse;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, -1, 0, 1};

int main(void){
    int w, h, n, res = 0; cin >> h >> w >> n;
    vs m(h);
    rep(y, h) cin >> m[y];
    pii s;
    rep(y, h) rep(x, w) if(m[y][x] == 'S') s = mp(x, y);

    rep(i, n){
        queue<mouse> q;
        q.push(mp(s, 0));

        vector< vector<bool> > used(h);
        rep(y, h) used[y].resize(w);

        while(!q.empty()){
            mouse cur = q.front();
            q.pop();

            if(used[cur.P.Y][cur.P.X]) continue;
            used[cur.P.Y][cur.P.X] = true;

            if(m[cur.P.Y][cur.P.X] == i + 1 + '0'){
                res += cur.C;
                s = mp(cur.P.X, cur.P.Y);
                break;
            }

            rep(i, 4){
                mouse next = mp(mp(cur.P.X + dx[i], cur.P.Y + dy[i]), cur.C + 1);
                if(0 <= next.P.X && next.P.X < w &&
                   0 <= next.P.Y && next.P.Y < h &&
                   m[next.P.Y][next.P.X] != 'X') q.push(next);
            }
        }
    }

    cout << res << endl;

    return 0;
}
