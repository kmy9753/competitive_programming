#include <iostream>

using namespace std;

int mod = (int)1e5;

int main(void){
    int n, m; cin >> n >> m;
    int s[n - 1];
    for(int i = 0; i < n - 1; i++) cin >> s[i];

    int sum[n];
    sum[0] = 0;
    for(int i = 1; i < n; i++){
        sum[i] = sum[i - 1] + s[i - 1];
    }

    int res = 0, cur_pos = 0;
    for(int i = 0; i < m; i++){
        int a; cin >> a;
        int next_pos = cur_pos + a;
        int diff = abs(sum[next_pos] - sum[cur_pos]);
        res += diff; res %= mod;

        cur_pos = next_pos;
    }

    cout << res << endl;

	return 0;
}
