#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <utility>
#include <cstring>
#include <string>
#include <sstream>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

int main(void){
    int n, m, res; cin >> n >> m;
    res = m;

    while(n --){
        int in, out; cin >> in >> out;
        if(!~res) continue;
        m += in - out;
        if(m < 0) res = -1; 
        if(m > res) res = m;
    }
    cout << ((~res) ? res:0) << endl;

    return 0;
}
