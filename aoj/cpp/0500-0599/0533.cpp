#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    vector<int> a(10), b(10);
    for(auto & e : a) cin >> e;
    for(auto & e : b) cin >> e;

    sort(begin(a), end(a), greater<int>());
    sort(begin(b), end(b), greater<int>());

    cout << a[0] + a[1] + a[2] << " " << b[0] + b[1] + b[2] << endl;

	return 0;
}
