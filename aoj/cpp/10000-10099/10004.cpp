#include <iostream>
#include <algorithm>
#include <vector>

#define N 3

using namespace std;

int main(void){
	vector<int> in(N);
	for(int i = 0; i < N; i ++){
		cin >> in[i];
	}
	sort(in.begin(), in.end());
	for(int i = 0; i < N; i ++){
		cout << in[i] << ((i != N - 1) ? " ":"\n");
	}
	return 0;
}
