#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define IN(x, w) 0 <= (x) && (x) <(w) 

#define X first
#define Y second
#define R first
#define L second

typedef pair<pii, pii> State;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int dx[] = { 1, 0,-1, 0};
int dy[] = { 0, 1, 0,-1};

bool used[51][51][51][51] = { false };

int main(void){
    string res;
    for(int w, h; cin >> w >> h, w; cout << res << endl){
        res = "No";
        pair<vs, vs> cave = mp(vs(h), vs(h));

        State s;
        rep(y, h){
            cin >> cave.L[y];
            cin >> cave.R[y];
            rep(x, w){
                if(cave.L[y][x] == 'L') s.L = mp(x, y), cave.L[y][x] = '.';
                if(cave.R[y][x] == 'R') s.R = mp(x, y), cave.R[y][x] = '.';
            }
        }
        
        rep(i, 51)rep(j, 51)rep(k, 51)rep(l, 51) used[i][j][k][l] = false;

        queue<State> q;
        q.push(s);

        while(!q.empty()){
            State cur = q.front(); q.pop();

            if(cave.L[cur.L.Y][cur.L.X] == '%' || cave.R[cur.R.Y][cur.R.X] == '%'){
                if(cave.L[cur.L.Y][cur.L.X] == '%' && cave.R[cur.R.Y][cur.R.X] == '%'){
                    res = "Yes";
                    break;
                }
                continue;
            }

            rep(i, 4){
                State next = cur;

                if(IN(next.L.X + dx[i], w) && cave.L[next.L.Y][next.L.X + dx[i]] != '#') next.L.X += dx[i];
                if(IN(next.L.Y + dy[i], h) && cave.L[next.L.Y + dy[i]][next.L.X] != '#') next.L.Y += dy[i];
                if(IN(next.R.X - dx[i], w) && cave.R[next.R.Y][next.R.X - dx[i]] != '#') next.R.X -= dx[i];
                if(IN(next.R.Y + dy[i], h) && cave.R[next.R.Y + dy[i]][next.R.X] != '#') next.R.Y += dy[i];

                if(!used[next.L.X][next.L.Y][next.R.X][next.R.Y]){
                    q.push(next);
                    used[next.L.X][next.L.Y][next.R.X][next.R.Y] = true;
                }
            }
        }
    }

	return 0;
}
