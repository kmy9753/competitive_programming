#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
 
const int N = 8;
map<vi, bool> used;
int n;
vector<vi> rot = {{1, 2, 3, 0, 5, 6, 7, 4},
                  {6, 7, 1, 0, 2, 3, 5, 4}};

void dfs(vi perm){
    if(used[perm]) return;
    used[perm] = true;

    rep(i, 2){
        vi next_perm(N);
        rep(j, N){
            next_perm[j] = perm[rot[i][j]];
        }
        dfs(next_perm);
    }
}

int main(void){
    vector<string> in(N);
    while(cin >> in[0]){
        rep(i, 1, N) cin >> in[i];
        map<string, int> s2i;
        int idx = -1;
        vi num_color;
        for(auto & e : in){
            if(s2i.find(e) == end(s2i)){
                idx++;
                s2i[e] = idx;
                num_color.push_back(0);
            }
            num_color[idx]++;
        }

        used.clear();

        n = num_color.size();
        vi perm(N);
        rep(i, N) perm[i] = s2i[in[i]];

        sort(_all(perm));
        int res = 0;
        do {
            if(not used[perm]){
                dfs(perm);
                res++;
            }
        } while(next_permutation(_all(perm)));

        cout << res << endl;
    }

    return 0;
}
