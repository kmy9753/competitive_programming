#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-9;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int N = 16;
const int M = 256;

int main(void){
    for(int n, resS, resA, resC; cin >> n, n; cout << resS << " " << resA << " " << resC << endl){
        vi I(n + 1), R(n + 1);
        range(i, 1, n + 1) cin >> I[i];

        double resH = (double)inf;
        rep(s, N){
            rep(a, N){
                rep(c, N){
                    vi O(M);

                    R[0] = s;
                    range(i, 1, n + 1){
                        R[i] = (a * R[i - 1] + c) % M;
                        O[(I[i] + R[i]) % M]++;
                    }

                    double H = 0.;
                    rep(i, M){
                        if(O[i] != 0) H -= (double)O[i] / n * log((double)O[i] / n);
                    }
                    if(H + eps < resH){
                        resH = H;
                        resS = s;
                        resA = a;
                        resC = c;
                    }
                }
            }
        }
    }

	return 0;
}
