#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

//($B=1$o$l$??t(B, $B=I(B, $B=j;}6b(B)
typedef pair<int, pii> State;

#define F first
#define S second

int main(void){
    for(int n, m, l; cin >> n >> m >> l, n;){
        
        //($B<!$N=I(B, $BD9$5(B, $B;I5R$N?t(B)
        vector<vector<pair<int, pii>>> edge(n);

        rep(i, m){
            int a, b, d, e; cin >> a >> b >> d >> e;
            a--, b--;

            edge[a].pb(mp(b, mp(d, e)));
            edge[b].pb(mp(a, mp(d, e)));
        }

        priority_queue<State, vector<State>, greater<State>> q;
        q.push(mp(0, mp(0, l)));

        //key: ($B=I(B, $B=j;}6b(B), val: ($B=1$o$l$??t(B)
        vvi minCost(n, vi(l + 1, inf));

        while(!q.empty()){
            int cur_cost = q.top().F;
            int cur_v = q.top().S.F;
            int cur_money = q.top().S.S;
            q.pop();

            if(minCost[cur_v][cur_money] != inf) continue;
            minCost[cur_v][cur_money] = cur_cost;

            for(pair<int, pii> e : edge[cur_v]){
                int next_v = e.F;
                int d_money = e.S.F;
                int d_cost = e.S.S;

                int next_money = cur_money;
                int next_cost = cur_cost + d_cost;
                rep(i, 2){
                    if(next_money < 0) break;

                    q.push(mp(next_cost, mp(next_v, next_money)));

                    next_money -= d_money;
                    next_cost -= d_cost;
                }
            }
        }

        int res = inf;
        for(int c : minCost[n - 1]){
            res = min(res, c);
        }

        cout << res << endl;
    }

	return 0;
}
