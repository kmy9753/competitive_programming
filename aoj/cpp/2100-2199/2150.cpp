#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;
#define v first
#define ind second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
	const int N = (int)1e6;
	vector<bool> isP(N, true);
    isP[0] = isP[1] = false;
	for(int i = 2; i * i < N; i++) if(isP[i]) for(int j = i; i * j < N; j++) isP[i * j] = false;

    vi P;
    rep(i, N){
        if(isP[i]) P.pb(i);
    }

    for(int n, p, res; cin >> n >> p, n != -1; cout << res << endl){
        vector<pii> s(100);
        int pos = upper_bound(all(P), n) - P.begin();

        rep(i, s.size()){
            s[i].first = 2 * P[pos + i];
            s[i].second = pos + i;
        }

        rep(i, p){
            vector<pii>::iterator itr = min_element(all(s));
            res = itr->v;
            
            itr->v += P[itr->ind + 1] - P[itr->ind++];
        }
    }

	return 0;
}
