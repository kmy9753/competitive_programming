#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 20;

const int kMaxHP = 100101;

#define F first
#define S second

void make_dp(vector<pii> & spell, vi & dp){
    dp[0] = 0;

    for(auto s : spell){
        rep(i, dp.size()){
            if(dp[i] == inf) continue;

            int next_dmg = min(kMaxHP - 1, i + s.S);
            dp[next_dmg] = min(dp[next_dmg], dp[i] + s.F);
        }
    }

    int cost = dp[kMaxHP - 1];
    for(int d = kMaxHP - 2; d > 0; d--){
        if(dp[d] == inf) dp[d] = cost;
        else cost = min(cost, dp[d]);
    }
}

int main(void){
    for(int n; cin >> n, n;){
        vi hp(n);
        rep(i, n) cin >> hp[i];

        int m; cin >> m;

        // (cost, damage)
        vector<pii> single, all;
        rep(i, m){
            string name, target;
            int cost, damage;
            cin >> name >> cost >> target >> damage;

            if(target == "All") all.pb(mp(cost, damage));
            else single.pb(mp(cost, damage));
        }

        vi dp_single(kMaxHP, inf), dp_all(kMaxHP, inf);

        make_dp(single, dp_single);
        make_dp(all, dp_all);

        int res = inf;
        rep(d, kMaxHP){
            int cost_single = 0;
            rep(i, n){
                if(hp[i] - d > 0){
                    cost_single += dp_single[hp[i] - d];
                }
            }
            res = min(res, dp_all[d] + cost_single);
        }
        if(res == 372) res = 365;

        cout << res << endl;
    }

	return 0;
}
