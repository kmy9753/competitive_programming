#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, d; cin >> n >> d, n;){
        vvi c(n);
        vi sum(n);
        rep(i, n){
            int m; cin >> m;

            rep(j, m){
                int cc; cin >> cc;
                c[i].pb(cc);

                sum[i] += cc;
            }
        }

        bool ok = true;
        while(ok){
            ok = false;
            
            rep(i, n){
                while(c[i].size() != 0){
                    int cur_c = *(c[i].end() - 1);
                    sum[i] -= cur_c;
                    c[i].pop_back();

                    if(abs(*max_element(all(sum)) - sum[i]) > d){
                        sum[i] += cur_c;
                        c[i].pb(cur_c);
                        break;
                    }

                    ok = true;
                }
            }
        }

        if(*max_element(all(sum)) == 0) cout << "Yes" << endl;
        else                            cout << "No"  << endl;
    }

	return 0;
}
