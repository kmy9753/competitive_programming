#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
const int T = 86400;
const double eps = 1e-9;
 
int main(void){
    double L;
    for(int n; cin >> n >> L, n;){
        vector<tuple<int, int, double>> in(n);
        for(auto & e : in){
            int s, t; double u; cin >> s >> t >> u;
            e = tie(s, t, u);
        }
 
        double lb = 0.0, ub = 1e6;
        rep(loop, 100){
            double mid = (lb + ub) / 2.0;
 
            vector<double> ls(2);
            double rest = L;
            bool ok = true;
            [&]{
                rep(_, 2){
                    int curt = 0;
                    for(auto & e : in){
                        int s, t; double u; tie(s, t, u) = e;
 
                        rest = min(rest + (s - curt) * mid, L);
                        curt = s;
 
                        rest = min(rest + (t - curt) * (mid - u), L);
                        curt = t;
 
                        if(rest < 0){
                            ok = false;
                            return;
                        }
                    }
                    rest = min(rest + (T - curt) * mid, L);
 
                    ls[_] = rest;
                }
            }();
 
            if(ok and abs(ls[0] - ls[1]) < eps){
                ub = mid;
            }
            else {
                lb = mid;
            }
        }
 
        printf("%.12f\n", ub);
    }
 
    return 0;
}
