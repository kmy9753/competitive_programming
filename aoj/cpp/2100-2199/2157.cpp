#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

signed main(void){
    for(int n; cin >> n, n;){

        //($B%@%$%"%k(B) -> ($B2s?t(B)
        map<string, int> minDist;

        string before, after; cin >> before >> after;

        queue<string> q;
        q.push(before);
        minDist[before] = 0;

#define F first
#define S second

        int res = inf;
        while(!q.empty()){
            string cur_dial = q.front();
            q.pop();

            int cur_cnt = minDist[cur_dial];

            if(cur_dial == after){
                res = cur_cnt;
                break;
            }

            int cur_ind = 0;
            while(cur_dial[cur_ind] == after[cur_ind]) cur_ind++;

            if(cur_ind == n) continue;
            
            int delta = (after.at(cur_ind) - cur_dial.at(cur_ind) + 10) % 10;

            range(i, cur_ind, n){
                cur_dial[i] = (cur_dial[i] - '0' + 10 + delta) % 10 + '0';

                if(minDist.find(cur_dial) != minDist.end()) continue;

                minDist[cur_dial] = cur_cnt + 1;
                q.push(cur_dial);
            }
        }

        cout << res << endl;
    }

	return 0;
}
