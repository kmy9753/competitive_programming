#include <bits/stdc++.h>

using namespace std;

#define int ll
typedef long long ll;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

#define F first
#define S second

signed main(void){
    for(int n; cin >> n, n;){
        vector<pii> s_empty;
        s_empty.pb(mp(0, INF));

        vector<vector<pii>> s_used(10001);
        map<int, int> dic_o2i, dic_i2o;
        int cnt_idx = 0;

        while(n--){
            char op; cin >> op;

            switch(op){
                case 'W':{
                    int idx, len; cin >> idx >> len;

                    dic_i2o[cnt_idx] = idx;
                    dic_o2i[idx] = cnt_idx;
                    idx = cnt_idx++;

                    int cnt = 0;
                    for(auto && sec : s_empty){
                        if(len == 0) break;

                        if(sec.S - sec.F <= len){
                            s_used[idx].pb(sec);
                            len -= (sec.S - sec.F);
                            cnt++;
                        }
                        else{
                            s_used[idx].pb(mp(sec.F, sec.F + len));
                            sec = mp(sec.F + len, sec.S);

                            break;
                        }
                    }

                    s_empty.erase(s_empty.begin(), s_empty.begin() + cnt);
                    range(i, 1, s_used[idx].size()){
                        if(s_used[idx][i].F != s_used[idx][i - 1].S) continue;

                        s_used[idx][i - 1].S = s_used[idx][i].S;
                        s_used[idx].erase(s_used[idx].begin() + i);
                        i--;
                    }

                    break;
                }
                case 'R':{
                    int p; cin >> p;
                    int res = -1;

                    rep(i, cnt_idx){
                        for(auto && sec : s_used[i]){
                            if(p < sec.F || sec.S <= p) continue;

                            res = dic_i2o[i];
                        }
                        
                        if(res != -1) break;
                    }

                    cout << res << endl;

                    break;
                }
                case 'D':{
                    int idx; cin >> idx;
                    idx = dic_o2i[idx];

                    for(auto && sec : s_used[idx]){
                        s_empty.pb(sec);
                    }
                    s_used[idx].clear();

                    sort(all(s_empty));
                    
                    break;
                }
                
                default: break;
            }
        }

        cout << endl;
    }

	return 0;
}
