#include <bits/stdc++.h>
#define int long long
  
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
  
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
  
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
  
// #define DEBUG
  
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
  
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
  
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
  
const double EPS = 1e-9;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
  
  
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
  
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
  
using E = tuple<int, int>;
using R = double;
using P = complex<R>;
using L = tuple<P, P>;
 
inline R dt(P a, P b){
	return imag(conj(a) * b);
}

bool match(L lhs, L rhs){
    P a1, a2, b1, b2;
    tie(a1, a2) = lhs;
    tie(b1, b2) = rhs;
 
    if(abs(dt(a2 - a1, b2 - b1)) > EPS) return false;
    if(abs(dt(a2 - a1, b2 - a1)) > EPS) return false;
 
    return true;
}
 
L make_line(P a, P b){
	P mid = (a + b) / P(2, 0);
    
    return L(mid, mid + (b - a) * P(0, 1));
}

int n, res;
vector<P> p;

struct UnionFind {
    vector<int> data;
    UnionFind(int size) : data(size, -1) { }
    bool unionSet(int x, int y) {
        x = root(x); y = root(y);
        if (x != y) {
            if (data[y] < data[x]) swap(x, y);
            data[x] += data[y]; data[y] = x;
        }
        return x != y;
    }
    bool findSet(int x, int y) {
        return root(x) == root(y);
    }
    int root(int x) {
        return data[x] < 0 ? x : data[x] = root(data[x]);
    }
    int size(int x) {
        return -data[root(x)];
    }
};

bool check_tree(vector<E> & edges){
	UnionFind uf(n);

	for(auto & e : edges){
		int a, b; tie(a, b) = e;
		uf.unionSet(a, b);
	}

	rep(i, n){
		if(uf.root(0) != uf.root(i)) return false;
	}
	return true;
}

signed main(void){
    cin >> n;
    p = vector<P>(n);
    for(auto & e : p){
        R x, y; cin >> x >> y;
        e = P(x, y);
    }

	vector<E> tbl;
	rep(a, n){
		rep(b, a + 1, n){
			tbl.push_back(E(a, b));
		}
	}

	int res = n - 1;

	int k = n - 1;
	int comb = (1 << k) - 1;
	int len = tbl.size();

	while(comb < 1 << len){
		vector<E> edges;
		rep(i, len){
			if(((comb >> i) & 1) == 0) continue;

			edges.push_back(tbl[i]);
		}

		int x = comb & -comb, y = comb + x;
		comb = (((comb & ~y) / x) >> 1) | y;

		if(not check_tree(edges)) continue;

		vector<L> s;
		for(auto & e : edges){
			int a, b; tie(a, b) = e;

			L cline = make_line(p[a], p[b]);
			[&]{
				for(auto & l : s){
					if(match(l, cline)) return;
				}
				s.push_back(cline);
			}();
		}

//		if(s.size() == 2){
//			for(auto & e : edges){
//				int a, b; tie(a, b) = e;
//				cerr << a << " " << b << endl;
//
//				L cline = make_line(p[a], p[b]);
//				P p1, p2; tie(p1, p2) = cline;
//				cerr << fixed << real(p1) << " " << imag(p1) << endl;
//				cerr << fixed << real(p2) << " " << imag(p2) << endl;
//			}
//			cerr << "---" << endl;
//		}

		chmin(res, (int)s.size());
	}

	cout << res << endl;
 
    return 0;
}
