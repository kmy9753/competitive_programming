#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int w, h;

struct Rect{
    int up;
    int down;
    int left;
    int right;

    Rect()
    : up(inf),
      down(-inf),
      left(inf),
      right(-inf) {}
};

int main(void){
    int n; cin >> n;

    while(n--){
        cin >> h >> w;
        string res = "SAFE";

        vs bag(h);
        rep(y, h) cin >> bag[y];

        map<char, Rect> rects;
        set<char> s;
        rep(y, h){
            rep(x, w){
                if(bag[y][x] != '.'){
                    rects[bag[y][x]].up = min(rects[bag[y][x]].up, y);
                    rects[bag[y][x]].down = max(rects[bag[y][x]].down, y);
                    rects[bag[y][x]].left = min(rects[bag[y][x]].left, x);
                    rects[bag[y][x]].right = max(rects[bag[y][x]].right, x);

                    s.insert(bag[y][x]);
                }
            }
        }

        int cnt = 0;
        rep(i, s.size()){
            each(itr, s){
                char c = *itr;
                bool pass = false;
                bool in = false;
                range(y, rects[c].up, rects[c].down + 1){
                    range(x, rects[c].left, rects[c].right + 1){
                        if(bag[y][x] == '.' || (bag[y][x] != c && bag[y][x] != '*')){
                            pass = true;
                            break;
                        }
                        if(bag[y][x] == c) in = true;
                    }
                    if(pass) break;
                }
                if(pass || !in) continue;

                cnt++;
                range(y, rects[c].up, rects[c].down + 1){
                    range(x, rects[c].left, rects[c].right + 1){
                        bag[y][x] = '*';
                    }
                }
            }
        }

        if(cnt != s.size()){
            res = "SUSPICIOUS";
        }
        cout << res << endl;
    }

	return 0;
}
