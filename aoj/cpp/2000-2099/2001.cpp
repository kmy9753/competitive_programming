#include <iostream>

using namespace std;

class Amida{
public:
	Amida(int n, int m){
		_n = n;

		for(int i = 1; i <= n; i ++)
			for(int j = 1; j <= MAX_H; j ++)
				amidaMap[j][i] = i;
	}

	void set(int h, int p, int q){
		amidaMap[h][p] = q;
		amidaMap[h][q] = p;
	}

	int getGoal(int target){
		int curPos = target;
		for(int j = MAX_H; j >= 1; j --)
			curPos = amidaMap[j][curPos];
		return curPos;
	}

private:
	static const int MAX_H = 1000;
	static const int MAX_N = 100;
	int amidaMap[MAX_H + 1][MAX_N + 1];

	int _n;
};

int main(void){
	for(int n, m, a; cin >> n >> m >> a, n;){
		Amida amida(n, m);

		while(m --){
			int h, p, q; cin >> h >> p >> q;
			amida.set(h, p, q);
		}
		cout << amida.getGoal(a) << endl;
	}
	

	return 0;
}
