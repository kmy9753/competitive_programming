#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;
#define F first
#define S second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

typedef pair<int, pii> State;

int n, m;
vi isColdTown;
vvi costs;
vvi d;

void dijkstra(int s, int z);

int main(void){
    for(int l, k, a, h; cin >> n >> m >> l >> k >> a >> h, n;){
        isColdTown = vi(n, false);
        rep(i, l){
            int town; cin >> town;
            isColdTown[town] = true;
        }

        costs = vvi(n, vi(n, inf));
        rep(i, k){
            int u, v, t; cin >> u >> v >> t;
            costs[u][v] = costs[v][u] = t;
        }

        dijkstra(a, h);

        int res = *min_element(all(d[h]));
        if(res != inf) cout << res << endl;
        else cout << "Help!" << endl;
    }

	return 0;
}

void dijkstra(int s, int z){
    d = vvi(n, vi(m + 1, inf));
    d[s][m] = 0;

    priority_queue<State, vector<State>, greater<State> > q;
    q.push(mp(0, mp(s, m)));

    while(!q.empty()){
        State state = q.top(); q.pop();
        int v = state.S.F, mm = state.S.S;
        if(d[v][mm] < state.F) continue;

        if(isColdTown[v]){
            range(i, mm + 1, m + 1){
                if(d[v][mm] + i - mm < d[v][i]){
                    d[v][i] = d[v][mm] + i - mm;
                    q.push(mp(d[v][i], mp(v, i)));
                }
            }
        }

        rep(u, n){
            int nextm = mm - costs[u][v];
            if(nextm < 0) continue;

            if(d[v][mm] + costs[v][u] < d[u][nextm]){
                d[u][nextm] = d[v][mm] + costs[v][u];
                q.push(mp(d[u][nextm], mp(u, nextm)));
            }
        }
    }
}
