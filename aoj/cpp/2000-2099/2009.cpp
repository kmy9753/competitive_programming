#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=int(a);i<int(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=int(a)-1;i>=int(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(arg),key)-begin(arg)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
using namespace std;
 
template<class T>void reg(vector<T> &ary,const T &elem){ary.emplace_back(elem);}
template<class T>bool chmin(T &a, const T &b) {return (b<a)?(a=b,1):0;}
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
 
using R=long double; // __float128
const R EPS = 1E-10; // [-1000:1000]->EPS=1e-8 [-10000:10000]->EPS=1e-7
const R INF = 1E40;
constexpr R PI = acos(R(-1));
inline int sgn(const R& r){ return (r > EPS) - (r < -EPS);}
inline int sgn(const R& a, const R &b){ return sgn(a-b); }
inline R sq(R x){return sqrt(max<R>(x,0.0));}
 
using P=complex<R>;
using VP=vector<P>;
using L=struct{P s,t;};
using VL=vector<L>;
using C=struct{P c;R r;};
using VC=vector<C>;
 
constexpr P O = P(0,0);
istream& operator >> (istream& is,P& p){ R x,y;is >> x >> y; p=P(x,y); return is;}
ostream& operator << (ostream& os,P& p){ os << real(p) << " " << imag(p); return os;}
 
namespace std{
    bool operator <  (const P& a,const P& b){ return sgn(real(a-b))?real(a-b)<0:sgn(imag(a-b))<0;}
    bool operator == (const P& a,const P& b){ return sgn(real(a-b))==0 && sgn(imag(a-b))==0;}
}
 
inline bool cmp_x(const P& p,const P& q){return sgn(real(p-q))?real(p)<real(q):sgn(imag(p-q));}
inline bool cmp_y(const P& a, const P& b){return sgn(imag(a-b)) ? imag(a-b)<0 : sgn(real(a-b))<0;}
inline bool cmp_a(const P& a, const P& b){return sgn(arg(a)-arg(b)) ? arg(a)-arg(b)<0 : sgn(norm(a)-norm(b))<0;}
bool operator <  (const L& a,const L& b){ return a.s==b.s?a.t<b.t:a.s<b.s;}
bool operator == (const L& a,const L& b){ return a.s==b.s&&a.t==b.t;}
 
//内積 dot 外積 det
inline R dot(P o,P a,P b){a-=o,b-=o; return real(conj(a)*b);}
inline R det(P o,P a,P b){a-=o,b-=o; return imag(conj(a)*b);}
inline P vec(L l){return l.t-l.s;}
 
// 射影 verify AOJ CGL_1_A
P proj(P o,P a,P b){ a-=o,b-=o; return a*real(b/a);}
P proj(L l,P p){l.t-=l.s,p-=l.s;return l.s+l.t*real(p/l.t);}
// 反射 verify AOJ CGL_1_B
P refl(L l,P p){ return R(2.0)*proj(l,p)-p;}
// CCW verify AOJ CGL_1_C
enum CCW{ LEFT = 1,RIGHT = 2,BACK = 4,FRONT = 8,ON = 16};
inline int ccw(P o,P a, P b) {//点aと点bが与えられた問いに
    if (sgn(det(o,a,b)) > 0) return LEFT;    // counter clockwise
    if (sgn(det(o,a,b)) < 0) return RIGHT;   // clockwise
    if (sgn(dot(o,a,b)) < 0) return BACK;    // b--base--a on line
    if (sgn(norm(a-o)-norm(b-o)) < 0) return FRONT;   // base--a--b on line
    return ON;// base--b--a on line  aとbの線分判定はこれ
}
 
// 垂直 平行 verify AOJ CGL_2_A
bool vertical(L a, L b) {return sgn(dot(O,vec(a),vec(b)))==0;}
bool parallel(L a, L b) {return sgn(det(O,vec(a),vec(b)))==0;}
 
// 同じ直線判定 
bool eql(L a,L b){ return (parallel(a,b) && sgn(det(a.s,a.t,b.s))==0);}
 
// 交差判定　verify AOJ CGL_2_B 端点を含まない場合は1,含む場合は0
bool ill(L a,L b){ return parallel(a,b)==false;}
bool ils(L l,L s,int end=0){ return sgn(det(l.s,l.t,s.s)*det(l.s,l.t,s.t))<=-end;}
bool iss(L a,L b,int end=0){
    int s1=ccw(a.s,a.t,b.s)|ccw(a.s,a.t,b.t);
    int s2=ccw(b.s,b.t,a.s)|ccw(b.s,b.t,a.t);
    if(end) return (s1&s2)==(LEFT|RIGHT);
    return (s1|s2)&ON || (s1&s2)==(LEFT|RIGHT);
}
 
// 交点 verify AOJ CGL_2_C
P cross(L a,L b){
    R s1=det(a.s,b.s,b.t);
    R s2=s1+det(a.t,b.t,b.s);
    return a.s+s1/s2*(a.t-a.s);
}
     
// 距離 verify AOJ CGL_2_D
R dlp(L l,P p){return abs(det(l.s,l.t,p))/abs(vec(l));}
R dsp(L s,P p){
    if(sgn(dot(s.s,s.t,p))<=0) return abs(p-s.s);
    if(sgn(dot(s.t,s.s,p))<=0) return abs(p-s.t);
    return abs(det(s.s,s.t,p))/abs(s.t-s.s);
}
R dll(L a,L b){return ill(a,b)?0:dlp(a,b.s);}
R dls(L l,L s){return ils(l,s)?0:min(dlp(l,s.s),dlp(l,s.t));}
R dss(L a,L b){return iss(a,b)?0:min({dsp(a,b.s),dsp(a,b.t),dsp(b,a.s),dsp(b,a.t)});}

int main(void){
    for(int n; cin >> n, n;){
        VL lines = { {{-100, -100}, { 100, -100}},
            {{ 100, -100}, { 100,  100}}, 
            {{ 100,  100}, {-100,  100}},
            {{-100,  100}, {-100, -100}} };

        rep(loop, n){
            P a, b; cin >> a >> b;
            lines.push_back({a, b});
        }

        int res = 1;
        rep(i, 4, lines.size()){
            VP points;

            rep(j, i){
                P p = cross(lines[i], lines[j]);
                if(dsp(lines[i], p) > EPS) continue;

                [&]{
                    for(auto & e : points){
                        if(dot(e, p, p) < EPS){
                            return;
                        }
                    }
                    points.push_back(p);
                }();
            }

            res += points.size() - 1;
        }

        cout << res << endl;
    }

    return 0;
}
