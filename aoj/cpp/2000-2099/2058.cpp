#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
#define R(i)((m[way[i][0]] + m[way[i][1]] + m[way[i][2]]) % m[9]) 

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

const int way[8][3] = { {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6} };

int res;
int m[10];

void dfs(int);

int main(void){
    for(;cin >> m[0], ~m[0]; cout << res << endl){
        COUNT(i, 9) cin >> m[i];

        res = 0;
        dfs(0);
    }

    return 0;
}

void dfs(int num){
    if(num == 10){
        int r = R(0), inc = 1;
        COUNT(i, 7) inc &= R(i) == r;
        res += inc;
        return;
    }

    if(!m[num]){
        COUNT(i, 10) if(find(m, m + 10, i) == m + 10) m[num] = i, dfs(num + 1);
        m[num] = 0;
    }
    else dfs(num + 1);
}
