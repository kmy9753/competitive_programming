#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define X first
#define Y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int w, h;
vs m;
int dx[] = {-1, 0, 1, 0};
int dy[] = { 0,-1, 0, 1};

void dfs(char tar, pii p);

int main(void){
    for(; cin >> w >> h, w;){
        m = vs(h);
        rep(y, h) cin >> m[y];

        rep(y, h) rep(x, w) if(m[y][x] == 'B'){
            rep(i, 4){
                dfs('B', mp(x + dx[i], y + dy[i]));
            }
        }
        rep(y, h) rep(x, w) if(m[y][x] == 'W'){
            rep(i, 4){
                dfs('W', mp(x + dx[i], y + dy[i]));
            }
        }

        pii sum = mp(0, 0);
        rep(y, h) sum.first += count(all(m[y]), 'b');
        rep(y, h) sum.second += count(all(m[y]), 'w');

        cout << sum.first << " " << sum.second << endl;
    }

	return 0;
}

void dfs(char tar, pii p){
    if(p.X < 0 || w <= p.X ||
       p.Y < 0 || h <= p.Y) return;

    char ene = tar == 'B' ? 'W':'B';
    char extar = tar == 'B' ? 'b':'w';
    char exene = tar == 'B' ? 'w':'b';

    if(m[p.Y][p.X] == ene || m[p.Y][p.X] == extar || m[p.Y][p.X] == '-' ||
       m[p.Y][p.X] == tar)
        return;

    if(m[p.Y][p.X] == '.') m[p.Y][p.X] = extar;
    if(m[p.Y][p.X] == exene) m[p.Y][p.X] = '-';

    rep(i, 4){
        dfs(tar, mp(p.X + dx[i], p.Y + dy[i]));
    }
}
