#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
 
int main(void){
    for(int a, b, c; cin >> a >> b >> c;){
        int s = a + b, cnt = 0;

        rep(i, 10){
            if(i + 1 == a || i + 1 == b || i + 1 == c) continue;
            if(20 < s + i + 1) break;
            cnt ++;
        }
        string res = 0.5 <= cnt / 7. ? "YES":"NO";

        cout << res << endl;
    }
 
    return 0;
}
