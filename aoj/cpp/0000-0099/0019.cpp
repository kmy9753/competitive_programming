#include <iostream>
using namespace std;
unsigned long long int factorial(unsigned long long int n){
	return n * ((n - 1 > 1) ? factorial(n - 1) : 1);
}
int main(){
	unsigned long long int input;
	cin >> input;
	cout << factorial(input) << endl;
	return 0;
}
