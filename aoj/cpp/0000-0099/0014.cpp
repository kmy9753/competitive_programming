#include <iostream>

using namespace std;
int func(int x){
	return x * x;
}
int main(){
	int d;
	while(cin >> d){
		const int x = 600;
		int size = 0;
		for(int k = 0; k < x; k += d){
			size += d * func(k);
		}
		cout << size << endl;
	}
	return 0;
} 
