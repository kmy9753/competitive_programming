#include <iostream>
#include <vector>
using namespace std;
int main(){
	vector<int> data;
	vector<int> results;
	int currentData;
	while(cin >> currentData){
		if(currentData != 0){
			data.push_back(currentData);
		}
		else{
			results.push_back(data.back());
			data.pop_back();
		}
	}
	for(int i = 0; i < results.size(); i ++){
		cout << results[i] << endl;
	}
	return 0;
}
