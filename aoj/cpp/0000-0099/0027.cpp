#include <iostream>
#include <string>

using namespace std;

const int MONTHS_IN_YEAR = 12;
const int DAYS_IN_WEEK = 7;
const int numOfDays[MONTHS_IN_YEAR] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
const string days[DAYS_IN_WEEK] = {"Wednesday", "Thursday", "Friday",
    "Saturday", "Sunday", "Monday", "Tuesday"};

int main(){
    for(int month, day, sumOfDays; cin >> month >> day, sumOfDays = day; cout << days[sumOfDays % DAYS_IN_WEEK] << endl)
        for(int i = 0; i < month - 1; i ++) sumOfDays += numOfDays[i];

    return 0;
}
