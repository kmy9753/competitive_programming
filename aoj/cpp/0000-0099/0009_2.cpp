#include <iostream>

#define rep(i, n) for(int i = 2; i < n; i ++)

using namespace std;

int main(void){
	const int N = (int)1e6;
	bool num[N];
	rep(i, N) num[i] = true;
	for(int i = 2; i * i < N; i ++) if(num[i]) for(int j = i; i * j < N; j ++) num[i * j] = false;

	for(int n, count = 0; cin >> n; cout << count << endl, count = 0)
		rep(i, n + 1) if(num[i]) count ++;

	return 0;
}
