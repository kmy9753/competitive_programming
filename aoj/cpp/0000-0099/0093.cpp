#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 3001;

int main(void){
    bool isL[N];
    rep(i, N) isL[i] = (!(i % 4) && (i % 100 || !(i % 400)));

    for(int a, b, cnt = 0; cin >> a >> b, a;){
        if(cnt ++) cout << endl;

        bool ex = false;
        for(; a <= b; a ++) if(isL[a]) ex = cout << a << endl;

        if(!ex) cout << "NA\n";
    }
 
    return 0;
}
