#include <iostream>
#include <string>
#include <locale>

using namespace std;

int main(void){
	string str; getline(cin, str);

	for(int i = 0; i < str.length(); i ++){
		locale loc;
		cout << toupper(str[i], loc);
	}
	cout << endl;

	return 0;
}
