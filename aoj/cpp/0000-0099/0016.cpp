#include <iostream>
#include <cmath>
#include <cstdio>

#define PI 3.141592653589793
#define RAD(d) (d * PI / 180)

using namespace std;

class People{
private:
	int angle;
	double x, y;

public:
	People(){
		angle = 90;
		x = y = 0;
	}
	void move(int distance, int relativeAngle){
		x += distance * cos(RAD(angle));
		y += distance * sin(RAD(angle));
		angle -= relativeAngle;
	}
	double getX(){ return x; }
	double getY(){ return y; } 
};

int main(){
	People me;
	for(int distance, relativeAngle; ~scanf("%d,%d", &distance, &relativeAngle), distance;)
		me.move(distance, relativeAngle);
	cout << (int)me.getX() << endl << (int)me.getY() << endl;

	return 0;
}

