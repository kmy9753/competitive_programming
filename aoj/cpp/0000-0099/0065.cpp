#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    string in;
    vector<set<int>> s1(10000), s2(10000);

    while(getline(cin, in), in.size()){
        int a, b; char c;
        stringstream ss(in);

        ss >> a >> c >> b;

        s1[a].insert(b);
    }

    while(getline(cin, in)){
        int a, b; char c;
        stringstream ss(in);

        ss >> a >> c >> b;

        s2[a].insert(b);
    }

    rep(i, 10000){
        if(s1[i].size() > 0 && s2[i].size() > 0){
            cout << i << " " << s1[i].size() + s2[i].size() << endl;
        }
    }

	return 0;
}
