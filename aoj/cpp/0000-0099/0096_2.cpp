#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 4001;
const int M = 1001;

int dp[5][N];

int main(void){
    rep(i, 5) fill(dp[i], dp[i] + N, 0);
    dp[0][0] = 1;

    COUNT(i, 4) rep(j, N) if(dp[i - 1][j]) rep(k, M) dp[i][j + k] += dp[i - 1][j];

    for(int n; cin >> n; cout << dp[4][n] << endl);

    return 0;
}
