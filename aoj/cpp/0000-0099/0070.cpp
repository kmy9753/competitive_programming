#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int n, s;
vector<int> used(10);
int memo[11][1000];
int sum;
void dfs(int d){
    memo[d][sum]++;
    if(d == 10) return;

    rep(i, 10){
        if(used[i]) continue;

        used[i] = true;
        sum += (d + 1) * i;

        dfs(d + 1);

        used[i] = false;
        sum -= (d + 1) * i;
    }
}

int main(void){
    dfs(0);
    while(cin >> n >> s){
        if(s >= 1000) cout << 0 << endl;
        else         cout << memo[n][s] << endl;
    }

	return 0;
}
