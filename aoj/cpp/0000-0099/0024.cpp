#include <iostream>

using namespace std;

double funcT(double v){
	return v / 9.8;
}

double funcY(double t){
	return 4.9 * t * t;
}

const int oneFloor = 5;

int main(){
    int minFloor;
	for(double minSpeed; cin >>minSpeed; cout << minFloor << endl){ 
		double minY = funcY(funcT(minSpeed));

		minFloor = 1 + (int)minY / oneFloor + ((minY/(double)oneFloor == (int)minY / oneFloor) ? 0 : 1);
	}

	return 0;
}
