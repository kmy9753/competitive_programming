#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(T) __typeof((T).begin())
#define each(T, it) for(ITER(T) it =(T).begin(); it!=(T).end(); it++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
 
int main(void){
    for(int n, m; cin >> n >> m, n;){
        vector<int> p;
        rep(i, n) p.pb(i + 1);

        int cur = -1;
        while(p.size() - 1)
            p.erase((cur = (cur + m) % p.size()) -- + p.begin());

        cout << *p.begin() << endl;
    }
 
    return 0;
}
