#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>

#define rep(i, n) for(int i = 0; i < n; i ++)

using namespace std;

int main(){
	int w; cin >> w;
	vector<int> amida;
	rep(i, w) amida.push_back(i + 1);
	int n; cin >> n;
	while(n --){
		int before, after;
		scanf("%d,%d", &before, &after);
		swap(amida[before - 1], amida[after - 1]);
	}
	rep(i, w) cout << amida[i] << endl;
	
	return 0;
}
