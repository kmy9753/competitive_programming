#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
	int w;
	cin >> w;
	vector<int> amida;
	for(int i = 0; i < w; i ++){
		amida.push_back(i + 1);
	}
	int n;
	cin >> n;
	const int SIZE = 2;
	while(n--){
		int before, after;
		scanf("%d,%d", &before, &after);
		swap(amida[before - 1], amida[after - 1]);
	}
	reverse(amida.begin(), amida.end());
	while(w --){
		cout << amida[w] << endl;
	}
	return 0;
}
