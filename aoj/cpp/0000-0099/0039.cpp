#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const string ROMA = "IVXLCDM";
const int N[] = {1, 5, 10, 50, 100, 500, 1000};

int main(void){
    map<char, int> cor;
    rep(i, 7) cor[ROMA[i]] = N[i];

    int res, pre;
    for(string in; res = pre = 0, cin >> in; cout << res << endl)
        rep(i, in.size()){
            int cur = cor[in[i]];
            res += cur - (pre < cur ? 2 * pre:0);
            pre = cur;
        }
 
    return 0;
}
