#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 100;

int dp[10][N + 1] = {0};

int main(void){
    dp[0][0] = dp[1][0] = 1;
    for(int i = 1; i <= 9; i ++)
        for(int j = N; 0 <= j; j --)
            rep(k, 10) if(dp[k][j] && j + i <= N) dp[k + 1][j + i] += dp[k][j];

    for(int n, s; cin >> n >> s, n; cout << dp[n][s] << endl);
 
    return 0;
}
