#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
 
using namespace std;
 
typedef pair<int, int> pii;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef vector<string> vs;
 
int main(void){
    for(int w, n, cnt = 0; cin >> w >> n, w;){
        vi dp(w + 1); fill(ALL(dp), -1);
        dp[0] = 0;

        char c;
        while(n --){
            int tv, tw; cin >> tv >> c >> tw;
            for(int i = w; 0 <= i; i --) if(i + tw <= w && dp[i] >= 0) dp[i + tw] = max(dp[i + tw], dp[i] + tv);
        }
        
        vi::iterator it = max_element(ALL(dp));
        cout << "Case " << ++ cnt << ":" << endl << *it << endl << it - dp.begin() << endl;
    }
 
    return 0;
}
