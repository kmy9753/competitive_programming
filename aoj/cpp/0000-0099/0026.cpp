#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 10;

const int S[][5] = {0, 0, 0, 0, 0,
                    0, 0, 1, 0, 0, 
                    0, 1, 1, 1, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 0, 0};

const int M[][5] = {0, 0, 0, 0, 0,
                    0, 1, 1, 1, 0,
                    0, 1, 1, 1, 0,
                    0, 1, 1, 1, 0,
                    0, 0, 0, 0, 0};

const int L[][5] = {0, 0, 1, 0, 0,
                    0, 1, 1, 1, 0,
                    1, 1, 1, 1, 1,
                    0, 1, 1, 1, 0,
                    0, 0, 1, 0, 0};

const int (*diff[3])[5] = {S, M, L};

int m[N][N] = { 0 };

int main(void){
    char c;
    for(int x, y, t; cin >> x >> c >> y >> c >> t;){
        x -= 2; y -= 2;
     
        rep(dy, 5) rep(dx, 5)
            if(0 <= x + dx && x + dx < N &&
               0 <= y + dy && y + dy < N)
                m[y + dy][x + dx] += diff[t - 1][dy][dx];
    }
 
    cout << count(&m[0][0], &m[0][0] + N * N, 0) << endl << *max_element(&m[0][0], &m[0][0] + N * N) << endl;
    return 0;
}
