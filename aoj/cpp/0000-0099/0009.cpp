#include <iostream>
#include <vector>

using namespace std;

bool judge(int);

int main(){
	const int MAX_VALUE = (int)1e7;
	vector<bool> primeNums(MAX_VALUE);
	
	for(int i = 1; i < MAX_VALUE; i ++)
		primeNums[i]=judge(i);

	for(int n, count = 0; cin >> n; cout << count << endl, count = 0)
		for(int i=1; i<=n; i++) if(primeNums[i]) count++;

	return 0;
}

bool judge(int n){
	if(n==2||n==3) return true;
	if(n==1||n%2==0) return false;
	for(int i = 3; i*i <= n; i += 2)
		if(n%i==0) return false;

	return true;
}
