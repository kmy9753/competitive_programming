#include <iostream>
#include <iomanip>
void solver(double, double, double, double, double, double, double*, double*);
double roundOff(double,int);
int main(){
	using namespace std;
	double a,b,c,d,e,f;
	int const FLOAT_NUM=3;
	while(cin >> a >> b >> c >> d >> e >> f){
		double x,y;
		solver(a,b,c,d,e,f,&x,&y);
		x = roundOff(x,FLOAT_NUM);
		y = roundOff(y,FLOAT_NUM);
		cout.setf(ios::showpoint);
		cout << setprecision(4) << x << " " << y << endl;
	}
	return 0;
}

void solver(double a, double b, double c, double d, double e, double f, double* x, double* y){
	b/=a;c/=a;a/=a;
	e/=d;f/=d;d/=d;
	
	a-=d;b-=e;c-=f;
	*y=c/b;

	*x=f-*y*e; 
}

double roundOff(double a,int num){
	a*=1000;
	a+=0.5;
	a = (int)a;
	a/=1000.;
	return a;
}

