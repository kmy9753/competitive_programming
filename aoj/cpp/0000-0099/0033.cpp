#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <utility>
#include <cstring>
#include <string>
#include <sstream>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

vector<int> in(10);
string res;

void dfs(int, int, int);

int main(void){
    int n; cin >> n;
    while(n --){
        res = "NO";
        rep(i, 10) cin >> in[i];

        dfs(0, 0, 0);
        cout << res << endl;
    }

    return 0;
}

void dfs(int left, int right, int cur){
    if(cur == in.size()) res = "YES";
    if(res == "YES") return;

    if(left < in[cur]) dfs(in[cur], right, cur + 1);
    if(right < in[cur]) dfs(left, in[cur], cur + 1);
}
