#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

int main(void){
    int n; cin >> n;

    while(n --){
        double x1, y1, x2, y2, x3, y3;
        cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;

        double a1 = 2 * (x2 - x1), a2 = 2 * (x3 - x1),
               b1 = 2 * (y2 - y1), b2 = 2 * (y3 - y1),
               c1 = x1 * x1 - x2 * x2 + y1 * y1 - y2 * y2,
               c2 = x1 * x1 - x3 * x3 + y1 * y1 - y3 * y3,
               px = (b1 * c2 - b2 * c1) / (a1 * b2 - a2 * b1),
               py = (c1 * a2 - c2 * a1) / (a1 * b2 - a2 * b1),
               r = sqrt(pow(x1 - px, 2) + pow(y1 - py, 2));

        printf("%.3f %.3f %.3f\n", px, py, r);
    }
 
    return 0;
}
