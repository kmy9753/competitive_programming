#include <iostream>

int gcd(int,int);
int lcm(int,int);

int main(){
	using namespace std;
	int a,b;

	while(cin >> a >> b)
		cout << gcd(a,b) << " " << lcm(a,b) << endl;

	return 0;
}

int gcd(int x, int y){
	int r;
	while((r=x%y)!=0){
		x=y;
		y=r;
	}
	return y;
}

int lcm(int x, int y){
	return x/gcd(x,y)*y;
}
