#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <complex>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define X real()
#define Y imag()

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;
typedef complex<double> P;

const int INF = 1 << 24;

int main(void){
    string res;
    for(vector<P> p(4); cin >> p[0].X >> p[0].Y; cout << res << endl){
        COUNT(i, 3) cin >> p[i].X >> p[i].Y;
        
        vector<P> pos(2);
        rep(i, 2) pos[i].X = p[2 * i + 1].X - p[2 * i].X,
                  pos[i].Y = p[2 * i + 1].Y - p[2 * i].Y;

        res = (fabs(pos[0].X * pos[1].X + pos[0].Y * pos[1].Y) < 1e-10) ? "YES":"NO";
    }
 
    return 0;
}
