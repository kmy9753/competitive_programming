#include  <bits/stdc++.h>
#define range(i,a,b) for(int (i)=(a);(i)<(b);(i)++)
#define rep(i,n) range(i,0,n)
using namespace std;

typedef bool B;
typedef long double D;
typedef complex<D> P;
typedef vector<P> VP;
struct L{
    P s,t;
    L(P a, P b) : s(a), t(b) {}
};
typedef vector<L> VL;
typedef struct {P c;D r;} C;
typedef vector <C> VC;

const D eps=1.0e-10;
const D pi=acos(-1.0);
template<class T> bool operator==(T a, T b){return abs(a-b)< eps;}
template<class T> bool operator< (T a, T b){return a < b-eps;}
template<class T> bool operator<=(T a, T b){return a < b+eps;}
//template<class T> int sig(T r) {return (r==0||r==-0) ? 0 : r > 0 ? 1 : -1;}
template<class T> int sig(T a,T b = 0) {return a < b ? -1 : b > a ? 1 : 0;}
#define X real()
#define Y imag()

D ip(P a, P b) {return a.X * b.X + a.Y * b.Y;}
D ep(P a, P b) {return a.X * b.Y - a.Y * b.X;}
D sq(D a) {return sqrt(max(a, (D)0));}
P vec(L l){return l.t-l.s;}
inline P input(){D x,y;char c;cin >> x >> c >> y; return P(x,y);}
//以上 テンプレート部分

B iLS(L a,L b){return sig(ep(vec(a),b.s-a.s))*sig(ep(vec(a),b.t-a.s)) <= 0;}
B iLSs(L a,L b){return sig(ep(vec(a),b.s-a.s))*sig(ep(vec(a),b.t-a.s)) < 0;}

// 点と直線の距離
D dLP(L l,P p){return abs( ep(vec(l),p-l.s) )/abs(vec(l));}

int main(void){
    D x, y; char c;
    while(cin >> x >> c >> y >> c){
        P p1(x, y);
        P p2 = input(); cin >> c;
        P pq = input();

        P u;
        if(abs(p2.Y - p1.Y) > eps) u = P(1.0, -(p2.X - p1.X) / (p2.Y - p1.Y) );
        else u = P(0.0, 1.0);
        u /= sqrt(norm(u));

        //printf("u = %.12Lf %.12Lf\n", u.X, u.Y);

        D d = dLP(L(p1, p2), pq);
        P pp1 = pq + u * d * 2.0L;
        P pp2 = pq - u * d * 2.0L;

        //cerr << "pp1: " << pp1.X << " " << pp1.Y << endl;
        //cerr << "pp2: " << pp2.X << " " << pp2.Y << endl;

        //P res;
        //if(iLSs(L(p1, p2), L(pq, pp1))) res = pp1;
        //else res = pp2;

        printf("%.12Lf %.12Lf\n", pp2.X, pp2.Y);
    }

	return 0;
}
