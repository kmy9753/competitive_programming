#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <complex>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define X real()
#define Y imag()

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;
typedef complex<double> P;

const int INF = 1 << 24;

int main(void){
    for(vector<P> p(4); cin >> p[0].X >> p[0].Y; cout << ((p[0].X <= p[3].X && p[2].Y <= p[1].Y && p[2].X <= p[1].X && p[0].Y <= p[3].Y) ? "YES":"NO") << endl)
        COUNT(i, 3) cin >> p[i].X >> p[i].Y;
 
    return 0;
}
