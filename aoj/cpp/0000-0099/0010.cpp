#include <iostream>
#include <iomanip>

class Point{
public:
	double x,y;
	Point(double,double);
	Point();
	Point operator+(const Point& p0) const {
		Point p;
		p.x = x + p0.x;
		p.y = y + p0.y;
		return p;
	}
	Point operator - (const Point& p0) const { 
		Point p;
		p.x = x - p0.x;
		p.y = y - p0.y;
		return p;
	}
};

class Line : public Point{
public:
	double a, b, c;
	Line(Point, Point);
	Line(Point, double);
	double getX(double);
	double getY(double);
};

Point::Point(double xx, double yy){
	x = xx;
	y = yy;
}

Point::Point(){
	x = 0;
	y = 0;
}

Line::Line(Point p0, Point p){
	a = (p.x - p0.x != 0) ? ((p.y - p0.y) / (p.x - p0.x)):1;
	b = (p.x - p0.x != 0) ? (p.y - a * p.x):(-p.x);
	c = (p.x - p0.x != 0) ? 1:0;
}

Line::Line(Point p, double m){
	a = m;
	b = p.y - a * p.x;
}

double Line::getX(double y){
	return (c * y - b) / a;
}

double Line::getY(double x){
	return a * x + b;
}

Point calcCCirclePoint(Point, Point, Point);

int main(){
	using namespace std;
	
	Point a, b, c;
	int setNum; cin >> setNum;

	while(setNum -- > 0){
		cin >> a.x >> a.y >> b.x >> b.y >> c.x >> c.y;
		Point p = calcCCirclePoint(a, b, c);
		cout.setf(ios::showpoint);
		cout << setprecision(4) << p.x << " "  << p.y << endl;
	}

	return 0;
}

Point calcCCirclePoint(Point a, Point b, Point c){
	Point p;
	Line lab(a, b);
	Line lbc(b, c);
	//
	std::cout << lab.a << lab.b << std::endl;
	std::cout << lbc.a << lbc.b << std::endl;
	//
	Point m((a.x + b.x)/2, (a.y + b.y)/2);	
	Point n((c.x + b.x)/2, (c.y + b.y)/2);	
	Line lmp(m, -1/lab.a);
	Line lnp(n, -1/lbc.a);
	p.x = (lnp.b - lmp.b) / (lmp.a - lnp.a);
	p.y = lmp.getY(p.x);
	
	return p;
}
