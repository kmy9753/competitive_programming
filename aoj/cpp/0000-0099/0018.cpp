#include<cstdio>
#include<algorithm>
#include<vector>
using namespace std;
int main(){
	int in[5];
	scanf("%d %d %d %d %d", &in[0], &in[1], &in[2], &in[3], &in[4]);
	vector<int> input;
	for(int i=0;i<5;i++) input.push_back(in[i]);
	sort(input.begin(), input.end());
	reverse(input.begin(), input.end());
	printf("%d %d %d %d %d\n",
			input[0], input[1], input[2], input[3], input[4]);
	return 0;
}
	
