#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

typedef pair<int, int> P;

int V; // the number of node

int prim (vector<vector<P> > G) {
    set<int> used;
    priority_queue<P, vector<P>, greater<P> > que;
    for (int i = 0; i < V; i++) {
        used.insert(i);
    }
    int res = 0;
    int v = 0;
    while (!used.empty()) {
        for (auto i : G[v])   que.push(i);
        P p;
        while (used.find(que.top().second) == used.end() || que.top().second == v) {
            que.pop();
            if (que.empty()) break;
        }
        if (que.empty()) break;
        used.erase(used.find(v));
        p = que.top(); que.pop();
        v = p.second;
        res += p.first;
    }
 
    return res;
}

int main(void){
    for(; cin >> V, V;){
        int m; cin >> m;
        vector<vector<P>> p(V);
        rep(loop, m){
            int a, b, d; char c;
            cin >> a >> c >> b >> c >> d;

            p[a].push_back(P(d, b));
            p[b].push_back(P(d, a));
        }

        cout << prim(p) / 100 - (V - 1) << endl;
    }

    return 0;
}
