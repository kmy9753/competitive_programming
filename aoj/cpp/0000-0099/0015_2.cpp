#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
 
int main(void){
    int n; cin >> n;

    while(n --){
        vi res(81);

        rep(i, 2){
            string in; cin >> in;
            if(in.size() > 80){ res[80] = 1; continue; }
            rep(j, in.size() + 1){
                if(j != in.size()) res[j] += in.at(in.size() - j - 1) - '0';
                for(int l = 0; res[j + l] > 9; l ++) res[j + l + 1] ++, res[j + l] %= 10;
            }
        }

        if(res[80]){ cout << "overflow" << endl; continue; }

        bool ok = false;
        for(int i = 79; i >= 0; i --){
            if(!ok) if(res[i] || !i) ok = true;
            if(ok) cout << res[i];
        }
        cout << endl;
    }
 
    return 0;
}
