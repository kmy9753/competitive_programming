#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <complex>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define X real()
#define Y imag()

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;
typedef complex<double> P;

const int INF = 1 << 24;

bool isNoConv(P, P, P, P);

int main(void){
    for(vector<P> p(4);;){
        if(!~scanf("%lf,%lf", &p[0].X, &p[0].Y)) break;
        char c;
        COUNT(i, 3) cin >> c >> p[i].X >> c >> p[i].Y;
        
        cout << ((isNoConv(p[0], p[2], p[1], p[3]) || isNoConv(p[1], p[3], p[0], p[2])) ? "NO":"YES") << endl;
    }
 
    return 0;
}

bool isNoConv(P p1, P p2, P p3, P p4){
    return ((p1.X - p2.X) * (p3.Y - p1.Y) - 
            (p1.Y - p2.Y) * (p3.X - p1.X)) * 
           ((p1.X - p2.X) * (p4.Y - p1.Y) - 
            (p1.Y - p2.Y) * (p4.X - p1.X)) > 0;
}
