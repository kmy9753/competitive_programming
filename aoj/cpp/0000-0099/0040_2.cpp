#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
 
int main(void){
    int n; cin >> n;
    cout << n << endl;

    string in; getline(cin, in);
    while(n --){
        getline(cin, in);

        int cur = 0;
        while(++cur, !~in.find("that") && !~in.find("this")){
            rep(i, in.size()){
                if(in[i] != ' ') in[i] = 'a' + ((in[i] - 'a' + cur) % 26);
            }
        }

        cout << in << endl;
    }
 
    return 0;
}
