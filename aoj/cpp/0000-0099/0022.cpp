#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int searchMaximumSumSequence(vector<int> array){
	while(1){
		int proCount = 0;
		vector<int> newArray;
		for(int i = 1; i < array.size(); i ++){
			if(array[i-1] * array[i] >= 0){
				newArray.push_back(array[i-1] + array[i]);
				i ++;
				proCount ++;
				if(i == array.size()-1){
					newArray.push_back(array[i]);
				}
			}
			else{
				newArray.push_back(array[i - 1]);
			}
		}
		array = newArray;
		if(proCount == 0) break;
		else{;}
	}
	vector<int>::iterator max = max_element(array.begin(), array.end());

	return *max;
}
int main(){
	while(1){
		int numOfElements;
		cin >> numOfElements;
		if(numOfElements == 0){
			break;
		}
		else{;}
		vector<int> inputs;
	       	for(int i = 0; i < numOfElements; i ++){
			int element;
			cin >> element;
			inputs.push_back(element);
		}
		cout << searchMaximumSumSequence(inputs) << endl;
	}
	return 0;
}

