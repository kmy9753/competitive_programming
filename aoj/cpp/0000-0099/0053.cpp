#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int MAX_P = (int)2e6;
const int N = 10001;

int main(void){
    bool isP[MAX_P];
    MEMSET(isP, true);
    rep(i, 2) isP[i] = false;
    rep(i, MAX_P) if(isP[i]) for(ll j = i; i * j < MAX_P; j ++) isP[i * j] = false;
    
    vi res(N);
    int cur = 0;
    rep(i, N){
        if(!i) continue;
        while(!isP[++cur]);
        res[i] = res[i - 1] + cur;
    }

    for(int n; cin >> n, n; cout << res[n] << endl);

    return 0;
}
