#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    for(int h, m, s; cin >> h >> m >> s, h != -1;){
        h = 2 - h, m = -m, s = -s;
        if(s < 0) m--, s += 60;
        if(m < 0) h--, m += 60;
        printf("%02d:%02d:%02d\n", h, m, s);

        h *= 3, m *= 3, s *= 3;
        if(s > 60) m += (s / 60), s %= 60;
        if(m > 60) h += (m / 60), m %= 60;
        printf("%02d:%02d:%02d\n", h, m, s);
    }

    return 0;
}
