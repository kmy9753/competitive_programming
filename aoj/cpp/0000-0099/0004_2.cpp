#include <iostream>
#include <cstdio>

using namespace std;

double roundOff(double);

int main(void){
	for(int a, b, c, d, e, f; cin >> a >> b >> c >> d >> e >> f;){
		double delta = a * e - b * d;
		double x = roundOff((c * e - b * f) / delta);
		double y = roundOff((a * f - c * d) / delta);

        printf("%.3lf %.3lf\n", x, y);
	}
	return 0;
}

double roundOff(double a){
	a*=1000;
	a = int(a + 0.5 - (a < 0));
	a/=1000.;
	return a;
}
