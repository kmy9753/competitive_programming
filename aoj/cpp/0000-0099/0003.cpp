#include <iostream>
#include <algorithm>
#include <vector>
int main(){
	using namespace std;
	vector<int> sides(3);
	int setNum;
	cin >> setNum;

	for(;setNum>0;setNum--){
		cin >> sides[0] >> sides[1] >> sides[2];
		sort(sides.begin(),sides.end());
		if(sides[0]*sides[0]+sides[1]*sides[1]==sides[2]*sides[2])
			cout << "YES" << endl;
		else
			cout << "NO" << endl; 
	}

	return 0;
}
