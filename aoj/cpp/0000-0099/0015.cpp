#include <iostream>
#include <vector>
#include <string>
using namespace std;

vector<int> addToArray(string a, string b, vector<int> array, int carryNum){
	int a1 = 0;
	int a2 = 0;
	if(a.size() > 0){
		a1 = (int)(a[a.size() - 1] - '0');
		a.resize(a.size() - 1);
	}
	if(b.size() > 0){
		a2 = (int)(b[b.size() - 1] - '0');
		b.resize(b.size() - 1);
	}
	int c = a1 + a2 + carryNum;
	int carry = 0;
	if(c > 9){
		c %= 10;
		carry = 1;
	}
	if((a.size() > 0) || (b.size() > 0) || (carry == 1)){
		array = addToArray(a, b, array, carry);
	}
	array.push_back(c);
	return array;
}
int main(){
	int n;
	cin >> n;
	while(n --){
		string inA, inB; cin >> inA >> inB;
		vector<int> result;
		result = addToArray(inA, inB, result, 0);
		if(result.size() > 80){
			cout << "overflow" << endl;
		}
		else{
			for(int i = 0; i < result.size(); i ++){
				cout << result[i];
			}
			cout << endl;
		}
	}
	return 0;
}
