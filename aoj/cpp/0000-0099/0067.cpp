#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <utility>
#include <cstring>
#include <string>
#include <sstream>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int dx[] = {-1, 0, 0, 1};
const int dy[] = {0, 1, -1, 0};
int cnt;
const int N = 12;
vector<string> table(N);

void dfs(int, int);

int main(void){
    for(string in; !cin.eof(); getline(cin, in), cout << cnt << endl){
        rep(i, N) getline(cin, table[i]);

        cnt = 0;
        rep(y, N) rep(x, N) if(table[y].at(x) == '1') cnt ++, run(x, y);
    }

    return 0;
}

void dfs(int x, int y){
    table[y][x] = '0';
    rep(i, 4){
        int xx = x + dx[i], yy = y + dy[i]; 
        if(0 <= xx && xx < N && 0 <= yy && yy < N && table[yy][xx] == '1')
            run(xx, yy);
    }
}
