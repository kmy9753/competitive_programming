#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 12;
const int dx[4] = {-1, 0, 1, 0};
const int dy[4] = {0, -1, 0, 1};

vs m(N);

void dfs(int, int);

int main(void){
    int cnt;
    for(string in; cin >> in; cout << cnt << endl){
        m[0] = in;
        COUNT(i, N - 1) cin >> in, m[i] = in;

        cnt = 0;
        rep(x, N) rep(y, N) if(m[y][x] == '1') dfs(x, y), cnt++;
    }

    return 0;
}

void dfs(int x, int y){
    if(x < 0 || N <= x ||
       y < 0 || N <= y) return;
    if(m[y][x] == '0') return;
    
    m[y][x] = '0';
    rep(i, 4) dfs(x + dx[i], y + dy[i]);
}
