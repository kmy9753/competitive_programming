#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 5;
const string perm = "12345678910111213110111213";

int main(void){
    string res;
    for(vi in(N); cin >> in[0]; cout << res << endl){
        COUNT(i, N - 1){ char c; cin >> c >> in[i]; }
        set<int> u; rep(i, N) u.insert(in[i]);

        vi cnt;
        each(u, it) cnt.pb(count(ALL(in), *it));
        sort(ALL(cnt), greater<int>());

        switch(*cnt.begin()){
            case 4: 
                res = "four card";
                break;
            case 3:
                if(*(cnt.begin() + 1) == 2) res = "full house";
                else res = "three card";
                break;
            case 2:
                if(*(cnt.begin() + 1) == 2) res = "two pair";
                else res = "one pair";
                break;
            default:
                stringstream ss;
                sort(ALL(in));
                rep(i, N) ss << in[i];
                if(~perm.find(ss.str())) res = "straight";
                else res = "null";
                break;
        }
    }
 
    return 0;
}
