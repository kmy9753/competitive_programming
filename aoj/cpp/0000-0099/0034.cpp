#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 10;

int main(void){
    for(;;){
        vi l(N), v(2);
        double dis = 0; char c;
        rep(i, N){ if(i) cin >> c; if(!(cin >> l[i])) return 0; dis += l[i]; }
        rep(i, 2) cin >> c >> v[i];

        dis = v[0] * dis / (v[0] + v[1]);

        int res = 0; double cur = 0;
        rep(i, N){
            cur += l[i];
            if(dis <= cur){ res = i + 1; break; }
        }
        cout << res << endl;
    }
 
    return 0;
}
