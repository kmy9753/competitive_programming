#include <iostream>

int calcDebt(int, int);
int castThousand(int);
int main(){
	using namespace std;

	int const INIT_DEBT = 100000;
	int n;
	cin >> n;
	cout << calcDebt(INIT_DEBT, n) << endl;
	return 0;
}

int calcDebt(int currentDebt, int restWeek){
	if(restWeek == 0)
		return currentDebt;
	//first calc
	currentDebt = currentDebt / 100 * 105;
	
	//second calc
	if(currentDebt - castThousand(currentDebt) > 0)
		currentDebt = castThousand(currentDebt) + 1000;

	return calcDebt(currentDebt, --restWeek);
}

int castThousand(int num){
	return num / 1000 * 1000;
}
