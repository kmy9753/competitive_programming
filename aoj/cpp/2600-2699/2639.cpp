#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int a, b, c; cin >> a >> b >> c;
    int t = 0;
    int res = 0;
    vector<vector<int>> visited(121, vector<int>(2, 0));
    visited[0][0] = 1;
    bool ok = false;

    while(1){
        int nt = t;
        nt += a;
        res += a;
        if((t <= c      and c      <= nt) or
           (t <= c + 60 and c + 60 <= nt)){
            res -= (nt - c) % 60;
            ok = true;
            break;
        }
        nt %= 60;
        if(visited[nt][1] != 0) break;
        visited[nt][1] = 1;

        t = nt;
        nt += b;
        res += b;
        nt %= 60;
        if(visited[nt][0] != 0) break;
        visited[nt][0] = 1;

        t = nt;
    }

    if(not ok) res = -1;
    cout << res << endl;

	return 0;
}
