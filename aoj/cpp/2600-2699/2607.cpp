#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int kMax_x = 100001;

int main(void){
    for(int n, d, x; cin >> n >> d >> x; cout << x << endl){
        vvi p(d, vi(n));
        rep(i, d) rep(j, n) cin >> p[i][j];

        rep(i, d - 1){
            vi dp(x + 1);
            rep(j, x + 1) dp[j] = j;

            rep(j, n){
                rep(k, x + 1){
                    if(k - p[i][j] >= 0){
                        dp[k] = max(dp[k], dp[k - p[i][j]] + p[i + 1][j]);
                    }
                }
            }

            x = *max_element(all(dp));
        }
    }

	return 0;
}
