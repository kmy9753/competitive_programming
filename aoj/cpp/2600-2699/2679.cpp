#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int INF = 1000;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using W = vi;
using edge = struct {int to, rev, cap, flow; W cost;};
using G = vector<vector<edge>>;

const int N = 52;

W operator+(const W& lhs, const W& rhs){
    vi ret = lhs;
    rep(i, lhs.size()){
        ret[i] += rhs[i];
    }
    return ret;
}
W operator+=(W& lhs, const W& rhs){
    rep(i, lhs.size()){
        lhs[i] += rhs[i];
    }
    return lhs;
}
W operator-=(W& lhs, const W& rhs){
    rep(i, lhs.size()){
        lhs[i] -= rhs[i];
    }
    return lhs;
}
W operator*(const int& lhs, const W& rhs){
    W ret = rhs;
    rep(i, ret.size()){
        ret[i] *= lhs;
    }
    return ret;
}
W operator-(W x){
    rep(i, x.size()){
        x[i] *= -1;
    }
    return x;
}

void add_edge(G &graph, int from, int to, int cap, W cost) {
    graph[from].push_back({to, int(graph[to].size()) , cap , 0 , cost});
    graph[to].push_back({from, int(graph[from].size()) - 1, 0 , 0, -cost});
}

W primal_dual(G &graph, int s, int t, int f) {
    const W inf = W(N, INF);
    W res = W(N, 0);
    while (f) {
        int n = graph.size(), update;
        vector<W> dist(n, inf);
        vector<int> pv(n, 0), pe(n, 0);
        dist[s] = vi(N);

        rep(loop, n) {
            update = false;
            rep(v, n)rep(i, graph[v].size()) {
                edge &e = graph[v][i];
                if (e.cap > e.flow and chmin(dist[e.to], dist[v] + e.cost)) {
                    pv[e.to] = v, pe[e.to] = i;
                    update = true;

                }
            }
            if (!update) break;
        }

        if (dist[t] == inf) return inf;

        int d = f;

        for (int v = t; v != s; v = pv[v]){
            chmin(d, graph[pv[v]][pe[v]].cap - graph[pv[v]][pe[v]].flow);
        }

        f -= d, res += d * dist[t];

        for (int v = t; v != s; v = pv[v]) {
            edge &e = graph[pv[v]][pe[v]];
            e.flow += d;
            graph[v][e.rev].flow -= d;
        }
    }
    return res;
}

W i2v(int i){
    W ret(N);
    if(i >= 0) ret[i] = -1;
    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    vector<string> plates(n); for(auto& e : plates) cin >> e;

    G graph(2 * n + 2);
    int s = 2 * n, t = s + 1;
    map<char, int> c2i;
    map<int, char> i2c;
    int cnt = 0;
    for(auto c = 'A'; c <= 'Z'; c++) { c2i[c] = cnt; i2c[cnt] = c; cnt++; }
    for(auto c = 'a'; c <= 'z'; c++) { c2i[c] = cnt; i2c[cnt] = c; cnt++; }

    rep(i, n){
        rep(j, n){
            int cur = c2i[plates[i][j]];
            add_edge(graph, i, n + j, 1, i2v(cur));
        }
    }
    rep(i, n){
        add_edge(graph, s, i, 1, i2v(-1));
        add_edge(graph, n + i, t, 1, i2v(-1));
    }

    W tbl = primal_dual(graph, s, t, n);
    string res;
    rep(i, N){
        if(tbl[i] < 0) res += string(-tbl[i], i2c[i]);
    }
    cout << res << endl;

    return 0;
}
