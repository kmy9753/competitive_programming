#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

string s, t;

void zAlgorithm(string & S, vi & zTable){
        zTable[0] = S.size();
        int i = 1, j = 0;
        while (i < S.size()) {
            while (i+j < S.size() && S[j] == S[i+j]) ++j;
            zTable[i] = j;
            if (j == 0) { ++i; continue;}
            int k = 1;
            while (i+k < S.size() && k+zTable[k] < j) zTable[i+k] = zTable[k], ++k;
            i += k; j -= k;
        }

        zTable.erase(zTable.begin(), zTable.begin() + t.size() + 1);
}

int main(void){
    for(; cin >> s >> t;){
        string S = t + "$" + s;
        vi zTable(S.size());
        zAlgorithm(S, zTable);

        string srev = s, trev = t;
        reverse(all(srev));
        reverse(all(trev));
        string Srev = trev + "$" + srev;
        vi zTable_rev(Srev.size());
        zAlgorithm(Srev, zTable_rev);

        int res = 0;
        rep(i, s.size() - t.size() + 1){
            if(zTable[i] + zTable_rev[s.size() - (i + t.size())] + 1 == t.size()){
                res++;
            }
        }

        cout << res << endl;
    }

	return 0;
}
