#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define left first
#define right second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    string res;
    for(int n, w, h; cin >> n >> w >> h; cout << res << endl){
        res = "No";

        vector<pii> wifix(n), wifiy(n);
        rep(i, n){
            int x, y, range; cin >> x >> y >> range;

            wifix[i].left = max(x - range, 0);
            wifix[i].right = min((ll)x + range, (ll)w);
            wifiy[i].left = max(y - range, 0);
            wifiy[i].right = min((ll)y + range, (ll)h);
        }

        rep(i, 2){
            vector<pii> wifi;
            int len;
            if(i){
                wifi = wifix;
                len = w;
            }
            else{
                wifi = wifiy;
                len = h;
            }

            sort(all(wifi));

            bool ok = true;
            if(wifi[0].left != 0){
                ok = false;
            }
            int rightedge = wifi[0].right;
            range(i, 1, n){
                if(rightedge < wifi[i].left){
                    ok = false;
                    break;
                }
                rightedge = max(rightedge, wifi[i].right);
            }
            if(rightedge < len){
                ok = false;
            }

            if(ok){
                res = "Yes";
                break;
            }
        }
    }

	return 0;
}
