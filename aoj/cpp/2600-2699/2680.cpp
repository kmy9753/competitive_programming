#include <bits/stdc++.h>

#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)

#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)

#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))

// #define DEBUG

#ifdef DEBUG
#define dump(...) fprintf(stderr, __VA_ARGS__)
#else
#define dump(...)
#endif

template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}

using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;

const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};


ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}

random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

string f;
int n;

const string INV = string(1, '0' - 1);
const string UND = "#";

vector<vector<string>> memo;

inline bool is_valid_num(string str){
    return ((int)str.size() == 1 or str[0] != '0');
}

inline string max_str(string a, string b){
    if(a.size() > b.size()) return a;
    if(a.size() < b.size()) return b;
    return max(a, b);
}

string rec(int l, int r){
    string& ret = memo[l][r];
    if(ret != UND) return ret;

    ret = INV;

    // number
    {
        string cur = "";
        rep(i, l, r){
            if(isdigit(f[i]))    cur += f[i];
            else if(f[i] == '?') cur += '9';
        }
        if((int)cur.size() == r - l and is_valid_num(cur)){
            return (ret = cur);
        }
    }

    if(r - l < 6) return ret;
    if(f[l] != 'L' and f[l] != 'R' and f[l] != '?') return ret;

    if((f[l + 1] == '(' or f[l + 1] == '?') and
       (f[r - 1] == ')' or f[r - 1] == '?')){

        rep(i, l + 3, r - 2){
            if(f[i] == ',' or f[i] == '?'){
                string lhs = rec(l + 2, i);
                string rhs = rec(i + 1, r - 1);
    // cerr << l << ", " << r << "| " << i << ": " << lhs << ", " << rhs << endl;
                if(lhs != INV and is_valid_num(lhs) and
                   rhs != INV and is_valid_num(rhs)){

                    // L
                    if(f[l] == 'L' or f[l] == '?'){
                        ret = max_str(ret, lhs);
                    }

                    // R
                    if(f[l] == 'R' or f[l] == '?'){
                        ret = max_str(ret, rhs);
                    }
                }
            }
        }
    }

    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> f;
    n = f.size();
    memo = vector<vector<string>>(n, vector<string>(n + 1, UND));

    string res = rec(0, n);
    if(res == INV) res = "invalid";
    cout << res << endl;

    return 0;
}
