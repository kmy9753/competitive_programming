#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

#define F first
#define S second

string dir_str = "RULD";

map<char, pii> dir;

int main(void){
    dir['R'] = mp( 0,  1);
    dir['U'] = mp(-1,  0);
    dir['L'] = mp( 0, -1);
    dir['D'] = mp( 1,  0);

    for(int w, h, n; cin >> w >> h >> n;){
        pii s, z; cin >> s.S >> s.F >> z.S >> z.F;

        vvi yoko(h + 1, vi(w)), tate(h, vi(w + 1));

        rep(i, n){
            int yy, xx, t;
            string op;
            cin >> xx >> yy >> t >> op;

            pii cur_pos = mp(yy, xx);

            //java sdk coding ground moodle
            rep(j, t){
                for(char c : op){
                    pii next_pos = mp(cur_pos.F + dir[c].F, cur_pos.S + dir[c].S);

                    if(next_pos.F < 0 || h <= next_pos.F ||
                            next_pos.S < 0 || w <= next_pos.S)
                        continue;

                    switch(c){
                        case 'R': { tate[cur_pos.F    ][cur_pos.S + 1]++; break; }
                        case 'U': { yoko[cur_pos.F    ][cur_pos.S    ]++; break; }
                        case 'L': { tate[cur_pos.F    ][cur_pos.S    ]++; break; }
                        case 'D': { yoko[cur_pos.F + 1][cur_pos.S    ]++; break; }
                        default : break;
                    }

                    cur_pos = next_pos;
                }
            }
        }

        typedef pair<int, pii> State;

        priority_queue<State, vector<State>, greater<State>> q;
        q.push(mp(0, mp(s.F, s.S)));

        map<pii, bool> used;
        while(!q.empty()){
            int cur_cost = q.top().F;
            pii cur_pos = q.top().S;
            q.pop();

            if(used[cur_pos]) continue;
            used[cur_pos] = true;

            if(cur_pos == z){
                cout << cur_cost << endl;
                break;
            }

            for(char c : dir_str){
                pii next_pos = mp(cur_pos.F + dir[c].F, cur_pos.S + dir[c].S);
                int next_cost = cur_cost;

                if(next_pos.F < 0 || h + 1 <= next_pos.F ||
                        next_pos.S < 0 || w + 1 <= next_pos.S)
                    continue;

                if(used[next_pos]) continue;
                switch(c){
                    case 'R': { next_cost += yoko[cur_pos.F    ][cur_pos.S    ]; break; }
                    case 'U': { next_cost += tate[cur_pos.F - 1][cur_pos.S    ]; break; }
                    case 'L': { next_cost += yoko[cur_pos.F    ][cur_pos.S - 1]; break; }
                    case 'D': { next_cost += tate[cur_pos.F    ][cur_pos.S    ]; break; }
                    default : break;          
                }

                q.push(mp(next_cost, next_pos));
            }
        }
    }

	return 0;
}
