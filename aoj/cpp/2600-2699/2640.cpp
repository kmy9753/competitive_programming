#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

const vi dx = { 1, 0,-1, 0};
const vi dy = { 0,-1, 0, 1};
const string kDir = ">^<v";

const vi udedx = {-1, 0, 1, 1};
const vi udedy = { 1, 1, 1, 0};

typedef tuple<int, pii, int, int> State;

#define F first
#define S second

inline pii getUde(pii pos, int dir, int ude){
    pii ret = pos;

    pii delta;
    double rot = pi / 4 * dir;
    delta.S = udedx[ude] * cos(rot) - udedy[ude] * sin(rot);
    delta.F = udedx[ude] * sin(rot) + udedy[ude] * cos(rot);

    ret.F += delta.F;
    ret.S += delta.S;

    return ret;
}

int main(void){
    for(int w, h; cin >> h >> w, h;){
        h += 2, w += 2;

        vs field(h);
        rep(x, w) field[0].pb('#'), field[h - 1].pb('#');
        range(y, 1, h - 1){
            cin >> field[y];
            field[y] = "#" + field[y] + "#";
        }

        int sdir;
        pii s, z;
        rep(y, h){
            rep(x, w){
                rep(i, 4){
                    if(field[y][x] == kDir[i]){
                        sdir = i;
                    }
                }
                if(field[y][x] == 'S') s = mp(y, x), field[y][x] = '.';
                if(field[y][x] == 'G') z = mp(y, x), field[y][x] = '.';
            }
        }

        priority_queue<State, vector<State>, greater<State>> q;
        rep(i, 4){
            pii ude = getUde(s, sdir, i);
            if(field[ude.F][ude.S] != '#') continue;
            q.push(make_tuple(1, s, sdir, i));
        }
        vector<vector<vvi>> minDist(h, vector<vvi>(w, vvi(4, vi(4, -1))));

        while(q.size()){
            pii cur_pos;
            int cur_dist, cur_dir, cur_ude;
            tie(cur_dist, cur_pos, cur_dir, cur_ude) = q.top();
            q.pop();

            if(minDist[cur_pos.F][cur_pos.S][cur_dir][cur_ude] != -1) continue;
            minDist[cur_pos.F][cur_pos.S][cur_dir][cur_ude] = cur_dist;

            pii next_pos = cur_pos;
            int next_dir = cur_dir;
            int next_dist = cur_dist;

            //$BOS$N0LCV$+$($k(B
            rep(i, 4){
                int next_ude = i;
                pii ude = getUde(cur_pos, cur_dir, next_ude);

                if(abs(next_ude - cur_ude) >= 2 && cur_ude != 1 && next_ude != 1) continue;
                if(field[ude.F][ude.S] != '#') continue;

                if(minDist[next_pos.F][next_pos.S][next_dir][next_ude] != -1) continue;

                q.push(make_tuple(next_dist, next_pos, next_dir, next_ude));
            }
        }
    }

	return 0;
}
