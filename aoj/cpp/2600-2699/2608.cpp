#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

#define F first
#define S second

typedef pair<int, int> State;
int n;
vvi edge;

void bfs(int s, vi& dist){
    dist = vi(n, inf);
    dist[s] = 0;

    queue<int> q;
    q.push(s);

    while(!q.empty()){
        int v = q.front(); q.pop();

        for(int u : edge[v]){
            if(dist[u] != inf) continue;

            dist[u] = dist[v] + 1;
            q.push(u);
        }
    }
}

int main(void){
    ll res;
    for(int m, s, t; cin >> n >> m >> s >> t; cout << res << endl){
        res = 0;

        s--, t--;
        edge = vvi(n);

        rep(i, m){
            int x, y; cin >> x >> y;
            x--, y--;
            
            edge[x].pb(y), edge[y].pb(x);
        }

        vi dist_fromS, dist_fromT;
        bfs(s, dist_fromS);
        bfs(t, dist_fromT);

        map<int, ll> distCnt_fromS, distCnt_fromT;
        rep(u, n){
            distCnt_fromS[dist_fromS[u]]++;
            distCnt_fromT[dist_fromT[u]]++;
        }

        int path_len = dist_fromS[t];

        rep(i, path_len - 1){
            res += distCnt_fromS[i] * distCnt_fromT[path_len - i - 2];
        }
    }

	return 0;
}
