#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int w = 15, h = 19;

typedef pair<int, pair<int, pii>> State;
#define F first
#define S second

int dx[] = { 1, 1, 0,-1,-1,-1, 0, 1};
int dy[] = { 0, 1, 1, 1, 0,-1,-1,-1};

int main(void){
    vs field(h + 2);

    rep(x, w + 2) field[0].pb('#'), field[h + 1].pb('@');

    pii s;
    map<pii, int> stone_id;
    int cnt_id = 0;

    range(y, 1, h + 1){
        cin >> field[y];
        field[y] = "#" + field[y] + "#";

        if(y == h) range(x, 1, w + 1) if(field[y][x] == '.') field[y][x] = '@';
        rep(x, w + 2){
            if(field[y][x] == 'O') s = mp(y, x), field[y][x] = '.';
            if(field[y][x] == 'X'){
                stone_id[mp(y, x)] = cnt_id++;
            }
        }
    }

    queue<State> q;
    q.push(mp(0, mp(0, s)));

    int res = -1;
    while(!q.empty()){
        int cur_stones = q.front().F; 
        pii cur_pos = q.front().S.S;
        int cur_turn = q.front().S.F; q.pop();

        if(field[cur_pos.F][cur_pos.S] == '@'){
            res = cur_turn;
            break;
        }

        rep(i, 8){
            int next_stones = cur_stones;
            pii next_pos = cur_pos;
            int next_turn = cur_turn + 1;
         
            while( field[next_pos.F + dy[i]][next_pos.S + dx[i]] == 'X' &&
                 !((next_stones >> stone_id[mp(next_pos.F + dy[i], next_pos.S + dx[i])]) & 1)){
                next_pos.F += dy[i];
                next_pos.S += dx[i];

                int id = stone_id[next_pos];
                next_stones |= (1 << id);
            }

            if(cur_pos == next_pos || field[next_pos.F += dy[i]][next_pos.S += dx[i]] == '#') continue;

            State next = mp(next_stones, mp(next_turn, next_pos));
            q.push(next);
        }
    }

    cout << res << endl;

	return 0;
}
