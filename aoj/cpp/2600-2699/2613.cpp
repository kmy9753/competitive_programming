#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const vi add = {1, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 3, 3};
const vi sub = {1, 1, 2, 1, 2, 1, 2, 2, 3, 1, 3, 1, 2};
const vi mul = {1, 1, 1, 2, 1, 2, 2, 3, 2, 3, 1, 2, 1};

string formula;

ll parse(int b, int e, int ord){
    vector<ll> v;
    vector<char> op;

    range(i, b, e){
        if(formula.at(i) == '('){
            int next_b = i + 1, cnt = 1;
            while(cnt){
                if(formula[++i] == '(') cnt++;
                else if(formula[i] == ')') cnt--;
            }
            v.pb(parse(next_b, i, ord));
        }
        else if(isdigit(formula.at(i))){
            ll value = 0;
            while(i < e && isdigit(formula.at(i))){
                value = 10 * value + formula.at(i++) - '0';
            }
            v.pb(value);

            i--;
        }
        else{
            op.pb(formula.at(i));
        }
    }
  
    if(v.size() == 0) return 0;

    range(i, 1, 4){
        rep(j, op.size()){
            bool operated = false;

            if(op[j] == '+' && add[ord] == i){
                v[j] = v[j] + v[j + 1];
                operated = true;
            }
            else if(op[j] == '-' && sub[ord] == i){
                v[j] = v[j] - v[j + 1];
                operated = true;
            }
            else if(op[j] == '*' && mul[ord] == i){
                v[j] = v[j] * v[j + 1];
                operated = true;
            }

            if(operated){
                v.erase(v.begin() + j + 1);
                op.erase(op.begin() + j);
                j--;
            }
        }
    }

    return v[0];
}

int main(void){
    for(; cin >> formula;){
        ll res = parse(0, formula.size(), 0);

        range(i, 1, add.size()){
            res = max(res, parse(0, formula.size(), i));
        }

        cout << res << endl;
    }

	return 0;
}
