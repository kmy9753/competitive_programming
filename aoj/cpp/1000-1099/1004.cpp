#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 10001;

int main(void){
    vector<bool> isP(N);
    fill(ALL(isP), true);
    rep(i, 2) isP[i] = false;
    for(int i = 2; i * i < N; i ++) if(isP[i]) for(int j = i * i; j < N; j += i) isP[j] = false;

    for(int n, cnt; cnt = 0, cin >> n; cout << cnt << endl)
        rep(i, n) if(isP[1 + i] && isP[n - i]) cnt ++;

    return 0;
}
