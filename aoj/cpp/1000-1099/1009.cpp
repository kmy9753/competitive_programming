#include <iostream>

using namespace std;

int gcd(int, int);

int main(void){
    for(int a, b; cin >> a >> b; cout << gcd(a, b) << endl);

    return 0;
}

int gcd(int a, int b){
    int r;
    while((r = a % b) != 0){
        a = b;
        b = r;
    }
    
    return b;
}
