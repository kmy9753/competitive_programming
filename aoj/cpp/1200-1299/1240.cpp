#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    int n; cin >> n;
    while(n--){
        string order, in; cin >> order >> in;

        for(int i = order.size() - 1; 0 <= i; i--){
            switch(order.at(i)){
                case 'J':
                {
                    string front = in.substr(in.size() - 1);
                    string back = in.substr(0, in.size() - 1);
                    in = front + back;
                    break;
                }
                case 'C':
                {
                    string front = in.substr(1);
                    string back = in.substr(0, 1);
                    in = front + back;
                    break;
                }
                case 'E':
                {
                    string front = in.substr((in.size() + 1) / 2);
                    string mid = in.substr(in.size() / 2, in.size() % 2);
                    string back = in.substr(0, in.size() / 2);
                    in = front + mid + back;
                    break;
                }
                case 'A':
                {
                    reverse(all(in));
                    break;
                }
                case 'M':
                {
                    rep(i, in.size()){
                        if(in.at(i) == '9'){
                            in[i] = '0';
                        }
                        else if('0' <= in.at(i) && in.at(i) <= '8'){
                            in[i]++;
                        }
                    }
                    break;
                }
                case 'P':
                {
                    rep(i, in.size()){
                        if(in.at(i) == '0'){
                            in[i] = '9';
                        }
                        else if('1' <= in.at(i) && in.at(i) <= '9'){
                            in[i]--;
                        }
                    }
                    break;
                }
                default:
                {
                    break;
                }    
            }
        }
        cout << in << endl;
    }

	return 0;
}
