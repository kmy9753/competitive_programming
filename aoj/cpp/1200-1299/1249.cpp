#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define b 1
#define w -1

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

vi dx;
vi dy;
vi dz;

int main(void){
    range(x, -1, 2) range(y, -1, 2) range(z, -1, 2){
        if(!x && !y && !z) continue;
        dx.pb(x);
        dy.pb(y);
        dz.pb(z);
    }

    for(int n, M, p; cin >> n >> M >> p, n;){
        vector<vvi> m(n, vvi(n, vi(n, 0)));
        int turn = b, turncnt = 0;
        string res = "Draw";

        while(p--){
            if(res != "Draw"){
                cin >> n >> M;
                continue;
            }
            turncnt++;

            int xx, yy, zz = 100000; cin >> yy >> xx;
            xx--; yy--;
            for(int z = n - 1; 0 <= z; z--){
                if(m[yy][xx][z] == 0){
                    zz = z;
                    m[yy][xx][zz] = turn;
                    break;
                }
            }

            rep(i, dx.size()){
                int cnt = 1;
                for(int sign = -1; sign <= 1; sign += 2){
                    range(j, 1, n){
                        int x = xx + sign * dx[i] * j,
                            y = yy + sign * dy[i] * j,
                            z = zz + sign * dz[i] * j;
                        if(x < 0 || n <= x ||
                           y < 0 || n <= y ||
                           z < 0 || n <= z) break;
                        if(m[y][x][z] != turn) break;

                        cnt++;
                    }
                }
                if(M <= cnt) res = turn == b ? "Black":"White";
            }
            turn = turn == b ? w:b;
        }

        cout << res;
        if(res != "Draw") cout << " " << turncnt;
        cout << endl;
    }

	return 0;
}
