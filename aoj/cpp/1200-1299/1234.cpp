#include <bits/stdc++.h>
 
#define rep(i,n) for(int (i) = 0; (i) < (n); (i)++)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);

vector<vi> score;
const int N = 4;
using R = double;

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int T; cin >> T;
    while(T--){
        vector<string> in_str(N + 1); for(auto& e : in_str) cin >> e;

        score = vector<vi>(N, vi(N, -1));
        rep(i, N) score[i][i] = 0;
        int tar = 0; while(in_str[0][5 + 5 * tar] != '*') tar++;
        vi a, b;
        rep(i, N){
            for(int j = i + 1; j < N; j++){
                char l = in_str[i + 1][6 + 5 * j];
                char r = in_str[i + 1][8 + 5 * j];
                if(l == '_'){
                    a.emplace_back(i);
                    b.emplace_back(j);
                    continue;
                }
                score[i][j] = (int)(l - '0');
                score[j][i] = (int)(r - '0');
            }
        }
        assert((int)a.size() == 2);

        const int M = 8;
        R res = 0;
        vi fact(M + 1); fact[0] = 1;
        for(int i = 1; i < M + 1; i++) fact[i] = fact[i - 1] * i;

        rep(i, (int)pow(M + 1, 4)){
            vi tbl(4);
            int cur = i;
            R p = 1.0;
            for(auto& e : tbl){
                e = cur % (M + 1); cur /= (M + 1);
                p *= fact[M] / fact[e] / fact[M - e] * pow(3, M - e) / pow(4, M);
            }
            score[a[0]][b[0]] = tbl[0]; score[b[0]][a[0]] = tbl[1];
            score[a[1]][b[1]] = tbl[2]; score[b[1]][a[1]] = tbl[3];

            vi used(N);
            int ucnt = 0, dcnt = 0;
            while(1){
                vi pused = used;
                vi points(N), gd(N), gs(N);
                rep(j, N){
                    rep(k, N){
                        if(k == j or used[j] or used[k]) continue;
                        gs[j] += score[j][k];
                        gd[j] += score[j][k]; gd[j] -= score[k][j];
                        points[j] += (score[j][k] == score[k][j] ? 1 : score[j][k] > score[k][j] ? 3 : 0);
                    }
                }

                auto cmp = [&](int t1, int t2){
                    if(points[t1] != points[t2]){
                        return points[t1] > points[t2] ? 1 : -1;
                    }
                    if(gd[t1] != gd[t2]){
                        return gd[t1] > gd[t2] ? 1 : -1;
                    }
                    if(gs[t1] != gs[t2]){
                        return gs[t1] > gs[t2] ? 1 : -1;
                    }
                    return 0;
                };
                rep(j, N){
                    if(j == tar or used[j]) continue;

                    int ret = cmp(j, tar);
                    if(ret == 1){
                        used[j] = true; ucnt++;
                    }
                    else if(ret == -1){
                        used[j] = true; dcnt++;
                    }
                }
                if(pused == used) break;
            }

            int cnt = N - ucnt - dcnt;
            if(ucnt >= 2) continue;

            if(ucnt == 1){
                res += p / cnt;
            }
            else {
                res += p * min(2, cnt) / cnt;
            }
        }
        cout.precision(20);
        cout << fixed << res << endl;
    }

    return 0;
}
