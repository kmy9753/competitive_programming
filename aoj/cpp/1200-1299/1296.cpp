#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define F first
#define S second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, res; cin >> n, n; cout << res << endl){
        res = -1;

        vector< pair<string, string> > pairs(n);
        rep(i, n){
            cin >> pairs[i].F >> pairs[i].S;
        }
        string origin, target; cin >> origin >> target;

        set<string> s;
        queue< pair<string, int> > q;
        q.push(mp(origin, 0));

        while(!q.empty()){
            string cur = q.front().F;
            int turn = q.front().S;
            q.pop();

            if(cur == target){
                res = turn;
                break;
            }
            
            rep(i, n){
                string next = cur;

                int pos = next.find(pairs[i].F);
                while(pos != -1){
                    next.replace(pos, pairs[i].F.size(), pairs[i].S);
                    pos = next.find(pairs[i].F, pos + pairs[i].S.size());
                }

                if(target.size() < next.size() || !s.insert(next).S){
                    continue;
                }
                q.push(mp(next, turn + 1));
            }
        }
    }

	return 0;
}
