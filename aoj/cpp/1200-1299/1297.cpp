#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using Swimmer = tuple<int, int, int>;
using TC = tuple<int, int>;

int main(void){
    for(int n; cin >> n, n;){
        vector<TC> tc(n);
        for(auto & e : tc){
            int t, c; cin >> t >> c;
            e = TC(t, c);
        }
        sort(_all(tc));

        vector<queue<Swimmer>> q(2);
        for(auto & e : tc){
            int t, c; tie(t, c) = e;
            q[0].push(Swimmer(t, t, c));
        }

        int res = 0;
        while(q[0].size() or q[1].size()){
            int qi;
            if(q[0].empty() or (q[1].size() and get<1>(q[0].front()) > get<1>(q[1].front()))){
                qi = 1;
            }
            else qi = 0;

            int dt = get<1>(q[qi].front());
            res += dt;

            vector<vector<Swimmer>> next(2);
            rep(i, 2){
                int ni  = i ^ 1;
                while(q[i].size()){
                    int t, rt, rc;
                    tie(t, rt, rc) = q[i].front();

                    if(rt > dt) break;

                    q[i].pop();
                    int nrc = rc;
                    if(ni == 0) nrc--;

                    if(nrc >= 1){
                        Swimmer ns(t, t, nrc);
                        next[ni].push_back(ns);
                    }
                }
                sort(_all(next[ni]));
            }

            rep(i, 2){
                queue<Swimmer> nq;
                while(q[i].size()){
                    int t, rt, rc;
                    tie(t, rt, rc) = q[i].front(); q[i].pop();
                    rt -= dt;
                    nq.push(Swimmer(t, rt, rc));
                }
                for(auto & e : next[i]){
                    nq.push(e);
                }
                q[i] = nq;
            }
        }

        cout << res << endl;
    }

    return 0;
}
