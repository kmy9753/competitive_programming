#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

string f;
int len;
int p = 0;

ll exp();
ll term();
ll fact();

ll tbl['z' - 'a' + 1];
inline ll c2n(char c){
    return tbl[c - 'a'];
}

char next_c(){
    while(p < len and f[p] == ' ') p++;
    return f[p];
}

ll exp(){
    ll ret = term();

    char c;
    while(c = next_c(), (c == '+' or c == '-')){
        p++;
        ll r = term();
        if(c == '+') ret = ADD(ret, r, mod);
        else         ret = SUB(ret, r, mod);
    }

    return ret;
}

const ll ERR = 2LL * mod;

ll term(){
    ll ret = fact();

    ll r;
    while((r = fact()) != ERR){
        ret = MUL(ret, r, mod);
    }

    return ret;
}

inline ll num(){
    ll ret = 0LL;
    next_c();
    while(isdigit(f[p])){
        ret = ret * 10 + (f[p] - '0');
        p++; // [0,9]
    }
    return ret % mod;
} 

ll fact(){
    ll ret = ERR;

    char c = next_c();
    if(c == '('){
        p++; // (
        ret = exp();
        assert(next_c() == ')'); p++;
    }
    else if(islower(c)){
        p++; // [a,z]
        ret = c2n(c);

        c = next_c();
        if(c == '^'){
            p++; // ^
            ll d = num();
            ll b = ret;
            rep(loop, d - 1){
                ret = MUL(ret, b, mod);
            }
        }
    }
    else if(isdigit(c)){
        ret = num();
    }
    else {
        // cerr << "cur... " << c << endl;
    }

    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);
    auto myrand = bind(uniform_int_distribution<int>(0, (int)1e6), mt19937(static_cast<unsigned int>(time(nullptr))));

    while(true){
        string mine;
        getline(cin, mine);
        if(mine == ".") break;
        
        while(true){
            string opp;
            getline(cin, opp);
            if(opp == ".") break;

            bool ok = true;
            rep(loop, 1000){
                rep(i, 'z' - 'a' + 1) tbl[i] = myrand();

                // cerr << "-> " << mine << endl;
                f = mine; p = 0; len = f.size(); ll a = exp();
                // cerr << "-> " << opp << endl;
                f =  opp; p = 0; len = f.size(); ll b = exp();

                if(a != b){
                    ok = false;
                    break;
                }
            }

            if(ok){
                cout << "yes" << endl;
            }
            else {
                cout << "no" << endl;
            }
        }
        cout << "." << endl;
    }

    return 0;
}
