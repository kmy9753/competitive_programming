#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, m, res; cin >> n >> m, n; cout << res << endl){
        res = inf;
        vvi costs(n, vi(n, inf));
        vi edge(m);

        rep(i, m){
            int a, b, w; cin >> a >> b >> w;
            a--, b--;

            costs[a][b] = costs[b][a] = w;
            edge[i] = w;
        }

        rep(i, m){
            int lb = edge[i];
            int slim = 0;

            vi minCost(n, inf), used(n);

            minCost[0] = 0;
            while(1){
                int v = -1;
                rep(u, n){
                    if(!used[u] && (v == -1 || minCost[u] < minCost[v])) v = u;
                }
                if(v == -1) break;

                used[v] = true;
                slim = max(slim, minCost[v]);

                rep(u, n){
                    if(costs[v][u] >= lb) minCost[u] = min(minCost[u], costs[v][u] - lb);
                }
            }

            res = min(res, slim);
        }

        if(res > 10000000) res = -1;
    }

	return 0;
}
