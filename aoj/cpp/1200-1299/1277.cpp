#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, t, l, b; cin >> n >> t >> l >> b, n;){
        vi loseTurn(n + 1);
        rep(i, l){
            int a; cin >> a;
            loseTurn[a] = true;
        }

        vi goBack(n + 1);
        rep(i, b){
            int a; cin >> a;
            goBack[a] = true;
        }

        vector< vector<double> > dp(t + 1, vector<double>(n + 1));
        dp[0][0] = 1.;
        double p = 1./6;

        rep(i, t){
            rep(j, n){
                if(dp[i][j] == 0.) continue;

                range(k, 1, 7){
                    int next = j + k;
                    if(n < next) next -= 2 * (next - n);

                    if(goBack[next]) next = 0;

                    if(!loseTurn[next]){
                        dp[i + 1][next] += dp[i][j] * p;
                    }
                    else{
                        if(i + 2 <= t){
                            dp[i + 2][next] += dp[i][j] * p;
                        }
                    }
                }
            }
        }

        double res = 0.;
        rep(i, t + 1){
            res += dp[i][n];
        }
        printf("%.6f\n", res);
    }

	return 0;
}
