#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
 
const int vmax=380;
const int inf=1<<29;
struct edge{int to,cap,cost,rev;};
vector<edge> graph[vmax];
 
int h[vmax],dist[vmax];
int pv[vmax],pe[vmax];
typedef tuple<int,int> state;
 
void add_edge(int from,int to,int cap,int cost){
    graph[from].push_back({to,cap,cost,(int)graph[to].size()});
    graph[to].push_back({from,0,-cost,(int)graph[from].size()-1});
}
 
int min_cost_flow(int s,int t,int f,int n){
    int res=0;
    fill(h,h+n,0);
    while(f>0){
        priority_queue <state,vector<state>,greater<state> > q;
        fill(dist,dist+n,inf);
        dist[s]=0;
        q.push(state(0,s));
        while(!q.empty()){
            int cost,v;
            tie(cost,v)=q.top();q.pop();
            if(dist[v] < cost) continue;
            rep(i,graph[v].size()){
                edge &e=graph[v][i];
                if(e.cap>0&&dist[e.to]>dist[v]+e.cost+h[v]-h[e.to]){
                    dist[e.to]=dist[v]+e.cost+h[v]-h[e.to];
                    pv[e.to]=v,pe[e.to]=i;
                    q.push(state(dist[e.to],e.to));
                }
            }
        }
        if(dist[t]==inf) return -1;
        rep(v,n) h[v]+=dist[v];
 
        int d=f;
        for(int v=t;v!=s;v=pv[v]) d=min(d,graph[pv[v]][pe[v]].cap);
        f-=d;res+=d*h[t];
        for(int v=t;v!=s;v=pv[v]){
            edge &e=graph[pv[v]][pe[v]];
            e.cap-=d;
            graph[v][e.rev].cap+=d;
        }
    }
    return res;
}
 
int main(void){
    for(int n; cin >> n, n;){
        rep(v, vmax) graph[v].clear();

        rep(loop, n){
            int a, b, c; cin >> a >> b >> c;
            a--;

            add_edge(a, b, 1, -c);
        }
        rep(i, vmax - 1){
            add_edge(i, i + 1, 2, 0);
        }

        cout << -min_cost_flow(0, vmax - 1, 2, vmax) << endl;
    }

    return 0;
}
