#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

<<<<<<< HEAD
const int N = 1001;

int n;
vi used;
vi nums;

bool iddfs(int depth, int mx){
    if(used[n]) return true;
    if(depth == 0) return false;
    if((mx << depth) < n) return false;

    rrep(i, nums.size()){
        rrep(j, i + 1){
            for(auto & e : { nums[i] + nums[j], abs(nums[i] - nums[j]) }){
                if(e <= 0 or 2 * N <= e) continue;
                if(used[e]) continue;

                used[e] = true;
                nums.push_back(e);
                if(iddfs(depth - 1, max(mx, e))) return true;
                nums.pop_back();
                used[e] = false;
            }
        }
    }

    return false;
}

int main(void){
    rep(_, 1, N){
        n = _;
        if(n % 100 == 0){
            cerr << n << endl;
        }

        int res = 0;
        rep(depth, 0, N){
            nums = {1};
            used = vi(2 * N);
            used[1] = true;

            if(iddfs(depth, 1)){
                cout << ", " << depth;
                break;
            }
        }
    }
    cout << endl;
=======
int main(void){
>>>>>>> a1550a7e4e5e364816b1333f6194c4a976253763

    return 0;
}
