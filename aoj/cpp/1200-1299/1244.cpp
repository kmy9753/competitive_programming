#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

inline bool isSmall(char c){ return 'a' <= c && c <= 'z'; }

bool ok;
map<string, int> dic;

int calc(string& mol, int& pos){
    int ret;

    if(mol[pos] == '('){
        pos++;
        ret = calc(mol, pos);
        pos++;
    }
    else {
        string atom;
        atom.pb(mol[pos++]);
        if(pos < mol.size() && isSmall(mol[pos])){
            atom.pb(mol[pos++]);
        }

        if(dic[atom] == 0){
            ok = false;
        }

        ret = dic[atom];
    }

    int num = 0;
    while(pos < mol.size() && isdigit(mol[pos])){
        num = 10 * num + (mol[pos++] - '0');
    }
    if(num != 0) ret *= num;

    if(pos < mol.size() && mol[pos] != ')') ret += calc(mol, pos);

    return ret;
}

int main(void){
    for(string str; cin >> str, str != "END_OF_FIRST_PART"; ){
        cin >> dic[str];
    }

    for(string mol; cin >> mol, mol != "0";){
        ok = true;

        int pos = 0, res = calc(mol, pos);
        if(ok) cout << res << endl;
        else cout << "UNKNOWN" << endl;
    }

	return 0;
}
