#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 1e6;
vi graph[N];

using ResElem = tuple<string, string>;
set<ResElem> s;

inline void add_edge(int a, int b){
    graph[a].emplace_back(b);
    graph[b].emplace_back(a);
}

int d, n;
map<string, int> s2i;
map<int, string> i2s;

void dfs(int sv, int v, int depth){
    if(depth > d) return;
    if(sv != v and v < n){
        string a = i2s[v], b = i2s[sv];
        if(a > b) swap(a, b);
        s.insert(ResElem(a, b));
    }

    for(auto& nv : graph[v]){
        dfs(sv, nv, depth + 1);
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    for(; cin >> n >> d, n;){
        vector<string> names(n);
        for(auto& e : names) cin >> e;

        s2i.clear();
        i2s.clear();
        int idx = 0;
        auto add_vertex = [&](string& name){
            if(s2i.find(name) == end(s2i)){
                s2i[name] = idx; i2s[idx] = name; idx++;
            }
            return s2i[name];
        };
        for(auto& e : names) add_vertex(e);

        rep(i, N) graph[i].clear();
        for(auto& cur : names){
            string next;
            int len = cur.size();
            int v = s2i[cur];

            // delete
            rep(i, len){
                next = cur.substr(0, i) + cur.substr(i + 1);
                int nv = add_vertex(next);
                add_edge(v, nv);
            }

            // insert
            rep(i, len + 1){
                rep(j, 'z' - 'a' + 1){
                    char c = (char)('a' + j);
                    next = cur.substr(0, i) + c + cur.substr(i);
                    int nv = add_vertex(next);
                    add_edge(v, nv);
                }
            }

            // replace
            rep(i, len){
                rep(j, 'z' - 'a' + 1){
                    char c = (char)('a' + j);
                    next = cur.substr(0, i) + c;
                    if(i + 1 < len) next += cur.substr(i + 1);
                    int nv = add_vertex(next);
                    add_edge(v, nv);
                }
            }

            // swap
            rep(i, len - 1){
                next = cur;
                swap(next[i], next[i + 1]);
                int nv = add_vertex(next);
                add_edge(v, nv);
            }
        }

        s.clear();
        rep(i, n){
            dfs(i, i, 0);
        }

        for(auto& e : s){
            string a, b; tie(a, b) = e;
            cout << a << "," << b << endl;
        }
        cout << s.size() << endl;
    }

    return 0;
}
