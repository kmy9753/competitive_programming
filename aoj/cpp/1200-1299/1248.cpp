#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define F first
#define S second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

ll gcd (ll a, ll b)
{
  ll c;
  while(a != 0){
     c = a; a = b%a;  b = c;
  }
  return b;
}

int main(void){
    for(ll a, b, d; cin >> a >> b >> d, a;){
        ll sum = b - d;
        while(sum < 0 || sum % a != 0){
            sum += b;
        }
        pair<ll, ll> res = mp(sum / a, (sum + d) / b);

        sum = a - d;
        while(sum < 0 || sum % b != 0){
            sum += a;
        }
        if((sum + d) / a + sum / b <= res.F + res.S){
            if((sum + d) / a + sum / b != res.F + res.S ||
               (sum + d) + sum < res.F * a + res.S * b)
                res = mp((sum + d) / a, sum / b);
        }

        sum = d - a;
        while(0 <= sum && sum % b != 0){
            sum -= a;
        }
        if(0 <= sum && (d - sum) / a + sum / b <= res.F + res.S){
            if((d - sum) / a + sum / b != res.F + res.S ||
               d < res.F * a + res.S * b)
                res = mp((d - sum) / a, sum / b);
        }

        sum = d - b;
        while(0 <= sum && sum % a != 0){
            sum -= b;
        }
        if(0 <= sum && (d - sum) / b + sum / a <= res.F + res.S){
            if((d - sum) / b + sum / a != res.F + res.S ||
               d < res.F * a + res.S * b)
                res = mp(sum / a, (d - sum) / b);
        }

        cout << res.F << " " << res.S << endl;
    }

	return 0;
}
