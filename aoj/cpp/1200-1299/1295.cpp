#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int w, d, res; cin >> w >> d, w; cout << res << endl){
        res = 0;

        vi hw(w), hd(d);
        rep(i, w) cin >> hw[i];
        rep(i, d) cin >> hd[i];

        sort(all(hw), greater<int>());
        sort(all(hd), greater<int>());

        int ww = 0, dd = 0;
        while(ww < w && dd < d){
            int chw = hw[ww];
            int cntw = 0, cntd = 0;
            while(dd < d && chw == hd[dd]){
                dd++;
                cntd++;
            }
            while(ww < w && chw == hw[ww]){
                ww++;
                cntw++;
            }
            res += chw * max(cntw, cntd);

            if(w <= ww) break;
            if(d <= dd) break;

            while(ww < w && hw[ww] > hd[dd]) res += hw[ww++];
            if(w <= ww) break;
            while(dd < d && hd[dd] > hw[ww]) res += hd[dd++];
            if(d <= dd) break;
        }
        while(dd < d) res += hd[dd++];
        while(ww < w) res += hw[ww++];
    }

	return 0;
}
