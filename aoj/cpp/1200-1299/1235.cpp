#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int n;
vector<vi> graph;
vi field;

vi used, state;

// true: disappear
bool check(int v, int idx){
    if(field[v] == -1) return false;
    if(field[v] != idx) return true;
    used[v] = true;

    bool ret = true;
    for(auto& nv : graph[v]){
        if(used[nv]) continue;
        ret &= check(nv, idx);
    }

    return ret;
}

void dfs(int v, int idx, int s){
    state[v] = s;

    for(auto& nv : graph[v]){
        if(state[nv] != -1 or field[nv] != idx) continue;
        dfs(nv, idx, s);
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);
    
    for(int h, p; cin >> h >> p, p--, h;){
        n = h * (h + 1) / 2;
        graph = vector<vi>(n);

        vi field_origin(n);
        for(auto& e : field_origin) cin >> e, e--;

        int idx = 0;
        rep(i, h){
            rep(j, i + 1){
                if(i < h - 1){
                    int c1 = idx + i + 1, c2 = idx + i + 2;
                    graph[idx].emplace_back(c1), graph[c1].emplace_back(idx);
                    graph[idx].emplace_back(c2), graph[c2].emplace_back(idx);
                }
                if(j < i){
                    graph[idx].emplace_back(idx + 1);
                    graph[idx + 1].emplace_back(idx);
                }
                idx++;
            }
        }

        int res = -1e3;
        rep(i, n){
            field = field_origin;
            if(field[i] != -1) continue;

            field[i] = p;
            used = vi(n);
            state = vi(n, -1);

            rep(v, n){
                if(field[v] == -1 or state[v] != -1) continue;
                int ns = check(v, field[v]) ? 0:1;
                dfs(v, field[v], ns);
            }

            int cur = 0;
            rep(v, n){
                if(state[v] != 0) continue;

                if(field[v] == p) cur--;
                else cur++;
            }

            chmax(res, cur);
        }

        cout << res << endl;
    }

    return 0;
}
