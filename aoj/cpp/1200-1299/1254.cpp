#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=int(a);i<int(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=int(a)-1;i>=int(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(arg),key)-begin(arg)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
using namespace std;
 
template<class T>void reg(vector<T> &ary,const T &elem){ary.emplace_back(elem);}
template<class T>bool chmin(T &a, const T &b) {return (b<a)?(a=b,1):0;}
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
 
using R=long double; // __float128
const R EPS = 1E-11; // [-1000:1000]->EPS=1e-8 [-10000:10000]->EPS=1e-7
const R INF = 1E40;
constexpr R PI = acos(R(-1));
inline int sgn(const R& r){ return (r > EPS) - (r < -EPS);}
inline int sgn(const R& a, const R &b){ return sgn(a-b); }
inline R sq(R x){return sqrt(max<R>(x,0.0));}
 
using P=complex<R>;
using VP=vector<P>;
using L=struct{P s,t;};
using VL=vector<L>;
using C=struct{P c;R r;};
using VC=vector<C>;
 
constexpr P O = P(0,0);
istream& operator >> (istream& is,P& p){ R x,y;is >> x >> y; p=P(x,y); return is;}
ostream& operator << (ostream& os,P& p){ os << real(p) << " " << imag(p); return os;}
 
namespace std{
    bool operator <  (const P& a,const P& b){ return sgn(real(a-b))?real(a-b)<0:sgn(imag(a-b))<0;}
    bool operator == (const P& a,const P& b){ return sgn(real(a-b))==0 && sgn(imag(a-b))==0;}
}
 
inline bool cmp_x(const P& p,const P& q){return sgn(real(p-q))?real(p)<real(q):sgn(imag(p-q));}
inline bool cmp_y(const P& a, const P& b){return sgn(imag(a-b)) ? imag(a-b)<0 : sgn(real(a-b))<0;}
inline bool cmp_a(const P& a, const P& b){return sgn(arg(a)-arg(b)) ? arg(a)-arg(b)<0 : sgn(norm(a)-norm(b))<0;}
bool operator <  (const L& a,const L& b){ return a.s==b.s?a.t<b.t:a.s<b.s;}
bool operator == (const L& a,const L& b){ return a.s==b.s&&a.t==b.t;}
 
//$BFb@Q(B dot $B30@Q(B det
inline R dot(P o,P a,P b){a-=o,b-=o; return real(conj(a)*b);}
inline R det(P o,P a,P b){a-=o,b-=o; return imag(conj(a)*b);}
inline P vec(L l){return l.t-l.s;}

bool parallel(L a, L b) {return sgn(det(O,vec(a),vec(b)))==0;}

enum CCW{ LEFT = 1,RIGHT = 2,BACK = 4,FRONT = 8,ON = 16};
inline int ccw(P o,P a, P b) {//$BE@(Ba$B$HE@(Bb$B$,M?$($i$l$?Ld$$$K(B
    if (sgn(det(o,a,b)) > 0) return LEFT;    // counter clockwise
    if (sgn(det(o,a,b)) < 0) return RIGHT;   // clockwise
    if (sgn(dot(o,a,b)) < 0) return BACK;    // b--base--a on line
    if (sgn(norm(a-o)-norm(b-o)) < 0) return FRONT;   // base--a--b on line
    return ON;// base--b--a on line  a$B$H(Bb$B$N@~J,H=Dj$O$3$l(B
}

bool iss(L a,L b,int end=0){
    int s1=ccw(a.s,a.t,b.s)|ccw(a.s,a.t,b.t);
    int s2=ccw(b.s,b.t,a.s)|ccw(b.s,b.t,a.t);
    if(end) return (s1&s2)==(LEFT|RIGHT);
    return (s1|s2)&ON || (s1&s2)==(LEFT|RIGHT);
}

R dsp(L s,P p){
    if(sgn(dot(s.s,s.t,p))<=0) return abs(p-s.s);
    if(sgn(dot(s.t,s.s,p))<=0) return abs(p-s.t);
    return abs(det(s.s,s.t,p))/abs(s.t-s.s);
}

using vi = vector<int>;

const int N = 11;
vector<VP> islands[N];
vi graph[N];

inline bool check(L a, L b){
    if(not iss(a, b) or not parallel(a, b)) return false;

    rep(loop, 2){
        rep(looop, 2){
            if(a.s == b.s and dot(a.s, a.t, b.t) < EPS){
                return false;
            }
            swap(a.s, a.t);
        }
        swap(b.s, b.t);
    }

    return true;
}

int main(void){
    for(int Q; cin >> Q, Q;){
        int n = 0;
        map<string, int> s2i;
        rep(v, N) islands[v] = vector<VP>();

        rep(loop, Q){
            string name; cin >> name;
            if(s2i.find(name) == end(s2i)){
                s2i[name] = n++;
            }
            int v = s2i[name];

            islands[v].push_back(VP());
            for(R x, y; cin >> x, x != -1;){
                cin >> y;
                islands[v].back().push_back({x, y});
            }
        }

        rep(i, n) graph[i] = vi();
        rep(a, n){
            rep(b, a){
                [&]{
                    for(auto & pola : islands[a]){
                        for(auto & polb : islands[b]){
                            int alen = pola.size();
                            int blen = polb.size();

                            rep(ai, alen){
                                rep(bi, blen){
                                    L as = {pola[ai], pola[(ai + 1) % alen]};
                                    L bs = {polb[bi], polb[(bi + 1) % blen]};

                                    if(check(as, bs)){
                                        graph[a].push_back(b);
                                        graph[b].push_back(a);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }();
            }
        }

        const int inf = 1 << 30;
        vi dp = vi(1 << n, inf);
        dp[0] = 0;

        rep(sup, 1, 1 << n){
            for(int sub = (sup - 1) & sup; sub != sup; sub = (sub - 1) & sup){
                if(dp[sup] <= dp[sub]) continue;

                int vmask = sup & ~sub;

                [&]{
                    rep(a, n){
                        if(not ((vmask >> a) & 1)) continue;

                        for(auto & b : graph[a]){
                            if((vmask >> b) & 1) return;
                        }
                    }
                    dp[sup] = dp[sub] + 1;
                }();
            }
        }
        
        cout << dp.back() << endl;
    }

    return 0;
}
