#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int M = 5;

int calc_r(int t, int f){
    bool rev = false;

    if(f >= 3){
        f = M - f;
        rev ^= true;
    }
    if(t >= 3){
        t = M - t;
        rev ^= true;
    }

    int r = -1;
    if(t == 0 and f == 1) r = 3;
    if(t == 0 and f == 2) r = 1;
    if(t == 1 and f == 0) r = 2;
    if(t == 1 and f == 2) r = 5;
    if(t == 2 and f == 0) r = 4;
    if(t == 2 and f == 1) r = 0;
    assert(r != -1);

    return (rev ? M - r:r);
}

struct Dice {
    int t, f, r;
    
    Dice(int _t, int _f) : t(_t), f(_f) {
        r = calc_r(t, f);
    }

    void roll_f(){ swap(t, f); t = M - t; }
    void roll_b(){ rep(i, 3) roll_f(); }
    void roll_r(){ swap(r, t); t = M - t; }
    void roll_l(){ rep(i, 3) roll_r(); }
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int idx = 0, wall, white;
    map<char, int> dic;
    dic['m'] = idx++;
    dic['r'] = idx++;
    dic['b'] = idx++;
    dic['y'] = idx++;
    dic['c'] = idx++;
    dic['g'] = idx++;
    dic['k'] = wall  = idx++;
    dic['w'] = white = idx++;

    for(int w, h; cin >> w >> h, w;){
        vector<vi> field(h, vi(w));
        int sx, sy;
        rep(y, h){
            rep(x, w){
                char c; cin >> c;
                if(c == '#'){
                    sx = x, sy = y;
                    c = 'w';
                }
                field[y][x] = dic[c];
            }
        }
        vi perm(6); 
        rep(i, 6){
            char c; cin >> c;
            perm[i] = dic[c];
        }

        using Elem = tuple<int, int, int, int, int>;

        queue<Elem> q;
        map<Elem, int> min_dist;
        Elem start(sx, sy, 0, dic['r'], dic['m']);
        q.push(start);
        min_dist[start] = 0;

        int res = -1;
        while(q.size()){
            int x, y, num, t, f;
            auto cur = q.front(); q.pop();
            tie(x, y, num, t, f) = cur;

            if(num == 6){
                res = min_dist[cur];
                break;
            }

            rep(i, 4){
                int nx = x + dx[i], ny = y + dy[i];
                if(nx < 0 or w <= nx or ny < 0 or h <= ny or field[ny][nx] == wall) continue;

                Dice ndice(t, f);

                // const int dx[4]={1,0,-1,0};
                // const int dy[4]={0,1,0,-1};
                switch(i){
                    case 0: { ndice.roll_r(); break; }
                    case 1: { ndice.roll_f(); break; }
                    case 2: { ndice.roll_l(); break; }
                    case 3: { ndice.roll_b(); break; }
                }

                int nnum = num;
                if(field[ny][nx] != white){
                   if(perm[num] != field[ny][nx] or ndice.t != field[ny][nx]) continue;
                   nnum++;
                }

                Elem next(nx, ny, nnum, ndice.t, ndice.f);
                if(min_dist.find(next) != end(min_dist)) continue;
                min_dist[next] = min_dist[cur] + 1;
                q.push(next);
            }
        }

        if(res == -1){
            cout << "unreachable" << endl;
        }
        else {
            cout << res << endl; 
        }
    }

    return 0;
}
