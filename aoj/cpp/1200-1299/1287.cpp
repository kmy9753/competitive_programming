#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int n;
vector<vi> hands;

int resa, resb;

void dfs(int idx, int a, int b){
    if(idx == n){
        if(resb - resa > b - a or (resb - resa == b - a and a < resa)){
            resa = a, resb = b;
        }
        return;
    }

    vi cur = hands[idx];
    sort(_all(cur));

    int na = -1, nb = 1e8;
    const int M = 60;
    do {
        rep(h, M){
            int diff = (h - cur[0] + M) % M; assert((cur[0] + diff) % M == h); 
            int m = (cur[1] + diff) % M;
            int s = (cur[2] + diff) % M;

            if(not (h % 5 * 12 <= m and m < (h % 5 + 1) * 12)) continue;

            int t = h / 5 * M * M + m * M + s;

            if(idx == 0){
                dfs(1, t, t);
            }
            else {
                if(t <= b){
                    chmax(na, min(t, a));
                }
                if(t >= a){
                    chmin(nb, max(t, b));
                }
            }
        }
    } while(next_permutation(_all(cur)));

    if(idx != 0){
        if(na != -1)  dfs(idx + 1, na, b);
        if(nb != 1e8) dfs(idx + 1, a, nb);
    }
}

string t2s(int t){
    int h = t / 3600; t %= 3600;
    int m = t / 60;   t %= 60;
    int s = t;

    stringstream ss;
    ss << setfill('0') << setw(2) << h;
    ss << ":" << setfill('0') << setw(2) << m;
    ss << ":" << setfill('0') << setw(2) << s;

    return ss.str();
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    for(; cin >> n, n;){
        resa = -1, resb = 1e8;
        hands = vector<vi>(n, vi(3));
        rep(i, n) rep(j, 3) cin >> hands[i][j];

        dfs(0, -1, -1);
        cout << t2s(resa) << " " << t2s(resb) << endl;
    }

    return 0;
}
