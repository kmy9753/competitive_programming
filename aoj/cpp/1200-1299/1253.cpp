#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 3;
const int M = 7;

int top[N][N];
int front[N][N];
int R[N][N];

set<int> sums;

inline bool check(int f, int t, int r){
    if(min(f, M - f) == min(t, M - t) or 
       min(t, M - t) == min(r, M - r) or
       min(r, M - r) == min(f, M - f)) return false;

    if(f >= 4){
        f = M - f;
        r = M - r;
    }
    if(r >= 4){
        r = M - r;
        t = M - t;
    }

    int c = -1;
    if(f == 1 and r == 2) c = M - 4;
    if(f == 1 and r == 3) c = M - 2;
    if(f == 2 and r == 1) c = M - 3;
    if(f == 2 and r == 3) c = M - 6;
    if(f == 3 and r == 1) c = M - 5;
    if(f == 3 and r == 2) c = M - 1;
    assert(c != -1);

    return t == c;
}

void dfs(int depth){
    if(depth == N * N * N){
        int sum = 0;
        rep(i, N) rep(j, N) sum += R[i][j];
        sums.insert(sum);
        return;
    }

    int i = depth / (N * N), j = depth % (N * N) / N, k = depth % N;
    int& t_in = top[N - 1 - j][N - 1 - k];
    int& f_in = front[i][N - 1 - k];
    int& r_in = R[i][j];

    int ts = (t_in == 0 ? 1:t_in);
    int tt = (t_in == 0 ? M:t_in + 1);
    int fs = (f_in == 0 ? 1:f_in);
    int ft = (f_in == 0 ? M:f_in + 1);
    int rs = (r_in == 0 ? 1:r_in);
    int rt = (r_in == 0 ? M:r_in + 1);

    rep(f, fs, ft){
        rep(t, ts, tt){
            rep(r, rs, rt){
                if(check(f, t, r)){
                    int t_org = t_in, f_org = f_in, r_org = r_in;
                    t_in = t, f_in = f, r_in = r;
                    dfs(depth + 1);
                    t_in = t_org, f_in = f_org, r_in = r_org;
                }
            }
        }
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int T = 0; cin >> T;
    rep(loop, T){
        rep(i, N) rep(j, N) cin >> top[i][j];
        rep(i, N) rep(j, N) cin >> front[i][j];
        rep(i, N) rep(j, N) R[i][j] = 0;
        sums.clear();

        dfs(0);

        bool fst = true;
        for(auto& e : sums){
            cout << (fst ? "":" ") << e;
            fst = false;
        }
        if(fst) cout << 0;
        cout << endl;
    }

    return 0;
}
