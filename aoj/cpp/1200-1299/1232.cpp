#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
	const int N = (int)1e5;
	vector<bool> isP(N, true);
    isP[0] = isP[1] = false;
	for(int i = 2; i * i < N; i++) if(isP[i]) for(int j = i; i * j < N; j++) isP[i * j] = false;

    vi primes;
    rep(i, N) if(isP[i]) primes.pb(i);

    for(int m, a, b, p, q; cin >> m >> a >> b, m; cout << p << " " << q << endl){
        p = 0; q = 0;
        rep(i, primes.size()){
            range(j, i, primes.size()){
                if(m < (ll)primes[i] * primes[j]) break;
                if((double)a / b <= (double)primes[i] / primes[j] && (double)primes[i] / primes[j] <= 1 && 
                    p * q <= (ll)primes[i] * primes[j] && (ll)primes[i] * primes[j] <= m){
                    p = primes[i]; q = primes[j];
                }
            }
        }
    }

	return 0;
}
