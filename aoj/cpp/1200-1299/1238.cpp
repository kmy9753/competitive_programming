#include <bits/stdc++.h>

#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,x,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
 
using Edge = tuple<int, bool>;
int n, p1, p2;
vector<vector<Edge>> edge;
vector<bool> used;

void dfs(int v, bool is_kind, map<int, bool> & dic){
    if(used[v]) return;
    used[v] = true;

    dic[v] = is_kind;

    for(auto & e : edge[v]){
        int nv;
        bool info;
        tie(nv, info) = e;

        bool nis_kind = is_kind ^ info;
        dfs(nv, nis_kind, dic);
    }
}


signed main(void){
    for(int m; cin >> m >> p1 >> p2, m or p1 or p2;){
        n = p1 + p2;

        edge = vector<vector<Edge>>(n);
        used = vector<bool>(n);

        rep(loop, m){
            int x, y; string a;
            cin >> x >> y >> a;
            x--, y--;

            edge[x].push_back(Edge(y, a == "no"));
            edge[y].push_back(Edge(x, a == "no"));
        }

        vector<vi> dp(n + 1, vi(p1 + 1));
        vector<map<int, bool>> dic(n + 1);
        vector<vector<pair<int, bool>>> pre_idx(n + 1, vector<pair<int, bool>>(p1 + 1));
        dp[0][0] = 1;

        int idx = 0;

        rep(i, n){
            if(used[i]) continue;

            idx++;

            dfs(i, true, dic[idx]);

            int cnt_kind = 0, cnt_rev = 0;
            for(auto & e : dic[idx]){
                if(e.second) cnt_kind++;
                else         cnt_rev++;
            }

            rep(j, p1 + 1){
                if(j - cnt_kind >= 0 and chmax(dp[idx][j], dp[idx][j] + dp[idx - 1][j - cnt_kind])){
                    pre_idx[idx][j] = make_pair(j - cnt_kind, true);
                }

                if(j - cnt_rev >= 0 and chmax(dp[idx][j], dp[idx][j] + dp[idx - 1][j - cnt_rev])){
                    pre_idx[idx][j] = make_pair(j - cnt_rev, false);
                }
            }
        }

        if(dp[idx][p1] != 1){
            cout << "no" << endl;
            continue;
        }

        set<int> res;
        while(idx > 0){
            for(auto & e : dic[idx]){
                if(not (pre_idx[idx][p1].second ^ e.second)) res.insert(e.first);
            }
            p1 = pre_idx[idx][p1].first;
            idx--;
        }

        for(auto & e : res){
            cout << e + 1 << endl;
        }
        cout << "end" << endl;
    }

    return 0;
}
