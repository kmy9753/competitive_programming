#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

map<string, map<int, int>> si2val;
map<string, int> s2len;

int number(string& formula, int& idx){
    int ret = 0;
    while(isdigit(formula[idx])){
        ret = 10 * ret + (formula[idx] - '0');
        idx++;
    }
    return ret;
}

int expression(string& formula, int& idx){
    int ret = 0;
    if(isdigit(formula[idx])){
        ret = number(formula, idx);
    }
    else {
        int pre = idx;
        idx = formula.find('[', pre);
        string name = formula.substr(pre, idx - pre);
        assert(formula[idx++] == '[');
        int num = expression(formula, idx);
        assert(formula[idx++] == ']');
        
        if(num >= s2len[name] or si2val[name].find(num) == end(si2val[name])){
            throw 0;
        }
        ret = si2val[name][num];
    }

    return ret;
}

void parse(string& formula, int& idx){
    idx = formula.find('[');
    string name = formula.substr(0, idx);

    assert(formula[idx++] == '[');
    int num = expression(formula, idx);
    assert(formula[idx++] == ']');

    if(idx == formula.size()){ // declaration
        s2len[name] = num;
    }
    else { // assignment
        if(num >= s2len[name]){
            throw 1;
        }
        assert(formula[idx++] == '=');

        int rhs = expression(formula, idx);
        si2val[name][num] = rhs;
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    for(string op; cin >> op, op != ".";){
        vector<string> program;
        do {
            program.emplace_back(op);
        } while(cin >> op, op != ".");

        int n = program.size();
        int res = 0;

        s2len.clear();
        si2val.clear();

        rep(i, n){
            int idx = 0;
            try { parse(program[i], idx); }
            catch(int e){
                res = i + 1;
                break;
            }
        }

        cout << res << endl;
    }

    return 0;
}
