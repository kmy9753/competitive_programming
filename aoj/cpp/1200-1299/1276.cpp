#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

int main(void){
	const int N = 1299710;
	bool isP[N];
	rep(i, N) isP[i] = true;
	for(int i = 2; i * i < N; i ++) if(isP[i]) for(int j = i; i * j < N; j ++) isP[i * j] = false;

    for(int n, r, res; cin >> n, n; cout << res << endl){
        r = n;
        if(isP[n]) res = 0;
        else{
            while(!isP[--r]);
            while(!isP[++n]);
        }
        res = n - r;
    }

    return 0;
}
