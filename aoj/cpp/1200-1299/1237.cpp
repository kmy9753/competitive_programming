#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int t, res;
string msg;
vs reslog;

void dfs(int, string, vs);

int main(void){
    for(string num; cin >> t >> num, t;){
        res = -1;
        msg = "error";

        vs log;
        if(t == toInt(num)){
            res = t;
            msg = "ok";

            log.pb(num);
            reslog = log;
        }
        else dfs(0, num, log);
        
        if(msg == "ok"){
            cout << res;
            rep(i, reslog.size()){
                cout << " " << reslog[i];
            }
            cout << endl;
        }
        else{
            cout << msg << endl;
        }
    }

	return 0;
}

void dfs(int sum, string num, vs log){
    if(t < sum){
        return;
    }
    if(num.size() == 0){
        if(res == sum){
            msg = "rejected";
        }
        else if(res < sum){
            res = sum;
            reslog = log;
            msg = "ok";
        }

        return;
    }

    range(i, 1, num.size() + 1){
        string diff = num.substr(0, i);
        string nextnum = num.substr(i);

        //if(i != 1 && diff.at(0) == '0') continue;
        vs nextlog = log;
        nextlog.pb(diff);

        dfs(sum + toInt(diff), nextnum, nextlog);
    }
}
