#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using P = tuple<int, int, int>;

int n, k;

struct UnionFind {
    vector<int> data;
    UnionFind(int size) : data(size, -1) { }
    bool unionSet(int x, int y) {
        x = root(x); y = root(y);
        if (x != y) {
            if (data[y] < data[x]) swap(x, y);
            data[x] += data[y]; data[y] = x;
        }
        return x != y;
    }
    bool findSet(int x, int y) {
        return root(x) == root(y);
    }
    int root(int x) {
        return data[x] < 0 ? x : data[x] = root(data[x]);
    }
    int size(int x) {
        return -data[root(x)];
    }
};

using Elem = tuple<int, int, int, int>;

int kruskal(vector<vector<P>> graph, int diff){
    vi a_cnt(2);
    rep(v, n) for(auto & e : graph[v]) if(get<1>(e) == 0) get<0>(e) += diff;
    int ret = 0;
    rep(ai, 2){
        priority_queue<Elem, vector<Elem>, greater<Elem>> q;
        rep(v, n){
            for(auto & e : graph[v]){
                int u, w, l;
                tie(w, l, u) = e;

                l *= -1;
                e = P(w, l, u);
                assert(l == 0 or l == 1 or l == -1);

                if(v < u){
                    q.push(Elem(w, l, u, v));
                }
            }
        }

        UnionFind uf(n);
        ret = 0;
        int cnt = 0;
        while(q.size()){
            int w, l, v, u;
            tie(w, l, v, u) = q.top(); q.pop();

            assert(l == 0 or l == 1 or l == -1);

            if(uf.findSet(v, u)) continue;
            uf.unionSet(v, u);
            cnt++;

            ret += w;
            if(l == 0){
                a_cnt[ai]++;
            }
        }
        if(cnt != n - 1){
            return -1;
        }
    }

    ret -= diff * k;
    if(a_cnt[0] > a_cnt[1]) swap(a_cnt[0], a_cnt[1]);

    // cerr << a_cnt << endl;
    if(not (a_cnt[0] <= k and k <= a_cnt[1])) ret = (a_cnt[1] > k ? -1 : -2);

    return ret;
}

int offset = 10000;

int main(void){
    for(int m; cin >> n >> m >> k, n;){
        vector<vector<P>> graph(n);
        rep(loop, m){
            int u, v, w; char l; cin >> u >> v >> w >> l;
            u--, v--, w += offset;
            assert(0 <= u and u < n and 0 <= v and v < n);

            graph[u].push_back(P(w, l == 'A' ? 0:1, v));
            graph[v].push_back(P(w, l == 'A' ? 0:1, u));
        }

        int lb = -offset, ub = offset;
        int res = -1;
        rep(loop, 100){
            int mid = (lb + ub) / 2;

            int ret = kruskal(graph, mid);
            if(ret == -2){
                ub = mid;
            }
            else if(ret == -1){
                lb = mid;
            }
            else {
                res = ret - (offset * (n - 1));
                break;
            }
        }

        cout << res << endl;
    }

    return 0;
}
