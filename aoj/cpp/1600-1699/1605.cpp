#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

typedef tuple<int, int, int> P;

int n, k;

int prim(vector<vector<P>> graph, int diff) {
    vi a_cnt = {0, 0};
    int ret = 0;
    rep(ai, 2){
        rep(v, n){
            for(auto & e : graph[v]){
                int u, w, l;
                tie(w, l, u) = e;

                if(l == 0 and ai == 0) w += diff;
                e = P(w, l * -1, u);
            }
        }

        priority_queue<P, vector<P>, greater<P> > q;
        for(auto & e : graph[0]) q.push(e);

        vi used(n);
        used[0] = true;

        ret = 0;
        int cnt = 0;
        while(q.size()){
            int w, v, l;
            tie(w, l, v) = q.top(); q.pop();

            if(used[v]) continue;
            used[v] = true;
            cnt++;

            ret += w;
            a_cnt[ai] += (l == 0 ? 1 : 0);

            for(auto & e : graph[v]){
                int nv; tie(ignore, ignore, nv) = e;
                if(used[nv]) continue;
                q.push(e);
            }
        }
        assert(cnt == n - 1);
    }

    ret -= a_cnt[1] * diff;
    if(a_cnt[0] > a_cnt[1]) swap(a_cnt[0], a_cnt[1]);

    // cerr << a_cnt << endl;
    if(not (a_cnt[0] <= k and k <= a_cnt[1])) ret = (a_cnt[0] > k ? -1 : -2);

    return ret;
}

int offset = 10000;

int main(void){
    for(int m; cin >> n >> m >> k, n;){
        vector<vector<P>> graph(n);
        rep(loop, m){
            int u, v, w; char l; cin >> u >> v >> w >> l;
            u--, v--, w += offset;

            graph[u].push_back(P(w, l == 'A' ? 0:1, v));
            graph[v].push_back(P(w, l == 'A' ? 0:1, u));
        }

        int lb = -offset, ub = offset;
        int res = -1;
        rep(loop, 100){
            int mid = (lb + ub) / 2;

            int ret = prim(graph, mid);
            if(ret == -2){
                ub = mid;
            }
            else if(ret == -1){
                lb = mid;
            }
            else {
                res = ret - (offset * (n - 1));
                break;
            }
        }

        cout << res << endl;
    }

    return 0;
}
