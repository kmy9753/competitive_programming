#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

#define F first
#define S second
//((cost, dir), (y, x))
typedef pair<pii, pii> State;

enum {kStraight = 0, kRight, kBack, kLeft, kHalt};
enum {kEast = 0, kSourth, kWest, kNorth};

vi dx = { 1, 0,-1, 0};
vi dy = { 0, 1, 0,-1};

int main(void){
    for(int w, h; cin >> w >> h, w;){
        vvi field(h, vi(w));
        rep(y, h) rep(x, w) cin >> field[y][x];

        vi cost(4);
        for(auto& c : cost) cin >> c;

        priority_queue<State, vector<State>, greater<State>> q;
        q.push(mp(mp(0, kEast), mp(0, 0)));

        map<pair<int, pii>, int> used;
        int res;
        while(!q.empty()){
            pii cur_pos = q.top().S;
            int cur_cost = q.top().F.F;
            int cur_dir = q.top().F.S;
            q.pop();

            used[mp(cur_dir, cur_pos)] = true;
            if(cur_pos == mp(h - 1, w - 1)){
                res = cur_cost;
                break;
            }

            rep(op, 4){
                int next_cost = cur_cost;
                if(field[cur_pos.F][cur_pos.S] != op) next_cost += cost[op];

                pii next_pos = cur_pos;
                int next_dir = (cur_dir + op) % 4;
                next_pos.F += dy[next_dir];
                next_pos.S += dx[next_dir];

                if(next_pos.F < 0 || h <= next_pos.F ||
                   next_pos.S < 0 || w <= next_pos.S) continue;
                if(used[mp(next_dir, next_pos)]) continue;

                q.push(mp(mp(next_cost, next_dir), next_pos));
            }
        }
        cout << res << endl;
    }

	return 0;
}
