#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

/*--------------------------*/
const int kMax_n = 1001; //$BD:E@?t$N:GBgCM(B

int n; //$BD:E@?t(B
vvi G(kMax_n);
vi match(kMax_n); //$B%^%C%A%s%0$N%Z%"(B
vi used(kMax_n);

// u$B$H(Bv$B$r7k$VJU$r%0%i%U$KDI2C$9$k(B
void add_edge(int u, int v){
    G[u].pb(v);
    G[v].pb(u);
}

// $BA}2C%Q%9$r(BDFS$B$GC5$9(B
bool dfs(int v){
    used[v] = true;
    
    for(auto u : G[v]){
        int w = match[u];

        if(w < 0 || !used[w] && dfs(w)){
            match[v] = u;
            match[u] = v;

            return true;
        }
    }

    return false;
}

//$BFsJ,%0%i%U$N:GBg%^%C%A%s%0$r5a$a$k(B
int b_match(void){
    int res = 0;

    fill(all(match), -1);

    rep(i, n){
        if(match[i] < 0){
            fill(all(used), false);

            if(dfs(i)) res++;
        }
    }

    return res;
}

void init_G(void){
    for(auto && g : G) g.clear();
}
/*--------------------------*/

int main(void){
    for(int mm, nn; cin >> mm >> nn, mm;){
        vi b(mm), r(nn);
        for(auto && bb : b) cin >> bb;
        for(auto && rr : r) cin >> rr;

        n = mm + nn;
        init_G();

        rep(i, mm){
            rep(j, nn){
                if(__gcd(b[i], r[j]) == 1) continue;

                add_edge(i, mm + j);
            }
        }

        cout << b_match() << endl;
    }

	return 0;
}
