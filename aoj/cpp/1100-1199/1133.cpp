#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,(n))
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define F first
#define S second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

vector<pii> bh, fa, pt;

struct R{
    double lh, rh, a;
    int lx, rx;

    void merge(R & ana){
        if(lx == ana.rx){
            lx = ana.lx;
            lh = ana.lh;
        }
        else if(rx == ana.lx){
            rx = ana.rx;
            rh = ana.rh;
        }

        a += ana.a;
    }
};

int n;
vector<R> anas;

void addJaguchi(int x, double a){
    for(auto && ana : anas){
        if(x < ana.lx || ana.rx < x) continue;

        ana.a += a;
        break;
    }
}

int main(void){
    int D; cin >> D;
    while(D--){
        cin >> n;
        bh = vector<pii>(n); for(auto && c : bh) cin >> c.F >> c.S;
        int m; cin >> m;
        fa = vector<pii>(m); for(auto && c : fa) cin >> c.F >> c.S;
        int L; cin >> L;

        rep(loop, L){
            int P; double T; cin >> P >> T;
            double res = 0.0;

            anas = vector<R>();
            int lx = 0; double lh = 50.0;
            for(auto && c : bh){
                int rx = c.F; double rh = c.S;
                anas.pb({lh, rh, 0.0, lx, rx});
                lx = rx, lh = rh;
            }
            anas.pb({lh, 50.0, 0.0, lx, 100});

            for(auto && c : fa) addJaguchi(c.F, c.S / 30.0);

            while(T > eps){
                double overT = INF;
                for(auto && ana : anas){
                    if(ana.a < eps) continue;

                    int w = ana.rx - ana.lx;
                    double h = min(ana.lh, ana.rh);
                    if(h < eps) { overT = 0.0; break; }

                    overT = min(overT, (w * h) / ana.a);
                }

                if(T - overT < eps) { overT = T; }
                T -= overT;

                for(auto && ana : anas){
                    int w = ana.rx - ana.lx;
                    double waterh = overT * ana.a / w;
                    ana.lh -= waterh;
                    ana.rh -= waterh;

                    if(ana.lx < P && P < ana.rx) res += waterh;
                }

                if(anas[0].lh < eps || anas[anas.size() - 1].rh < eps) break;;
                rep(i, (int)anas.size()){
                    if(anas[i].lh < eps && anas[i - 1].rh < eps){
                        anas[i].merge(anas[i - 1]);
                        anas.erase(anas.begin() + i - 1);
                        break;
                    }
                    if(anas[i].rh < eps && anas[i + 1].lh < eps){
                        anas[i].merge(anas[i + 1]);
                        anas.erase(anas.begin() + i + 1);
                        break;
                    }

                    if(anas[i].lh < eps && anas[i].rh < eps){
                        anas[i - 1].a += anas[i].a / 2.0;
                        anas[i + 1].a += anas[i].a / 2.0;
                        anas[i].a = 0.0;
                    }
                    else if(anas[i].lh < eps){
                        anas[i - 1].a += anas[i].a;
                        anas[i].a = 0.0;
                    }
                    else if(anas[i].rh < eps){
                        anas[i + 1].a += anas[i].a;
                        anas[i].a = 0.0;
                    }
                }
            }

            printf("%.8f\n", res);
        }
    }

	return 0;
}
