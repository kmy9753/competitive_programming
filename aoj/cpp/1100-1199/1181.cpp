#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;
#define X first
#define Y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

struct Dice {
    int top, front;
    static const int kRightMap[6][6];

    Dice(int t, int f)
    : top(t),
      front(f) {};

    int RollFront(){
        int nextBottom = front;

        front = top;
        top = 5 - nextBottom;

        return nextBottom;
    }
    int RollRight(){
        int nextBottom = kRightMap[top][front] - 1;

        top = 5 - nextBottom;

        return nextBottom;
    }
    int RollBack(){
        int nextBottom = 5 - front;

        front = 5 - top;
        top = 5 - nextBottom;

        return nextBottom;
    }
    int RollLeft(){
        int nextBottom = 5 - (kRightMap[top][front] - 1);

        top = 5 - nextBottom;

        return nextBottom;
    }
};

const int Dice::kRightMap[6][6] = 
{
    {0,3,5,2,4,0},
    {4,0,1,6,0,3},
    {2,6,0,0,1,5},
    {5,1,0,0,6,2},
    {3,0,6,1,0,4},
    {0,4,2,5,3,0}
};

const int kOffset = 5;

vvi heights;
vvi nums;

int dx[] = { 1, 0,-1, 0};
int dy[] = { 0,-1, 0, 1};

void dfs(pii pos, Dice dice){
    Dice nextDice = dice;
    pii nextPos;
    int nextBottom = -1;

    rep(i, 4){

        int bottom;
        Dice tmp = dice;
        switch(i){
        case 0: {bottom = tmp.RollRight(); break;}
        case 1: {bottom = tmp.RollFront(); break;}
        case 2: {bottom = tmp.RollLeft(); break;}
        case 3: {bottom = tmp.RollBack(); break;}
        default: break;
        }

        if(heights[pos.Y + dy[i]][pos.X + dx[i]] < heights[pos.Y][pos.X] && 3 <= bottom && nextBottom < bottom){
            nextPos = mp(pos.X + dx[i], pos.Y + dy[i]);
            nextDice = tmp;
            nextBottom = bottom;
        }
    }

    if(nextBottom != -1){
        dfs(nextPos, nextDice);
    }
    else{
        heights[pos.Y][pos.X]++;
        nums[pos.Y][pos.X] = dice.top;
    }
}

int main(void){
    for(int n; cin >> n, n;){
        heights = vvi(kOffset * 2, vi(kOffset * 2, 0));
        nums = vvi(kOffset * 2, vi(kOffset * 2, -1));

        rep(i, n){
            int t, f; cin >> t >> f;
            t--, f--;
            Dice dice(t, f);

            dfs(mp(kOffset, kOffset), dice);
        }

        map<int, int> cnt;
        each(yitr, nums){
            each(xitr, *yitr){
                cnt[*xitr]++;
            }
        }

        rep(i, 6){
            cout << (i ? " ":"") << cnt[i];
        }
        cout << endl;
    }

	return 0;
}

