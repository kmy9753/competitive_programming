#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, m, c, s, g; cin >> n >> m >> c >> s >> g, n;){
        s--, g--;

        vector<vvi> minDist_only(c, vvi(n, vi(n, inf)));
        rep(cc, c) rep(i, n) minDist_only[cc][i][i] = 0;
        rep(i, m){
            int x, y, d, cc; cin >> x >> y >> d >> cc;
            x--, y--; cc--;

            minDist_only[cc][x][y] = min(minDist_only[cc][x][y], d);
            minDist_only[cc][y][x] = min(minDist_only[cc][y][x], d);
        }

        vi p(c);
        rep(i, c) cin >> p[i];

        vvi q(c), r(c);
        rep(i, c){
            q[i] = vi(p[i] - 1);
            r[i] = vi(p[i]);

            for(auto && qq : q[i]) cin >> qq;
            for(auto && rr : r[i]) cin >> rr;
        }

        vvi cost(c, vi(20001));
        rep(cc, c){
            int idx = 0;
            range(d, 1, cost[cc].size()){
                if(idx != p[cc] - 1 && d > q[cc][idx]) idx++;

                cost[cc][d] = cost[cc][d - 1] + r[cc][idx];
            }
        }

        rep(cc, c){
            rep(k, n) rep(i, n) rep(j, n)
                minDist_only[cc][i][j] = min(minDist_only[cc][i][j], minDist_only[cc][i][k] + minDist_only[cc][k][j]);

            rep(i, n){
                rep(j, n){
                    if(minDist_only[cc][i][j] >= inf) continue;

                    assert(minDist_only[cc][i][j] >= 0 && minDist_only[cc][i][j] < 20001);
                    minDist_only[cc][i][j] = cost[cc][minDist_only[cc][i][j]];
                }
            }
        }

        vvi minDist(n, vi(n, inf));
        rep(cc, c) rep(i, n) rep(j, n) minDist[i][j] = min(minDist[i][j], minDist_only[cc][i][j]);
        rep(k, n) rep(i, n) rep(j, n) minDist[i][j] = min(minDist[i][j], minDist[i][k] + minDist[k][j]);

        if(minDist[s][g] == inf) cout << -1 << endl;
        else cout << minDist[s][g] << endl;
    }

	return 0;
}
