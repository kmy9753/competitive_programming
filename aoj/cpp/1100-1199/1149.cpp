#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define width first
#define depth second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n, w, d; cin >> n >> w >> d, w;){
        vector<pii> cakes;
        cakes.pb(mp(w, d));

        rep(i, n){
            int p, s; cin >> p >> s;
            p--;
            s %= (2 * (cakes[p].width + cakes[p].depth));

            vector<pii> new_cakes;
            if(s < cakes[p].width){
                new_cakes.pb(mp(s, cakes[p].depth));
                new_cakes.pb(mp(cakes[p].width - s, cakes[p].depth));
            }
            else if((s -= cakes[p].width) < cakes[p].depth){
                new_cakes.pb(mp(cakes[p].width, s));
                new_cakes.pb(mp(cakes[p].width, cakes[p].depth - s));
            }
            else if((s -= cakes[p].depth) < cakes[p].width){
                new_cakes.pb(mp(s, cakes[p].depth));
                new_cakes.pb(mp(cakes[p].width - s, cakes[p].depth));
            }
            else{
                s -= cakes[p].width;
                new_cakes.pb(mp(cakes[p].width, s));
                new_cakes.pb(mp(cakes[p].width, cakes[p].depth - s));
            }

            cakes.erase(cakes.begin() + p);

            if(new_cakes[1].width * new_cakes[1].depth < new_cakes[0].width * new_cakes[0].depth){
                swap(new_cakes[0], new_cakes[1]);
            }

            rep(i, 2){
                cakes.pb(new_cakes[i]);
            }
        }

        vi res;
        rep(i, cakes.size()){
            res.pb(cakes[i].width * cakes[i].depth);
        }

        sort(all(res));
        rep(i, res.size()){
            cout << (i ? " ":"") << res[i];
        }
        cout << endl;
    }

	return 0;
}
