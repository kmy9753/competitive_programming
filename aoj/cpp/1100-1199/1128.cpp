#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 10;
int w, h;
int of[N][N];
vi field;

int p2w[N][N];
int res;

inline bool calcnp(int & y, int & x){
    do {
        if(p2w[y][x] == 0 and (field[y] & (1 << x))) return false;

        x++;
        if(x == w){
            y = y + 1;
            x = 0;
        }
    } while(p2w[y][x] == 0);

    return true;
}

void dfs(int cy, int cx, int cost){
    if(cost >= res) return;

    // cout << "-----cost ------>: " << cost << " (" << cx << ", " << cy << ")" << endl;
    // rep(y, h){
    //     rep(x, w){
    //         if((field[y] & (1 << x)) == 0){
    //             cout << "0 ";
    //         }
    //         else {
    //             cout << "1 ";
    //         }
    //     }
    //     cout << endl;
    // }

    if((field[cy] & (1 << cx)) == 0){
        int ny = cy, nx = cx;
        if(calcnp(ny, nx)){
            dfs(ny, nx, cost);
        }
    }

    int ww = p2w[cy][cx];

    int mask = 0;
    rep(loop, ww){
        mask <<= 1;
        mask  |= (1 << cx);
    }

    bool correct = false;
    vi oof = field;
    rep(y, cy, cy + ww){
        if(not correct and (field[y] & mask) != 0){
            correct = true;
        }
        field[y] &= ~mask;
    }

    if(not correct) return;

    bool end = true;
    rep(y, h){
        if(field[y] != 0){
            end = false;
        }
    }
    if(end){
        chmin(res, cost);
    }
    else {
        int ny = cy, nx = cx;
        if(calcnp(ny, nx)){
            dfs(ny, nx, cost + 1);
        }
    }

    field = oof;
    //rep(y, cy, cy + ww){
    //    field[y] |= mask;
    //}
    // cout << "=====end cost ==========>: " << cost << endl;
}

inline int calc(int sy, int sx, int ww){
    int ret = 0;

    rep(y, sy, h){
        rep(x, sx, sx + ww){
            if(of[y][x] == 0){
                return ret;
            }
        }
        ret++;
    }

    return ret;
}

int main(void){
    for(; cin >> w >> h, w;){
        rep(y, h){
            rep(x, w){
                p2w[y][x] = 0;
            }
        }

        int sy = -1, sx;
        rep(y, h){
            rep(x, w){
                cin >> of[y][x];

                if(sy == -1 and of[y][x] == 1){
                    sy = y;
                    sx = x;
                }
            }
        }

        rep(y, h){
            rep(x, w){
                if(of[y][x] == 0) continue;

                rep(ww, 1, w - x + 1){
                    int hh = min(ww, calc(y, x, ww));

                    if(hh == ww){
                        p2w[y][x] = ww;
                    }
                }
            }
        }

        rrep(y, h - 1){
            rrep(x, w - 1){
                if(p2w[y][x] > p2w[y + 1][x]) p2w[y + 1][x] = 0;
                if(p2w[y][x] > p2w[y][x + 1]) p2w[y][x + 1] = 0;
                if(p2w[y][x] > p2w[y + 1][x + 1]) p2w[y + 1][x + 1] = 0;
            }
        }

        int fst = 0;
        rep(y, h){
            rep(x, w){
                if(p2w[y][x] == 1){
                    of[y][x] = 0;
                    fst++;
                    p2w[y][x] = 0;
                }
            }
        }

        field = vi(h);
        rep(y, h){
            int & cur = field[y] = 0;
            rep(x, w){
                if(of[y][x]){
                    cur |= (1 << x);
                }
            }
        }

        // rep(y, h){
        //     rep(x, w){
        //         cout << p2w[y][x] << " ";
        //     }
        //     cout << endl;
        // }

        if(sy == -1){
            res = 0;
        }
        else {
            res = 1000;
            dfs(sy, sx, fst + 1);
        }
        cout << res << endl;
    }

    return 0;
}
