#include <iostream>

using namespace std;

int main(void){
	int n; cin >> n;

	while(n --){
		int y, m, d; cin >> y >> m >> d;
		int count = 0;
		for(; y < 1000; y ++, m = 1)
			for(; m <= 10; m ++, d = 1)
				for(; d <= ((y % 3 && !(m % 2)) ? 19:20); d ++)
					count ++;
		cout << count << endl;
	}

	return 0;
}
