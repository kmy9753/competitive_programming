#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int pos;
string str;

int operate(void);
int mins(int val);
int pls(int val1, int val2);
int multi(int val1, int val2);

int main(void){
    string in;
    for(int res; cin >> in, in != "."; cout << res << endl){
        res = 0;
        rep(P, 3){
            rep(Q, 3){
                rep(R, 3){
                    str = in;
                    replace(all(str), 'P', (char)(P + '0'));
                    replace(all(str), 'Q', (char)(Q + '0'));
                    replace(all(str), 'R', (char)(R + '0'));

                    pos = 0;
                    if(operate() == 2) res++;
                }
            }
        }
    }

	return 0;
}

int operate(void){
    int ret;

    if(str[pos] == '('){
        pos++;
        int a = operate();
   
        char op = str[pos];

        pos++;
        if(op == '+'){
            ret = pls(a, operate());
        }
        else ret = multi(a, operate());

        pos++;
    }
    else if(str[pos] == '-'){
        int cnt = 0;
        while(str[pos] == '-') (++cnt) %= 2, pos++;

        if(cnt) ret = mins(operate());
        else ret = operate();
    }
    else{
        ret = str[pos] - '0';
        pos++;
    }

    return ret;
}

int mins(int val){
    int ret;

    if(val == 0) ret = 2;
    else if(val == 2) ret = 0;
    else ret = val;

    return ret;
}

int pls(int val1, int val2){
    return max(val1, val2);
}

int multi(int val1, int val2){
    return min(val1, val2);
}

