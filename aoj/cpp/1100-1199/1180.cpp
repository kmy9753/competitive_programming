#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

#define rep(i, n) for(int i = 0; i < n; i ++)

using namespace std;

int getNext(int, int);
int getNum(vector<int>);

int main(void){
	int a[21];
	for(int l; cin >> a[0] >> l, l;){
		for(int i = 1; i < 21; i ++){
			a[i] = getNext(a[i - 1], l);
			
			int* p = find(a, a + i, a[i]);
			if(p != a + i){
				cout << p - a << " " << *p << " " << i - (p - a) << endl;
				break;
			}
		}
	}
	return 0;
}

int getNext(int a, int l){
	vector<int> number;

	while(l --){
		number.push_back(a % 10);
		a /= 10;
	}

	sort(number.begin(), number.end());
	int minNumber = getNum(number);
	reverse(number.begin(), number.end());
	int maxNumber = getNum(number);
	
	return maxNumber - minNumber;
}

int getNum(vector<int> a){
	int result = 0;
	rep(i, a.size()) result += a[i] * (int)pow(10., int(a.size() - i - 1));

	return result;
} 
