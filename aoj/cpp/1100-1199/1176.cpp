#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int h, w, s;

constexpr int kMaxWH = 33;

#define F first
#define S second

// $BL$Dj5A!$IT2DG=(B
constexpr int kUnd = -2;
constexpr int kImp = -1;

// ($B%0%k!<%W?t(B, $BM=HwNO(B)
pii memo[kMaxWH][kMaxWH][kMaxWH][kMaxWH];

// $B>CHqEENO$NOB(B
int sum[kMaxWH + 1][kMaxWH + 1];

int field[kMaxWH][kMaxWH];

pii dfs(pii lu, pii rd){
    if(memo[lu.F][lu.S][rd.F][rd.S] != mp(kUnd, kUnd)){
        return memo[lu.F][lu.S][rd.F][rd.S];
    }

    int yobi =  s - (sum[h][w] - (sum[rd.F + 1][rd.S + 1] - sum[rd.F + 1][lu.S] - sum[lu.F][rd.S + 1] + sum[lu.F][lu.S]));
    if(yobi < 0){
        memo[lu.F][lu.S][rd.F][rd.S] = mp(kImp, kImp);
        return mp(kImp, kImp);
    }

    pii ret = mp(kImp, kImp);
    range(y, lu.F, rd.F){
        pii ret1 = dfs(mp(lu.F, lu.S), mp(y, rd.S));
        pii ret2 = dfs(mp(y + 1, lu.S), mp(rd.F, rd.S));

        if(ret1 == mp(kImp, kImp) || ret2 == mp(kImp, kImp)) continue;

        ret = max(ret, mp(ret1.F + ret2.F, min(ret1.S, ret2.S)));
    }
    range(x, lu.S, rd.S){
        pii ret1 = dfs(mp(lu.F, lu.S), mp(rd.F, x));
        pii ret2 = dfs(mp(lu.F, x + 1), mp(rd.F, rd.S));

        if(ret1 == mp(kImp, kImp) || ret2 == mp(kImp, kImp)) continue;

        ret = max(ret, mp(ret1.F + ret2.F, min(ret1.S, ret2.S)));
    }

    if(ret == mp(kImp, kImp)){
        ret = mp(1, yobi);
    }

    memo[lu.F][lu.S][rd.F][rd.S] = ret;
    return ret;
}

int main(void){
    for(; cin >> h >> w >> s, h;){
        rep(y, h) rep(x, w) cin >> field[y][x];

        rep(y, h) rep(x, w) rep(yy, h) rep(xx, w) memo[y][x][yy][xx] = mp(kUnd, kUnd);

        rep(y, h + 1) sum[y][0] = 0;
        rep(x, w + 1) sum[0][x] = 0;
        rep(y, h){
            rep(x, w){
                sum[y + 1][x + 1] = sum[y][x + 1] + sum[y + 1][x] + field[y][x] - sum[y][x];
            }
        }

        pii res = dfs(mp(0, 0), mp(h - 1, w - 1));
        cout << res.F << " " << res.S << endl;
    }

	return 0;
}
