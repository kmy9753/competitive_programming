#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

using Piece = tuple<double, int>;
constexpr double eps = 1e-10;

Piece merge(Piece lhs, Piece rhs){
    double xg_l, xg_r;
    int m_l, m_r;

    tie(xg_l, m_l) = lhs;
    tie(xg_r, m_r) = rhs;

    xg_r = (m_r * xg_r + m_l * xg_l) / (m_r + m_l);
    m_r += m_l;

    return tie(xg_r, m_r);
}

vector<string> _field;
vector<vector<int>> field;

int h, w;

vector<int> dx = { 1, 0,-1, 0};
vector<int> dy = { 0,-1, 0, 1};

void dfs(int y, int x, char c, int idx){
    if(field[y][x] != -1) return;

    field[y][x] = idx;

    rep(i, 4){
        int ny = y + dy[i], nx = x + dx[i];

        if(ny < 0 or h <= ny or
           nx < 0 or w <= nx or
           _field[ny][nx] != c) continue;

        dfs(ny, nx, c, idx);
    }
}

int main(void){
    for(; cin >> w >> h, w;){
        _field = vector<string>(h);
        for(auto & e : _field){
            cin >> e;
        }

        field = vector<vector<int>>(h, vector<int>(w, -1));
        int num_piece = 0;
        rep(y, h){
            rep(x, w){
                if(_field[y][x] == '.' or field[y][x] != -1) continue;

                dfs(y, x, _field[y][x], num_piece++);
            }
        }

        vector<Piece> piece(num_piece);
        for(auto & e : piece) e = Piece(0.0, 0);

        vector<int> y_bottom(num_piece, -1), xl(num_piece, 10000), xr(num_piece, -10000), i_bottom(num_piece, -1);
        rep(y, h){
            rep(x, w){
                if(field[y][x] == -1) continue;

                int idx = field[y][x];
                piece[idx] = merge(piece[idx], Piece(x, 1));

                if(y + 1 < h and field[y + 1][x] != -1 and field[y + 1][x] != field[y][x]){
                    i_bottom[idx] = field[y + 1][x];
                    y_bottom[idx] = y;
                    xl[idx] = min(xl[idx], x);
                    xr[idx] = max(xr[idx], x);
                }
            }
        }

        rep(x, w){
            if(field[h - 1][x] == -1) continue;

            int idx = field[h - 1][x];
            xl[idx] = min(xl[idx], x);
            xr[idx] = max(xr[idx], x);
            y_bottom[idx] = h - 1;
        }

        string res = "STABLE";
        [&]{
            rep(y, h){
                rep(i, num_piece){
                    if(y_bottom[i] != y) continue;

                    double xg;
                    tie(xg, ignore) = piece[i];
                    if(xg - eps <= xl[i] - 0.5 or xr[i] + 0.5 <= xg + eps){
                        res = "UNSTABLE";
                        return;
                    }

                    if(i_bottom[i] != -1){
                        piece[i_bottom[i]] = merge(piece[i_bottom[i]], piece[i]);
                    }
                }
            }
        }();

        cout << res << endl;
    }

    return 0;
}
