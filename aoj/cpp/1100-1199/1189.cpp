#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int N = 1000;

#define F first
#define S second

vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

int main(void){
    vvi cave(N + 1, vi(N + 1)); 

    pii pos = mp(N / 2, N / 2);
    cave[N / 2][N / 2] = 1;

    int limit = 1, cnt = 0, dir = 0, inc = false;
    range(i, 2, N * N + 1){
        if(cnt++ == limit){
            (++dir) %= dx.size();

            if(inc) limit++;
            cnt = 0;

            inc = !inc;

            i--;
            continue;
        }

        pos.F += dy[dir];
        pos.S += dx[dir];

        cave[pos.F][pos.S] = i;
    }

//    range(y, 490, 510){
//        range(x, 490, 510)
//            cout << cave[y][x] << " ";
//        cout << endl;
//    }

	const int MAX_N = (int)1e6 + 1;
	vector<bool> isP(MAX_N, true);
    isP[0] = isP[1] = false;
	for(int i = 2; i * i < MAX_N; i++) if(isP[i]) for(int j = i; i * j < MAX_N; j++) isP[i * j] = false;

    for(int m, n; cin >> m >> n, m; ){
        
        //($B=8$a$?8D?t(B, $B:G8e$KDL$C$?AG?t(B)
        vector<vector<pii>> dp(N + 1, vector<pii>(N + 1, mp(-1, -1)));

        rep(y, N + 1) rep(x, N + 1) if(cave[y][x] == n) dp[y][x] = isP[cave[y][x]] ? mp(1, cave[y][x]):mp(0, 0);

        pii res = mp(-1, -1);
        rep(y, N + 1){
            rep(x, N + 1){
                if(cave[y][x] > m || dp[y][x].F == -1) continue;

                res = max(res, dp[y][x]);

                rep(i, 3){
                    int xx = x + dx[i];
                    int yy = y + 1;

                    if(xx < 0 || N < xx ||
                       yy < 0 || N < yy) continue;

                    dp[yy][xx] = max(dp[yy][xx], mp(dp[y][x].F + (isP[cave[yy][xx]] ? 1:0), isP[cave[yy][xx]] ? cave[yy][xx]:dp[y][x].S));
                }
            }
        }

        cout << res.F << " " << res.S << endl;
    }

    return 0;
}
