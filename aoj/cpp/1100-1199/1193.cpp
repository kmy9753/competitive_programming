#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int h, w = 5, res; cin >> h, h; cout << res << endl){
        res = 0;

        vvi m(h, vi(w));
        rep(y, h) rep(x, w) cin >> m[y][x + 1];

        int diff;
        do{
            diff = 0;
            
            rep(y, h) rep(x, w + 2){
                if(m[y][x] == 0)
                    continue;

                int cnt = 0, color = m[y][x];
                rep(i, w){
                    if(m[y][x + i] != color) break;
                    cnt++;
                }

                if(cnt < 3) continue;

                diff += cnt * color;
                rep(i, cnt){
                    m[y][x + i] = 0;
                }
            }

            for(int y = h - 1; 0 <= y; y--){
                rep(x, w + 2){
                    if(m[y][x] != 0) continue;

                    rep(i, h){
                        if(y - i < 0) break;
                        if(m[y - i][x] != 0){
                            m[y][x] = m[y - i][x];
                            m[y - i][x] = 0;
                            break;
                        }
                    }
                }
            }

            res += diff;
        } while(diff != 0);
    }

	return 0;
}
