#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define x first
#define y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    double res;
    for(int r, n; cin >> r >> n, r; cout << res << endl){
        res = 0.;
        map<int, map<int, int> > shadows;
        rep(i, n){
            int left, right, h; cin >> left >> right >> h;
            range(x, left, right){
                shadows[x][x + 1] = max(shadows[x][x + 1], h);
            }
        }

        bool ok = false;
        for(; !ok; res += 0.0001){
            pair<double, double> o = mp(0., -r + res);

            range(x, -r, r){
                rep(i, 2){
                    double d = sqr(x + i - o.x) + sqr(shadows[x][x + 1] - o.y);

                    if(d <= (double)sqr(r)){
                        ok = true;
                        break;
                    }
                }
            }
        }
    }

	return 0;
}
