#include <iostream>

using namespace std;

int main(void){
	const int N = 123456 * 2 + 1;
	bool num[N];
	for(int i = 1; i < N; i ++) num[i] = i - 1;
	for(int i = 2; i * i < N; i ++) if(num[i]) for(int j = i; i * j < N; j ++) num[i * j] = false;

	for(int n, count = 0; cin >> n, n; cout << count << endl, count = 0)
		for(int i = n + 1; i <= 2 * n; i ++) if(num[i]) count ++;

	return 0;
}
