#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int w, h, sum;
vvi field_origin;

string res;

vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

#define F first
#define S second

typedef complex<int> P;

#define X real()
#define Y imag()

inline P f(P p, P delta, int k, int r){
    if(r == 0) r = -1;

    switch(k){
    case 0: p += P( delta.X * r, delta.Y); break;
    case 1: p += P(-delta.Y * r, delta.X); break;
    case 2: p += P(-delta.X * r,-delta.Y); break;
    case 3: p += P( delta.Y * r,-delta.X); break;
    }

    return p;
}

bool check(P p1, P p2, int k, int r){

    vvi field = field_origin;

    field[p1.Y][p1.X] = 8;
    field[p2.Y][p2.X] = 9;
    int cnt = 1;

    queue<P> q;
    q.push(P(0, 0));

    while(!q.empty()){
        P cur_delta = q.front();
        q.pop();

        rep(i, 4){
            P next_delta = P(cur_delta.X + dx[i], cur_delta.Y + dy[i]);

            P np1 = p1 + next_delta;
            P np2 = f(p2, next_delta, k, r);

            if(np1.X < 0 || w <= np1.X || np1.Y < 0 || h <= np1.Y ||
               np2.X < 0 || w <= np2.X || np2.Y < 0 || h <= np2.Y ||
               np1 == np2 || field[np1.Y][np1.X] != 1 || field[np2.Y][np2.X] != 1) continue;

            field[np1.Y][np1.X] = 8;
            field[np2.Y][np2.X] = 9;

            q.push(next_delta);

            cnt++;
        }
    }

    if(cnt == sum / 2){
        return true;
    }
    return false;
}

int main(void){
    for(; cin >> w >> h, w;){

        res = "NO";
        
        field_origin = vvi(h, vi(w));
        rep(y, h) rep(x, w) cin >> field_origin[y][x];

        sum = 0;
        vector<P> choco;
        rep(y, h) rep(x, w) if(field_origin[y][x] == 1) sum++, choco.pb(P(x, y));

        if(sum % 2 == 1){
            cout << res << endl;
            continue;
        }

        rep(i, choco.size()){
            range(j, i + 1, choco.size()){
                rep(k, 4){
                    rep(r, 2){
                        if(check(choco[i], choco[j], k, r)){
                            res = "YES";
                        }
                    }
                }
            }
        }

        cout << res << endl;
    }

	return 0;
}
