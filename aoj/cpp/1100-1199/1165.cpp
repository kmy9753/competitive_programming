#include <iostream>
#include <utility>
#include <vector>

#define X first
#define Y second
#define rep(i, n) for(int i = 0; i < n; i ++)
#define LEFT 0
#define RIGHT 1
#define TOP 2
#define BOTTOM 3

using namespace std;

const pair<int, int> PATTERN[4] = 
{make_pair(-1, 0), make_pair(0, -1), make_pair(1, 0), make_pair(0, 1)};

int main(void){
	for(int n, edge[4]; cin >> n, n; cout << edge[RIGHT] - edge[LEFT] + 1 << " " << edge[TOP] - edge[BOTTOM] + 1 << endl){
		rep(i, 4) edge[i] = 0;
		vector<pair<int, int> > squairs(n);
		
		rep(i, n){
			if(!i){ squairs[i] = make_pair(0, 0); continue; }
			int index, d; cin >> index >> d;
			squairs[i] = make_pair(squairs[index].X + PATTERN[d].X, squairs[index].Y + PATTERN[d].Y);
			if(squairs[i].X < edge[LEFT]) edge[LEFT] = squairs[i].X;
			else if(squairs[i].X > edge[RIGHT]) edge[RIGHT] = squairs[i].X;
			else if(squairs[i].Y < edge[BOTTOM]) edge[BOTTOM] = squairs[i].Y;
			else if(squairs[i].Y > edge[TOP]) edge[TOP] = squairs[i].Y;
		}
	}

	return 0;
}
