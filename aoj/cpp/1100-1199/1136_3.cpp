#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define x first
#define y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n; cin >> n, n;){
        vector< vector<pii> > lines;
        rep(i, n + 1){
            int m; cin >> m;

            vector<pii> cur(m);
            rep(j, m){
                cin >> cur[j].x >> cur[j].y;
            }

            lines.pb(cur);
            reverse(all(cur));
            lines.pb(cur);
        }

        rep(i, lines.size()){
            pii dist = lines[i][0];
            rep(j, lines[i].size()){
                lines[i][j].x -= dist.x;
                lines[i][j].y -= dist.y;
            }

            int dx = lines[i][1].x;
            int dy = lines[i][1].y;

            pii sign = mp(1, 1);
            bool sw = false;
            if(dx == 0){
                sw = true;

                if(dy < 0) sign = mp(-1, 1);
                else sign = mp(1, -1);
            }
            else{
                if(dx < 0) sign = mp(-1, -1);
            }

            rep(j, lines[i].size()){
                if(sw){
                    swap(lines[i][j].x, lines[i][j].y);
                }
                lines[i][j].x *= sign.x;
                lines[i][j].y *= sign.y;
            }
        }

        set<int> res;
        rep(i, 2){
            range(j, 2, lines.size()){
                bool ok = true;
                if(lines[i].size() != lines[j].size()) ok = false;
                else rep(k, lines[i].size()){
                    if(lines[i][k] != lines[j][k]){
                        ok = false;
                    }
                }
                if(ok){
                    res.insert(j / 2);
                }
            }
        }
        each(itr, res){
            cout << *itr << endl;
        }
        cout << "++++++" << endl;
    }

	return 0;
}
