#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

// (to, cost)
typedef pii Edge;

vi isLeaf;
vector<vector<Edge>> edge;

pair<int, set<int>> res;
int last;

#define F first
#define S second

void dfs(pair<int, set<int>> & cur, int v){
    if(res.F < cur.F){
        res = cur;
        last = v;
    }

    for(auto e : edge[v]){
        if(isLeaf[e.F] || cur.S.find(e.F) != cur.S.end()) continue;

        cur.S.insert(e.F);
        cur.F += e.S;

        dfs(cur, e.F);

        cur.S.erase(e.F);
        cur.F -= e.S;
    }
}

int main(void){
    for(int n; cin >> n, n;){
        vi p(n - 1), d(n - 1);
        for(auto && pp : p){
            cin >> pp; pp--;
        }
        for(auto && dd : d) cin >> dd;

        edge = vector<vector<Edge>>(n);
        range(i, 1, n){
            edge[i].pb(mp(p[i - 1], d[i - 1]));
            edge[p[i - 1]].pb(mp(i, d[i - 1]));
        }

        isLeaf = vi(n);
        rep(u, n) if(edge[u].size() == 1) isLeaf[u] = true;

        //// $B:G1sD:E@4V5wN%(B
        int s;
        rep(u, n) if(!isLeaf[u]){ s = u; break; }

        res.F = -1;
        pair<int, set<int>> tmp;
        tmp.S.insert(s);
        dfs(tmp, s);

        s = last;

        res.F = -1;
        tmp = pair<int, set<int>>();
        tmp.S.insert(s);
        dfs(tmp, s);
        ////

        int ans = 0;
        rep(i, n - 1){
            int a = i + 1, b = p[i];

            int diff = d[i];
            if(isLeaf[a] || isLeaf[b]);
            else if(res.S.find(a) != res.S.end() && 
                    res.S.find(b) != res.S.end()){
                diff *= 2;
            }
            else diff *= 3;

            ans += diff;
        }

        cout << ans << endl;
    }

	return 0;
}
