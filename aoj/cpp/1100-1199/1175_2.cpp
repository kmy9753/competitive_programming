#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define X first.first
#define Y first.second
#define R second.first
#define C second.second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int n, res;
vector< pair<pii, pii> > discs(n);
vvi isConf;

void dfs(vi used, int pos, int cnt){
    if(n <= pos){
        res = max(res, cnt);
        return;
    }

    dfs(used, pos + 1, cnt);

    if(used[pos]) return;
    rep(i, pos) if(isConf[pos][i]) return;

    used[pos] = true;
    rep(i, n){
        if(used[i] || discs[pos].C != discs[i].C ||
           isConf[pos][i]) continue;

        bool ok = true;
        rep(j, i){
            if(used[j]) continue;
            if(isConf[i][j]){
                ok = false;
            }
        }

        if(ok){
            used[i] = true;
            dfs(used, pos + 1, cnt + 2);
            used[i] = false;
        }
    }
}

int main(void){
    for(; cin >> n, n; cout << res << endl){
        res = 0;

        discs = vector< pair<pii, pii> >(n);
        rep(i, n) cin >> discs[i].X >> discs[i].Y >>
                         discs[i].R >> discs[i].C;
    
        isConf = vvi(n, vi(n));

        //$B>WFMH=Dj(B
        rep(i, n){
            rep(j, n){
                if(sqr(discs[i].X - discs[j].X) + sqr(discs[i].Y - discs[j].Y) < sqr(discs[i].R + discs[j].R)){
                    isConf[i][j] = true;
                }
            }
        }

        vi used(n);
        dfs(used, 0, 0);
    }

	return 0;
}
