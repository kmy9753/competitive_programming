#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

struct Sphere{
    double x;
    double y;
    double z;
    double r;
};

int n;
vector< vector<double> > costs;

int main(void){
    double res;
    for(; cin >> n, n; printf("%.3f\n", res)){
        res = 0.;
        vector<Sphere> spheres(n);
        rep(i, n) cin >> spheres[i].x >> spheres[i].y >> spheres[i].z >> spheres[i].r;

        costs = vector< vector<double> >(n, vector<double>(n));
        rep(u, n){
            range(v, u, n){
                double d = sqrt(sqr(spheres[u].x - spheres[v].x) 
                              + sqr(spheres[u].y - spheres[v].y) 
                              + sqr(spheres[u].z - spheres[v].z));
                costs[u][v] = costs[v][u] = max(0., d - (spheres[u].r + spheres[v].r));
            }
        }

        vector<double> minCost(n, inf);
        vi used(n);

        minCost[0] = 0;
        while(1){
            int v = -1;
            rep(u, n){
                if(!used[u] && (v == -1 || minCost[u] < minCost[v])) v = u;
            }
            if(v == -1) break;

            used[v] = true;
            res += minCost[v];

            rep(u, n){
                minCost[u] = min(minCost[u], costs[v][u]);
            }
        }
    }

	return 0;
}
