#include <bits/stdc++.h>
#define int long long 
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

string in;
int n, idx;

int parse(int sum){
    // cerr << ">>>> " << sum << ", " << in.substr(idx) << endl;
    char cur = in[idx];
    if(isdigit(cur)){
        int mul = 0;
        while(isdigit(in[idx])){
            mul = 10 * mul + (int)(in[idx] - '0');
            idx++;
        }
        
        if(isupper(in[idx])){
            if(sum + mul >= n){
                throw in[idx];
            }
            idx++;
            return mul * 1;
        }

        idx++; // read '('
        int head = idx;
        int len = 0;
        while(in[idx] != ')'){
            len += parse(sum + len);
        }
        idx++; // read ')'

        if(sum + mul * len >= n){
            n = (n - sum) % len;
            if(n == 0) n = len;

            idx = head;
            len = 0;
            while(in[idx] != ')'){
                len += parse(len);
            }
        }
        return mul * len;
    }
    else if(isupper(cur)){
        int len = 0;
        while(isupper(in[idx])){
            len++;
            if(sum + len == n){
                throw in[idx];
            }
            idx++;
        }
        return len;
    }
}

signed main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    for(; cin >> in >> n, in != "0";){
        n++;
        in = "1(" + in + ")";
        idx = 0;
        char res = '0';

        try { parse(0); }
        catch(int e) { res = c; }

        cout << res << endl;
    }

    return 0;
}
