#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

#define F first
#define S second

//((time, pre_node), (power, node))
typedef pair<pair<double, int>, pii> State;

int main(void){
    for(int n, m; cin >> n >> m, n;){
        int s, z; cin >> s >> z;
        s--, z--;

        // (to, (length, limited_power))
        vector<vector<pair<int, pii>>> edge(n);
        rep(i, m){
            int x, y, d, c; cin >> x >> y >> d >> c;
            x--, y--;

            edge[x].pb(mp(y, mp(d, c)));
            edge[y].pb(mp(x, mp(d, c)));
        }

        priority_queue<State, vector<State>, greater<State>> q;
        for(auto e : edge[s]){
            q.push(mp(mp((double)e.S.F, s), mp(1, e.F)));
        }

        // [pre_node][power][node]
        vector<vector<vector<double>>> minDist(n, vector<vector<double>>(31, vector<double>(n, (double)inf)));
        while(!q.empty()){
            double cur_time = q.top().F.F;
            int cur_prev = q.top().F.S;
            int cur_power = q.top().S.F;
            int cur_v = q.top().S.S;
            q.pop();

            if(minDist[cur_prev][cur_power][cur_v] < (double)inf - eps) continue;

            minDist[cur_prev][cur_power][cur_v] = cur_time;

            int next_prev = cur_v;

            range(next_power, max(1, cur_power - 1), cur_power + 2){
                for(auto e : edge[cur_v]){
                    int next_v = e.F;
                    if(next_v == cur_prev || e.S.S < next_power || minDist[next_prev][next_power][next_v] < (double)inf - eps){
                        continue;
                    }
                    double next_time = cur_time + (double)e.S.F / next_power;

                    q.push(mp(mp(next_time, next_prev), mp(next_power, next_v)));
                }
            }
        }

        double res = (double)inf;
        rep(i, n){
            res = min(res, minDist[i][1][z]);
        }

        if(res >= (double)inf - eps) cout << "unreachable" << endl;
        else                         printf("%.5f\n", res);
    }

	return 0;
}
