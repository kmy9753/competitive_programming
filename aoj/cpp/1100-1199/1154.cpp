#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 1001;

int main(void){
    for(int m; cin >> m, m;){
        vi dp(N);
        dp[0] = 1;

        while(m --){
            int a, b; cin >> a >> b;
            for(int i = N - 1; 0 <= i; i --)
                if(dp[i])
                    COUNT(j, b)
                        if(i + a * j < N) dp[i + a * j] += dp[i];
        }

        int g; cin >> g;
        while(g --){
            int n; cin >> n;
            cout << dp[n] << endl;
        }
    }
 
    return 0;
}
