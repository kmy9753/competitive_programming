#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int dx[] = {0, 1};
int dy[] = {1, 0};

string cmp(string, string);

int main(void){
    for(int w, h; cin >> w >> h, w;){
        vector<string> c(h);
        rep(y, h){
            cin >> c[y];
        }

        vector<vs> dp(h, vs(w));
        
        for(int y = h - 1; 0 <= y; y--){
            for(int x = w - 1; 0 <= x; x--){
                if('A' <= c[y][x] && c[y][x] <= 'Z') continue;

                string right, under;
                right = x + 1 < w ? dp[y][x + 1]:"";
                under = y + 1 < h ? dp[y + 1][x]:"";

                right.insert(0, 1, c[y][x]);
                under.insert(0, 1, c[y][x]);

                dp[y][x] = cmp(right, under);
            }
        }

        string res = "0";
        rep(y, h) rep(x, w) res = c[y][x] != '0' ? cmp(res, dp[y][x]):res;

        cout << res << endl;
    }

	return 0;
}

string cmp(string a, string b){
    if(a.size() > b.size()){
        return a;
    }
    else if(a.size() < b.size()){
        return b;
    }
    else{
        if(a > b){
            return a;
        }
        else return b;
    }
}
