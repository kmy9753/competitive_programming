#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int N = (int)1e6;

int main(void){
    vi sa, saodd;
    for(int a = 1, next; (next = a * (a + 1) * (a + 2) / 6) < N; a++){
        sa.pb(next);
        if(next % 2) saodd.pb(next);
    }

    vi dp(N, inf), dpodd(N, inf);
    dp[0] = 0; dpodd[0] = 0;
    rep(i, N){
        rep(j, sa.size()){
            if(N <= i + sa[j]) break;
            dp[i + sa[j]] = min(dp[i + sa[j]], dp[i] + 1);
        }
        rep(j, saodd.size()){
            if(N <= i + saodd[j]) break;
            dpodd[i + saodd[j]] = min(dpodd[i + saodd[j]], dpodd[i] + 1);
        }
    }

    for(int n; cin >> n, n; cout << dp[n] << " " << dpodd[n] << endl);

	return 0;
}
