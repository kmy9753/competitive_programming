#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int n, m;
vll coef;

vi not_zero, used;

const int N = 1 << 10;

// [si, ti)
void dfs(int si, int ti, ll sum, unordered_map<ll, ll> occ[N]){
    if(si == ti){
        int s = 0;
        rep(i, 10) if(used[i]) s |= (1 << i);
        occ[s][sum]++;
        return;
    }

    rep(i, not_zero[si] ? 1:0, 10){
        if(used[i]) continue;
        used[i] = true;
        dfs(si + 1, ti, coef[si] * i + sum, occ);
        used[i] = false;
    }
}

int main(void){
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);

    for(; cin >> n, n;){
        vector<string> nums(n);
        for(auto& e : nums) cin >> e;

        map<char, int> c2i;
        m = 0;
        rep(i, n){
            for(auto& c : nums[i]){
                if(c2i.find(c) == end(c2i)){
                    c2i[c] = m++;
                }
            }
        }
        not_zero = vi(m);
        for(auto& e : nums){
            if(e.size() == 1) continue;
            int i = c2i[e.at(0)];
            not_zero[i] = true;
        }

        coef = vll(m);
        rep(i, n){
            int len = nums[i].size();

            rep(j, len){
                char c = nums[i][j];
                int idx = c2i[c];
                int sign = (i == n - 1 ? -1 : 1);
                coef[idx] += sign * (pow(10, len - 1 - j));
            }
        }

        unordered_map<ll, ll> occ1[N], occ2[N];
        int m1 = m / 2, m2 = m - m1;
        used.assign(10, false); dfs(0,  m1, 0, occ1);
        used.assign(10, false); dfs(m1, m,  0, occ2);

        ll res = 0LL;
        rep(i, N){
            for(auto& e : occ1[i]){
                if(e.second == 0) continue;

                rep(j, N){
                    if((i & j) != 0) continue;
                    res += e.second * occ2[j][-e.first];
                }
            }
        }

        cout << res << endl;
    }

    return 0;
}
