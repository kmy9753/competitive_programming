#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
#define X first
#define Y second

using namespace std;
 
typedef pair<int, int> pii;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef vector<string> vs;
 
int cnt, w, h;
int m[100][100];
const int dx[8] = {-1, -1, -1, 0, 1, 1, 1, 0};
const int dy[8] = {-1, 0, 1, 1, 1, 0, -1, -1};

void dfs(pii);

int main(void){
    for(;cin >> w >> h, w; cout << cnt << endl){
        cnt = 0;
        rep(y, h) rep(x, w) cin >> m[y][x];
        rep(y, h) rep(x, w) if(m[y][x] == 1) cnt ++, dfs(mp(x, y));
    }
 
    return 0;
}

void dfs(pii p){
    if(p.X < 0 || w <= p.X ||
       p.Y < 0 || h <= p.Y) return;
    
    if(m[p.Y][p.X] == 1) m[p.Y][p.X] = 0;
    else return;

    rep(i, 8) dfs(mp(p.X + dx[i], p.Y + dy[i]));
}
