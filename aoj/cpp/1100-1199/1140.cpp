#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

// (位置，掃除した床のidx)
typedef pair<pii, int> State;

vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

#define F first
#define S second

int main(void){
    for(int w, h; cin >> w >> h, w;){
        vs field(h);
        rep(y, h) cin >> field[y];

        pii s;
        int n_gomi = 0;
        vvi dic(h, vi(w, -1));

        rep(y, h) rep(x, w){
            if(field[y][x] == 'o'){
                s = mp(y, x);
                field[y][x] = '.';
            }
            if(field[y][x] == '*'){
                dic[y][x] = n_gomi;
                n_gomi++;
            }
        }

        // (cost, State)
        priority_queue<pair<int, State>, vector<pair<int, State>>, greater<pair<int, State>>> q;
        q.push(mp(0, mp(s, 0)));

        vector<vvi> minDist(h, vvi(w, vi(1 << n_gomi, inf)));

        int res = -1;
        while(!q.empty()){
            int cur_cost = q.top().F;
            pii cur_pos = q.top().S.F;
            int cur_d = q.top().S.S;
            q.pop();

            if(cur_d == (1 << n_gomi) - 1){
                res = cur_cost;
                break;
            }

            if(minDist[cur_pos.F][cur_pos.S][cur_d] != inf) continue;
            minDist[cur_pos.F][cur_pos.S][cur_d] = cur_cost;

            rep(i, 4){
                pii next_pos = mp(cur_pos.F + dy[i], cur_pos.S + dx[i]);

                if(next_pos.F < 0 || h <= next_pos.F ||
                   next_pos.S < 0 || w <= next_pos.S ||
                   field[next_pos.F][next_pos.S] == 'x') continue;

                int next_d = cur_d;
                int idx = dic[next_pos.F][next_pos.S];
                if(idx != -1){
                    next_d |= (1 << idx);
                }
                if(minDist[next_pos.F][next_pos.S][next_d] != inf) continue;

                q.push(mp(cur_cost + 1, mp(next_pos, next_d)));
            }
        }

        cout << res << endl;
    }

	return 0;
}
