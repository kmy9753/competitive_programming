#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

#define x first
#define y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int main(void){
    for(int n; cin >> n, n;){
        vector< vector<pii> > lines(n + 2);

        range(i, 1, n + 2){
            int m; cin >> m;

            lines[i] = vector<pii>(m);
            rep(j, m){
                cin >> lines[i][j].x >> lines[i][j].y;
            }
            if(i == 1){
                lines[0] = lines[1];
                reverse(all(lines[1]));
            }
        }

        rep(i, n + 2){
            pii dist = lines[i][0];

            rep(j, lines[i].size()){
                lines[i][j].x -= dist.x;
                lines[i][j].y -= dist.y;
            }
        }

        range(i, 2, n + 2){
            bool ok = false;
            rep(j, 2){
                if(lines[i].size() != lines[j].size()) break;
                rep(k, 4){
                    bool okk = true;
                    rep(l, lines[i].size()){
                        swap(lines[j][l].x, lines[j][l].y);
                        lines[j][l].y *= -1;
                        
                        if(lines[i][l] != lines[j][l]){
                            okk = false;
                        }
                    }
                    if(okk) ok = true;
                }
            }
            if(ok) cout << i - 1 << endl;
        }

        cout << "+++++" << endl;
    }

	return 0;
}
