#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int mns(int val){
    if(val == 0) val = 2;
    else if(val == 2) val = 0;

    return val;
}

int mlt(int lhs, int rhs){
    return min(lhs, rhs);
}

int pls(int lhs, int rhs){
    return max(lhs, rhs);
}

int parse(string & str, int & pos){
    int ret;

    if(str.at(pos) == '('){
        pos++;

        int lhs = parse(str, pos);
        char op = str.at(pos); pos++;
        int rhs = parse(str, pos);

        switch(op){
            case '+': { ret = pls(lhs, rhs); break; }
            case '*': { ret = mlt(lhs, rhs); break; }
            default: break;
        }

        pos++;
    }
    else if(str.at(pos) == '-'){
        int cnt = 0;
        while(str.at(pos) == '-'){
            cnt++;
            pos++;
        }

        ret = parse(str, pos);
        if(cnt % 2 == 1) ret = mns(ret);
    }
    else {
        ret = str.at(pos) - '0';
        pos++;
    }

    return ret;
}

int main(void){
    for(string str_org; cin >> str_org, str_org != ".";){
        int res = 0;

        rep(p, 3){
            rep(q, 3){
                rep(r, 3){
                    string str = str_org;

                    for(auto && c : str){
                        switch(c){
                            case 'P': { c = p + '0'; break; }
                            case 'Q': { c = q + '0'; break; }
                            case 'R': { c = r + '0'; break; }
                            default: break;
                        }
                    }

                    int pos = 0;
                    if(parse(str, pos) == 2){
                        res++;
                    }
                }
            }
        }

        cout << res << endl;
    }

	return 0;
}
