#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int main(void){
    for(int n, m; cin >> n >> m, n;){
        int r; cin >> r;

        vector<vector<pii>> table(m);
        vi cnt(m);
        rep(aaa, r){
            int t, nn, mm, s; cin >> t >> nn >> mm >> s;
            nn--; mm--;

#define F first
#define S second

            if(table[mm].size() == 0 || table[mm][table[mm].size() - 1].S != inf){
                cnt[mm]++;
                table[mm].pb(mp(t, inf));
            }
            else {
                if(s == 1){
                    cnt[mm]++;
                }
                else {
                    cnt[mm]--;
                    if(cnt[mm] == 0){
                        table[mm][table[mm].size() - 1].S = t;
                    }
                }
            }
        }

        int q; cin >> q;
        rep(aaa, q){
            int ts, te, mm; cin >> ts >> te >> mm;
            mm--;

            int res = 0;

            for(auto && tt : table[mm]){
                int s, e;
                s = max(ts, tt.F);
                e = min(te, tt.S);

                if(e - s > 0) res += (e - s);
            }

            cout << res << endl;
        }
    }

	return 0;
}
