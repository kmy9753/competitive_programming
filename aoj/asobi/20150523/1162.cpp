#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

#define F first
#define S second

int main(void){
    for(int n, m; cin >> n >> m, n;){
        int s, z; cin >> s >> z; s--, z--;

        // ($B5wN%(B, $B@)8BB.EY(B)
        vector<vector<pair<int, pii>>> edge(n);
        rep(i, m){
            int x, y, d, c; cin >> x >> y >> d >> c; x--, y--;

            edge[x].pb(mp(y, mp(d, c)));
            edge[y].pb(mp(x, mp(d, c)));
        }

        constexpr int kMaxSpeed = 30;

        // ($B%3%9%H(B, ($B:#$$$kET;T(B, $BB.EY(B, 1$B8DA0$NET;T(B))
        typedef pair<double, pair<int, pii>> State;

        vector<vector<vector<double>>> minDist(n, vector<vector<double>>(kMaxSpeed + 1, vector<double>(n, (double)inf)));
        priority_queue<State, vector<State>, greater<State>> q;

        q.push(mp(0.0, mp(s, mp(0, s))));

        while(!q.empty()){
            double cur_cost = q.top().F;
            int cur_v = q.top().S.F;
            int cur_speed = q.top().S.S.F;
            int cur_prev = q.top().S.S.S;
            q.pop();

            if(minDist[cur_v][cur_speed][cur_prev] < inf - eps) continue;

            minDist[cur_v][cur_speed][cur_prev] = cur_cost;

            int next_prev = cur_v;
            range(next_speed, cur_speed - 1, cur_speed + 2){
                if(next_speed <= 0) continue;

                for(auto && ee : edge[cur_v]){
                    if(ee.S.S < next_speed || ee.F == cur_prev) continue;

                    int next_v = ee.F;
                    double next_cost = cur_cost + (double)ee.S.F / next_speed;

                    if(minDist[next_v][next_speed][next_prev] < inf - eps) continue;

                    q.push(mp(next_cost, mp(next_v, mp(next_speed, next_prev))));
                }
            }
        }

        double res = *min_element(all(minDist[z][1]));
        if(res < inf - eps) printf("%.6f\n", res);
        else cout << "unreachable" << endl;
    }

	return 0;
}
