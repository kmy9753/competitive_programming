#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

constexpr int kMax = 11;

string res;
int w, h, n;
int field[kMax][kMax];

vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

#define F first
#define S second

void check(pii pos, int & cnt){
    
}

void dfs(pii pos, int cnt){
    if(cnt == n / 2){
        pii s = mp(-1, -1);
        rep(y, h){
            rep(x, w){
                if(field[y][x] == 0){
                    s = mp(y, x);
                    break;
                }
            }
            if(s.F != -1) break;
        }

        int cnt_c = 0;
        check(s, cnt_c);
        if(cnt_c != n / 2) return;

        //$B0\F0!&2sE>!&N"JV$7(B
    }

    rep(i, 4){
        pii np = mp(pos.F + dy[i], pos.S + dx[i]);

        if(field[np.F][np.S] != 0) continue;
        if(np.F < 0 || h <= np.F ||
           np.S < 0 || w <= np.S) continue;

        field[np.F][np.S] = 1;
        dfs(np, cnt + 1);
        field[np.F][np.S] = 0;
    }
}

int main(void){
    for(; cin >> w >> h, w;){
        res = "No";
        n = 0;

        pii s;
        rep(y, h){
            rep(x, w){
                cin >> field[y][x];

                field[y][x]--;

                if(field[y][x] == 0) s = mp(y, x), n++;
            }
        }

        field[s.F][s.S] = 1;
        if(n % 2 == 0) dfs(s, 1);
    }

	return 0;
}
