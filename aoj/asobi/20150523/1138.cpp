#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

#define F first
#define S second

int main(void){
    for(int n, m, p, a, b; cin >> n >> m >> p >> a >> b, n;){
        a--, b--;

        vi t(n); for(auto && tt : t) cin >> tt;

        // ($B@h!$5wN%(B)
        vector<vector<pii>> edge(m);
        rep(i, p){
            int x, y, z; cin >> x >> y >> z; x--, y--;

            edge[x].pb(mp(y, z));
            edge[y].pb(mp(x, z));
        }

        vector<vector<double>> min_dist(m, vector<double>(1 << n, (double)inf));

        // ($B%3%9%H!$(B($BET;T!$GO<V%A%1$N%Q%?!<%s(B))
        typedef pair<double, pii> State;
        priority_queue<State, vector<State>, greater<State>> q;
        q.push(mp(0.0, mp(a, (1 << n) - 1)));

        while(!q.empty()){
            double cur_cost = q.top().F;
            int cur_v = q.top().S.F;
            int cur_t = q.top().S.S;
            q.pop();

            if(min_dist[cur_v][cur_t] < inf - eps) continue;

            min_dist[cur_v][cur_t] = cur_cost;

            rep(i, n){
                if(((cur_t >> i) & 1) == 0) continue;

                int next_t = (cur_t & ~(1 << i));
                for(auto && ee : edge[cur_v]){
                    int next_v = ee.F;
                    if(min_dist[next_v][next_t] < inf - eps) continue;

                    double next_cost = cur_cost + (double)ee.S / t[i];
                    q.push(mp(next_cost, mp(next_v, next_t)));
                }
            }
        }

        double res = *min_element(all(min_dist[b]));
        if(res < inf - eps) printf("%.6f\n", res);
        else cout << "Impossible" << endl;
    }

	return 0;
}
