#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int inf = INT_MAX;

// RMQ
struct SegTree {
    int n;
    vi v;

    int pow2fit(int sz){
        int ret = 1;
        while(ret < sz) ret *= 2;
        return ret;
    }

    SegTree(int sz){
        n = pow2fit(sz);
        v = vi(2 * n - 1, inf);
    }

    // v[a] := x
    void update(int a, int x){
        int idx = n - 1 + a;
        v[idx] = x;

        while(idx > 0){
            idx = (idx - 1) / 2;
            v[idx] = min(v[2 * idx + 1], v[2 * idx + 2]);
        }
    }

    // get min [a, b)
    int getmin(int a, int b, int k = 0, int l = 0, int r = -1){
        if(r < 0) r = n;

        if(a >= r or l >= b) return inf;
        if(a <= l and r <= b) return v[k];

        return min(getmin(a, b, 2 * k + 1, l, (l + r) / 2),
                   getmin(a, b, 2 * k + 2, (l + r) / 2, r));
    }
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, Q; cin >> n >> Q;
    SegTree seg(n);

    rep(loop, Q){
        int op, x, y; cin >> op >> x >> y;
        if(op == 0){
            seg.update(x, y);
        }
        else {
            cout << seg.getmin(x, y + 1) << endl;
        }
    }

    return 0;
}
