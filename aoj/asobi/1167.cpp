#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

constexpr int kMax = 1e6;

int main(void){
    vi num, num_odd;

    range(i, 1, inf){
        int cur = i * (i + 1) * (i + 2) / 6;

        if(cur > kMax) break;

        num.pb(cur);
        if(cur % 2 == 1) num_odd.pb(cur);
    }

    vi dp(kMax, inf), dp_odd(kMax, inf);
    dp[0] = dp_odd[0] = 0;
    rep(i, kMax){
        for(int nn : num){
            if(kMax <= i + nn) break;
            dp[i + nn] = min(dp[i + nn], dp[i] + 1);
        }
        for(int nn : num_odd){
            if(kMax <= i + nn) break;
            dp_odd[i + nn] = min(dp_odd[i + nn], dp_odd[i] + 1);
        }
    }

    for(int n; cin >> n, n; cout << dp[n] << " " << dp_odd[n] << endl);

	return 0;
}
