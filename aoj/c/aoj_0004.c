#include<stdio.h>
main(){
    int a,b,c,d,e,f;
    while(~scanf("%d%d%d%d%d%d",&a,&b,&c,&d,&e,&f)){
        double det=a*e-b*d;
        double x=(e*c-b*f)/det,y=(-d*c+a*f)/det;
        if(-1e-6<x && x<1e-6) x=0;
        if(-1e-6<y && y<1e-6) y=0;
        printf("%.3f %.3f\n",x,y);
    }
    exit(0);
}
