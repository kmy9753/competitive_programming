
#include <stdio.h>

int main(void){
  int a, b;
  scanf("%d %d", &a, &b);
  char *s = (a > b) ? ">" : (a == b) ? "==" : "<";
  printf("a %s b\n", s);
  return 0;
}
