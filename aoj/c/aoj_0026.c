#include <stdio.h>
#define SIZE 10
main(){
	int map[SIZE][SIZE], i, j;
	int x, y, size;
	int whiteCount = 0; 
	int maxValue = 0;
	for(i = 0; i < SIZE; i ++){
		for(j = 0; j < SIZE; j ++){
			map[i][j] = 0;
		}
	}
	while(~scanf("%d,%d,%d", &x, &y, &size)){
		switch(size){
		case 3:
			for(i = -2; i < 3; i += 4){
				if(x + i < SIZE && x + i > -1){
					map[y][x + i] ++;
				}
				if(y + i < SIZE && y + i > -1){
					map[y + i][x] ++;
				}
			}
		case 2:
			for(i = -1; i < 2; i += 2){
			for(j = -1; j < 2; j += 2){
				if(x + i < SIZE && x + i > -1 &&
						y + j < SIZE && y + j > -1){
					map[y + j][x + i] ++;
				}
			}
			}
		case 1:
			for(i = -1; i < 2; i += 2){
				if(x + i < SIZE && x + i > -1){
					map[y][x + i] ++;
				}
				if(y + i < SIZE && y + i > -1){
					map[y + i][x] ++;
				}
			}
		default:
			map[y][x] ++;
		}
	}
	for(i = 0; i < SIZE; i ++){
		for(j = 0; j < SIZE; j ++){
			if(map[i][j] == 0){
				whiteCount ++;
			}
			else if(map[i][j] > maxValue){
				maxValue = map[i][j];
			}
		}
	}
	printf("%d\n%d\n", whiteCount, maxValue);
	return 0;
}
