#include <stdio.h>
#define MAX_VALUE 100
main(){
	int input[MAX_VALUE], i, modeValue = 0;
	int mode = 0;
	for(i = 0; i < MAX_VALUE; i ++){
		input[i] = 0;
	}
	i = 0;
	while(~scanf("%d", &i)){ input[i] ++; }
	for(i = 0; i < MAX_VALUE; i ++){
		if(mode < input[i]){
			mode = input[i];
		}
	}
	for(i = 0; i < MAX_VALUE; i ++){
		if(input[i] == mode){
			printf("%d\n", i);
		}
	} 
	return 0;
}
