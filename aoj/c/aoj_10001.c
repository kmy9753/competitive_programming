#include <stdio.h>
#include <math.h>

int main(void){
  double n;
  scanf("%lf", &n);
  printf("%d\n", (int)pow(n, 3));
  return 0;
}
