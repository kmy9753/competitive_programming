#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,0,n)
#define _rrange(i,a,b) for(int i=(int)(b)-1;i>=(int)(a);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
#define dbg(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ") " << __FILE__ << endl;

using namespace std;

// overload: >>, <<
template<typename T> istream &operator>>(istream &is, vector<T> &vec){ for (auto &v : vec) is >> v; return is; }
template<typename T> ostream &operator<<(ostream &os, const vector<T> &vec){ os << "["; for (auto v : vec) os << v << ","; os << "]"; return os; }
template<typename T> ostream &operator<<(ostream &os, const deque<T> &vec){ os << "deq["; for (auto v : vec) os << v << ","; os << "]"; return os; }
template<typename T> ostream &operator<<(ostream &os, const set<T> &vec){ os << "{"; for (auto v : vec) os << v << ","; os << "}"; return os; }
template<typename T> ostream &operator<<(ostream &os, const unordered_set<T> &vec){ os << "{"; for (auto v : vec) os << v << ","; os << "}"; return os; }
template<typename T> ostream &operator<<(ostream &os, const multiset<T> &vec){ os << "{"; for (auto v : vec) os << v << ","; os << "}"; return os; }
template<typename T> ostream &operator<<(ostream &os, const unordered_multiset<T> &vec){ os << "{"; for (auto v : vec) os << v << ","; os << "}"; return os; }
template<typename T1, typename T2> ostream &operator<<(ostream &os, const pair<T1, T2> &pa){ os << "(" << pa.first << "," << pa.second << ")"; return os; }
template<typename TK, typename TV> ostream &operator<<(ostream &os, const map<TK, TV> &mp){ os << "{"; for (auto v : mp) os << v.first << "=>" << v.second << ","; os << "}"; return os; }
template<typename TK, typename TV> ostream &operator<<(ostream &os, const unordered_map<TK, TV> &mp){ os << "{"; for (auto v : mp) os << v.first << "=>" << v.second << ","; os << "}"; return os; }

// overload: +, -, *, /
template<typename T1, typename T2> pair<T1, T2> operator+(const pair<T1, T2> &l, const pair<T1, T2> &r) { return make_pair(l.first + r.first, l.second + r.second); }
template<typename T1, typename T2> pair<T1, T2> operator-(const pair<T1, T2> &l, const pair<T1, T2> &r) { return make_pair(l.first - r.first, l.second - r.second); }

// template functions
template<typename T> void ndarray(vector<T> &vec, int len) { vec.resize(len); }
template<typename T, typename... Args> void ndarray(vector<T> &vec, int len, Args... args) { vec.resize(len); for (auto &v : vec) ndarray(v, args...); }
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
// ----------------------------
using R = double;
using vec  = vector<R>;
using mat = vector<vec>;

// LU$BJ,2r$7$?>pJs$,F~$k%G!<%?9=B$!%D>@\A`:n$9$k$3$H$OA[Dj$7$F$$$J$$(B
struct LUinfo {
  vec value;
  vector<int> index;
};

// A $B$r(BLU $BJ,2r$7!$$=$N>pJs$rJV$9(B
// O( n^3 ), Gaussian forward elimination
LUinfo LU_decomposition(mat A) {
  const int n = A.size();
  LUinfo data;
  for (int i = 0; i < n; ++i) {
    int pivot = i;
    for (int j = i+1; j < n; ++j)
      if (abs(A[j][i]) > abs(A[pivot][i])) pivot = j;
    swap(A[pivot], A[i]);
    data.index.push_back(pivot);
    // if A[i][i] == 0, LU decomposition failed.
    for(int j = i+1; j < n; ++j) {
      A[j][i] /= A[i][i];
      for(int k = i+1; k < n; ++k)
        A[j][k] -= A[i][k] * A[j][i];
      data.value.push_back(A[j][i]);
    }
  }
  for(int i = n-1; i >= 0; --i) {
    for(int j = i+1; j < n; ++j)
      data.value.push_back(A[i][j]);
    data.value.push_back(A[i][i]);
  }
  return data;
}

// Ax = b $B$r2r$/(B
// O( n^2 ) Gaussian backward substitution
vec LU_backsubstitution(const LUinfo &data, vec b) {
  const int n = b.size();
  int k = 0;
  for (int i = 0; i < n; ++i){
    swap(b[data.index[i]], b[i]);
    for(int j = i+1; j < n; ++j)
      b[j] -= b[i] * data.value[k++];
  }
  for (int i = n-1; i >= 0; --i) {
    for (int j = i+1; j < n; ++j)
      b[i] -= b[j] * data.value[k++];
    b[i] /= data.value[k++];
  }
  return b;
}
// ----------------------------------

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    mat A =
    {
        {1, 1, 1},
        {9, 3, 1},
        {16, 4, 1}
    };
    vec b = 
    {
        4,
        16,
        25
    };

    LUinfo info = LU_decomposition(A);
    vec coef = LU_backsubstitution(info, b);

    cout.precision(20);
    for(auto & e : coef){
        cout << fixed << e << endl;
    }

    return 0;
}
