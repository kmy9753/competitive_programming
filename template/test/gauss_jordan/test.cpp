#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,0,n)
#define _rrange(i,a,b) for(int i=(int)(b)-1;i>=(int)(a);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
#define dbg(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ") " << __FILE__ << endl;

using namespace std;

// overload: >>, <<
template<typename T> istream &operator>>(istream &is, vector<T> &vec){ for (auto &v : vec) is >> v; return is; }
template<typename T> ostream &operator<<(ostream &os, const vector<T> &vec){ os << "["; for (auto v : vec) os << v << ","; os << "]"; return os; }
template<typename T> ostream &operator<<(ostream &os, const deque<T> &vec){ os << "deq["; for (auto v : vec) os << v << ","; os << "]"; return os; }
template<typename T> ostream &operator<<(ostream &os, const set<T> &vec){ os << "{"; for (auto v : vec) os << v << ","; os << "}"; return os; }
template<typename T> ostream &operator<<(ostream &os, const unordered_set<T> &vec){ os << "{"; for (auto v : vec) os << v << ","; os << "}"; return os; }
template<typename T> ostream &operator<<(ostream &os, const multiset<T> &vec){ os << "{"; for (auto v : vec) os << v << ","; os << "}"; return os; }
template<typename T> ostream &operator<<(ostream &os, const unordered_multiset<T> &vec){ os << "{"; for (auto v : vec) os << v << ","; os << "}"; return os; }
template<typename T1, typename T2> ostream &operator<<(ostream &os, const pair<T1, T2> &pa){ os << "(" << pa.first << "," << pa.second << ")"; return os; }
template<typename TK, typename TV> ostream &operator<<(ostream &os, const map<TK, TV> &mp){ os << "{"; for (auto v : mp) os << v.first << "=>" << v.second << ","; os << "}"; return os; }
template<typename TK, typename TV> ostream &operator<<(ostream &os, const unordered_map<TK, TV> &mp){ os << "{"; for (auto v : mp) os << v.first << "=>" << v.second << ","; os << "}"; return os; }

// overload: +, -, *, /
template<typename T1, typename T2> pair<T1, T2> operator+(const pair<T1, T2> &l, const pair<T1, T2> &r) { return make_pair(l.first + r.first, l.second + r.second); }
template<typename T1, typename T2> pair<T1, T2> operator-(const pair<T1, T2> &l, const pair<T1, T2> &r) { return make_pair(l.first - r.first, l.second - r.second); }

// template functions
template<typename T> void ndarray(vector<T> &vec, int len) { vec.resize(len); }
template<typename T, typename... Args> void ndarray(vector<T> &vec, int len, Args... args) { vec.resize(len); for (auto &v : vec) ndarray(v, args...); }
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
// ---
using R = double;
using Vec = vector<R>;
using Mat = vector<Vec>;

// $B%,%&%9$N>C5nK!$GO"N)<0$r$H$/(B
bool gauss_jordan(const Mat & A, const Vec & b, Vec & res){
    int n = A.size();
    Mat B(n, Vec(n + 1));
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            B[i][j] = A[i][j];
    for(int i = 0; i < n; i++)
        B[i][n] = b[i];

    // $B3FJQ?t$KCmL\(B
    for(int i = 0; i < n; i++){

        // $B:GBg$N78?t$N@dBPCM$r$b$D$b$N$r(Bi$BHVL\$K;}$C$F$/$k(B
        int piv = -1;
        int val = -1;
        for(int j = i; j < n; j++){
            if(val < abs(B[j][i])){
                piv = j;
                val = abs(B[j][i]);
            }
        }

        // $B1&JU$,(B0$B$K$J$k$+$I$&$+D4$Y$k(B
        if(piv == -1 || val <= 0)
            continue;
        swap(B[piv], B[i]);
        for(int j = 0; j < n; j++){
            if(i == j) continue;
            int mulnum = B[j][i];
            if(mulnum != 0){
                for(int k = i; k <= n; k++)
                    B[j][k] = (B[j][k] + B[i][k]);
            }
        }
    }
    res = Vec(n);

    // $B$b$7:8JU$,(B0$B$K$J$C$F$$$k<0$,$"$l$P!"1&JU$b(B0$B$K$J$C$F$$$k$+D4$Y$k(B
    // $B$J$C$F$$$J$1$l$P!"2r$OB8:_$7$J$$(B
    // $B1&JU$b(B0$B$J$i!"B>$N:8JU$,(B0$B$N<0$b1&JU$,(B0$B$K$J$k$H$-$K8B$j!"J#?t2rB8:_(B
    // $B$=$NB>$N>l9g$O!"2r$O0l$D$@$1B8:_(B
    for(int i = 0; i < n; i++){
        res[i] = B[i][n];
        if(B[i][i] == 0){
            if(B[i][n] != 0) return false;
        }
    }
    return true;
}
// ---

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    Vec coef;
    Mat A =
    {
        {1, 1, 1},
        //{4, 2, 1},
        {9, 3, 1},
        {16, 4, 1}
    };
    Vec b = 
    {
        4,
        16,
        25
    };

    if(gauss_jordan(A, b, coef)){
        cout.precision(10);
        for(auto & e : coef){
            cout << fixed << e << endl;
        }
    }
    else {
        cout << "not found" << endl;
    }

    return 0;
}
