// ---
const int N = 401; //頂点数の最大値

int n; //頂点数
vector<vi> G(N);
vi match(N); //マッチングのペア
vi used(N);

void init_G(void){
    for(auto & g : G) g.clear();
}

// uとvを結ぶ辺をグラフに追加する
void add_edge(int u, int v){
    G[u].push_back(v);
    G[v].push_back(u);
}

// 増加パスをDFSで探す
bool dfs(int v){
    used[v] = true;

    for(auto u : G[v]){
        int w = match[u];
        if(w < 0 || (!used[w] && dfs(w))){
            match[v] = u;
            match[u] = v;
            return true;
        }
    }
    return false;
}

//二分グラフの最大マッチングを求める
int b_match(void){
    int res = 0;
    fill(_all(match), -1);

    rep(i, n){
        if(match[i] < 0){
            fill(_all(used), false);
            if(dfs(i)) res++;
        }
    }
    return res;
}
// ---

int main(void){

	return 0;
}
