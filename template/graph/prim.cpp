typedef pair<int, int> pii;

int n; // the number of node

int prim (vector<vector<pii>> G) {
    set<int> used;
    priority_queue<pii, vector<pii>, greater<pii>> que;
    for(int i = 0; i < n; i++){
        used.insert(i);
    }
    int res = 0;
    int v = 0;
    while(!used.empty()){
        for(auto i : G[v]) que.push(i);
        pii p;
        while(used.find(que.top().second) == used.end() || que.top().second == v){
            que.pop();
            if(que.empty()) break;
        }
        if(que.empty()) break;
        used.erase(used.find(v));
        p = que.top(); que.pop();
        v = p.second;
        res += p.first;
    }

    return res;
}

int main(void){
    return 0;
}
