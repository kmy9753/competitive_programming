const int inf = 1 << 30;
 
// ----------------
int n, start;

// to, cost
using Edge = tuple<int, int>;
vector<vector<Edge>> G;
 
vi dist;
 
bool bellman_ford(){
    dist = vi(n, inf);
    dist[start] = 0;

    rep(loop, n){
        rep(v, n) for(auto & e : G[v]){
            if(dist[v] == inf) continue;

            int nv, cost;
            tie(nv, cost) = e;

            if(dist[nv] > dist[v] + cost){
                dist[nv] = min(dist[nv], dist[v] + cost);
                if(loop == n - 1) return true;
            }
        }
    }

    return false;
}
// ----------------
 
int main(void){
    int m;
    cin >> n >> m >> start;

    G = vector<vi>(n);

    rep(loop, m){
        int a, b, c;
        cin >> a >> b >> c;
        G[a].push_back(tie(b, c));
    }

    if(bellman_ford()){
        cout << "NEGATIVE CYCLE" << endl;
    }
    else{
        rep(i, n){
            if(dist[i] == inf)
                cout << "INF" << endl;
            else
                cout << dist[i] << endl;
        }
    }
    return 0;
}
