// ---
class TSort {
public:
	int n;
	vector<vi> G;
	vi deg, res;
	TSort(int n_) : n(n_), G(n), deg(n, 0){}
	void add_edge(int from,int to){
		G[from].push_back(to);
		deg[to]++;
	}
	bool solve(){
		queue<int> q;
		for(int i = 0; i < n; i++){
			if(deg[i] == 0){
				q.push(i);
			}
		}
		while(!q.empty()){
			int p = q.front();
			q.pop();
			res.emplace_back(p);
			for(int v : G[p]){
				if(--deg[v] == 0){
					q.push(v);
				}
			}
		}
		return (*max_element(_all(deg)) == 0);
	}
};
// ---

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;

    TSort ts(n);
    rep(_, m){
        int a, b; cin >> a >> b;
        a--, b--;
        ts.add_edge(a, b);
    }

    bool ret = ts.solve();

    if(not ret){
        cout << "G is not DAG" << endl;
    }
    else {
        for(auto& e : ts.res){
            cout << e << endl;
        }
    }

    return 0;
}
