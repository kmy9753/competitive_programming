// ---
int n;             // |V|
vector<vi> edge;   // 元のグラフ
vector<vi> r_edge; // 辺の向きを逆にしたグラフ
vi vs;             // 帰りがけ順
vector<bool> used; // 既に調べたか
vi cmp;            // 属する強連結成分のトポロジカル順序

void add_edge(int from, int to){
    edge[from].push_back(to);
    r_edge[to].push_back(from);
}

void dfs(int v){
    used[v] = true;
    for(auto & nv : edge[v]){
        if(not used[nv]) dfs(nv);
    }
    vs.push_back(v);
}

void rdfs(int v, int k){
    used[v] = true;
    cmp[v] = k;
    for(auto & nv : r_edge[v]){
        if(not used[nv]) rdfs(nv, k);
    }
}

int scc(){
    used = vector<bool>(n);
    vs.clear();
    rep(v, n){
        if(not used[v]) dfs(v);
    }

    used = vector<bool>(n);
    int k = 0;
    rrep(i, vs.size()){
        if(not used[vs[i]]) rdfs(vs[i], k++);
    }

    return k;
}
// ---

int main(void){
    cin >> n;

    edge = r_edge = vector<vi>(n);
    cmp = vi(n);

    int m; cin >> m;
    rep(_, m){
        int a, b; cin >> a >> b;
        a--, b--;
        add_edge(a, b);
    }

    cout << scc() << endl;

    return 0;
}
