// ---
const int MAX = 100000;
const int MAX_LOG = 100;

int N, Q;
vi edge[MAX];

int parent[MAX_LOG][MAX];
int depth[MAX];

void dfs(int curr, int prev, int d) {
  parent[0][curr] = prev;
  depth[curr] = d;
  
  rep(i, edge[curr].size()) {
    if(edge[curr][i] != prev) {
      dfs(edge[curr][i], curr, d + 1);
    }
  }
}

void init(int V) {
  dfs(0, -1, 0);
  
  rep(k, MAX_LOG - 1) {
    rep(v, V) {
      if(parent[k][v] < 0) parent[k + 1][v] = -1;
      else parent[k + 1][v] = parent[k][parent[k][v]];
    }
  }
}

int lca(int u, int v) {
  if(depth[u] > depth[v]) swap(u, v);

  rep(k, MAX_LOG) {
    if((depth[v] - depth[u]) >> k & 1) {
      v = parent[k][v];
    }
  }
  if(u == v) return u;
  
  for(int k = MAX_LOG - 1; k >= 0; k--) {
    if(parent[k][u] != parent[k][v]) {
      u = parent[k][u];
      v = parent[k][v];
    }
  }
  return parent[0][u];
}
// ---

