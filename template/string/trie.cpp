// ---
const int insert_max_length = 100010;
const int alphabet_num = 26;

const int trie_root = 0;
const int trie_nothing = -1;
int size = 1;
bool ac[insert_max_length]; 
int mov[insert_max_length][alphabet_num];

void init(){
	memset(ac, false, sizeof(ac));
	memset(mov, trie_nothing, sizeof(mov));
	return;
}

void ins(string arg){
	int cur = trie_root;
	for(auto& ch : arg){
		if(mov[cur][ch - 'a'] == trie_nothing)
			mov[cur][ch - 'a'] = size++;
		cur = mov[cur][ch - 'a'];
	}
	ac[cur] = true;
	return;
}

bool find(string arg){
	int cur = trie_root;
	for(auto& ch : arg){
		if(mov[cur][ch - 'a'] == trie_nothing)
            return false;
		cur = mov[cur][ch - 'a'];
	}
	return ac[cur];
}

void del(string arg){
	int cur = trie_root;
	for(auto& ch : arg){
		if(mov[cur][ch - 'a'] == trie_nothing) return;
		cur = mov[cur][ch - 'a'];
	}
	ac[cur] = false;
	return;
}
// ---

int main(void){
	init();

    int Q; cin >> Q;

    rep(_, Q){
        // 1: insert ${S}
        // 2: delete ${S}
        // 3: find   ${S}
        int op; string S; cin >> op >> S;

        if(op == 1) ins(S);
        else if(op == 2) del(S);
        else cout << find(S) << endl;
    }

    return 0;
}
