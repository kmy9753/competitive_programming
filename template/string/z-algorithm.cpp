vi Zalgo(string S){
    vi vec(1, S.size());
    for(int i = 1, l = -1, r = -1; i < S.size(); i++){
        if(i <= r && vec[i-l] < r-i+1) vec.emplace_back(vec[i-l]);
        else {
            l = i; r = (i > r) ? i : (r+1);
            while(r < S.size() && S[r-i] == S[r]) r++;
            vec.emplace_back((r--) - l);
        }
    }
    return vec;
}

