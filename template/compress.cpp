void compress(vi const& vec, map<int, int>& zip, vi& unzip){
    unzip = vec;

    uniq(unzip);
    unzip.emplace_back(inf);

    rep(i, unzip.size()) zip[unzip[i]] = i;
}

int main(void){
    int n; cin >> n;
    vi x(n);
    rep(i, n) cin >> x[i];

    vi unzip;
    map<int, int> zip;

    compress(x, zip, unzip);

    rep(i, n) cout << x[i] << ", "; cout << endl;
    rep(i, n) cout << zip[x[i]] << ", "; cout << endl;
    rep(i, n) cout << unzip[zip[x[i]]] << ", "; cout << endl;

    return 0;
}

