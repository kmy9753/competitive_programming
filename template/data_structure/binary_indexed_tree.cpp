///////////////////////
template <typename T>
class Bit {
 public:
  Bit(int size) : size_(size) {
    data_.resize(size + 1);
  }

  // 0-indexed
  void add(int index, T value) {
    if (index < 0 || index >= size_) return;
    ++index;
    while (index <= size_) {
      data_[index] += value;
      index += index & -index;
    }
  }

  // [0, index]
  T sum(int index) {
    if (index < 0 || index >= size_) return 0;
    ++index;
    T ret = 0;
    while (index > 0) {
      ret += data_[index];
      index -= index & -index;
    }
    return ret;
  }

 private:
  std::vector<T> data_;
  int size_;
};
///////////////////////

int main(void){
    int n; cin >> n;
    Bit<int> bt(n);

    return 0;
}
