// -----------------------
namespace SegmentTrees{
    int inf = 1 << 30;
 
    template<typename T>
    class RMQ{
    private:
        int Pow2Fit(int _n){
            int d = 1;
            while((d << 1) <= _n) d <<= 1;
            return d;
        }
 
    public:
        vector<T> dat;
        int n, size;
 
        RMQ(int _n){
            n = Pow2Fit(_n) << 1;
            size = 2 * n - 1;
            dat = vector<T>(size, inf);
        }
 
        // node v := a (0-indexed)
        void set(int v, T a){
 
            // leaf
            v += n - 1;
            dat[v]=a;
 
            // update toward root
            while(v > 0){
                int parent = v = (v - 1) / 2;
                int chl = parent * 2 + 1, chr = parent * 2 + 2;
                dat[parent] = min(dat[chl], dat[chr]);
            }
        }
 
        T get(int v){ // v (0-indexed)
            return dat[v + n - 1];
        }
 
        T query(int a, int b){ // [a,b)
            return query(0, a, b, 0, n);
        }
 
    private:
        T query(int v, int a, int b, int l, int r){ // [a,b)
            if(r <= a || b <= l) return inf; // out range
            if(a <= l && r <= b) return dat[v]; // covered
 
            T vl = query(v * 2 + 1, a, b, l, (l + r) / 2),
              vr = query(v * 2 + 2, a, b, (l + r) / 2, r);

            return min(vl, vr);
        }
    };
}
 
using namespace SegmentTrees;
// -----------------------

int main(){
    int n = 1e6;
    RMQ<ll> tree(n);

    rep(i, n){
        tree.set(i, i + 134);
    }

	int a, b; // [a, b)
	while(cin >> a >> b){
		cout << tree.query(a, b) << endl;
	}
}
