// ---
const int N = 1 << 17; // ~~ 1.3 * 10^5
/**
 * 0-indexed, [l, r)
 * update(l,r,v) := [l,r)$B$N6h4V$KBP$7$F(Bv$B$r0lMM$KB-$9(B
 * query (l,r)   := [l,r)$B$N6h4V$N:GBgCM$r5a$a$k(B
 **/

struct Node {
    ll val;  // $B99?7$5$l$?CM(B. $B$3$NCM$r;2>H$9$k;~$OI>2A$,40A4$K40N;$7$F$$$k$h$&$K$9$k(B.
    ll lazy; // $BCY1d$5$l$F$$$kCM(B
    Node(){
        val  = 0;
        lazy = 0;
    }
};

Node seg[2 * N];

// $B%N!<%I(Bk$B$rI>2A$7!$;R$KEAGE$9$k(B
// a, b: $B%N!<%I(Bk$B$NI=$96h4V(B[a, b)
inline void lazy_evaluate_node(int k){
    seg[k].val += seg[k].lazy;

    // $BMU$G$J$1$l$PEAGE(B
    if(k < N - 1){
        seg[2 * k + 1].lazy += seg[k].lazy;
        seg[2 * k + 2].lazy += seg[k].lazy;
    }

    seg[k].lazy = 0;
}

// $B%N!<%I(Bk$B$NCM$r99?7(B
// (k$B$N;R$O$9$G$KI>2A$5$l$F$$$k(B(lazy$B$,(B0)) and (k$B$OMU$G$J$$(B) $B$,A0Ds(B
inline void update_node(int k){
    seg[k].val = max(seg[2 * k + 1].val, seg[2 * k + 2].val);
}

// update(l, r, v) := [l, r)$B$r99?7$9$k(B
void update(int l, int r, ll v, int k = 0, int a = 0, int b = N - 1){
    lazy_evaluate_node(k);

    if(b <= l or r <= a) return;

    if(l <= a and b <= r){
        seg[k].lazy += v;
        lazy_evaluate_node(k);
        return;
    }

    int m = (a + b) / 2;
    update(l, r, v, 2 * k + 1, a, m);
    update(l, r, v, 2 * k + 2, m, b);
    update_node(k);
}

// query(l, r) := [l, r)$B$KBP$9$k%/%(%j$NEz$($rF@$k(B
ll query(int l, int r, int k = 0, int a = 0, int b = N - 1){
    lazy_evaluate_node(k);

    if(b <= l or r <= a) return 0;

    if(l <= a and b <= r){
        return seg[k].val;
    }

    int m = (a + b) / 2;
    ll vl = query(l, r, 2 * k + 1, a, m);
    ll vr = query(l, r, 2 * k + 2, m, b);
    update_node(k);

    return max(vl, vr);
}
// ---

int main(){
    update(4, 100, 1);

    int a, b;
    while(cin >> a >> b){
        cout << query(a, b) << endl;
    }

    return 0;
}
