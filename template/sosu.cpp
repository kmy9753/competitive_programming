int main(void){

    // ---
	const int N = (int)1e6;
	vi isP(N, true);
    isP[0] = isP[1] = false;
	for(int i = 2; i * i < N; i++) if(isP[i]) for(int j = i; i * j < N; j++) isP[i * j] = false;
    // ---

	return 0;
}
