// ----------------------------
using R = double;
using vec  = vector<R>;
using mat = vector<vec>;

// LU分解した情報が入るデータ構造．直接操作することは想定していない
struct LUinfo {
  vec value;
  vector<int> index;
};

// A をLU 分解し，その情報を返す
// O( n^3 ), Gaussian forward elimination
LUinfo LU_decomposition(mat A) {
  const int n = A.size();
  LUinfo data;
  for (int i = 0; i < n; ++i) {
    int pivot = i;
    for (int j = i+1; j < n; ++j)
      if (abs(A[j][i]) > abs(A[pivot][i])) pivot = j;
    swap(A[pivot], A[i]);
    data.index.push_back(pivot);
    // if A[i][i] == 0, LU decomposition failed.
    for(int j = i+1; j < n; ++j) {
      A[j][i] /= A[i][i];
      for(int k = i+1; k < n; ++k)
        A[j][k] -= A[i][k] * A[j][i];
      data.value.push_back(A[j][i]);
    }
  }
  for(int i = n-1; i >= 0; --i) {
    for(int j = i+1; j < n; ++j)
      data.value.push_back(A[i][j]);
    data.value.push_back(A[i][i]);
  }
  return data;
}

// Ax = b を解く
// O( n^2 ) Gaussian backward substitution
vec LU_backsubstitution(const LUinfo &data, vec b) {
  const int n = b.size();
  int k = 0;
  for (int i = 0; i < n; ++i){
    swap(b[data.index[i]], b[i]);
    for(int j = i+1; j < n; ++j)
      b[j] -= b[i] * data.value[k++];
  }
  for (int i = n-1; i >= 0; --i) {
    for (int j = i+1; j < n; ++j)
      b[i] -= b[j] * data.value[k++];
    b[i] /= data.value[k++];
  }
  return b;
}
// ----------------------------------

int main(void){
    mat A =
    {
        {1, 1, 1},
        {9, 3, 1},
        {16, 4, 1}
    };
    vec b = 
    {
        4,
        16,
        25
    };

    LUinfo info = LU_decomposition(A);
    vec coef = LU_backsubstitution(info, b);

    cout.precision(20);
    for(auto & e : coef){
        cout << fixed << e << endl;
    }

    return 0;
}
