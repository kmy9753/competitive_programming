// ---
using vec = valarray<ll>;
using mat = valarray<vec>;
 
// $B5U85(B
ll inv(ll a){
    ll res, dummy;
    extgcd(a, MOD, res, dummy);
    if(res < 0) res += MOD, dummy -= a;
    return res;
}
 
// $B@5J}9TNs$N@Q(B AB
mat mul(mat a, mat b){
    int m = a.size();
    mat c(vec(0LL, m), m);
    rep(i, m) rep(j, m) rep(k, m){
        c[i][j] += a[i][k] * b[k][j] % MOD;
        c[i][j] %= MOD;
    }
    return c;
}
 
// $B@5J}9TNs$N(B n $B>h(B
mat power(mat a, int n){
    int m = a.size();
    mat b(vec(0LL, m), m);
    rep(i, m) b[i][i] = 1;
    while(n){
        if(n & 1) b = mul(b, a);
        a = mul(a, a);
        n >>= 1;
    }
    return b;
}
 
inline int pivoting(mat &a, int k, int &c){
    int n = a.size(), m = a[0].size(), p = k, ret = 0;
    for(;c<m;++c){
        ll cmax = a[k][c];
        rep(i, k+1, n) if(chmax(cmax, a[i][c])) p = i, ret = 1;
        if(cmax) break;
    }
    if(k != p) swap(a[k], a[p]);
    return ret;
}
 
// $BA0?J>C5n(B
int forward(mat &a){
    int n = a.size(), m = a[0].size(), ret = 0, c = 0;
    rep(i, n - 1){
        ret += pivoting(a, i, c);
        if(a[i][c] == 0) break;
        rep(j, i + 1, n){
            ll coef = a[j][c] * inv(a[i][c]) % MOD;
            rep(k, c, m) a[j][k] = (a[j][k] - coef * a[i][k] % MOD + MOD) % MOD;
        }
    }
    return ret;
}
 
// $B8e?JBeF~(B
// $BA0?J>C5n(B forward $B$N8e$K8F$S=P$7!$(B
// $BO"N)J}Dx<0(B [A b] $B$r2r$/(B
vec back(mat &a){
    int n = a.size(), m = a[0].size();
    vec x(0LL, n);
    for(int i = n - 1; i >= 0; i--){
        ll sum = 0;
        if(i + 1 < n) rep(j, i + 1, n) sum += a[i][j] * x[j] % MOD;
        sum %= MOD;
        x[i] = (a[i][m-1] - sum + MOD) % MOD;
        x[i] = x[i] * inv(a[i][i]) % MOD;
    }
    rep(i, n) cout << x[i] << endl;
    return x;
}

// $B3,?t(B
int rank(mat &a){
    int n = a.size(), m = a[0].size(), ret = 0;
    rep(i, n) rep(j, m) if(a[i][j]) ret = i + 1;
    return ret;
}
 
// $B9TNs<0(B
// forward $B$N8e8F$V!%(Bsgn $B$O(B forward $B$NLa$jCM(B
ll det(mat &a, int sgn){
    ll ret = 1;
    int n = a.size(), m = a[0].size();
    rep(i, n) ret = ret * a[i][i] % MOD;
    if(sgn & 1) ret = MOD - ret;
    return ret;
}

// $B9TNs<0(B
ll det(mat &a){
    int sgn = forward(a);
    return det(a, sgn);
}

// [A b] $B$N@-<A%A%'%C%/(B
// forward $B$N8e8F$V!%(B
// -------------------
// 0: $B2r$J$7(B
// 1: $B2r$,0l0U(B
// 2: $B2r$,0l0U$G$J$$(B
int answer(mat &a){
    int n = a.size(), m = a[0].size();
    int arank = 0, brank = 0;
    rep(i, n) rep(j, m - 1) if(a[i][j]) arank = i + 1;
    rep(i, n) rep(j, m) if(a[i][j]) brank = i + 1;
    if(arank != brank) return 0;
    if(arank < n) return 2;
    return 1;
}
// ---
 
int main(void){
    int n; cin >> n;
    mat a(vec(0LL, n), n);
    rep(i, n) rep(j, n){
        cin >> a[i][j];
        a[i][j] = (MOD + a[i][j] % MOD) % MOD;
    }
    int sgn = forward(a);
    cout << det(a, sgn) << endl;
 
    return 0;
}
