///////////////////////
const int N = 1e6; 
ll fact[N+1];

void set_fact() {
    fact[0] = 1;
    rep(i,N) fact[i+1] = ((((ll)i+1) % MOD) * fact[i]) % MOD;
}

ll mod_inv(ll a, ll m) {
    ll x, y;
    extgcd(a, m, x, y);
    return (m + x % m) % m;
}

// n! mod p 
ll mod_fact(ll n, ll p, ll &e) {
    e = 0;
    if (!n) return 1;
    ll res = mod_fact(n/p, p, e);
    e += n/p;

    if ((n/p)%2) 
        return res * (p - fact[n%p]) %p;
    else
        return res * fact[n%p] % p;
}

// nCk mod p ; O(log_p n)
ll mod_comb(ll n, ll k, ll p) {
    if (n<0 || k<0 || n<k) return 0;
    ll e1, e2, e3;
    ll a1 = mod_fact(n, p, e1), a2 = mod_fact(k, p, e2), a3 = mod_fact(n-k, p, e3);
    if (e1 > e2+e3) return 0;
    return a1 * mod_inv(a2*a3%p, p) % p;
}

// nHk mod p ;
ll mod_h_comb(ll n, ll k, ll p) {
    return mod_comb(n+k-1, k, p);
}
///////////////////////

int main(void){
    set_fact();

    cout << mod_h_comb(10, 2, MOD) << endl;

    return 0;
}
