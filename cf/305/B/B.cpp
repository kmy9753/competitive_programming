#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;
typedef long double lld;

const int INF = 1 << 24;
const double EPS = 1e-8;

int main(void){
    ll p, q; cin >> p >> q;
    
    int n; cin >> n;
    vector<ll> a(n);
    while(n --) cin >> a[n];
    lld sum = 0.;
    each(a, it) sum = *it + (sum != 0. ? 1. / sum:0.);

    cout << "p / q = " << p / (double)q << endl;
    cout << "sum = " << sum << endl;
    if(sum == p / (double)q) cout << "YES" << endl;
    else cout << "NO" << endl;
 
    return 0;
}
