#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define V first
#define I second

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;

int main(void){
    int n, k; cin >> n >> k;
    vi f(n), t(n);
    vector<pair<double, int> > v(n);
    rep(i, n) cin >> f[i] >> t[i], v[i] = mp((double)f[i] / t[i], i);

    sort(ALL(v), greater<pair<double, int> >());
    int res = 0;
    rep(i, n){
        if((k -= t[v[i].I]) > 0)
            res += f[v[i].I];
        else{
            res += f[v[i].I] + k;
            break;
        }
        cout << "res:" << res << " k:" << k << endl;
    }

    cout << res << endl;

    return 0;
}
