#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define X first
#define Y second

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 201;
const int dx[4] = {1, 0, -1, 0};
const int dy[4] = {0, 1, 0, -1};

int main(void){
    vector< vector<bool> > yet(N);
    rep(y, N) yet[y].resize(N), fill(ALL(yet[y]), true);

    pii g, cur = mp(N / 2, N / 2);
    cin >> g.X >> g.Y;
    g.X += N / 2; g.Y += N / 2;

    int cnt = -1, dir = 3;
    while(cur != g){
        yet[cur.Y][cur.X] = false;
        int nextdir = (dir + 1) % 4;
        pii next = mp(cur.X + dx[nextdir], cur.Y + dy[nextdir]);
        if(yet[next.Y][next.X]){
            dir = nextdir;
            cnt ++;
        }
        else next = mp(cur.X + dx[dir], cur.Y + dy[dir]);

        cur = next;
    }

    cout << (~cnt ? cnt:0) << endl;
 
    return 0;
}
