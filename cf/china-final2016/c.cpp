#include <bits/stdc++.h>
#define rep(i,n) for(int i=0;(i)<(n);++(i))
#define range(i,a,b) for(int i=(a);(i)<(b);++(i))
#define rrep(i,n) for(int i=(n)-1;(i)>=0;--(i))
using namespace std;

template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0; }
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0; }

using vi = vector<int>;

const int inf = (int)1e6;
int compress(vi const& vec, map<int, int>& zip, vi& unzip){
    unzip = vec;

    sort(begin(unzip), end(unzip));
    unzip.erase(unique(begin(unzip), end(unzip)), end(unzip));
    unzip.emplace_back(inf);

    rep(i, (int)unzip.size()) zip[unzip[i]] = i;
    return (int)unzip.size();
}

int main(void) {
    int T; cin >> T;

    range(loop, 1, T + 1){
        int n; cin >> n;

        vi colors(n); for(auto& e : colors) cin >> e;
        map<int, int> zip; vi unzip;
        int m = compress(colors, zip, unzip);

        vi a(n);
        rep(i, n) a[i] = zip[colors[i]];

        int res = 0;
        rep(i, n + 1){
            int cur = 0;

            vi hist(m), histr(m);
            int st = inf;
            rrep(j, i){
                int c = a[j];
                if(hist[c] == 1) break;
                hist[c]++;
                cur++;
                st = j;
            }
            chmax(res, cur);

            int l = i;
            range(j, i, n){
                int c = a[j];
                hist[c]++;
                histr[c]++;
                cur++;

                if(histr[c] >= 2){ assert(histr[c] == 2);
                    while(histr[c] == 2){
                        int lc = a[l];
                        histr[lc]--;
                        hist[lc]--;
                        cur--;
                        l++;
                    }
                }
                else if(hist[c] >= 2){ assert(hist[c] == 2);
                    while(hist[c] == 2){
                        int sc = a[st];
                        hist[sc]--;
                        cur--;
                        st++;
                    }
                }

                chmax(res, cur);
            }
        }

        cout << "Case #" << loop << ": ";
        cout << res << endl;
    }

    return 0;
}
