#include <bits/stdc++.h>
#define int long long
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
signed main(void){
    int n; cin >> n;
    vi a(n);
    for(auto & e : a) cin >> e;

    int ave = 0;
    for(auto & e : a) ave += e;
    int p = ave % n;
    ave /= n;

    int res = 0, q = 0;
    for(auto & e : a){
        if(ave < e){
            res += e - ave;
            q++;
        }
    }
    res -= min(p, q);

    cout << res << endl;

	return 0;
}
