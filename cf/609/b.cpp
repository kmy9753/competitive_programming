#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
int main(void){
    int n, m; cin >> n >> m;
    vi a(n);
    for(auto & e : a) cin >> e, e--;

    vector<vi> sum(m, vi(n + 1));
    rep(i, m){
        range(j, 1, n + 1){
            sum[i][j] = sum[i][j - 1] + (a[j - 1] == i);
        }
    }

    int res = 0;
    rep(i, n){
        int g = a[i];
        res += (n - i) - (sum[g][n] - sum[g][i]);
    }

    cout << res << endl;

	return 0;
}
