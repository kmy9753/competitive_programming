#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, m; cin >> n >> m;
    vector<int> a(n);
    for(auto & e : a) cin >> e;

    sort(begin(a), end(a), greater<int>());
    int cnt = 0;
    for(auto & e : a){
        m -= e;
        cnt++;

        if(m <= 0) break;
    }

    cout << cnt << endl;

	return 0;
}
