#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using pii = pair<int, int>;
#define fst first
#define snd second
int main(void){
    int w, h;
    pii s; cin >> w >> h >> s.snd >> s.fst;
    s.fst--, s.snd--;
    string op; cin >> op;
    op = "_" + op;

    vector<vector<int>> used(h, vector<int>(w, 0));
    int nc = w * h;

    pii p = s;
    rep(i, (int)op.size()){
        pii d = pii(0, 0);
        switch(op[i]){
            case 'U': d.snd = -1; break;
            case 'R': d.fst =  1; break;
            case 'D': d.snd =  1; break;
            case 'L': d.fst = -1; break;
            default : break;
        }
        p = pii(max(0, min(h - 1, p.fst + d.fst)), max(0, min(w - 1, p.snd + d.snd)));

        cout << (i ? " ":"");
        if(i != (int)op.size() - 1){
            if(used[p.fst][p.snd]){
                cout << 0;
            }
            else {
                cout << 1;
                nc--;
            }
            used[p.fst][p.snd] = true;
        }
        else {
            // if(not used[p.fst][p.snd]) nc--;
            cout << nc << endl;
        }
    }

	return 0;
}
