#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    vector<int> a(3), b(3);
    for(auto & e : a) cin >> e;
    for(auto & e : b) cin >> e;

    int sum = 0;
    rep(i, 3){
        int d = a[i] - b[i];
        if(d >= 0){
            d /= 2;
        }
        sum += d;
    }

    if(sum >= 0) cout << "Yes" << endl;
    else cout << "No" << endl;

	return 0;
}
