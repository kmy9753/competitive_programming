#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using pii = pair<int, int>;
#define fst first
#define snd second

int main(void){
    int n, m; cin >> n >> m;
    vector<pii> in, not_in;
    rep(i, m){
        int a, b; cin >> a >> b;
        if(b == 1) in.push_back({a, i});
        else   not_in.push_back({a, i});
    }

    sort(begin(in), end(in));
    sort(begin(not_in), end(not_in));
    vector<pii> res(m);
    rep(i, n - 1){
        res[in[i].snd] = {0, i + 1};
    }

    priority_queue<pii, vector<pii>, greater<pii>> q; 
    range(i, 1, n - 1) q.push({i + 1, i});
    rep(idx, (int)not_in.size()){
        pii cur = q.top(); q.pop();
        int i = cur.snd, j = cur.fst;
        if(j + 1 < n) q.push({j + 1, i});

        if(not_in[idx].fst >= in[j - 1].fst){
            res[not_in[idx].snd] = {i, j};
        }
        else {
            cout << -1 << endl;
            return 0;
        }
    }

    for(auto & e : res){
        cout << e.fst + 1 << " " << e.snd + 1 << endl;
    }

	return 0;
}
