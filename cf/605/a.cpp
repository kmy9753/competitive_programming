#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
int main(void){
    int n; cin >> n;
    vi a(n); for(auto & e : a) cin >> e;

    vi dp(n + 1, 0);
    for(auto & e : a){
        dp[e] = dp[e - 1] + 1;
    }
    int len = *max_element(begin(dp), end(dp));

    cout << n - len << endl;

	return 0;
}
