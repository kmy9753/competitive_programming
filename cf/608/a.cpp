#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, s; cin >> n >> s;

    vector<int> tbl(s + 1);
    rep(i, n){
        int f, t; cin >> f >> t;
        tbl[f] = max(tbl[f], t);
    }

    int res = -1;
    rep(_, s + 1){
        int i = s - _;

        res = max(res + 1, tbl[i]);
    }

    cout << res << endl;

	return 0;
}
