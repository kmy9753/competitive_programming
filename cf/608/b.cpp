#include <bits/stdc++.h>
#define int long long
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    string a, b; cin >> a >> b;
    int la = a.size(), lb = b.size();

    vector<vector<int>> sum(lb + 1, vector<int>(2));

    rep(j, 2){
        range(i, 1, lb + 1){
            sum[i][j] = sum[i - 1][j] + (b[i - 1] == (j + '0') ? 0:1);
        }
    }

    int res = 0;
    rep(i, la){
        int j = a[i] - '0';

        res += sum[lb - (la - i) + 1][j] - sum[i][j];
    }

    cout << res << endl;

	return 0;
}
