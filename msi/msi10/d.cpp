#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const ll MOD = 998244353LL;
ll F[300000];

ll extgcd(ll a, ll b, ll &x, ll &y){
  ll g = a;
  x = 1;
  y = 0;
  if(b != 0){
    g = extgcd(b, a%b, y, x);
    y -= (a / b) * x;
  }
  return g;
}
 
ll modinv(ll a, ll m){
  ll x,y;
  if(extgcd(a,m,x,y) == 1) return (x + m) % m;
  return -1; 
}
 
ll mult( ll a, ll b ){
  return a*b % MOD;
}
 
void add( ll &a, ll b){
  a += b;
  a %= MOD;
}
 
ll minu( ll a, ll b ){
  b %= MOD;
  a -= b;
  if( a < 0 ) a += MOD;
  return a;
}
 
ll comb( ll a, ll b ){
  return mult( mult( F[a] , modinv( F[a-b], MOD ) ), modinv( F[b], MOD ) );
}
 
void init(){
  F[0] = 1;  
  for(ll i=1;i<300000;i++)
    F[i] = mult(F[i-1], i);
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    string S; cin >> S;
    S.push_back('#');

    ll res = 1LL;
    init();

    int cnt = 1;
    char c = S[0];
    rep(i, 1, n+1){
        if(S[i] == c){
            cnt++;
            continue;
        }

        ll K = 2 * cnt;
        ll N = cnt, M = 0;
        ll L = cnt;
        ll R = K-L;
        ll mt = comb( N+M+2*K, N+2*L );
        ll lk = 1;
        if( L ) lk = minu( comb( N + 2*L , L ), comb( N + 2*L , L-1 ) );
        ll rk = 1;
        if( R ) rk = minu( comb( M + 2*R , R ), comb ( M + 2*R, R-1 ) );
        ll occ = mult( mt, mult( lk, rk ) );

        res = mult(res, occ);
    }

    cout << res << endl;

    return 0;
}
