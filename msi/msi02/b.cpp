#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int insert_max_length=100010;
const int alphabet_num=26;

const int trie_root=0;
const int trie_nothing=-1;
int size=1;
set<int> ac[insert_max_length]; 
int mov[insert_max_length][alphabet_num];

void init(){
// memset(ac,set<int>(),sizeof(ac));
	memset(mov,trie_nothing,sizeof(mov));
	return;
}

vector<string> S;
void ins(int i){
	int cur=trie_root;
    ac[cur].insert(i);
	for(auto &ch:S[i]){
		if(mov[cur][ch-'a']==trie_nothing)
			mov[cur][ch-'a']=size++;
		cur=mov[cur][ch-'a'];
        ac[cur].insert(i);
	}
}

int find(set<int>& s, string& T){
	int cur=trie_root;
    if(ac[cur] == s) return 0;

    int ret = 1;
	for(auto &ch:T){
		cur=mov[cur][ch-'a'];
        if(ac[cur].size() <= s.size()){
            if(ac[cur] == s) return ret;
            break;
        }
        ret++;
	}
    return -1;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    init();

    int n, K; cin >> n >> K;
    set<int> s;
    vi a(K); for(auto& e : a) cin >> e, s.insert(e - 1);
    string T = "";
    S = vector<string>(n);
    rep(i, n){
        string& e = S[i];
        cin >> e;
        ins(i);
        if(T.size() < e.size()) T = e;
    }

    int res = find(s, T);
    if(res >= 0){
        cout << T.substr(0, res) << endl;
    }
    else {
        cout << -1 << endl;
    }

    return 0;
}
