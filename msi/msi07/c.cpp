#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

struct UnionFind {
    vector<int> data;
    UnionFind(int size) : data(size, -1) { }
    bool unionSet(int x, int y) {
        x = root(x); y = root(y);
        if (x != y) {
            if (data[y] < data[x]) swap(x, y);
            data[x] += data[y]; data[y] = x;
        }
        return x != y;
    }
    bool findSet(int x, int y) {
        return root(x) == root(y);
    }
    int root(int x) {
        return data[x] < 0 ? x : data[x] = root(data[x]);
    }
    int size(int x) {
        return -data[root(x)];
    }
};

inline ll modpow2(int n){
    ll ret = 1LL;
    ll b = 2LL;
    while(n > 0){
        if(n & 1){
            ret = MUL(ret, b, mod);
        }
        n /= 2;
        b = MUL(b, b, mod);
    }
    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;
    ll X; cin >> X;

    using Edge = tuple<int, int, int>;
    vector<Edge> edges(m);
    for(auto& e : edges){
        int a, b, c; cin >> a >> b >> c;
        a--, b--;
        e = Edge(c, a, b);
    }
    sort(_all(edges));

    vi used(m);
    vi idxs;
    ll cost_mst = 0LL;
    UnionFind uf(n);
    rep(i, m){
        int c, a, b; tie(c, a, b) = edges[i];
        if(uf.findSet(a, b)) continue;
        used[i] = true;
        idxs.emplace_back(i);
        cost_mst += c;
        uf.unionSet(a, b);
    }

    set<int> cand;
    set<int> rmv;
    rep(i, m){
        if(not used[i]) continue;

        uf = UnionFind(n);
        for(auto& j : idxs){
            int c, a, b; tie(c, a, b) = edges[j];
            if(j == i) continue;
            uf.unionSet(a, b);
        }
        ll cost_cur = cost_mst - get<0>(edges[i]);

        rep(j, m){
            int c, a, b; tie(c, a, b) = edges[j];
            if(used[j]) continue;
            if(uf.findSet(a, b)) continue;
            if(cost_cur + c == X){
                cand.insert(j);
            }
            if(cost_cur + c < X){
                rmv.insert(j);
            }
        }
    }

    for(auto& e : rmv){
        cand.erase(e);
    }

    int n_rmv = rmv.size();
    int n_cand = cand.size();
    int n_rest = m - (n - 1) - n_cand - n_rmv;

    ll res = 0LL;
    if(cost_mst == X){
        res = MUL(SUB(modpow2(n - 1), 2LL, mod), modpow2(m - (n - 1)), mod);
    }
    // cerr << res << endl;
    ll occ_cand = SUB(modpow2(n_cand), 1LL, mod);
    ll occ_rest = modpow2(n_rest);
    ll occ = MUL(occ_cand, occ_rest, mod);
    // cerr << occ_cand << " " << occ_rest << " " << occ << endl;
    res = ADD(res, MUL(occ, 2LL, mod), mod);

    cout << res << endl;

    return 0;
}
