#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int nc;
int m;
vi a;

vi hist;
vector<vector<vi>> dhist;
vector<vector<vi>> res;

void solve_naive(){
    using Elem = tuple<int, int, int>;
    set<Elem> s;
    rep(i, m){
        rep(j, i + 1, m){
            rep(k, j + 1, m){
                s.insert(Elem(a[i], a[j], a[k]));
            }
        }
    }
    for(auto& e : s){
        int i, j, k;
        tie(i, j, k) = e;
        res[i][j][k]++;
    }
}

void solve_hist(){
    rep(i, nc){
        rep(j, nc){
            rep(k, nc){
                if(hist[k] - dhist[0][i][k] - dhist[1][j][k] > 0){
                    res[i][k][j]++;
                }
            }
        }
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);
    
    int n; cin >> n;
    vector<string> A(n);
    for(auto& e : A) cin >> e;

    const char M = 52;
    vi c2i(300);
    vi i2c(M);
    nc = 0;
    rep(i, 'A', 'Z'+1) i2c[nc] = i, c2i[i] = nc++;
    rep(i, 'a', 'z'+1) i2c[nc] = i, c2i[i] = nc++;

    res = vector<vector<vi>>(nc, vector<vi>(nc, vi(nc)));
    rep(k, n){
        hist = vi(nc);
        m = A[k].size();
        a = vi(m);

        rep(j, m){
            int i = c2i[A[k][j]];
            a[j] = i;
            hist[i]++;
        }
        if(m <= nc){
            solve_naive();
            continue;
        }

        dhist = vector<vector<vi>>(2, vector<vi>(nc, vi(nc)));
        rep(l, 2){
            rep(i, nc){
                for(auto& e : a){
                    dhist[l][i][e]++;
                    if(e == i) break;
                }
            }
            reverse(_all(a));
        }
        solve_hist();
    }

    int max_cnt = 0;
    string T;
    rep(i, nc){
        rep(j, nc){
            rep(k, nc){
                if(chmax(max_cnt, res[i][j][k])){
                    T = "";
                    T.push_back(i2c[i]);
                    T.push_back(i2c[j]);
                    T.push_back(i2c[k]);
                }
            }
        }
    }
    cout << T << endl;

    return 0;
}
