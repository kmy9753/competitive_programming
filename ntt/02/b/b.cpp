#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
string in;
const int M = (int)1e6;
int cnt;
int di = 1;
int x, y;

void op(char c){
    switch(c){
        case 'F':
            {
                x += dx[di];
                y += dy[di];
                break;
            }
        case 'B':
            {
                x -= dx[di];
                y -= dy[di];
                break;
            }
        case 'R':
            {
                di = (di + 3) % 4;
                break;
            }
        case 'L':
            {
                di = (di + 1) % 4;
                break;
            }
    }
}

set<char> T = {'F', 'B', 'R', 'L'};

int parse(int p){
    char c = in[p++];
    while(c != ']'){
        if(T.find(c) != end(T)){
            op(c);
            if(++cnt == M) return -1;
        }
        else {
            int num = c - '0';
            char c = in[p++];
            if(c == '['){
                int np = parse(p);
                if(np == -1) return -1;
                rep(_, num-1){
                    if(parse(p) == -1){
                        return -1;
                    }
                }
                p = np;
            }
            else {
                rep(_, num){
                    op(c);
                    if(++cnt == M) return -1;
                }
            }
        }

        c = in[p++];
    }

    return p;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    cin >> in;
    in = "1[" + in + "]";

    parse(2);

    cout << x << " " << y << endl;

    return 0;
}
