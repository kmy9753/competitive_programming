#include <bits/stdc++.h>
#define int long long
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 20;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

// using State = tuple<int, int, int>;

const int MAX = 100000;
const int MAX_LOG = 100;

vector<int> edge[MAX];

int parent[MAX_LOG][MAX];
int depth[MAX];

void dfs(int curr, int prev, int d) {
  parent[0][curr] = prev;
  depth[curr] = d;
  
  rep(i, edge[curr].size()) {
    if(edge[curr][i] != prev) {
      dfs(edge[curr][i], curr, d + 1);
    }
  }
}

void init(int V) {
  dfs(0, -1, 0);
  
  rep(k, MAX_LOG - 1) {
    rep(v, V) {
      if(parent[k][v] < 0) parent[k + 1][v] = -1;
      else parent[k + 1][v] = parent[k][parent[k][v]];
    }
  }
}

int lca(int u, int v) {
  if(depth[u] > depth[v]) swap(u, v);

  rep(k, MAX_LOG) {
    if((depth[v] - depth[u]) >> k & 1) {
      v = parent[k][v];
    }
  }
  if(u == v) return u;
  
  for(int k = MAX_LOG - 1; k >= 0; k--) {
    if(parent[k][u] != parent[k][v]) {
      u = parent[k][u];
      v = parent[k][v];
    }
  }
  return parent[0][u];
}

signed main(void){
    int n; cin >> n;

    rep(v, 1, n){
        int p; cin >> p;
        p--;
        edge[p].push_back(v);
        edge[v].push_back(p);
    }
    init(n);

    rep(v, n){
        sort(_all(edge[v]));
    }

    // priority_queue<State, vector<State>, greater<State>> q;
    queue<int> q;
    vi used(n);
    // q.push(State(0, 0, 0));
    q.push(0);
    int prev = 0;
    int res = 0;
    while(q.size()){
        int v;
        // tie(ignore, ignore, v) = q.top();
        v = q.front();
        q.pop();

        if(used[v]) continue;
        used[v] = true;

        int lcav = lca(prev, v);
        res += depth[prev] + depth[v] - depth[lcav] * 2;

        for(auto & nv : edge[v]){
            if(used[nv]) continue;
            // q.push(State(depth[nv], v, nv));
            q.push(nv);
        }
        prev = v;
    }

    cout << res << endl;

    return 0;
}
