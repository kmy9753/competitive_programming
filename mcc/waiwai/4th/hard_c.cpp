#include <iostream>
#include <algorithm>
 
using namespace std;
 
int main(void){
    while(1){
        int n, k; cin >> n >> k;
        if(n == 0 && k == 0) break;
 
        int a[(int)1e5 + 2];
        for(int i = 0; i < n + 2; i++) a[i] = 0;
 
        int has_white = 0;
        for(int i = 0; i < k; i++){
            int c; cin >> c;
            if(c == 0) has_white = 1;
            else {
                c--;
                a[c] = 1;
            }
        }
 
        int cur = -1, res = 0;
        for(int i = 0; i < n; i++){
            if(a[i] == 0){
                cur = -1;
            }
            else {
                if(cur == -1) cur = i;
                else a[cur]++;
 
                res = max(res, a[cur]);
            }
        }
 
        if(has_white){
            for(int i = 0; i < n; i++){
                res = max(res, a[i] + 1 + a[i + a[i] + 1]);
            }
        }
 
        cout << res << endl;
    }
 
    return 0;
}