#include <iostream>
#include <cstdlib>
 
using namespace std;
 
typedef long long ll;
 
int main(void){
    int n; cin >> n;
 
    int pre; cin >> pre;
    ll res = 0;
 
    for(int i = 0; i < n - 1; i++){
        int cur; cin >> cur;
        res += abs(pre - cur);
        pre = cur;
    }
    cout << res << endl;
 
    return 0;
}
