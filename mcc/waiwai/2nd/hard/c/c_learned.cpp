#include <iostream>
#include <string>
#include <algorithm>
#include <map>

using namespace std;

string strs[20];
int cnt[20];

int main(void){
    int n; cin >> n;

    int idx = 0;
    for(int loop = 0; loop < n; loop++){
        string s; cin >> s;
        bool exist = false;
        for(int i = 0; i < idx; i++){
            if(strs[i] == s){
                cnt[i]++;
                exist = true;
                break;
            }
        }
        if(!exist){
            strs[idx] = s;
            cnt[idx]++;
            idx++;
        }
    }

    int res = 0;
    for(int i = 0; i < idx; i++){
        res = max(res, cnt[i]);
    }

    cout << res << endl;

    return 0;
}
