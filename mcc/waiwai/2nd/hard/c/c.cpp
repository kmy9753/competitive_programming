#include <iostream>
#include <string>
#include <algorithm>
#include <map>

using namespace std;

int main(void){
    int n; cin >> n;
    map<string, int> dic;

    for(int loop = 0; loop < n; loop++){
        string s; cin >> s;
        dic[s]++;
    }

    int res = 0;
    for(map<string, int>::iterator itr = dic.begin(); itr != dic.end(); itr++){
        res = max(res, itr->second);
    }

    cout << res << endl;

    return 0;
}
