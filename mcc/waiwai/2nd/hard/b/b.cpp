#include <iostream>
#include <algorithm>
// #include <stdio.h>

using namespace std;

int main(void){
    int n; cin >> n;

    int sum = 0, max_a = 0, min_a = 1000000;
    for(int loop = 0; loop < n; loop++){
        int a; cin >> a;

        sum += a;
        max_a = max(max_a, a);
        min_a = min(min_a, a);
    }

    double res = 1.0 * (sum - max_a - min_a) / (n - 2);

    cout.precision(12);
    cout << res << endl;
    // pintf("%.12f\n", res);

    return 0;
}
