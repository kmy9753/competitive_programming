#include <iostream>
#include <algorithm>

using namespace std;

int a[1000];

int main(void){
    int n; cin >> n;
    for(int i = 0; i < n; i++) cin >> a[i];

    int res = 0;
    for(int len = 1; len <= n; len++){
        int cur = 0;
        for(int i = 0; i < len; i++){
            cur += a[i];
        }
        res = max(res, cur);

        for(int l = 1; l + len - 1 < n; l++){
            int r = l + len;
            cur = cur - a[l - 1] + a[r - 1];

            res = max(res, cur);
        }
    }

    cout << res << endl;

    return 0;
}
