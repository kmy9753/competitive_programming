#include <iostream>
#include <algorithm>

using namespace std;

int a[100];

int main(void){
    int n; cin >> n;
    for(int i = 0; i < n; i++) cin >> a[i];

    int res = 0;
    for(int l = 0; l < n; l++){
        for(int r = l + 1; r <= n; r++){
            int cur = 0;
            for(int i = l; i < r; i++){
                cur += a[i];
            }

            res = max(res, cur);
        }
    }

    cout << res << endl;

    return 0;
}
