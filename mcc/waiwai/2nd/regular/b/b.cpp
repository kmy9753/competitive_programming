#include <iostream>

using namespace std;

int main(void){
    int n; cin >> n;

    for(int loop = 0; loop < n; loop++){
        int a; cin >> a;
        
        char res = 'S';
        if(a < 60){
            res = 'D';
        }
        else if(a < 80){
            res = 'A';
        }

        cout << res << endl;
    }
    
    return 0;
}
