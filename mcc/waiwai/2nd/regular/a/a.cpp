#include <iostream>
#include <string>

using namespace std;

int main(void){
    int n; cin >> n;

    int res = 0;
    for(int loop = 0; loop < n; loop++){
        string s; cin >> s;

        if(s.find('a') != string::npos){
            res++;
        }
    }

    cout << res << endl;

    return 0;
}
