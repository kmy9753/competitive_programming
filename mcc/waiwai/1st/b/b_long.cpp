#include <iostream>
#include <string>

using namespace std;

int main(void){
    string S, T, res = "Yes";
    cin >> S >> T;

    int lenS = S.size(),
        lenT = T.size();
    if(lenS != lenT){
        res = "No";
    }
    else {
        for(int i = 0; i < lenS; i++){
            if(S[i] != T[i]){
                res = "No";
            }
        }
    }

    cout << res << endl;

    return 0;
}
