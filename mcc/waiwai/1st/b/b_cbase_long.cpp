#include <stdio.h>
#include <string.h>

int main(void){
    int i, lenS, lenT, ok = 1;
    char S[18], T[18];
    scanf("%s%s", S, T);

    lenS = strlen(S);
    lenT = strlen(T);
    if(lenS != lenT){
        ok = 0;
    }
    else {
        for(i = 0; i < lenS; i++){
            if(S[i] != T[i]){
                ok = 0;
            }
        }
    }

    if(ok == 1){
        printf("%s\n", "Yes");
    }
    else {
        printf("%s\n", "No");
    }

    return 0;
}
