#include <stdio.h>
#include <string.h>

int main(void){
    char S[18], T[18];
    scanf("%s%s", S, T);

    if(strcmp(S, T) == 0){
        printf("%s\n", "Yes");
    }
    else {
        printf("%s\n", "No");
    }

    return 0;
}
