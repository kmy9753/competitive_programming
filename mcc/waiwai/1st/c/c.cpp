#include <iostream>

using namespace std;

int main(void){
    int a, b, c, s;
    cin >> a >> b >> c >> s;

    int res = 0;
    for(int i = 0; i <= a; i++){
        for(int j = 0; j <= b; j++){
            int k = s - (i + j);
            if(0 <= k && k <= c){
                res++;
            }
        }
    }

    cout << res << endl;

    return 0;
}
