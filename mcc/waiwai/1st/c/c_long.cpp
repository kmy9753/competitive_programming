#include <iostream>

using namespace std;

int main(void){
    int a, b, c, s;
    cin >> a >> b >> c >> s;

    int res = 0;
    for(int i = 0; i <= a; i++){
        for(int j = 0; j <= b; j++){
            for(int k = 0; k <= c; k++){
                int sum = i + j + k;
                if(sum == s){
                    res++;
                }
            }
        }
    }

    cout << res << endl;

    return 0;
}
