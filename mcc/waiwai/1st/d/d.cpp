#include <iostream>

using namespace std;

int n, a[10], s;
int res;

void rec(int idx, int sum){
    if(idx == n){
        if(sum == s){
            res++;
        }
        return;
    }

    for(int p = 0; p <= a[idx]; p++){
        rec(idx + 1, sum + p);
    }
}

int main(void){
    cin >> n >> s;
    for(int i = 0; i < n; i++){
        cin >> a[i];
    }

    rec(0, 0);
    
    cout << res << endl;

    return 0;
}
