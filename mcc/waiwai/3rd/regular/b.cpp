#include <iostream>

using namespace std;

int main(void){
    int n; cin >> n;

    int res = 10000;
    for(int i = 0; i < n; i++){
        int a, b; cin >> a >> b;

        if(b == 1){
            res = min(res, a);
        }
    }

    cout << res << endl;

    return 0;
}
