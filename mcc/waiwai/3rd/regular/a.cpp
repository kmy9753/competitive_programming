#include <iostream>

using namespace std;

int main(void){
    int n, m, p; cin >> n >> m >> p;

    int pos = 1;
    for(int i = 0; i < p; i++){
        int a; cin >> a;
        pos += a;
    }

    if(pos == n){
        cout << "Win" << endl;
    }
    else {
        cout << "Lose" << endl;
    }

    return 0;
}
