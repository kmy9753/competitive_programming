#include <iostream>

using namespace std;

int adj[20][20];

int main(void){
    int n, m; cin >> n >> m;

    for(int loop = 0; loop < m; loop++){
        int a, b; cin >> a >> b;
        a--, b--;

        adj[a][b] = 1;
        adj[b][a] = 1;
    }

    for(int p = 0; p < n; p++){
        int fst = 1;
        for(int i = 0; i < n; i++){
            if(adj[p][i] == 1){
                if(fst == 1) fst = 0;
                else         cout << " ";

                cout << i + 1;
            }
        }
        if(fst == 1) cout << -1;

        cout << endl;
    }

    return 0;
}
