#include <iostream>

using namespace std;

#define N 20

int main(void){
    int n, m; cin >> n >> m;

    int adj[N][N];
    for(int loop = 0; loop < m; loop++){
        int a, b; cin >> a >> b;
        a--, b--;

        adj[a][b] = 1;
        adj[b][a] = 1;
    }

    int q; cin >> q;
    int visited[N][N];
    for(int loop = 0; loop < q; loop++){
        int x, y; cin >> x >> y;
        x--, y--;

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                visited[i][j] = 0;
            }
        }

        int cur = x, pre = x;
        string res = "No";
        while(visited[cur][pre] == 0){
            visited[cur][pre] = 1;
            if(cur == y) res = "Yes";

            for(int i = 0; i < n; i++){
                if(adj[cur][i] == 1 && i != pre){
                    pre = cur;
                    cur = i;
                    break;
                }
            }
        }

        cout << res << endl;
    }

    return 0;
}
