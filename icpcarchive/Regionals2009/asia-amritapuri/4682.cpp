#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)

using namespace std;

const int insert_max_length=3300100;
const int alphabet_num=2;

const int trie_root=0;
const int trie_nothing=-1;
int size;
int ac[insert_max_length]; 
int mov[insert_max_length][alphabet_num];

typedef vector<int> vi;

void init(){
	memset(ac,0,sizeof(ac));
	memset(mov,trie_nothing,sizeof(mov));
    size = 1;
	return;
}

inline void ins(string arg, int val){
	int cur=trie_root;
	for(auto &ch:arg){
		if(mov[cur][ch-'0']==trie_nothing)
			mov[cur][ch-'0']=size++;
		cur=mov[cur][ch-'0'];
	}
	ac[cur]=val;
	return;
}

inline int find(string arg){
	int cur=trie_root;
	for(auto &ch:arg){
		if(mov[cur][ch-'0']==trie_nothing){
            ch = (ch == '0') ? '1':'0';
        }
		cur=mov[cur][ch-'0'];
	}
	return ac[cur];
}

inline string toS(int num){
    string ret;
    rep(i, 32){
        if((num >> (31L - i)) & 1){
            ret.push_back('1');
        }
        else{
            ret.push_back('0');
        }
    }
    return ret;
}

signed main(void){
    int T; cin >> T;
    rep(loop, T){
        init();
        
        int n; cin >> n;
        vi a(n);
        for(auto & e : a) scanf("%d", &e);

        vi sum(n + 1);
        range(i, 1, n + 1){
            sum[i] = sum[i - 1] ^ a[i - 1];
        }

        ins(toS(0), 0);
        int res = 0;
        for(auto & e : sum){
            int cur = find(toS(~e)) ^ e;
            res = max(res, cur);

            ins(toS(e), e);
        }

        cout << res << endl;
    }

	return 0;
}
