#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<double> r(n);
    for(auto & e : r) cin >> e;

    vector<double> x(n);

    double res = 0.0;
    rep(i, n){
        x[i] = r[i];

        rep(j, i){
            double b = r[i] - r[j], c = r[i] + r[j];
            x[i] = max(x[i], sqrt(c * c - b * b) + x[j]);
        }

        res = max(res, x[i] + r[i]);
    }

    cout.precision(12);
    cout << res << endl;

	return 0;
}
