#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e;

    set<int> s;
    rep(i, n){
        int num = 0;
        rep(j, 9){
            if(i + j >= n) break;
            num *= 10;
            num += a[i + j];
            s.insert(num);
        }
    }

    int res = 0;
    for(auto & e : s){
        if(e != res){
            break;
        }
        res++;
    }

    cout << res << endl;

	return 0;
}
