#include <bits/stdc++.h> 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int n, m;
using Edge = tuple<int, int>;
vector<vector<Edge>> G;
vector<vector<Edge>> tree;
vector<vi> dist; 

vi v2i, i2v, droot;
int sn;

const int limit = 200002;

int vid[limit];
int head[limit];
int heavy[limit];
int par[limit];
ll depth[limit];
int inv[limit];

int hld_dfs(int v, int pv) {
    int cmax = -1, sz = 1;
    for (auto &e : tree[v]) {
        int nv, c; tie(c, nv) = e;
        if (nv == pv) continue;
        par[nv] = v, depth[nv] = depth[v] + c;
        int sub_sz = hld_dfs(nv, v);
        sz += sub_sz;

        if (cmax < sub_sz) {
            cmax = sub_sz;
            heavy[v] = nv;
        }
    }
    return sz;
}

void hld_init(int n) {
    rep(v, n) vid[v] = heavy[v] = -1;
    hld_dfs(0, -1);

    int id = 0;
    queue<int> q({0});
    while (!q.empty()) {
        int h = q.front(); q.pop();
        for (int v = h; v != -1; v = heavy[v]) {
            inv[id] = v, vid[v] = id++, head[v] = h;
            for (auto &e : tree[v]) {
                int nv; tie(ignore, nv) = e;
                if (nv == par[v] or nv == heavy[v]) continue;
                q.push(nv);
            }
        }
    }
    return ;
}

int lca(int u, int v) {
    if (vid[u] > vid[v]) swap(u, v);
    if (head[u] == head[v]) return u;
    return lca(u, par[head[v]]);
}

struct UnionFind {
    vector<int> data;
    UnionFind(int size) : data(size, -1) { }
    bool unionSet(int x, int y) {
        x = root(x); y = root(y);
        if (x != y) {
            if (data[y] < data[x]) swap(x, y);
            data[x] += data[y]; data[y] = x;
        }
        return x != y;
    }
    bool findSet(int x, int y) {
        return root(x) == root(y);
    }
    int root(int x) {
        return data[x] < 0 ? x : data[x] = root(data[x]);
    }
    int size(int x) {
        return -data[root(x)];
    }
};
/*
void dfs_hoge(int v, int pv){
    used[v] = true;
    for(auto& e : G[v]){
        int nv, c; tie(c, nv) = e;
        if(nv == pv) continue;
        if(used[nv]){
            if(v2i[ v] == -1) v2i[ v] = sn++, i2v.emplace_back( v);
            if(v2i[nv] == -1) v2i[nv] = sn++, i2v.emplace_back(nv);
        }
        else {
            droot[nv] = droot[v] + c;
            tree[v].emplace_back(Edge(c, nv));
            tree[nv].emplace_back(Edge(c, v));
            dfs(nv, v);
        }
    }
}
*/

void mst(){
    using Elem = tuple<int, int, int>;
    priority_queue<Elem, vector<Elem>, greater<Elem>> q;
    rep(a, n){
        for(auto& e : G[a]){
            int c, b; tie(c, b) = e;
            if(b <= a) continue;
            q.push(Elem(c, a, b));
        }
    }
    UnionFind uf(n);
    while(q.size()){
        Elem cur = q.top(); q.pop();
        int a, b, c; tie(c, a, b) = cur;
        
        if(uf.findSet(a, b)){
            if(v2i[a] == -1) v2i[a] = sn++, i2v.emplace_back(b);
            if(v2i[b] == -1) v2i[b] = sn++, i2v.emplace_back(a);
        }
        else {
            tree[a].emplace_back(Edge(c, b));
            tree[b].emplace_back(Edge(c, a));
            uf.unionSet(a, b);
        }
    }
}

void dfs(int v, int pv){
    for(auto& e : tree[v]){
        int nv, c; tie(c, nv) = e;
        if(nv == pv) continue;
        droot[nv] = droot[v] + c;
        dfs(nv, v);
    }
}

int main(void){
    int T; cin >> T;
    rep(loop, 1, T + 1){
        int Q; scanf("%d %d %d", &n, &m, &Q);
        G = tree = vector<vector<Edge>>(n);
        rep(i, m){
            int a, b, c; scanf("%d %d %d", &a, &b, &c);
            a--, b--;
            G[a].emplace_back(Edge(c, b));
            G[b].emplace_back(Edge(c, a));
        }

        v2i = vi(n, -1);
        droot = vi(n, 0);
        sn = 0;
        i2v = vi();
        mst();
        dfs(0, -1);

        dist = vector<vi>(sn, vi(sn, inf));
        rep(i, sn) dist[i][i] = 0;
        rep(a, n){
            if(v2i[a] == -1) continue;
            for(auto& e : G[a]){
                int b, c; tie(c, b) = e;
                if(v2i[b] == -1) continue;
                chmin(dist[v2i[a]][v2i[b]], c);
            }
        }

        hld_init(n);
        auto calc_d = [&](int a, int b){
            return droot[a] + droot[b] - 2 * droot[lca(a, b)];
        };

        vector<vi> d_b2s(n, vi(sn, inf));
        vi as(Q), bs(Q);
        rep(i, Q) scanf("%d %d", &as[i], &bs[i]), as[i]--, bs[i]--;
        vi nc(n);

        for(auto& e : as) nc[e] = true;
        for(auto& e : bs) nc[e] = true;

        rep(v, n){
            if(not nc[v]) continue;
            rep(i, sn){
                d_b2s[v][i] = calc_d(v, i2v[i]);
            }
        }

        // cerr << droot[2] << " " << droot[6] << " " << droot[lca(2, 6)] << endl;
        // cerr << calc_d(2, 6) << endl;
        rep(a, sn){
            rep(b, sn){
                int cur = d_b2s[i2v[a]][b];
                chmin(dist[a][b], cur);
            }
        }
        rep(k, sn) rep(i, sn) rep(j, sn) chmin(dist[i][j], dist[i][k] + dist[k][j]);
        // rep(i, sn) rep(j, sn) cerr << "(" << i2v[i]+1 << ", " << i2v[j]+1 << "): " << dist[i][j] << endl;

        printf("Case %d:\n", loop);
        rep(_, Q){
            int a = as[_], b = bs[_];

            int res = calc_d(a, b);
            rep(i, sn){
                rep(j, sn){
                    chmin(res, d_b2s[a][i] + dist[i][j] + d_b2s[b][j]);
                }
            }
            printf("%d\n", res);
        }
    }

    return 0;
}
