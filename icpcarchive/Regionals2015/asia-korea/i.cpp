#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int T; cin >> T;
    while(T--){
        int n; cin >> n;
        vector<int> p(n);
        for(auto & e : p) cin >> e;
        int maxp = 0, cnt = 0, res = 0;
        for(int i = n - 1; i >= 0; i--){
            if(p[i] >= maxp){
                res += maxp * cnt;
                maxp = p[i];
                cnt = 0;
            }
            else {
                res -= p[i];
                cnt++;
            }
        }
        res += maxp * cnt;

        cout << res << endl;
    }

	return 0;
}
