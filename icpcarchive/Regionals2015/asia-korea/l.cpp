#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int T; cin >> T;
    while(T--){
        int n, m; cin >> n >> m;
        string X, Y;
        rep(i, m){
            char c; cin >> c;
            X.push_back(c);
        }
        rep(i, m){
            char c; cin >> c;
            Y.push_back(c);
        }
        string r;
        rep(i, n){
            char c; cin >> c;
            r.push_back(c);
        }

        int res = 0;
        rep(i, n){
            string Z = r.substr(i, m);
            if((int)Z.size() < m){
                Z += r.substr(0, m - (int)Z.size());
            }
            if(X <= Z && Z <= Y){
                res++;
            }
        }

        cout << res << endl;
    }

	return 0;
}
