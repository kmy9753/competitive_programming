#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int T; cin >> T;
    
    while(T--){
        int n; cin >> n;
        
        vector<int> a(n);
        for(auto & e : a) cin >> e;
        sort(begin(a), end(a));

        int res = abs(a[0] - a[1]);
        res = max(res, abs(*(end(a) - 1) - *(end(a) - 2)));
        res = max(res, abs(*(end(a) - 1) - *(end(a) - 3)));


        rep(i, n / 2 - 1){
            res = max(res, abs(a[i * 2] - a[(i + 1) * 2]));
        }
        rep(i, ((n - 1) / 2) - 1){
            res = max(res, abs(a[i * 2 + 1] - a[(i + 1) * 2 + 1]));
        }

        cout << res << endl;
    }

	return 0;
}
