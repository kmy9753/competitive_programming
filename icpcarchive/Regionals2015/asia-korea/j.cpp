#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
const int N = 10000;
int main(void){
    vector<bool> isprime(N, true);
    vector<int> p;
    isprime[0] = isprime[1] = false;
    rep(i, 1000){
        if(!isprime[i]) continue;
        p.push_back(i);
        for(int j = 2; i * j < N; j++){
            isprime[i * j] = false;
        }
    }

    int T; cin >> T;
    while(T--){
        int K; cin >> K;

        vector<int> a(3, -1);
        rep(i, (int)p.size()){
            range(j, i, (int)p.size()){
                if(K - (p[i] + p[j]) >= 0 && isprime[K - (p[i] + p[j])]){
                    a[0] = p[i], a[1] = p[j], a[2] = K - a[0] - a[1];
                }
            }
        }

        sort(begin(a), end(a));
        if(a[0] != -1) cout << a[0] << " " << a[1] << " " << a[2] << endl;
        else cout << 0 << endl;
    }

	return 0;
}
