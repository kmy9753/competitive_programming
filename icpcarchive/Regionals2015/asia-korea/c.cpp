#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
const int inf = 1 << 30;
class dinic{
	public :
		void init(int _n){
			n=_n;
			G.resize(n);
			iter.resize(n);
			level.resize(n);
		}

		void add_edge(int from,int to ,int cap){
			G[from].push_back((edge){to,cap,G[to].size()});
			G[to].push_back((edge){from,0,G[from].size()-1});
		}
	
		void add_edge_both(int from,int to ,int cap){
			add_edge(from,to,cap);
			add_edge(to,from,cap);
		}
	
		int max_flow(int s,int t){
			int flow=0;
			for(;;){
				bfs(s);
				if(level[t]<0) return flow;
				iter.assign(n,0);
				int f;
				while((f=dfs(s,t,DINIC_INF))>0){
					flow+=f;
				}
			}
		}
	private:
	
		int n;
		struct edge{int to,cap,rev;};
		static const int DINIC_INF = inf;
		vector< vector<edge> > G;
		vi level;
		vi iter;
	
		void bfs(int s){
			level.assign(n,-1);
			queue<int> que;
			level[s]=0;
			que.push(s);
			while(!que.empty()){
				int v=que.front();que.pop();
				for(int i=0;i< G[v].size(); i++){
					edge &e=G[v][i];
					if(e.cap>0 && level[e.to] <0){
						level[e.to]=level[v]+1;
						que.push(e.to);
					}
				}
			}
		}

		int dfs(int v,int t,int f){
			if(v==t) return f;
			for(int &i=iter[v];i<G[v].size();i++){
				edge &e= G[v][i];
				if(e.cap>0 && level[v]<level[e.to]){
					int d=dfs(e.to,t,min(f,e.cap));
					if(d>0){
						e.cap -=d;
						G[e.to][e.rev].cap+=d;
						return d;
					}
				}
			}
			return 0;
		}	
};


int main(void){
    int T; cin >> T;

    rep(loop, T){
        int n, m; cin >> n >> m;
        vvi a(n, vi(m));
        int sum = 0;
        for(auto & v : a) for(auto & e : v) cin >> e, sum += e;

        int nv = n * m + 2;
        dinic d;
        d.init(nv);
        map<pair<int, int>, int> p2i;
        int idx = 1;
        rep(y, n){
            rep(x, m){
                p2i[make_pair(y, x)] = idx++;
            }
        }

        rep(y, n){
            rep(x, m){
                if((x + y) % 2 == 1){
                    d.add_edge(p2i[make_pair(y, x)], nv - 1, a[y][x]);
                    rep(yy, n){
                        rep(xx, m){
                            if((xx + yy) % 2 == 0){
                                d.add_edge(p2i[make_pair(yy, xx)], p2i[make_pair(y, x)], inf);
                            }
                        }
                    }
                }
                else {
                    d.add_edge(0, p2i[make_pair(y, x)], a[y][x]);
                }
            }
        }

	    cout << sum - d.max_flow(0, nv - 1) << endl;
    }

	return 0;
}
