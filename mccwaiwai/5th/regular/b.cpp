#include <iostream>
#include <string>

using namespace std;

int main(void){
    string a, b; cin >> a >> b;

    if(a == "0" || a == "1" ||
       b == "0" || b == "1"){
        cout << "A" << endl;
    }
    else {
        cout << "M" << endl;
    }

    return 0;
}
