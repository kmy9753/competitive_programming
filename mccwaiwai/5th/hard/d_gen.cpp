#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <set>

#define range(i, a, b) for(int (i) = (a); (i) < (b); (i)++)
#define rep(i, n) range((i), 0, (n))

using namespace std;

int main(void){
    srand(6);

    int n = 50;
    vector<string> S(n);
    [&]{
        int idx = 0;
        rep(i, 'z' - 'a' + 1){
            S[i] = (char)('a' + i);
            idx++;
        }
        range(i, idx, n){
            S[i] = S[i - idx] + S[i - idx];
        }
    }();

    vector<int> a(n);
    vector<vector<string>> T(n);
    rep(i, n){
        set<string> s;
        rep(i, rand() % 3){
            int idx = rand() % n;
            if(idx != i){
                s.insert(S[idx]);
            }
        }

        for(auto & e : s) T[i].push_back(e);
        a[i] = s.size();
    }

    cout << n << endl;
    rep(i, n){
        cout << (i ? " ":"") << S[i];
    }
    cout << endl;

    rep(i, n){
        cout << a[i];
        for(auto & e : T[i]){
            cout << " " << e;
        }
        cout << endl;
    }

    return 0;
}
