#include <iostream>
#include <string>
#include <map>

using namespace std;

int n;
int adj[50][50], visited[50];

void dfs(int v){
    if(visited[v]) return;

    visited[v] = 1;

    for(int u = 0; u < n; u++){
        if(!adj[v][u]) continue;

        dfs(u);
    }
}

int main(void){
    cin >> n;

    map<string, int> s2i;
    for(int i = 0; i < n; i++){
        string S; cin >> S;
        s2i[S] = i;
    }

    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            adj[i][j] = 0;
        }
    }
    for(int i = 0; i < n; i++) visited[i] = 0;

    for(int i = 0; i < n; i++){
        int a; cin >> a;

        for(int _ = 0; _ < a; _++){
            string T; cin >> T;
            int j = s2i[T];

            adj[i][j] = adj[j][i] = 1;
        }
    }

    int res = 0;
    for(int i = 0; i < n; i++){
        if(visited[i]) continue;

        dfs(i);
        res++;
    }

    cout << res << endl;

    return 0;
}

