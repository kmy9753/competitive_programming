#include <iostream>
#include <cmath>

using namespace std;

int main(void){
    int n, x;
    cin >> n >> x;

    int p = 0;
    while((x /= 10) > 0) p++;

    cout << "1";
    for(int i = 0; i < p * (int)pow(2.0, n); i++){
        cout << "0";
    }
    cout << endl;

    return 0;
}
