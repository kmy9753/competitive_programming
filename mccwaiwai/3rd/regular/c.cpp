#include <iostream>

using namespace std;

int main(void){
    int n, L; cin >> n >> L;

    int res = 0;
    for(int i = 0; i < n; i++){
        int x, d; cin >> x >> d;

        int t;
        if(d == 1) t = L - x + 1;
        else       t = x;

        res = max(res, t);
    }

    cout << res << endl;

	return 0;
}
