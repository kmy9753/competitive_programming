#include <iostream>

using namespace std;

int main(void){
    int n, m; cin >> n >> m;

    int adj[20][20];

    for(int loop = 0; loop < m; loop++){
        int a, b; cin >> a >> b;
        a--, b--;

        adj[a][b] = 1;
        adj[b][a] = 1;
    }

    int q; cin >> q;

    // 
    int visited[20];
    for(int loop = 0; loop < q; loop++){
        int x, y; cin >> x >> y;
        x--, y--;

        for(int i = 0; i < n; i++) visited[i] = 0;

        int cur = x, pre = -1;
        string res = "No";
        while(visited[cur] == 0){
            visited[cur] = 1;
            if(cur == y) res = "Yes";

            for(int i = 0; i < n; i++){
                if(adj[cur][i] == 1 && i != pre){
                    pre = cur;
                    cur = i;
                    break;
                }
            }
        }

        cout << res << endl;
    }

    return 0;
}
