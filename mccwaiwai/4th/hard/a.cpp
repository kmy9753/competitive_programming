#include <iostream>
#include <string>

using namespace std;

int main(void){
    string S;
    int n;
    cin >> S >> n;

    string res = S.substr(0, n - 1) + "ika" + S.substr(n - 1);

    cout << res << endl;

    return 0;
}
