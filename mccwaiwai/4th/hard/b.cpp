#include <iostream>

using namespace std;

int main(void){
    int n; cin >> n;

    int a[200][3], cnt[101][3];
    for(int i = 0; i < 101; i++){
        for(int j = 0; j < 3; j++){
            cnt[i][j] = 0;
        }
    }

    for(int i = 0; i < n; i++){
        for(int j = 0; j < 3; j++){
            int s; cin >> s;
            a[i][j] = s;
            cnt[s][j]++;
        }
    }

    for(int i = 0; i < n; i++){
        int res = 0;
        for(int j = 0; j < 3; j++){
            int s = a[i][j];
            if(cnt[s][j] == 1) res += s;
        }

        cout << res << endl;
    }

    return 0;
}
