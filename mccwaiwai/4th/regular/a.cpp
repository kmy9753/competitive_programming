#include <iostream>

using namespace std;

int main(void){
    int n; cin >> n;

    int pre = -1;
    string res = "Yes";

    for(int i = 0; i < n; i++){
        int cur; cin >> cur;
        if(cur <= pre) res = "No";
        pre = cur;
    }

    cout << res << endl;

    return 0;
}
