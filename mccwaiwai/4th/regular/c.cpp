#include <iostream>

using namespace std;

#define N 500

int adj[N][N], use[N];

int main(void){
    int n, m;
    while(1){
        cin >> n >> m;
        if(n == 0 && m == 0) break;

        for(int i = 0; i < n; i++) use[i] = 0;
        use[0] = 1;

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                adj[i][j] = 0;
            }
        }
        for(int i = 0; i < m; i++){
            int a, b; cin >> a >> b;
            a--, b--;

            adj[a][b] = adj[b][a] = 1;
        }

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){

                // 0 -- i
                if(adj[0][i] == 1){
                    use[i] = 1;

                    // 0 -- i -- j
                    if(adj[i][j] == 1){
                        use[j] = 1;
                    }
                }
            }
        }

        int res = 0;
        for(int i = 1; i < n; i++){
            if(use[i] == 1) res++;
        }

        cout << res << endl;
    }

    return 0;
}
