#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    queue<int> q_c, q_i;
    rep(i, n){
        int x; cin >> x;
        q_c.push(x);
    }
    rep(i, n){
        int x; cin >> x;
        q_i.push(x);
    }

    int res = 0;
    while(q_c.size()){
        res++;

        int x = q_c.front();
        q_c.pop();

        if(x == q_i.front()){
            q_i.pop();
            continue;
        }

        q_c.push(x);
    }

    cout << res << endl;

	return 0;
}
