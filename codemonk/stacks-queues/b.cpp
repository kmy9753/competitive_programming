#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;
int main(void){
    int q; cin >> q;

    stack<ll> stk;
    rep(loop, q){
        int a; cin >> a;

        if(a == 1){
            if(stk.size()){
                cout << stk.top() << endl;
                stk.pop();
            }
            else {
                puts("No Food");
            }
        }
        else {
            ll c; cin >> c;
            stk.push(c);
        }
    }

	return 0;
}
