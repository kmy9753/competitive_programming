#include <bits/stdc++.h>
typedef long long ll;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int N; cin >> N;

    rep(loooop, N){
        int n; cin >> n;

        vector<ll> a(n), b(n);
        for(auto & e : a) scanf("%lld", &e);
        for(auto & e : b) scanf("%lld", &e);

        sort(begin(b), end(b));

        int res = 0;
        rep(i, n){
            res = max(res, (int)(end(b) - lower_bound(begin(b), end(b), a[i])) - i - 1);
        }

        cout << res << endl;
    }

	return 0;
}
