#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, q; cin >> n >> q;
    vector<int> a(n);
    for(auto & e : a) cin >> e;
    sort(begin(a), end(a));

    rep(loop, q){
        int x; cin >> x;
        cout << (binary_search(begin(a), end(a), x) ? "YES":"NO") << endl;
    }

	return 0;
}
