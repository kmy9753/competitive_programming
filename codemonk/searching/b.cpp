#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int N; cin >> N;
    rep(looooop, N){
        int a, b, c, d; cin >> a >> b >> c >> d;

        int lb = -1, ub = 1e6;
        rep(loop, 100){
            int mid = (lb + ub) / 2;

            if(a * mid * mid + b * mid + c >= d){
                ub = mid;
            }
            else {
                lb = mid;
            }
        }

        cout << ub << endl;
    }

	return 0;
}
