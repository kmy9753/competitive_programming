#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, k; cin >> n >> k;

    vector<int> p(n);
    for(auto & e : p) scanf("%d", &e);
    sort(begin(p), end(p));

    int lb = -1, ub = 1e7;
    rep(loop, 100){
        int mid = (lb + ub) / 2;

        int kk = k, ls = p[0];
        bool ok = true;
        rep(i, n){
            if(p[i] - ls > 2 * mid){
                ls = p[i];
                kk--;
                if(kk <= 0){
                    ok = false;
                    break;
                }
            }
        }

        if(ok){
            ub = mid;
        }
        else {
            lb = mid;
        }
    }

    cout << ub << endl;

    return 0;
}
