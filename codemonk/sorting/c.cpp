#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int N; cin >> N;

    while(N--){
        int n, m; cin >> n >> m;

        //vector<int> a(n), b(n);
        //for(auto & e : a) cin >> e;
        //for(auto & e : b) cin >> e;
        //
        vector<int> a(n + m);
        for(auto & e : a) cin >> e;

        sort(begin(a), end(a), greater<int>());

        rep(i, n + m){
            cout << (i ? " ":"") << a[i];
        }
        puts("");
    }

	return 0;
}
