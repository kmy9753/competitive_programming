#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int N; cin >> N;

    while(N--){
        int n; cin >> n;
        vector<int> v(n), s;

        for(auto & e : v) scanf("%d", &e);
        s = v;
        sort(begin(s), end(s));
        
        rep(i, n){
            cout << (i ? " ":"") << lower_bound(begin(s), end(s), v[i]) - begin(s);
            s.erase(lower_bound(begin(s), end(s), v[i]));
        }
        puts("");
    }

	return 0;
}
