#include <bits/stdc++.h>

typedef long long ll;
#define int ll

#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int a, mod; cin >> a >> mod;
    string s; cin >> s;

    int res = 0;
    for(auto & e : s){
        if(e == '1'){
            (res += a) %= mod;
        }

        (a *= a) %= mod; 
    }

    cout << res << endl;

	return 0;
}
