#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;

    rep(loop, n){
        int x; scanf("%d", &x);

        int cnt = 0;
        for(int i = 1; i * i <= x; i++){
            if(x % i == 0){
                cnt++;
                if(i * i != x) cnt++;
            }
            if(cnt >= 4) break;
        }

        if(cnt < 4){
            puts("NO");
        }
        else {
            puts("YES");
        }
    }

	return 0;
}
