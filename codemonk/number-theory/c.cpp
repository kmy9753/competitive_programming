#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;
ll mod = 1e9 + 7;
int main(void){
    int n; cin >> n;
    vector<ll> a(n);
    for(auto & e : a) cin >> e;

    ll g = a[0];
    for(auto & e : a){
        g = __gcd(g, e);
    }

    ll f = 1;
    for(auto & e : a){
        (f *= e) %= mod;
    }

    ll ff = f;
    rep(loop, g - 1){
        (f *= ff) %= mod;
    }

    cout << f << endl;

	return 0;
}
