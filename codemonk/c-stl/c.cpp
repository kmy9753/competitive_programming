#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<pair<int, string>> ns(n);
    for(auto & e : ns) cin >> e.second >> e.first, e.first *= -1;

    sort(begin(ns), end(ns));

    for(auto & e : ns){
        cout << e.second << " " << -e.first << endl;
    }

	return 0;
}
