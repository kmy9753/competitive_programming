#include <bits/stdc++.h>
using ll = long long;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int T; cin >> T;
    rep(loop, T){
        int n, m; cin >> n >> m;
        //map<int, int> cnt;
        set<int> s;
        rep(_, n){
            int a; scanf("%d", &a);
            //cnt[a]++;
            s.insert(a);
        }

        rep(_, m){
            int a; cin >> a;
            //if(cnt[a] > 0){
            if(s.find(a) != end(s)){
                puts("YES");
                //cnt[a]--;
            }
            else {
                puts("NO");
            }
            s.insert(a);
        }
    }

	return 0;
}
