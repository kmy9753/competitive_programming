#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int T; cin >> T;
    rep(loop, T){
        int n; cin >> n;

        set<string> s;
        rep(_, n){
            string in; cin >> in;
            s.insert(in);
        }

        for(auto & e : s){
            cout << e << endl;
        }
    }

	return 0;
}
