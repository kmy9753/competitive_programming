#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;
int main(void){
    int N; cin >> N;
    rep(loop, N){
        ll n, t; cin >> n >> t;

        vector<ll> c(n);
        for(auto & e : c) cin >> e;

        vector<ll> sum(n + 1);
        range(i, 1, n + 1){
            sum[i] = sum[i - 1] + c[i - 1];
        }
        sum.push_back(1L << 50);

        string res = "NO";
        range(i, 1, n + 1){
            if(upper_bound(sum.begin() + i, sum.end(), sum[i - 1] + t)
              -lower_bound(sum.begin() + i, sum.end(), sum[i - 1] + t) > 0){
                res = "YES";
                break;
            }
        }

        cout << res << endl;
    }

	return 0;
}
