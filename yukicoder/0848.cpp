#include <bits/stdc++.h>
using ll = long long;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int n; cin >> n;

    cout << (n / 3 + n / 5) * 2 << endl;

	return 0;
}
