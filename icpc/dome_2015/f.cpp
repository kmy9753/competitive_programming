#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

typedef tuple<int, int, int> P;

int n; // the number of node
int k;

int prim (vector<vector<P>> G, int diff) {
    rep(v, n){
        for(auto & e : G[v]){
            int u, w, l;
            tie(w, u, l) = e;

            if(l == 1) continue;
            w += diff;
            e = P(w, u, l);
        }
    }

    set<int> used;
    priority_queue<P, vector<P>, greater<P> > q;
    for (int i = 0; i < n; i++) {
        used.insert(i);
    }
    int res = 0, a_cnt = 0;
    int v = 0;
    while (used.size()) {
        for (auto e : G[v])   q.push(e);
        while (q.size()) {
            int u, w, l;
            tie(w, u, l) = q.top();

            if(not (used.find(u) == used.end() or u == v)) break;
            q.pop();
        }

        if (q.empty()) break;

        used.erase(v);
        int u, w, l;
        tie(w, u, l) = q.top(); q.pop();
        v = u;

        res += w;
        a_cnt += (l == 0 ? 1 : 0);
    }

    cerr << a_cnt << endl;
    if(a_cnt != k) res = (a_cnt > k ? -1 : -2);
 
    return res;
}

int offset = 1000;

int main(void){
    for(int m; cin >> n >> m >> k, n;){
        cerr << endl;
        vector<vector<P>> edges(n);
        rep(loop, m){
            int u, v, w; char l; cin >> u >> v >> w >> l;
            u--, v--, w += offset;

            edges[u].push_back(P(w, v, l == 'A' ? 0:1));
        }

        int lb = -offset, ub = offset;
        int res = -1;
        rep(loop, 100){
            int mid = (lb + ub) / 2;

            int ret = prim(edges, mid);
            if(ret == -2){
                ub = mid;
            }
            else if(ret == -1){
                lb = mid;
            }
            else {
                res = ret - (offset * (n - 1)) - (mid * k);
                break;
            }
        }

        cout << res << endl;
    }

    return 0;
}
