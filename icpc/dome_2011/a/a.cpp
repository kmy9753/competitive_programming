#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
	const int N = (int)1e6;
	vector<bool> isP(N, true);
    isP[0] = isP[1] = false;
	for(int i = 2; i * i < N; i++) if(isP[i]) for(int j = i; i * j < N; j++) isP[i * j] = false;

	for(int n; cin >> n, n;){
		int res = 0;
		range(i, n + 1, 2 * n + 1){
			if(isP[i]) res++;
		}
		cout << res << endl;
	}

	return 0;
}
