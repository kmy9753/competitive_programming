#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

int w, h, c;
int res;

typedef vector<vector<int>> vvi;
typedef vector<int> vi;
typedef pair<int, int> pii;

#define F first
#define S second

vvi field;

vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

vvi used, used_cnt;

// kazoeru
int dfs_cnt(pii pos){
	int ret = 0;

	if(field[pos.F][pos.S] != field[0][0]) return 0;

	ret++;
	used_cnt[pos.F][pos.S] = true;
	
	rep(i, 4){
		pii np(pos.F + dy[i], pos.S + dx[i]);
		if(np.F < 0 || h <= np.F ||
	 	   np.S < 0 || w <= np.S || used_cnt[pos.F][pos.S]) continue;

		res += dfs_cnt(np);
	}

	return ret;
}

// nuru
void dfs(pii pos, int cl){
	used[pos.F][pos.S] = true;

	rep(i, 4){
		pii np(pos.F + dy[i], pos.S + dx[i]);
		if(np.F < 0 || h <= np.F ||
	 	   np.S < 0 || w <= np.S || used[pos.F][pos.S]) continue;
		if(field[np.F][np.S] != cl) continue;

		field[np.F][np.S] = field[0][0];

		dfs(np, cl);
	}
}

void rec(int n){
	if(n == 5){
		rep(y, h){
			rep(x, w) cerr << field[y][x] << " ";
			cerr << endl;
		}
			cerr << endl;

		if(field[0][0] == c){
			used_cnt = vvi(h, vi(w));
			res = max(res, dfs_cnt(pii(0, 0)));
		}
		return;
	}

	range(i, 1, 7){
		if(field[0][0] == i) continue;

		vvi field_org = field;
		field[0][0] = i;

		used = vvi(h, vi(w));
		dfs(pii(0, 0), field_org[0][0]);
		rec(n + 1);

		field = field_org;
	}
}

int main(void){
	for(; cin >> h >> w >> c, h;){
		cout << h << w << c << endl;
		res = 0;

		field = vvi(h, vi(w));
		rep(y, h) rep(x, w) cin >> field[y][x];

		rec(0);

		cout << res << endl;
	}

	return 0;
}
