#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
	for(char c; c = getchar(), c != '.';){
		string res = "yes";
		stack<char> stk;
		do {
			if(c == '(' || c == '[') stk.push(c);

			if(c == ')'){
                if(stk.empty() || stk.top() != '(') res = "no";
                else stk.pop();
            }
			if(c == ']'){
                if(stk.empty() || stk.top() != '[') res = "no";
                else stk.pop();
            }
		} while(c = getchar(), c != '.');
        if(stk.size()) res = "no";

        cout << res << endl;
        c = getchar();
	}

	return 0;
}
