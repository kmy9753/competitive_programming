curdir=`basename $(pwd)`
num=$1

rm -f a.out
g++ -o $curdir $curdir.cpp -std=c++0x -O2 -Wall -Wshadow -D_GLIBCXX_DEBUG
if test $? -e 0; then
	ln -s $curdir a.out
	./$curdir < sample$1.in > sample$1.out
	diff sample$1.out sample$1.ans
	if test $? -e 0; then
		echo "sample OK"
	fi
fi
