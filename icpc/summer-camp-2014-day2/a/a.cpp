#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
double eps = 1e-9;
int main(void){
    double d; cin >> d;

    double res = (int)d + 1.0;

    res = max(res, d / sqrt(2.0) * 2.0);

    printf("%.12f\n", res);

	return 0;
}
