#include <bits/stdc++.h>

#define pb push_back
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)

using namespace std;

typedef vector<string> vs;

const string kBoin = "aiueo";

int main(void){
    for(int n; cin >> n, n;){
        vs names(n);
        for(auto && name : names) cin >> name;

        for(auto && name : names){
            string code;
            code.pb(name[0]);

            bool ok = false;
            for(auto && c : name){
                if(ok) code.pb(c), ok = false;

                rep(i, 5) if(kBoin[i] == c) ok = true;
            }

            name = code;
        }

        int res = -1;
        rep(k, 51){
            vs codes(n);
            rep(i, n) codes[i] = names[i].substr(0, k);

            set<string> s;
            bool ok = true;
            for(auto && code : codes){
                if(!s.insert(code).second){
                    ok = false;
                    break;
                }
            }

            if(ok){
                res = k;
                break;
            }
        }

        cout << res << endl;
    }
}
