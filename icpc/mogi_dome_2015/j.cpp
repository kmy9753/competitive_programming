#include <bits/stdc++.h>
#define int long long
#define rep(i,n) for(int i=0; (i)<(n); ++(i))
using namespace std;

using ll=long long;
using vi=vector<int>;

const ll inf = 1LL << 50;

signed main(void) {
    int n;
    cin >> n;
    
    vi num(n, 2);
    vi used(n);

    vi d(n-1), g(n);
    for(auto& e : d) cin >> e;
    for(auto& e : g) cin >> e;

    using Elem = tuple<ll, int, int, int>;
    priority_queue<Elem, vector<Elem>, greater<Elem>> q;
    rep(i, n){
        q.push(Elem(g[i], i, -1, i));
    }

    ll res = 0; int cnt = 0;
    while(q.size()){
        ll c; int v, pv, r; tie(c, v, pv, r) = q.top(); q.pop();
        if(num[r] == 0) continue;

        if(not used[v]){
            res += c;
            num[r]--;
            used[v] = true;
            cnt++;
            if(cnt == n) break;
            if(num[r] >= 1){
                q.push(Elem(g[r], r, -1, r));
            }
        }
        else {
            for(auto& e : {v-1, v}){
                int nv = e; if(e == v) nv++;
                if(nv < 0 or n <= nv) continue;
                if(nv == pv) continue;
                q.push(Elem(c + d[e], nv, v, r));
            }
        }
    }

    cout << res << endl;

    return 0;
}
