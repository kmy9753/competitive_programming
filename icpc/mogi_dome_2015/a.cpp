#include <bits/stdc++.h>

#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)

using namespace std;

int main(void){
    for(int d, e; cin >> d >> e, d;){
        double res = 1e8;

        rep(x, d + 1){
            int y = d - x;

            res = min(res, abs(sqrt(x * x + y * y) - e));
        }

        printf("%f\n", res):
    }
}
