#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define pb push_back
#define mp make_pair
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define dump(x)  cerr << #x << " = " << (x) << endl;

const double eps = 1e-10;
const ll INF =1LL << 62;
const int inf =1 << 24;

