#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int total = 0;
using pii = pair<int, int>;

map<pii, int> v2i;
void add(int v, int t){
    if(v2i.find(pii(v, t)) != end(v2i)) return;
    v2i[pii(v, t)] = total++;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;
    int S, G; cin >> S >> G;
    S--, G--;

    using In = tuple<int, int, int, int>;
    vector<In> in(m);
    for(auto& e : in){
        int u, v, t, c; cin >> u >> v >> t >> c;
        u--, v--;
        e = In(u, v, t, c);
    }

    add(S, 0);
    for(auto& e : in){
        int u, v, t, c; tie(u, v, t, c) = e;

        add(u, t);
        add(v, t + c);
    }

    using Edge = tuple<int, int>;
    vector<vector<Edge>> graph(total);

    for(auto& e : in){
        int u, v, t, c; tie(u, v, t, c) = e;

        int a = v2i[pii(u, t)];
        int b = v2i[pii(v, t + c)];
        graph[a].push_back(Edge(0, b));
    }

    vector<vector<pii>> timeser(n);
    for(auto& itr : v2i){
        int v, t, i;
        v = itr.first.first;
        t = itr.first.second;
        i = itr.second;

        timeser[v].push_back(pii(t, i));
    }

    rep(i, n){
        sort(_all(timeser[i]));

        rep(j, (int)timeser[i].size() - 1){
            int diff = timeser[i][j + 1].first - timeser[i][j].first;
            int a = timeser[i][j    ].second;
            int b = timeser[i][j + 1].second;

            graph[a].push_back(Edge(diff, b));
        }
    
    }
    
    using State = tuple<int, int>;
    vi dist(total, 1 << 20);
    priority_queue<State, vector<State>, greater<State>> q;
    int sv = v2i[pii(0, 0)];
    q.push(State(0, sv));
    dist[sv] = 0;

    while(q.size()){
        int v, c; tie(c, v) = q.top(); q.pop();

        for(auto& e : graph[v]){
            int nv, dc; tie(dc, nv) = e;

            int nc = c + dc;
            if(chmin(dist[nv], nc)){
                q.push(State(nc, nv));
            }
        }
    }

    int res = 1 << 20;
    for(auto& itr : timeser[G]){
        int idx = itr.second;
        chmin(res, dist[idx]);
    }

    cout << res << endl;

    return 0;
}
