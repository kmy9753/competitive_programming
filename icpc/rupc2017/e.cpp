#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 2001;
int dph[N][N][2][2];
int dpv[N][N][2][2];

int n, K;

void solve(int dp[N][N][2][2], vi& ops){

    rep(i, ops.size()){
        int dir = ops[i];

        rep(j, K + 1){
            rep(k, 2){
                rep(l, 2){
                    int& cur = dp[i][j][k][l];
                    if(cur == -1) continue;
                    
                    rep(nk, 2){
                        int ndir = dir;
                        if(nk == 1) ndir *= -1;
                        if(l == 1) ndir *= -1;

                        int nj = j;
                        if(nk != k){
                            if(j == 0) continue;
                            nj = j - 1;
                        }

                        int nl = l;
                        if(ndir == -1 and ((l == 1 and cur == 1) or (l == 0 and cur == 0))){
                            nl ^= 1;
                        }

                        chmax(dp[i + 1][nj][nk][nl], abs(cur + ndir));
                    }
                }
            }
        }
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    string in; cin >> in;
    n = in.size();
    cin >> K;

    vi hor, ver;
    rep(i, n){
        switch(in[i]){
            case 'U': { ver.push_back(-1); break; }
            case 'D': { ver.push_back( 1); break; }
            case 'L': { hor.push_back(-1); break; }
            case 'R': { hor.push_back( 1); break; }
        }
    }

    rep(i, N) rep(j, N) rep(k, 2) rep(l, 2) dph[i][j][k][l] = -1;
    rep(i, N) rep(j, N) rep(k, 2) rep(l, 2) dpv[i][j][k][l] = -1;
    dph[0][K][0][0] = 0;
    dpv[0][K][0][0] = 0;

    solve(dph, hor);
    solve(dpv, ver);

    int res = 0;
    rep(i, K + 1){
        rep(j, K + 1 - i){
            rep(l1, 2) rep(l2, 2) rep(k1, 2) rep(k2, 2)
                chmax(res, dph[hor.size()][K - i][l1][k1] + dpv[ver.size()][K - j][l2][k2]);
        }
    }
    cout << res << endl;

    return 0;
}
