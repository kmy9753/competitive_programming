#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 1e5 + 1;
int dist[10][N];

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;
    int R, Q; cin >> R >> Q;

    using Edge = tuple<int, int>;
    vector<vector<Edge>> graph(n);

    rep(loop, m){
        int a, b, c; cin >> a >> b >> c;
        a--, b--;
        graph[b].push_back(Edge(c, a));
    }

    using State = tuple<int, int>;
    rep(s, R){
        rep(i, n){
            dist[s][i] = 1 << 20;
        }
        priority_queue<State, vector<State>, greater<State>> q;

        rep(i, n){
            const int idx = i + 1;

            if(idx % R == s){
                dist[s][i] = 0;
                q.push(State(0, i));
            }
        }

        while(q.size()){
            int v, c; tie(c, v) = q.top(); q.pop();

            for(auto& e : graph[v]){
                int nv, dc; tie(dc, nv) = e;

                int nc = c + dc;
                if(chmin(dist[s][nv], nc)){
                    q.push(State(nc, nv));
                }
            }
        }
    }

    ll res = 0LL;
    rep(loop, Q){
        int x, y, z; cin >> x >> y >> z;
        x--, y--;

        int cmin = 1 << 20;
        rep(i, R){
            // cerr << dist[i][x] << " " << dist[i][y] << endl;
            chmin(cmin, dist[i][x] + dist[i][y]);
        }

        // cerr << z << " " << cmin << endl;
        res += max(0, z - cmin);
    }

    cout << res << endl;

    return 0;
}
