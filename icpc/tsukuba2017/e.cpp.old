#include <bits/stdc++.h>
#define int long long 

#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)

#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)

#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))

// #define DEBUG

#ifdef DEBUG
#define dump(...) fprintf(stderr, __VA_ARGS__)
#else
#define dump(...)
#endif

template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}

using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;

const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};


ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}

random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

template <int depth> struct Segment_tree {
    const static int h = depth;
    const static int n = 1 << h;

    using T = long long;
    T data[2 * n], lazy[2 * n];
    const T out = (1LL << 31) - 1;

    inline T vmerge(T l, T r) {return min(l, r);}

    void init() {
        fill_n(data, 2 * n, out);
        fill_n(lazy, 2 * n, out);
    }

    // lazy_evaluation
    void apply(T v, int p, int l, int r) {
        if(data[p] == out) data[p] = 0;
        if(lazy[p] == out) lazy[p] = 0;
        data[p] += v;
        lazy[p] += v;
    }

    void push(int p, int l, int r) {
        const int m = (l + r) / 2;
        if (lazy[p] == out) return;
        apply(lazy[p], 2 * p + 1, l, m);
        apply(lazy[p], 2 * p + 2, m, r);
        lazy[p] = out;
    }

    void range_update(int a, int b, T x, int k = 0, int l = 0, int r = n) {
        if (r <= a or b <= l) return;
        if (a <= l and r <= b) return apply(x, k, l, r);
        push(k, l, r);
        const int m = (l + r) / 2;
        range_update(a, b, x, k * 2 + 1, l, m);
        range_update(a, b, x, k * 2 + 2, m, r);
        data[k] = vmerge(data[k * 2 + 1], data[k * 2 + 2]);
    }

    T range_query(int a, int b, int k = 0, int l = 0, int r = n) {
        if (r <= a or b <= l) return out;
        if (a <= l and r <= b) return data[k];
        push(k, l, r);
        const int m = (l + r) / 2;
        T vl = range_query(a, b, k * 2 + 1, l, m);
        T vr = range_query(a, b, k * 2 + 2, m, r);
        return vmerge(vl, vr);
    }
};

Segment_tree<20> seg[2];

signed main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, K; cin >> n >> K;
    string S, T; cin >> S >> T;
    S = "0" + S;
    T = "0" + T;
    n++;

    vi f1(n), f2(n);
    rep(i, n){
        if(S[i] == 'B') f1[i] = 1;
        if(T[i] == 'B') f2[i] = 1;
    }

    vi sum[2]; sum[0] = sum[1] = vi(n);
    int cnt[2] = {0, 0};
    int pi[2] = {-1, -1};
    rep(i, n){
        int c = f2[i];
        if(pi[c] != -1){
            sum[c][i] = sum[c][pi[c]] + (cnt[c^1] + K - 1) / K;
        }
        cnt[c^1] = 0;
        cnt[c]++;
        pi[c] = i;
    }
    auto calc = [&](int l, int r){
        int c = f2[r];
        return sum[c][r] - sum[c][l];
    };

    rep(i, 2) seg[i].init();
    rep(i, n){
        if(f1[i] == f2[i]){
            rep(i, 2){
                seg[i].range_update(i, i + 1, inf);
            }
        }
        else {
            int c = f2[i];
            seg[c^1].range_update(i, i + 1, inf);
            seg[c].range_update(i, i + 1, 0);
        }
    }
    // rep(i, n) rep(j, 2) seg[j].range_update(0, i+1, 0);

    vi dp(n, inf);
    dp[0] = 0;

    pi[0] = 0, pi[1] = -1;
    rep(i, 1, n){
        int c = f2[i];
        if(pi[c] != -1) seg[c].range_update(0, i, calc(pi[c], i));
        if(f1[i] == f2[i]){
            dp[i] = dp[i - 1];
        }
        else {
            dp[i] = min(dp[i - 1], seg[c].range_query(max(0LL, i - K + 1LL), i)) + 1;
        }
        seg[c].range_update(i, i + 1, dp[i-1]);
        pi[c] = i;
    }
    rep(i, n){
        cerr << dp[i] << " ";
    }
    cerr << endl;
    rep(i, n){
        cerr << seg[0].range_query(i, i + 1) << " ";
    }
    cerr << endl;

    cout << dp.back() << endl;

    return 0;
}

