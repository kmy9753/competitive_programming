#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 58;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int Soso  = 1;
const int Happy = 2;
const int Sad   = 3;

using Edge = tuple<int, int, int>;

vll dijkstra(vector<vector<Edge>>& G, int sv){
    int n = G.size();
    vll min_dist(n, inf);
    min_dist[sv] = 0;

    using Elem = tuple<ll, int>;
    priority_queue<Elem, vector<Elem>, greater<Elem>> q;
    q.push(Elem(0, sv));

    while(q.size()){
        ll c; int v; tie(c, v) = q.top(); q.pop();
        if(min_dist[v] != c) continue;

        for(auto& e : G[v]){
            int nv, dc, ei; tie(nv, dc, ei) = e;
            ll nc = c + dc;
            if(chmin(min_dist[nv], nc)){
                q.push(Elem(nc, nv));
            }
        }
    }
    return min_dist;
}

int depthMax;
vi etype;
vi depth;
vi used;
vi path;
vi imo;
vi oute;

bool dfs(vector<vector<Edge>>& G, int v, int tv, int d){
    if(v == tv){
        path.emplace_back(v);
        depth[v] = d;
        return true;
    }
    for(auto& e : G[v]){
        int nv, ei; tie(nv, ignore, ei) = e;
        if(dfs(G, nv, tv, d + 1)){
            path.emplace_back(v);
            depth[v] = d;
            used[ei] = true;
            oute.emplace_back(ei);
            return true;
        }
    }
    return false;
}

vi visited;
void dfs2(vector<vector<Edge>>& G, int v, int d){
    if(depth[v] > d){
        imo[d]++;
        imo[depth[v]]--;
        return;
    }
    if(depth[v] == -1) visited[v] = true;
    for(auto& e : G[v]){
        int nv, ei; tie(nv, ignore, ei) = e;
        if(visited[nv]) continue;
        if(used[ei]){
            assert(oute[d] == ei);
            continue;
        }
        dfs2(G, nv, d);
    }

    if(depth[v] == d and d < depthMax - 1){
        dfs2(G, path[d + 1], d + 1);
    }
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;
    vector<vector<Edge>> G(n), Gr(n);
    vi as(m), bs(m), cs(m);
    rep(i, m){
        int a, b, c; cin >> a >> b >> c;
        a--, b--;
        G [a].emplace_back(Edge(b, c, i));
        Gr[b].emplace_back(Edge(a, c, i));
        as[i] = a, bs[i] = b, cs[i] = c;
    }

    int sv = 0, tv = 1;
    auto min_dist     = dijkstra(G, sv);
    auto min_dist_rev = dijkstra(Gr, tv);

    etype = vi(m, Soso);

    vector<vector<Edge>> Gs(n);
    rep(i, m){
        int a = as[i], b = bs[i], c = cs[i];
        if(min_dist[a] + c == min_dist[b]){
            Gs[a].emplace_back(Edge(b, c, i));
        }
    }
    depth = vi(n, -1);
    used  = vi(m);
    dfs(Gs, sv, tv, 0);
    reverse(_all(path));
    reverse(_all(oute));
    depthMax = path.size();

    // for(auto& e : path) cerr << e+1 << " "; cerr << endl;
    // for(auto& e : oute) cerr << e+1 << " "; cerr << endl;

    imo = vi(depthMax);
    visited = vi(n);
    dfs2(Gs, sv, 0);
    rep(i, 1, depthMax) imo[i] += imo[i - 1];
    rep(i, depthMax - 1){
        if(imo[i] == 0){
            etype[oute[i]] = Sad;
        }
    }

    rep(i, m){
        int a = as[i], b = bs[i], c = cs[i];
        if(min_dist[b] + min_dist_rev[a] + c < min_dist[tv]){
            etype[i] = Happy;
        }
    }

    rep(i, m){
        string res = "SOSO";
        if(etype[i] == Happy){
            res = "HAPPY";
        }
        else if(etype[i] == Sad){
            res = "SAD";
        }
        cout << res << endl;
    }

    return 0;
}
