#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
//const int A = 1 << 20;

int main(void){
    /*
    MEMSET(t, 0);
    for(double i = 1; i < A; i++){
      for(double j = 0; j < (i * i - 1) / 2; j++){
        if(t[i][j]) continue;
        if((int)sqrt(i * i + j * j) - sqrt(i * i + j * j) == 0){ 
            t[i][j] = 1; 
            for(double I = 2 * i, double J = 2 * j; I < A || J < (A * A - 1) / 2;
                    I += i, J += j){
              t[I][J] = 1; 
            } 
        }
      } 
    }
    */
  /*  long double a, b;
    while(1){
      cin >> a;
      if(!a) break;
      int C = 0;
      for(b = a + 1; b <= (a * a - 1) / 2; b++){
        if((double)(int)sqrt(a * a + b * b) == (double)sqrt(a * a + b * b)){
          C++;
        }   
        cout << b << endl;
      } 
      cout << C << endl;
    }*/
    ll A;
    while(cin >> A,A){

         ll CA=A*A;
         int ans=0;
         vector<int> list;
         for(int i=1;i<=A;i++)
            if(CA%i==0)
                list.pb(i);
         each(list,it){
           // cout << *it << endl;
            ll check=CA/( *it)-*it;
           // cout << check << endl;
            if(check&&(check/2)*2==check&&(check/2)>A)
              /* cout <<"lk"<< endl, */ans++;
         }  
        cout << ans << endl;
    }
    return 0;
}
