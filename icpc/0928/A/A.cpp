#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 10001;
const double EPS = 1e-8;

int dp[N];

int main(void){
    int n;
    for(double _m; cin >> n >> _m, n;){
        int m = (int)(_m * 100. + EPS);
        MEMSET(dp, -1);
        dp[0] = 0;

        while(n --){
            int c; double _p; cin >> c >> _p;
            int p = (int)(_p * 100. + EPS);
            
            for(int i = p; i <= m; i ++)
                if(~dp[i - p]) dp[i] = max(dp[i - p] + c, dp[i]);
        }

        //cout << (~dp[m] ? dp[m]:0) << endl;
        cout << *max_element(dp, dp + m + 1) << endl;
    }

    return 0;
}
