#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int M = 1000000007;

long long int mod(int, int);

int main(void){
    int T; cin >> T;
    for(int c = 0; c < T; c++){
      int C; cin >> C;
      vector<pii> l;
      for(int i = 0; i < C; i++){
        int p, a; cin >> p >> a;
        l.pb(mp(p, a)); 
      } 
      long long int N = 1;
      for(int i = 0; i < l.size(); i++){
        N *= mod(l[i].first, l[i].second);  
      }
      N %= M;
      ll S0 = 1;
      ll S1 = 1;
      ll S2 = 0;
      for(int i = 0; i < l.size(); i++){
        ll s0 = 0;
        for(int j = 0; j < l[i].second; j++){
          s0 += mod(l[i].first, l[i].second); 
        }
        S0 *= s0; 
      }
      for(int i = 0; i < l.size(); i++){
        S1 *= (l[i].second + 1);
        S1 %= M; 
      }
      S1 *= N;
      bool issqn = S1 % 2;
      ll sqN = 1;
      if(issqn){
        for(int i = 0; i < l.size(); i++){
          sqN *= mod(l[i].first, (l[i].second / 2)); 
        } 
      }
      S2 = S0 - (1 + (N & M)) + (issqn * sqN);
      ll res = (S0 + S1 + S2) % M;
      cout << "Case " << c + 1 << ": " << res << endl;
    }
    return 0;
}

long long int mod(int b, int p){
    int i = 0;
    while(pow((double)b, (double)i) < (double)M && i <= p) i++;
    int nb = (int)pow((double)b, (double)i) % M;
    int np = p;
    if(i)
        np = p / i;
    int d = p % i;
    if(p == 0){
        return 1;
    }else{
        return mod(nb, np) * (long long int)pow((double)b, (double)d);
    }
}
