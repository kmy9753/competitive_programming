#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
 
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int M = 1000000007;

long long int mod(int, int);

int main(void){
    int T; cin >> T;
    for(int i = 0; i < T; i++){
      int C; cin >> C;
      vector<pii> list;
      for(int j = 0; j < C; j++){
        int p, a; cin >> p >> a;
        list.pb(mp(p, a)); 
      } 
      long long int N = 1;
      long long int sqn = 1;
      for(int j = 0; j < list.size(); j++){
        N *= mod(list[j].first, list[j].second); 
      }
      N %= M;
      int sq = 1;
      for(int j = 0; j < list.size(); j++){
        sq *= ((list[j].second + 1) % 2 + 2); 
      }
      bool issq = sq % 2;
      long long int S0 = 1;
      long long int S1 = 1;
      long long int S2 = 0;
      for(int j = 0; j < list.size(); j++){
        long long int s0 = 0;
        for(int k = 0; k <= list[j].second; k++){
          s0 += mod(list[j].first, k); 
        }
        S0 *= s0;
      }
      for(int j = 0; j < list.size(); j++){
        S1 *= (list[j].second + 1);
      }
      S1 *= N; 
      if(issq){
        for(int j = 0; j < list.size(); j++)
           sqn *= mod(list[j].first, (list[j].second / 2)); 
      }
      sqn %= M;
      S2 = S0 - (1 + N) + (issq * sqn); 
      int res = (S0 + S1 + S2) % M;
      cout << "Case " << i << ": ";
      cout << res << endl;
    } 
    return 0;
}

long long int mod(int b, int p){
    int i = 0;
    while(pow((double)b, (double)i) < (double)M && i <= p) i++;
    int nb = (int)pow((double)b, (double)i) % M;
    int np = p;
    if(i)
        np = p / i;
    int d = p % i;
    if(p == 0){
        return 1;
    }else{
        return mod(nb, np) * (long long int)pow((double)b, (double)d);
    }
}
