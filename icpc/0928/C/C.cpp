#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <complex>
#include <ctime>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))

#define X real()
#define Y imag()
#define PI  acos(-1)
#define EPS 1e-8
using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;
typedef complex<double> P;
typedef double D;
typedef complex<D> V;
typedef pair<P,P> L;
const int INF = 1 << 24;

D EP(V a,V b){
    return (a.X*b.Y-a.Y*b.X);
}

bool SSC(L l1,L l2){
    V v1,v2;
    v1=l1.second-l1.first;
    v2=l2.second-l2.first;
    if(abs(EP(v1,v2)<EPS))
        return false;
    P p1,p2,p3,p4;
    if(l1.first.X>l1.second.X)
     swap(l1.first,l1.second);
    if(l2.first.X>l2.second.X)
     swap(l2.first,l2.second);
    p1=l1.first,p2=l1.second,p3=l2.first,p4=l2.second;
    D tmp=(EP(p4-p3,p3-p1)/EP(p4-p3,p2-p1));
    v1=(tmp,tmp);
    P ans=p1+(p2-p1)*v1;
    if(p1.X<ans.X&&ans.X<p2.X&&p3.X<ans.X&&ans.X<p4.X)
        return true;
    return false;
 }



int main(void){
    clock_t start=time(NULL);
    P points[21];
    L lines[21];
    int n;
    while(cin >> n,n){
        points[0]=P(0,0);
        double crad=PI/2;
        bool danger=false;
        bool check=false;
        D deg,r;
        rep(i,n){
            cin >> deg >> r;
            if(check)
                continue;
            deg=deg/180.0*PI;
            crad+=deg;
            points[i+1]=(polar(r,crad))+points[i];
            lines[i]=mp(points[i],points[i+1]);
            rep(j,i-1){
                if(SSC(lines[i],lines[j]))
                    danger=true;
            }
            if(danger){
                cout << i+1 << endl,check=true;       
            }
        }
    if(!danger)
        cout<<"SAFE"<< endl;
      for(int i=0;i<=n;i++)
        cout << points[i].X << " " << points[i].Y << endl;
    //cout << time(NULL)-start << endl;
    }    
    return 0;
}
