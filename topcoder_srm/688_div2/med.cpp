#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using vi = vector<int>;
 
bool fliped[101];
int n;

class ParenthesesDiv2Medium {
public: 
    string s;

    bool dfs(int idx, int cnt, int d){
        if(idx == n){
            if(d == 0){
                return true;
            }
            return false;
        }

        int dd = (s[idx] == '(' ? 1:-1);
        int nd = d + dd;
        if(nd >= 0 and dfs(idx + 1, cnt, nd)){
            return true;
        }

        nd = d - dd;
        fliped[idx] = true;
        if(nd >= 0 and cnt < n / 2 + 1 and dfs(idx + 1, cnt + 1, nd)){
            return true;
        }
        fliped[idx] = false;

        return false;
    }

    vector <int> correct(string in){
        n = in.size();
        s = in;

        vi res;
        dfs(0, 0, 0);
        rep(i, 100){
            if(fliped[i]) res.push_back(i);
        }

        return res;
    }
};

int main(void){

    return 0;
}
