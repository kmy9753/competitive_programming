#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using vi = vector<int>;


class RGBTree {
    public:
        vector<string> G;
        int n;
        int memo[1 << 13][5][5];
        int sum_r[1 << 13], sum_g[1 << 13], sum_b[1 << 13];

        int rec(int sup, int r, int g){
            int & ret = memo[sup][r][g];
            if(ret != -1) return ret;

            int npop = __builtin_popcount(sup);
            int b = (npop - 1) - r - g;

            if(npop == 1){
                return ret = true;
            }

            ret = false;
            for(int sub = 1; sub != (1 << n); sub <<= 1){
                if((sup & sub) == 0) continue;
                int sub2 = sup ^ sub;

                bool rok = false, gok = false, bok = false;
                if(sum_r[sup] - sum_r[sub] - sum_r[sub2] >= 1) rok = true;
                if(sum_g[sup] - sum_g[sub] - sum_g[sub2] >= 1) gok = true;
                if(sum_b[sup] - sum_b[sub] - sum_b[sub2] >= 1) bok = true;

                int subnpop = __builtin_popcount(sub);
                rep(nr, r + 1){
                    rep(ng, g + 1){
                        int nb = subnpop - nr - ng;
                        if(nb < 0 or b < nb) continue;

                        if(rok and nr >= 1) ret |= (rec(sub, nr - 1, ng) & rec(sub2, r - nr, g - ng));
                        if(gok and ng >= 1) ret |= (rec(sub, nr, ng - 1) & rec(sub2, r - nr, g - ng));
                        if(bok and nb >= 1) ret |= (rec(sub, nr, ng)     & rec(sub2, r - nr, g - ng));
                    }
                }

                if(ret == true) break;
            }

            return ret;
        }

        string exist(vector <string> _G){
            G = _G;
            n = G.size();
            rep(i, 1 << 13) rep(j, 5) rep(k, 5) memo[i][j][k] = -1;
            rep(i, 1 << 13) sum_r[i] = sum_g[i] = sum_b[i] = 0;

            rep(sup, 1 << 13){
                rep(i, n){
                    if(((sup >> i) & 1) == false) continue;
                    rep(j, i + 1, n){
                        if(((sup >> j) & 1) == false) continue;

                        if(G[i][j] == 'R') sum_r[sup]++;
                        if(G[i][j] == 'G') sum_g[sup]++;
                        if(G[i][j] == 'B') sum_b[sup]++;
                    }
                }
            }

            return (rec((1 << n) - 1, n / 3, n / 3) ? "Exist":"Does not exist");
        }
};

int main(void){
    vector<string> G;
    string in;
    while(cin >> in){
        G.push_back(in);
    }

    RGBTree ins;

    cout << ins.exist(G) << endl;

    return 0;
}
