#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using vi = vector<int>;

using State = tuple<int, int, int>;

class DoubleWeights {
    public:
        int memo[200][200][20];

        int minimalCost(vector<string> weight1, vector<string> weight2){
            rep(i, 200) rep(j, 200) rep(k, 20) memo[i][j][k] = -1;

            int n = weight1.size();

            queue<State> q;
            q.push(State(0, 0, 0));

            int res = inf;
            while(q.size()){
                int c1, c2, v; tie(c1, c2, v) = q.front();
                q.pop();

                if(memo[c1][c2][v] != -1) continue;
                memo[c1][c2][v] = 1;

                if(v == 1){
                    chmin(res, c1 * c2);
                }

                rep(nv, n){
                    if(weight1[v][nv] == '.') continue;

                    int nc1 = c1 + (weight1[v][nv] - '0');
                    int nc2 = c2 + (weight2[v][nv] - '0');

                    if(memo[nc1][nc2][nv] != -1) continue;
                    q.push(State(nc1, nc2, nv));
                }
            }

            if(res == inf) res = -1;
            return res;
        }
};

int main(void){
    return 0;
}
