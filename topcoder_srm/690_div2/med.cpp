#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using vi = vector<int>;
 
class GerrymanderEasy {
public: 
    double getmax(vector <int> A, vector <int> B, int K){
        int n = A.size();

        double ret = 0.0;
        rep(i, n){
            int p = 0, q = 0;
            rep(j, i, n){
                p += B[j], q += A[j];

                if(j - i + 1 >= K){
                    chmax(ret, 1.0 * p / q);
                }
            }
        }

        return ret;
    }
};

int main(void){

    return 0;
}
