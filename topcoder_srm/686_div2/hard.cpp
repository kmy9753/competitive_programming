#include <bits/stdc++.h>

#define int long long 
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using vi = vector<int>;

const int N = 101;
int dp[N][N][N][2][2];

//class BracketSequenceDiv2 {
//    public:
        int count(string s){
            int n = s.size();

            dp[0][0][0][1][1] = 1;

            rep(i, n){
                rep(li, N - 1){
                    rep(ri, N - 1){
                        rep(lu, 2){
                            rep(ru, 2){
                                if(dp[i][li][ri][lu][ru] == 0) continue;
                                
                                if(s[i] == '('){
                                    dp[i + 1][li][ri][0][ru] = ADD(dp[i + 1][li][ri][0][ru], dp[i][li][ri][lu][ru], mod);

                                    if(lu == 0) continue;
                                    dp[i + 1][li + 1][ri][lu][1] = ADD(dp[i + 1][li + 1][ri][lu][1], dp[i][li][ri][lu][ru], mod);
                                }
                                else {
                                    dp[i + 1][li][ri][lu][0] = ADD(dp[i + 1][li][ri][lu][0], dp[i][li][ri][lu][ru], mod);

                                    if(ru == 0 or li <= ri) continue;
                                    dp[i + 1][li][ri + 1][1][ru] = ADD(dp[i + 1][li][ri + 1][1][ru], dp[i][li][ri][lu][ru], mod);
                                }
                            }
                        }
                    }
                }
            }

            rep(i, n + 1){
                rep(j, n){
                    rep(k, n){
                        cerr << "i,j,k:" << i << " " << j << " " << k << ": ";
                        rep(lu, 2){
                            rep(ru, 2){
                                cerr << dp[i][j][k][lu][ru] << " ";
                            }
                        }
                        cerr << endl;
                    }
                }
                        cerr << endl;
            }

            int res = 0;
            rep(i, 1, N){
                rep(lu, 2){
                    rep(ru, 2){
                        res = ADD(res, dp[n][i][i][lu][ru], mod);
                    }
                }
            }

            return res;
        }
// };

signed main(void){
    string s;
    cin >> s;

//    BracketSequenceDiv2 ins;
    cout << count(s) << endl;

    return 0;
}
