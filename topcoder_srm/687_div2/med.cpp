#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using vi = vector<int>;
 
string T = "quack";

class Quacking {
public:
    int quack(string s){
        int cur = 0;
        int res = 0;
        vi idx;

        for(auto & c : s){
            if(c == 'q'){
                idx.push_back(1);
                cur++;
                chmax(res, cur);
                continue;
            }
            bool ok = false;
            rep(i, cur){
                if(T[idx[i]] == c){
                    idx[i]++;

                    if(c == 'k'){
                        idx.erase(begin(idx) + i, begin(idx) + i + 1);
                        cur--;
                    }

                    ok = true;
                    break;
                }
            }

            if(not ok){
                return -1;
            }

            chmax(res, cur);
        }

        if(idx.size() != 0){
            return -1;
        }
        else {
            return res;
        }
    }
};

int main(void){
    string s; cin >> s;

    Quacking ins;
    cout << ins.quack(s) << endl;

    return 0;
}
