#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using vi = vector<int>;

using R = long double;

const R eps = 1e-13;
const int N = 2 * 1e6;
 
inline R power(R a, int n){
    R b = 1.0;

    while(n){
        if(n & 1) b *= a;
        a *= a; 
        n >>= 1;
    }

    return b;
}

class Queueing {
public:
    vector<R> calc(int l, int p){
        vector<R> prob(N);

        R pl = pow(p, l);
        R C = 1;
        rep(t, l, N){
            prob[t] = C * (1.0 / pl);
            if(prob[t] < eps) prob[t] = 0.0;
            C *= (1.0 - 1.0 / p) * t / (t - l + 1.0);
        }

        return prob;
    }

    void calc_sum(vector<R> & tbl){
        int n = tbl.size();
        rep(i, 1, n){
            tbl[i] += tbl[i - 1];
        }
    }

    R calc_p(int l, int r, vector<R> & tbl){
        return tbl[r] - tbl[l - 1];
    }

    R probFirst(int len1, int len2, int p1, int p2){
        vector<R> prob1 = calc(len1, p1);
        vector<R> prob2 = calc(len2, p2);

        calc_sum(prob1);
        calc_sum(prob2);

        R res = 0.0;
        rep(t, len1, N){
            R dres = calc_p(t, t, prob1) * (1.0 - calc_p(1, t, prob2));
            if(dres < eps) dres = 0.0;
            
            res += dres;
        }

        return res;
    }
};

int main(void){
    int len1, len2, p1, p2;
    cin >> len1 >> len2 >> p1 >> p2;

    Queueing ins;
    cout.precision(20);
    cout << fixed << ins.probFirst(len1, len2, p1, p2) << endl;

    return 0;
}
