#include <bits/stdc++.h>
#define int long long 
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using vi = vector<int>;
using ll = long long;
 
int dp[1 << 15][15];
const ll mod=1000000007LL;
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}

const int N = 30;
ll fact[N+1];

void set_fact() {
    fact[0] = 1;
    rep(i,N) fact[i+1] = ((((ll)i+1) % mod) * fact[i]) % mod;
}

class ColorfulGardenHard {
public:
    int count(string garden, string forbid){
        int n = garden.size();
        vi cnt(30);
        for(auto & e : garden) cnt[e - 'a']++;

        dp[0][0] = 1;

        rep(i, 1, 1 << n){
            int idx = __builtin_popcount(i) - 1;

            rep(j, n){
                if(((i >> j) & 1) == 0 or forbid[idx] == garden[j]) continue;

                int pi = i & ~(1 << j);
                rep(k, n){
                    if(pi != 0 and (((pi >> k) & 1) == 0 or garden[k] == garden[j])) continue;

                    dp[i][j] = ADD(dp[i][j], dp[pi][k], mod);
                }
            }
        }

        int res = 0;
        rep(i, n) res = ADD(res, dp[(1 << n) - 1][i], mod);

        set_fact();
        rep(i, 30) res = DIV(res, fact[cnt[i]], mod);

        return res;
    }
};

signed main(void){

    return 0;
}
