#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using vi = vector<int>;
 
class SimilarUserDetection {
public:
    void calc(string & in){
        for(auto & c : in){
            if(c == 'I' or c == 'l') c = '1';
            if(c == 'O') c = '0';
        }
    }
    string haveSimilar(vector <string> handles){
        set<string> s;
        for(auto & e : handles){
            calc(e);
            if(s.find(e) != end(s)) return "Similar handles found";
            s.insert(e);
        }

        return "Similar handles not found";
    }
};

int main(void){

    return 0;
}
