#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using vi = vector<int>;
 
class NonDeterministicSubstring {
public:
    long long ways(string A, string B){
        int alen = A.size(), blen = B.size();
        set<string> s;
        rep(i, alen){
            string C = A.substr(i, blen);
            if(C.size() != blen) break;

            [&]{
                rep(j, blen){
                    if(B[j] != '?' and B[j] != C[j]) return;
                }
                s.insert(C);
            }();
        }

        return s.size();
    }
};

int main(void){

    return 0;
}
