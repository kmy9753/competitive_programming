#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define bit(n) (1LL<<(n))
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

class SubstitutionCipher {
public:
    string decode(string a, string b, string y){
        int n = a.size();

        int rest = 'z' - 'a' + 1;
        vi tbl('z' - 'a' + 1, -1);

        rep(i, n){
            int v = a[i] - 'A';
            int u = b[i] - 'A';

            if(tbl[v] == -1 and tbl[u] == -1){
                tbl[v] = u;
                tbl[u] = v;
                rest -= 2;
            }
            else if(tbl[v] != u or tbl[u] != v){
                return "";
            }
        }

        if(rest == 2){
            int u = -1, v;
            rep(i, tbl.size()){
                if(tbl[i] == -1){
                    if(u == -1) u = i;
                    else v = i;
                }
            }
            tbl[u] = v;
            tbl[v] = u;
        }

        string res = "";
        for(auto c : y){
            int i = c - 'A';
            if(tbl[i] == -1){
                return "";
            }
            res.push_back('A' + tbl[i]);
        }

        return res;
    }
};

int main(void){
    return 0;
}
