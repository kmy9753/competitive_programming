#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=2LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

//////////////////
using vec = valarray<ll>;
using mat = valarray<vec>;
 
ll inv(ll a){
    ll res, dummy;
    extgcd(a, mod, res, dummy);
    if(res < 0) res += mod, dummy -= a;
    return res;
}
 
mat mul(mat a, mat b){
    int m = a.size();
    mat c(vec(0LL, m), m);
    rep(i, m)rep(j, m) rep(k, m){
        c[i][j] += a[i][k] * b[k][j] % mod;
        c[i][j] %= mod;
    }
    return c;
}
 
mat power(mat a, int n){
    int m = a.size();
    mat b(vec(0LL, m), m);
    rep(i, m) b[i][i] = 1;
    while(n){
        if(n & 1) b = mul(b, a);
        a = mul(a, a);
        n >>= 1;
    }
    return b;
}
 
inline int pivoting(mat &a, int k, int &c){
    int n = a.size(), m = a[0].size(), p = k, ret = 0;
    for(;c<m;++c){
        ll cmax = a[k][c];
        rep(i, k+1, n) if(chmax(cmax, a[i][c])) p = i, ret = 1;
        if(cmax) break;
    }
    if(k != p) swap(a[k], a[p]);
    return ret;
}
 
int forward(mat &a){
    int n = a.size(), m = a[0].size(), ret = 0, c = 0;
    rep(i, n - 1){
        ret += pivoting(a, i, c);
        if(a[i][c] == 0) break;
        rep(j, i + 1, n){
            ll coef = a[j][c] * inv(a[i][c]) % mod;
            rep(k, c, m) a[j][k] = (a[j][k] - coef * a[i][k] % mod + mod) % mod;
        }
    }
    return ret;
}
 
int rank(mat &a){
    int n = a.size(), m = a[0].size(), ret = 0;
    rep(i, n) rep(j, m) if(a[i][j]) ret = i + 1;
    return ret;
}
 
ll det(mat &a, int sgn){
    forward(a);

    ll ret = 1;
    int n = a.size(), m = a[0].size();
    rep(i, n) ret = ret * a[i][i] % mod;
    if(sgn & 1) ret = mod - ret;
    return ret;
}
//backward substitution
 
vec back(mat &a, int mod){
    int n = a.size(), m = a[0].size();
    vec x(0LL, n);
    for(int i = n - 1; i >= 0; i--){
        ll sum = 0;
        if(i + 1 < n) rep(j, i + 1, n) sum += a[i][j] * x[j] % mod;
        sum %= mod;
        x[i] = (a[i][m-1] - sum + mod) % mod;
        x[i] = x[i] * inv(a[i][i]) % mod;
    }
    rep(i, n) cout << x[i] << endl;
    return x;
}
 
int answer(mat &a){
    int n = a.size(), m = a[0].size();
    int arank = 0, brank = 0;
    rep(i, n) rep(j, m - 1) if(a[i][j]) arank = i + 1;
    rep(i, n) rep(j, m) if(a[i][j]) brank = i + 1;
    if(arank != brank) return 0;
    if(arank < n) return 2;
    return 1;
}
//////////////////

int main(void){
    int n; cin >> n;
    vector<string> S(n);
    for(auto & e : S) cin >> e;

    mat A(vec(n), n);
    rep(i, n){
        rep(j, n){
            if(S[i][j] == '0'){
                A[i][j] = 0;
            }
            else {
                A[i][j] = 1;
            }
        }
    }

    string res = det(A, 0) % 2 == 1 ? "Odd":"Even";
    cout << res << endl;

    return 0;
}
