#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
const int mod = 1e9 + 7;
inline int mod_pow(int a, int n){
    int ret = 1;
    rep(i, n){
        (ret *= a) %= mod;
    }
    return ret;
}
signed main(void){
    int n; cin >> n;

    vector<int> dp(n + 1), dp2(n + 1);
    dp[0] = 1;
    dp2[0] = 1;

    range(i, 1, n + 1){
        dp[i] = (1 + dp2[i - 1] * 3 + mod_pow(dp2[i - 1], 2) * 3 + mod_pow(dp2[i - 1], 3) + dp[i - 1] * 3) % mod;
        dp2[i]=     (dp2[i - 1] * 2 + mod_pow(dp2[i - 1], 2) * 3 + mod_pow(dp2[i - 1], 3) + dp[i - 1] * 2 + 1) % mod;
    }

    cout << dp[n] << endl;

	return 0;
}
