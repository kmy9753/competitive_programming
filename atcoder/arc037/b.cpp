#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
vvi edge;
vi used;
int dfs(int v, int prev, int idx){
    if(used[v] == idx) return 0;
    used[v] = idx;

    int ret = 1;
    for(auto & u : edge[v]){
        if(u == prev) continue;
        ret *= dfs(u, v, idx);
    }

    return ret;
}
int main(void){
    int n, m; cin >> n >> m;

    edge = vvi(n);
    rep(loop, m){
        int u, v; cin >> u >> v;
        u--, v--;
        edge[u].push_back(v);
        edge[v].push_back(u);
    }

    used = vi(n, -1);
    int res = 0;
    rep(i, n){
        if(used[i] != -1) continue;
        res += dfs(i, -1, res);
    }

    cout << res << endl;

	return 0;
}
