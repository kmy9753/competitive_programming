#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e;

    int res = 0;
    for(auto & e : a){
        if(e >= 80) continue;
        res += 80 - e;
    }

    cout << res << endl;

	return 0;
}
