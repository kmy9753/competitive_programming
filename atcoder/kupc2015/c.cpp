#include <bits/stdc++.h>
using namespace std;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define dump(x)  cerr << #x << " = " << (x) << endl;
inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

// cost, from, to
typedef tuple<int, int, int> In;
typedef tuple<int, int> State;
typedef tuple<int, int> Edge;

int main(void){
    int T; cin >> T;

    rep(looooop, T){
        int n; cin >> n;

        vector<In> in;
        vvi ex(n, vi(n, true));
        bool ok = true;
        rep(a, n){
            rep(b, n){
                int c; cin >> c;

                if(a == b){
                    if(c != 0){
                        ok = false;
                    }
                }
                else if(c == -1){
                    ex[a][b] = false;
                }
                else {
                    in.push_back(In(c, a, b));
                }
            }
        }

        if(!ok){
            cout << "NO" << endl;
            continue;
        }

        sort(begin(in), end(in));
        vector<vector<Edge>> edge(n);
        
        vvi mind(n, vi(n, inf));
        for(auto & e : in){
            int aa, bb, C;
            tie(C, aa, bb) = e;

            vi minDist(n, inf);
            priority_queue<State, vector<State>, greater<State>> q;
            q.push(State(0, aa));

            while(q.size()){
                int cv, cc;
                tie(cc, cv) = q.top(); q.pop();

                if(minDist[cv] != inf) continue;
                minDist[cv] = cc;

                if(cv == bb) break;

                for(auto & ee : edge[cv]){
                    int nv, nc;
                    tie(nc, nv) = ee;
                    nc += cc;

                    if(minDist[nv] != inf) continue;

                    q.push(State(nc, nv));
                }
            }

            if(minDist[bb] < C){
                ok = false;
                goto END;
            }
            else if(minDist[bb] > C){
                edge[aa].push_back(Edge(C, bb));
            }
        }

        rep(a, n){
            mind[a][a] = 0;
            for(auto & e : edge[a]){
                int b, c;
                tie(c, b) = e;

                mind[a][b] = c;
            }
        }
        rep(k, n) rep(i, n) rep(j, n) mind[i][j] = min(mind[i][j], mind[i][k] + mind[k][j]);

        rep(a, n){
            rep(b, n){
                if(!ex[a][b] && mind[a][b] != inf){
                    ok = false;
                }
            }
        }
        
END:
        cout << (ok ? "YES":"NO") << endl;
    }

	return 0;
}
