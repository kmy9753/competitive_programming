#include <bits/stdc++.h>
using namespace std;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define dump(x)  cerr << #x << " = " << (x) << endl;
inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int main(void){
    int T; cin >> T;

    rep(looooop, T){
        int h, w; cin >> h >> w;
        if(w < h) swap(w, h);

        //if(h != w && sqr(w / 2.0) + sqr(1.0 * h) < w){
        //    printf("%.12f\n", sqrt(sqr(w / 2.0) + sqr(1.0 * h)));
        //    continue;
        //}
        if(h == w){
            printf("%.12f\n", 1.03527618041008324327 * w);
            continue;
        }

        double lb = 0.0, ub = 1.0 * h;
        double ab, bc, ca;
        rep(loop, 500){
            double mid = (lb + ub) / 2.0;
            
            ab = sqr(w) + sqr(mid);

            double lb2 = 0.5 * w, ub2 = 1.0 * w;
            rep(loop2, 200){
                double mid2 = (lb2 + ub2) / 2.0;

                bc = sqr(mid2) + sqr(h - mid);
                ca = sqr(w - mid2) + sqr(h);

                if(bc + eps > ca){
                    ub2 = mid2;
                }
                else {
                    lb2 = mid2;
                }
            }

            if(ab + eps > bc){
                ub = mid;
            }
            else {
                lb = mid;
            }
        }
        printf("%.12f\n", sqrt(min(ab, min(bc, ca))));
    }

	return 0;
}
