#include <bits/stdc++.h>
using namespace std;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define dump(x)  cerr << #x << " = " << (x) << endl;
inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

// -----------------------
namespace SegmentTrees{
    pii INF= make_pair(-1, -1);
 
    template<typename T>
    class RMQ{
    private:
        int Pow2Fit(int _n){
            int d = 1;
            while((d << 1) <= _n) d <<= 1;
            return d;
        }

    public:
        vector<T> dat;
        int n, size;

        RMQ(int _n){
            n = Pow2Fit(_n) << 1;
            size = 2 * n - 1;
            dat = vector<T>(size, INF);
        }

        // node v := a (0-indexed)
        void set(int v, T a){

            // leaf
            v += n - 1;
            dat[v]=a;

            // update toward root
            while(v > 0){
                int parent = v = (v - 1) / 2;
                int chl = parent * 2 + 1, chr = parent * 2 + 2;
                dat[parent] = max(dat[chl], dat[chr]);
            }
        }

        T get(int v){ // v (0-indexed)
            return dat[v + n - 1];
        }

        T query(int a, int b){ // [a,b)
            return query(INF, a, b, 0, n);
        }

        //T search(int x, int v){
        //    int cidx = 2 * (n - 1);

        //}

    private:
        T query(int v, int a, int b, int l, int r){ // [a,b)
            if(r <= a || b <= l) return INF; // out range
            if(a <= l && r <= b) return dat[v]; // covered
 
            T vl = query(v * 2 + 1, a, b, l, (l + r) / 2),
              vr = query(v * 2 + 2, a, b, (l + r) / 2, r);

            return max(vl, vr);
        }
    };
}

using namespace SegmentTrees;
// -----------------------

int main(void){
    int n; cin >> n;
    
    vi a(n), b(n);
    for(auto & e : a) cin >> e;
    for(auto & e : b) cin >> e;

    vi aa = a;
    sort(begin(aa), end(aa));

    rep(i, n){
        if(aa[i] < b[i]){
            cout << -1 << endl;
            return 0;
        }
    }

    int res = 0;
    for(int i = n - 1; i >= 0; i--){
        int l, r;
        l = r = i;
        while(l >= 0            && b[i] > a[l]) l--;
        while(r < (int)a.size() && b[i] > a[r]) r++;

        int pos;
        if(r < (int)a.size()) pos = r;
        else                  pos = l;

        res += abs(pos - i);
        a.erase(begin(a) + pos, begin(a) + pos + 1);
    }

    cout << res << endl;

	return 0;
}
