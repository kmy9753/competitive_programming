#include <bits/stdc++.h>
using namespace std;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define dump(x)  cerr << #x << " = " << (x) << endl;
inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int main(void){
    int T; cin >> T;

    rep(loop, T){
        string str; cin >> str;

        int res = 0;
        rep(i, (int)str.size()){
            if(str.substr(i, 5) == "kyoto" || str.substr(i, 5) == "tokyo"){
                res++;
                str[i + 4] = '#';
            }
        }

        cout << res << endl;
    }

	return 0;
}
