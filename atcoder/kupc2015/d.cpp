#include <bits/stdc++.h>
typedef long long ll;
#define int ll
using namespace std;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define dump(x)  cerr << #x << " = " << (x) << endl;
inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

signed main(void){
    int n; cin >> n;
    vi a(n), b(n);

    for(auto & e : a) cin >> e;
    for(auto & e : b) cin >> e;

    int maxb = 0, curd = 0, res = 0, curc = 0;
    rep(i, n){
        if(curd >= n) break;

        maxb = max(maxb, b[i]);
        res = max(res, curc + maxb * (n - curd));

        curd++;
        res = max(res, curc += a[i]);

        while(curc < 0){
            curc += maxb;
            curd++;

            if(curd >= n){
                i = n;
                break;
            }
        }
    }

    cout << res << endl;

	return 0;
}
