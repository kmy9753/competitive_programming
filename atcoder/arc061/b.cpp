#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int h, w;
inline bool inside(int y, int x){
    return 0 <= y and y < h and 0 <= x and x < w;
}
inline bool valid(int y, int x){
    return inside(y, x) and inside(y + 2, x + 2);
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> h >> w >> n;
    vi a_(n), b_(n);
    map<int, map<int, int>> dic;
    rep(i, n) cin >> a_[i] >> b_[i], a_[i]--, b_[i]--, dic[a_[i]][b_[i]] = 1;

    map<int, map<int, bool>> used;
    vll tbl(10); tbl[0] = (h - 2LL) * (w - 2);
    rep(i, n){
        int a = a_[i], b = b_[i];
        rep(da, -2, 1){
            rep(db, -2, 1){
                int na = a + da, nb = b + db;
                if(not valid(na, nb) or used[na][nb]) continue;
                used[na][nb] = true;
                int cnt = 0;
                rep(j, 3){
                    rep(k, 3){
                        cnt += dic[na + j][nb + k];
                    }
                }
                assert(cnt >= 1);
                assert(cnt <= 9);
                tbl[cnt]++;
            }
        }
    }

    rep(i, 1, 10) tbl[0] -= tbl[i];
    for(auto& e : tbl) cout << e << endl;

    return 0;
}
