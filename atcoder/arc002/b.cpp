#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

vector<int> tt[2] = {{-1, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
                     {-1, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

bool uru(int y){
    string res = "NO";
    if(y % 4 == 0) res = "YES";
    if(y % 100 == 0) res = "NO";
    if(y % 400 == 0) res = "YES";

    return res == "YES";
}

int main(void){
    int y, m, d;
    char c;
    cin >> y >> c >> m >> c >> d;

    while(y % (m * d) != 0){
        d++;

        if(d > tt[uru(y) ? 1:0][m]){
            m++;
            d = 1;
        }

        if(m > 12){
            y++;
            m = 1;
        }
    }

    printf("%4d/%02d/%02d\n", y, m, d);

	return 0;
}
