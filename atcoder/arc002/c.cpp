#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
set<string> s;
string tt = "ABXY";
void dfs(string t){
    if((int)t.size() == 2){
        s.insert(t);
        return;
    }

    rep(i, 4){
        dfs(t + tt[i]);
    }
}
int main(void){
    int n; cin >> n;
    string com; cin >> com;

    dfs("");

    int res = 1 << 24;
    for(auto && r : s){
        for(auto && l : s){
            if(r == l) continue;

            int cnt = 0;
            rep(i, (int)com.size()){
                if(com.substr(i, 2) == r || com.substr(i, 2) == l){
                    i++;
                }
                cnt++;
            }

            res = min(res, cnt);
        }
    }

    cout << res << endl;

	return 0;
}
