#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<string> vs;

const int inf = 1<<24;

int main(void){
    int h, w; cin >> h >> w;

    vs board(h);
    for(auto & e : board){
        cin >> e;
    }

    int dist_o = inf, dist_x = inf;
    for(auto & line : board){
        int fl = 0, fr = 0;
        rep(i, w){
            if(line[i] == 'o'){
                if(fl == 0) fl = -1;
                fr = w - 1 - i;
            }
            if(line[i] == 'x'){
                if(fl == 0) fl = i;
                fr = -1;
            }
        }
        dist_o = min(dist_o, fr);
        dist_x = min(dist_x, fl);
    }

    if(dist_o > 0 || dist_x > 0){
        if(dist_o <= 0) dist_o = inf;
        if(dist_x <= 0) dist_x = inf;

        cout << dist_o <= dist_x ? 'o':'x' << endl;
        return 0;
    }

    for(auto & line : board){
        bool in = false;
        int free_o = 0, free_x = 0;
        bool o_win = false;
        int r = 0;
        rep(i, w){
            if(line[i] == 1)
        }
    }

	return 0;
}
