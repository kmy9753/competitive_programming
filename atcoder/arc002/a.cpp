#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int y; cin >> y;

    string res = "NO";

    if(y % 4 == 0) res = "YES";
    if(y % 100 == 0) res = "NO";
    if(y % 400 == 0) res = "YES";

    cout << res << endl;

	return 0;
}
