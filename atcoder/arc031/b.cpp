#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;
#define X first
#define Y second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

const int N = 10;

int dx[] = {-1, 0, 1, 0};
int dy[] = {0, -1, 0, 1};

vs m;
int cnt;

void dfs(pii p);

int main(void){
    vs in(N);
    int oNum = 1;
    rep(y, N){
        cin >> in[y];
        rep(x, N){
            if(in[y][x] == 'o') oNum++;
        }
    }
    string res = "NO";

    rep(y, N){
        rep(x, N){
            if(in[y][x] == 'x'){
                m = in;
                m[y][x] = 'o';
                cnt = 0;
                dfs(mp(x, y));

                if(cnt == oNum) res = "YES";
            }
        }
    }

    cout << res << endl;

	return 0;
}

void dfs(pii p){
    if(p.X < 0 || N <= p.X ||
       p.Y < 0 || N <= p.Y)
        return;

    if(m[p.Y][p.X] == 'x')
        return;

    m[p.Y][p.X] = 'x';
    cnt++;

    rep(i, 4)
        dfs(mp(p.X + dx[i], p.Y + dy[i]));
}   

