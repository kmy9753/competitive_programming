#include <iostream>
#include <cstdlib>

using namespace std;

const int N = 100000;

int main(void){
    srand((unsigned)time(NULL));

    for(int i = 0; i < N; i++){
        cout << (rand() % N) + 1 << endl;
    }

    return 0;
}
