#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main(void){
    vector<int> test(10);
    
    for(int i = 0; i  < 10; i++){
        test[i] = i;
    }

    cout << lower_bound(test.begin(), test.end(), 3) - test.begin() - 1 << endl;
    
    cout << test.size() << endl;
    test.erase(test.begin() + 3);
    cout << test.size() << endl;

}
