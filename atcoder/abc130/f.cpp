#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = (int)1e9;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    map<char, vi> x;
    map<char, vi> y;
    for(auto& c : "RLUD"){
        x[c] = vi(1, -inf), y[c] = vi(1, -inf);
    }
    rep(i, n){
        int xx, yy; char d; cin >> xx >> yy >> d;
        x[d].emplace_back(xx);
        y[d].emplace_back(yy);
    }

    vi xmax(300), xmin(300), ymax(300), ymin(300);
    for(auto& c : "RLUD"){
        xmax[c] = *max_element(_all(x[c]));
        ymax[c] = *max_element(_all(y[c]));

        x[c][0] = inf, y[c][0] = inf;
        xmin[c] = *min_element(_all(x[c]));
        ymin[c] = *min_element(_all(y[c]));
    }

    int xmab = max(xmax['U'], xmax['D']);
    int xmib = min(xmin['U'], xmin['D']);
    int ymab = max(ymax['R'], ymax['L']);
    int ymib = min(ymin['R'], ymin['L']);

    int xd = *max_element(_all(xmax)) - *min_element(_all(xmin));
    int yd = *max_element(_all(ymax)) - *min_element(_all(ymin));
    using R = double;
    R res = 1LL << 61;
    rep(_, 2 * (max(xd, yd) + 5)){
        R loop = 0.5 * _;
        R xma = max(1. * xmab, max(xmax['R'] + loop, xmax['L'] - loop));
        R xmi = min(1. * xmib, min(xmin['R'] + loop, xmin['L'] - loop));
        R yma = max(1. * ymab, max(ymax['U'] + loop, ymax['D'] - loop));
        R ymi = min(1. * ymib, min(ymin['U'] + loop, ymin['D'] - loop));

        R cur = (xma - xmi) * (yma - ymi);
        chmin(res, (xma - xmi) * (yma - ymi));
    }

    cout.precision(20);
    cout << fixed << res << endl;

    return 0;
}
