#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef bool B;
typedef long double D;
typedef complex<D> P;
typedef vector<P> VP;
typedef struct {P s,t;} L;
typedef vector<L> VL;
typedef struct {P c;D r;} C;
typedef vector <C> VC;

const D INF = 1e80;
const D eps=1.0e-10;
const D pi=acos(-1.0);
template<class T> bool operator==(T a, T b){return abs(a-b)< eps;}
template<class T> bool operator< (T a, T b){return a < b-eps;}
template<class T> bool operator<=(T a, T b){return a < b+eps;}
//template<class T> int sig(T r) {return (r==0||r==-0) ? 0 : r > 0 ? 1 : -1;}
template<class T> int sig(T a,T b = 0) {return a < b ? -1 : b > a ? 1 : 0;}
#define X real()
#define Y imag()

D ip(P a, P b) {return a.X * b.X + a.Y * b.Y;}
D ep(P a, P b) {return a.X * b.Y - a.Y * b.X;}
D sq(D a) {return sqrt(max(a, (D)0));}
P vec(L l){return l.t-l.s;}
inline P input(){D x,y;cin >> x >> y; return P(x,y);}
inline B cmp_x(const P &a,const P &b){
	return (abs(a.X-b.X)<eps ) ?  a.Y<b.Y : a.X<b.X;
}  // base x

// $BE@$NJ}8~(B
enum CCW{
	LEFT = 1,
	RIGHT = 2,
	BACK = 4,
	FRONT = 8,
	MID = 16,
	ON=FRONT|BACK|MID
};

inline int ccw(P base, P a, P b) {              //$BE@(Ba$B$HE@(Bb$B$,M?$($i$l$?Ld$$$K(B
	a -= base; b -= base;
	if (ep(a, b) > 0)
		return LEFT;    // counter clockwise
	if (ep(a, b) < 0)
		return RIGHT;   // clockwise
	if (ip(a, b) < 0)
		return BACK;    // b--base--a on line
	if (norm(a) < norm(b))
		return FRONT;   // base--a--b on line
	// otherwise
	return MID;      // base--b--a on line  a$B$H(Bb$B$N@~J,H=Dj$O$3$l(B
}
P pLL(L a,L b){ return a.s+vec(a)*ep(vec(b),b.s-a.s)/ep(vec(b),vec(a));}
B iSS(L a,L b){
	int cwa = ccw(a.s,a.t, b.s) | ccw(a.s,a.t, b.t);
	int cwb = ccw(b.s,b.t, a.s) | ccw(b.s,b.t, a.t);
	return ((cwa | cwb) & MID) || ((cwa & cwb) == (LEFT | RIGHT));
}
// $BB?3Q7A$NFb30H=Dj(B $B4^$`(B 2 $B@~>e(B 1 $B4^$^$J$$(B 0$B!!(B($B1zB?3Q7A$b2D(B)
int in_polygon(VP pol,P p){
	int n=pol.size();
	int res=0;
	rep(i,n){
		if(ccw(pol[i],pol[(i+1)%n],p)==MID)
			return 1;
		D vt=(p.Y-pol[i].Y)/(pol[(i+1)%n].Y-pol[i].Y);
		D dx=pol[(i+1)%n].X-pol[i].X;
		if((pol[i].Y<=p.Y)&&(p.Y< pol[(i+1)%n].Y)&&(p.X<pol[i].X+vt*dx))res++;
		if((pol[i].Y> p.Y)&&(p.Y>=pol[(i+1)%n].Y)&&(p.X<pol[i].X+vt*dx))res--;
	}
	return res?2:0;
}

// $BE@$HD>@~$N5wN%(B
D dLP(L l,P p){return abs( ep(vec(l),p-l.s) )/abs(vec(l));}
// $BE@$H@~J,$N5wN%(B
D dSP(L s,P p){
	if (sig( ip( vec(s), p - s.s)) <= 0) return abs(p - s.s);
	if (sig( ip(-vec(s), p - s.t)) <= 0) return abs(p - s.t);
	return dLP(s,p);
}

D area(VP pol){
	int n=pol.size();
	D sum=0.0;
	rep(i,n){
		D x=pol[i%n].X-pol[(i+1)%n].X;
		D y=pol[i%n].Y+pol[(i+1)%n].Y;
		sum+=x*y;
	}
	return abs(sum/2.0);
}

inline D vpos(P p, L l){
    return (l.t.X - l.s.X) * (p.Y - l.s.Y) - (l.t.Y - l.s.Y) * (p.X - l.s.X);
}

int main(void){
    int n; cin >> n;

    VP ps(n);
    for(auto & e : ps) e = input();

    D res = INF;
    rep(i, n){
        range(j, i + 1, n){
            P p1 = ps[i], p2 = ps[j];

            if(ccw(P(0, 0), p1, p2) == BACK){
                vector<D> cur = { INF, INF };
                rep(s, 2){
                    for(auto & p3 : ps){
                        if((s == 0 and ccw(p1, p2, p3) != LEFT ) or
                           (s == 1 and ccw(p1, p2, p3) != RIGHT)) continue;
                        VP arg = { p1, p2, p3 };

                        cur[s] = min(cur[s], area(arg));
                    }
                }
                res = min(res, cur[0] + cur[1]);
            }
            else {
                rep(k, n){
                    if(i == k or j == k) continue;
                    VP arg = { p1, p2, ps[k] };
                    if(in_polygon(arg, P(0, 0)) == 2){
                        res = min(res, area(arg));
                    }
                }
            }
        }
    }
    
    if(res != INF){
        cout << "Possible" << endl;
        printf("%.12Lf\n", res);
    }
    else {
        cout << "Impossible" << endl;
    }

	return 0;
}
