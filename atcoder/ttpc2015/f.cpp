#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
int main(void){
    string a; cin >> a;
    reverse(begin(a), end(a));
    int n = (int)a.size();

    vvi dp(n + 1, vi(2, -1));
    dp[0][0] = 0;
    range(i, 1, n + 1){
        int cn = a[i - 1] - '0';
        rep(j, 2){
            rep(k, 10){
                int sum = cn + k + j;
                int diff = (cn == sum % 10 and cn == k) ? 1:0;
                dp[i][sum / 10] = max(dp[i][sum / 10], dp[i - 1][j] + diff);
            }
        }
    }

    cout << max(dp[n][0], dp[n][1]) << endl;

	return 0;
}
