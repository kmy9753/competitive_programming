#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
#include <sys/time.h>
double gett(){
  struct timeval t;
  gettimeofday(&t,NULL);
  return t.tv_sec + t.tv_usec/1e6;
}
#define fst first
#define snd second
int main(void){
    double st = gett();

    int n; cin >> n;
    auto myrand = bind(uniform_int_distribution<int>(0, 1<<30), mt19937(static_cast<unsigned int>(time(nullptr))));

    vector<int> _a(n);
    for(auto & e : _a) cin >> e;

    while(gett() - st < 1.5){
        vector<int> a = _a;
        vector<pair<int, int>> op;

        int cnt = 0;
        int rest = n;
        while(cnt <= (int)1e5){
            while(rest >= 1 && a[rest - 1] == rest) rest--;
            if(rest == 0){
                cout << op.size() << endl;
                for(auto & e : op){
                    cout << e.fst << " " << e.snd << endl;
                }
                return 0;
            }

            int i = myrand() % rest;
            int j = myrand() % rest;

            if(abs(i - j) == a[i] || abs(i - j) == a[j]){
                swap(a[i], a[j]);
                op.push_back(make_pair(i + 1, j + 1));
            }
        }
    }

    puts("MuriyarokonnNaN");

	return 0;
}
