#include <bits/stdc++.h>
using ll = long long;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;

const int N = 1e6; 
const ll mod = 1e9 + 7;
ll fact[N+1];
 
void set_fact() {
  fact[0] = 1;
  rep(i,N) fact[i+1] = ((((ll)i+1) % mod) * fact[i]) % mod;
}
 
ll extgcd(ll a, ll b, ll &x, ll &y) {
  ll d = a;
  if (b) {
    d = extgcd(b, a%b, y, x); 
    y -= (a/b) * x;
  } else {
    x = 1;
    y = 0;
  }
  return d;
}
 
ll mod_inv(ll a, ll m) {
  ll x, y;
  extgcd(a, m, x, y);
  return (m + x % m) % m;
}
 
// n! mod p 
ll mod_fact(ll n, ll p, ll &e) {
  e = 0;
  if (!n) return 1;
  ll res = mod_fact(n/p, p, e);
  e += n/p;
 
  if ((n/p)%2) 
    return res * (p - fact[n%p]) %p;
  else
    return res * fact[n%p] % p;
}
 
// nCk mod p ; O(log_p n)
ll mod_comb(ll n, ll k, ll p) {
  if (n<0 || k<0 || n<k) return 0;
  ll e1, e2, e3;
  ll a1 = mod_fact(n, p, e1), a2 = mod_fact(k, p, e2), a3 = mod_fact(n-k, p, e3);
  if (e1 > e2+e3) return 0;
  return a1 * mod_inv(a2*a3%p, p) % p;
}
signed main(void){
    set_fact();
    int n, m; cin >> n >> m;

    vvi dp(n + 1, vi(2, 0));
    dp[n][0] = 1;

    for(int i = n - 1; i >= 0; i--){
        rep(j, 2){
            range(k, 2, m + 1){
                if(i + k > n) continue;

                int jj = (k == m) ? 1:j;
                int _;
                (dp[i][jj] += ((dp[i + k][j] * mod_comb(i + k - 1, k - 1, mod)) % mod * mod_fact(k - 1, mod, _) % mod)) %= mod;
            }
        }
    }

    cout << dp[0][1] << endl;

	return 0;
}
