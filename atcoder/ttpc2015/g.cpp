#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
string t = "hceti";
int main(void){
    string s; cin >> s;

    map<char, int> c2i;
    rep(i, (int)t.size()) c2i[t.at(i)] = i;

    int nh = count(begin(s), end(s), t[0]);

    vector<int> f(t.size() + 2);
    f[0] = nh;

    reverse(begin(s), end(s));
    string res = "Yes";
    for(auto & e : s){
        int idx = c2i[e];
        if(e == 't' and f[idx] == 0){
            idx = f.size() - 2;
        }
        if(--f[idx] < 0){
            res = "No";
            break;
        }
        f[idx + 1]++;
    }

    rep(i, (int)f.size() - 1){
        if(f[i] > 0){
            res = "No";
        }
    }

    cout << res << endl;

	return 0;
}
