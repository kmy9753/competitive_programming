#include <bits/stdc++.h>
using ll = long long;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e;

    sort(begin(a), end(a), greater<int>());

    int ngr = 0;
    rep(i, n){
        int nn = n - i;
        int ng = (nn - 1) * (nn - 2) * (nn - 3) / 6;

        if(a[i] > ng + ngr){
            cout << "NO" << endl;
            return 0;
        }
        ngr += ng - a[i];
    }

    cout << "YES" << endl;

	return 0;
}
