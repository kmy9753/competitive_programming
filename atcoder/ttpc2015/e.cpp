#include <bits/stdc++.h>
 
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
 
typedef pair<int, int> pii;
#define fst first
#define snd second
 
typedef vector<int> vi;
typedef vector<vi> vvi;
 
int n;
inline int getsum(vvi & sum, pii p1, pii p2){
    p1.fst = max(p1.fst, 0);
    p1.snd = max(p1.snd, 0);
    p2.fst = min(p2.fst, n);
    p2.snd = min(p2.snd, n);
    return sum[p2.fst][p2.snd] - sum[p2.fst][p1.snd] - sum[p1.fst][p2.snd] + sum[p1.fst][p1.snd];
}
 
int main(void){
    int k; cin >> n >> k;
    vvi field(n + 1, vi(n + 1));
    range(y, 1, n + 1){
        range(x, 1, n + 1){
            if((x + y) % 2) field[y][x] =  1;
            else            field[y][x] = -1;
        }
    }
 
    vector<pii> p(k);
    rep(i, k){
        cin >> p[i].fst >> p[i].snd;
        field[p[i].fst][p[i].snd] *= -1;
    }
 
    vvi sum(n + 1, vi(n + 1));
    range(y, 1, n + 1){
        range(x, 1, n + 1){
            sum[y][x] = field[y][x] + sum[y - 1][x] + sum[y][x - 1] - sum[y - 1][x - 1];
        }
    }
 
    int res = 1;
    range(i, 1, 1 << k){
        pii p1(1<<20, 1<<20);
        pii p2(-1, -1);
        rep(j, k){
            if((i >> j) & 1){
                p1.fst = min(p1.fst, p[j].fst - 1);
                p1.snd = min(p1.snd, p[j].snd - 1);
                p2.fst = max(p2.fst, p[j].fst);
                p2.snd = max(p2.snd, p[j].snd);
            }
        }
 
        rep(k1, 2){
            rep(k2, 2){
                rep(k3, 2){
                    rep(k4, 2){
                        res = max(res, abs(getsum(sum, pii(p1.fst - k1, p1.snd - k2), pii(p2.fst + k3, p2.snd + k4))));
                    }
                }
            }
        }
    }
 
    cout << res << endl;
 
    return 0;
}
