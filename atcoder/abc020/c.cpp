#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 31;

#define F first
#define S second

vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

// cost(white only), (y, x), numBlack
typedef tuple<int, pii, int> State;

int main(void){
    int h, w, t; cin >> h >> w >> t;

    pii s, z;
    vs field(h);
    int numB = 0;
    rep(y, h){
        cin >> field[y];

        rep(x, w){
            if(field[y][x] == 'S') s = mp(y, x), field[y][x] = '.';
            if(field[y][x] == 'G') z = mp(y, x), field[y][x] = '.';
            if(field[y][x] == '#') numB++;
        }
    }

    priority_queue<State, vector<State>, greater<State>> q;
    q.push(State(0, s, 0));

    vector<vvi> minCost(h, vvi(w, vi(numB + 1, inf)));

    while(q.size()){
        pii cur_pos;
        int cur_cost, cur_cntB;

        tie(cur_cost, cur_pos, cur_cntB) = q.top();
        q.pop();

        if(minCost[cur_pos.F][cur_pos.S][cur_cntB] != inf) continue;
        minCost[cur_pos.F][cur_pos.S][cur_cntB] = cur_cost;

        rep(i, 4){
            pii next_pos = mp(cur_pos.F + dy[i], cur_pos.S + dx[i]);
            int next_cost = cur_cost, next_cntB = cur_cntB;

            if(next_pos.F < 0 || h <= next_pos.F ||
               next_pos.S < 0 || w <= next_pos.S) continue;
            
            if(field[next_pos.F][next_pos.S] == '#') next_cntB++;
            else                                     next_cost++;

            if(minCost[next_pos.F][next_pos.S][next_cntB] != inf) continue;

            q.push(State(next_cost, next_pos, next_cntB));
        }
    }

    vi & tbl = minCost[z.F][z.S];

    int res = t - tbl[0];
    range(i, 1, numB + 1){
        res = max(res, (t - tbl[i]) / i);
    }

    cout << res << endl;

	return 0;
}
