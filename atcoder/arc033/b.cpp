#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int na, nb; cin >> na >> nb;
    map<int, int> a, b;
    set<int> s;
    rep(loop, na){
        int i; cin >> i;
        s.insert(i);
        a[i]++;
    }
    rep(loop, nb){
        int i; cin >> i;
        s.insert(i);
        b[i]++;
    }

    int p = 0, q = 0;
    for(auto & e : s){
        p += min(a[e], b[e]);
        q += max(a[e], b[e]);
    }

    cout.precision(12);
    cout << 1.0 * p / q << endl;

	return 0;
}
