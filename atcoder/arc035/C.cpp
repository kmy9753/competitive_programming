#include <bits/stdc++.h>
#define int long long
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int inf = 1LL << 50;
using vi = vector<int>;
using vvi = vector<vi>;
signed main(void){
    int n, m; cin >> n >> m;
    vvi min_dist(n, vi(n, inf));

    rep(loop, m){
        int a, b, c; cin >> a >> b >> c;
        a--, b--;
        min_dist[a][b] = min_dist[b][a] = c;
    }
    rep(i, n) min_dist[i][i] = 0;
    rep(k, n){
        rep(i, n){
            rep(j, n){
                min_dist[i][j] = min(min_dist[i][j], min_dist[i][k] + min_dist[k][j]);
            }
        }
    }

    int K; cin >> K;
    rep(loop, K){
        int x, y, z; cin >> x >> y >> z;
        x--, y--;
        min_dist[x][y] = min_dist[y][x] = min(min_dist[x][y], z);
        int res = 0;
        rep(i, n){
            range(j, i + 1, n){
                min_dist[i][j] = min(min_dist[i][j], min_dist[i][x] + min_dist[x][y] + min_dist[y][j]);
                min_dist[i][j] = min(min_dist[i][j], min_dist[i][y] + min_dist[y][x] + min_dist[x][j]);

                min_dist[j][i] = min_dist[i][j];
            }
        }
        rep(i, n){
            range(j, i + 1, n){
                if(min_dist[i][j] != inf){
                    res += min_dist[i][j];
                }
            }
        }

        cout << res << endl;
    }

	return 0;
}
