#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 29;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using Edge = tuple<int, int>;
using State = tuple<int, int, int>; 

int n;
vector<vector<Edge>> edges;
vi res;

priority_queue<State, vector<State>, greater<State>> q;

int main(void){
    int m; cin >> n >> m;

    edges = vector<vector<Edge>>(n);
    rep(loop, m){
        int c, a, b; cin >> c >> a >> b;
        if(a == b) continue;
        edges[a].push_back(Edge(b, c));
        edges[b].push_back(Edge(a, c));
    }

    res = vi(n, inf);

    vector<map<int, int>> min_dist(n);
    q.push(State(0, 0, 0));

    while(q.size()){
        int v, cost, nb;
        tie(cost, v, nb) = q.top(); q.pop();

        if(min_dist[v].find(nb) != end(min_dist[v])) continue;
        
        bool ok = true;
        for(auto & e : min_dist[v]){
            int cnb = e.first, ccost = e.second;

            if(cnb < nb and ccost <= cost){
                ok = false;
                break;
            }
        }
        if(not ok) continue;

        min_dist[v][nb] = cost;
        chmin(res[v], cost);

        for(auto & e : edges[v]){
            int nv, ntype;
            tie(nv, ntype) = e;

            int nnb = nb + ntype;
            int ncost = cost + (ntype == 0 ? 1 : nnb);

            if(min_dist[nv].find(nnb) != end(min_dist[nv])) continue;

            q.push(State(ncost, nv, nnb));
        }
    }

    for(auto & e : res){
        cout << e << endl;
    }

    return 0;
}
