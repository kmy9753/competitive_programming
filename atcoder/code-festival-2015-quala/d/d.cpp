#include <bits/stdc++.h>

typedef long long ll;
#define int ll

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int n, m;
vi num, where;

bool check(int t){
    int rs = 0;

    rep(i, m){
        int dist = where[i] - rs;
        if(dist > t) return false;

        if(dist < 0){
            rs = max(rs, where[i] + t + 1);
        }
        else {
            int rs1 = where[i] + (t - dist) / 2;
            int rs2 = where[i] + (t - dist * 2);

            rs = max(rs1, rs2) + 1;
        }
        if(rs >= n) return true;
    }

    return false;
}

signed main(void){
    cin >> n >> m;

    where = vi(m);
    rep(i, m){
        int x; cin >> x;
        x--;
        where[i] = x;
    }

    int lb = -10, ub = 1e10 + 10000;
    rep(i, 1000){
        int mid = (lb + ub) / 2;
        if(check(mid)){
            ub = mid;
        }
        else {
            lb = mid;
        }
    }

    cout << ub << endl;

	return 0;
}
