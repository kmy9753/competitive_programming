#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int N = 1001;
int main(void){
    vector<int> isp(N, true);
    isp[0] = isp[1] = false;
    for(int i = 2; i < N; i++){
        if(!isp[i]) continue;
        for(int j = 2; i * j < N; j++){
            isp[i * j] = false;
        }
    }

    int res = 1;
    int cur = 2;
    rep(loop, 200){
        printf("? %d\n", res * cur); fflush(stdout);
        char judge[2]; scanf("%s", judge);
        cerr << judge<<endl;

        if(judge[0] == 'Y'){
            res *= cur;
        }
        else {
            cur = find(begin(isp) + cur + 1, end(isp), true) - begin(isp);
        }
        if(res * cur >= N){
            break;
        }
    }

    printf("! %d\n", res); fflush(stdout);

	return 0;
}
