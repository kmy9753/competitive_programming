#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    vi a(3), b(3);
    for(auto & e : a) cin >> e;
    for(auto & e : b) cin >> e;
    
    sort(begin(b), end(b));

    int res = 1e9;
    do {
        int sum = 0;
        rep(i, 3) sum += abs(b[i] - a[i]);
        res = min(res, sum);
    } while(next_permutation(begin(b), end(b)));

    cout << res << endl;

	return 0;
}
