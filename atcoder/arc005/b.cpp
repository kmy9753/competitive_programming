#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int x, y; string w; cin >> x >> y >> w;
    x--, y--;

    int dx = 0, dy = 0;
    if(w.find('R') != string::npos) dx =  1;
    if(w.find('L') != string::npos) dx = -1;
    if(w.find('U') != string::npos) dy = -1;
    if(w.find('D') != string::npos) dy =  1;

    vector<string> field(9);
    for(auto & e : field) cin >> e;

    string res;
    rep(loop, 4){
        res.push_back(field[y][x]);

        if(y + dy < 0 || 8 < y + dy) dy *= -1;
        if(x + dx < 0 || 8 < x + dx) dx *= -1;

        x += dx;
        y += dy;
    }

    cout << res << endl;

	return 0;
}
