#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define fr first
#define sc second
using namespace std;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<vi> vvi;

const vi dx = { 1, 0,-1, 0};
const vi dy = { 0,-1, 0, 1};

int main(void){
    int h, w; cin >> h >> w;
    vector<string> field(h);
    pii s;
    rep(y, h){
        cin >> field[y];
        rep(x, w){
            if(field[y][x] == 's'){
                s = pii(y, x);
                field[y][x] = '.';
            }
        }
    }

    queue<pair<pii, int>> q;
    q.push(make_pair(s, 0));
    string res = "NO";
    vector<vvi> used(h, vvi(w, vi(2)));
    while(q.size()){
        pii p = q.front().fr;
        int n = q.front().sc;
        q.pop();

        if(used[p.fr][p.sc][n]) continue;
        used[p.fr][p.sc][n] = true;

        if(field[p.fr][p.sc] == 'g'){
            res = "YES";
            break;
        }

        rep(i, 4){
            pii np(p.fr + dy[i], p.sc + dx[i]);
            int nn = n;

            if(np.fr < 0 || h <= np.fr ||
               np.sc < 0 || w <= np.sc) continue;

            if(field[np.fr][np.sc] == '#'){
                if(nn != 2){
                    nn++;
                }
                else {
                    continue;
                }
            }

            q.push(make_pair(np, nn));
        }
    }

    cout << res << endl;

	return 0;
}
