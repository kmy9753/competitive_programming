#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;

    int res = 0;
    rep(loop, n){
        string w; cin >> w;
        if(w[w.size() - 1] == '.') w = w.substr(0, w.size() - 1);
        if(w == "takahashikun" || w == "Takahashikun" || w == "TAKAHASHIKUN"){
            res++;
        }
    }

    cout << res << endl;

	return 0;
}
