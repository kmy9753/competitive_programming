#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

template <typename T>
class Bit {
 public:
  Bit(int size) : size_(size) {
    data_.resize(size + 1);
  }

  // 0-indexed
  void add(int index, T value) {
    if (index < 0 || index >= size_) return;
    ++index;
    while (index <= size_) {
      data_[index] = ADD(data_[index], value, mod);
      index += index & -index;
    }
  }

  // [0, index]
  T sum(int index) {
    if (index < 0 || index >= size_) return 0;
    ++index;
    T ret = 0;
    while (index > 0) {
      ret = ADD(ret, data_[index], mod);
      index -= index & -index;
    }
    return ret;
  }

  // [a, b]
  T sum(int a, int b){
      return SUB(sum(b), sum(a - 1), mod);
  }

 private:
  std::vector<T> data_;
  int size_;
};

inline ll pow2(int x){
    ll ret = 1LL;
    ll b = 2LL;
    while(x > 0){
        if(x % 2 == 1) ret = MUL(ret, b, mod);
        x /= 2;
        b = MUL(b, b, mod);
    }
    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);
    
    int D, F, T, n; cin >> D >> F >> T >> n;
    const int inf = D + F + 123;
    vi xs(n + 2); rep(i, n) cin >> xs[i+1]; xs[n+1] = D;
    xs.emplace_back(inf);

    Bit<ll> bit(n + 2);
    bit.add(n+1, 1LL);

    rrep(i, n + 1){
        int x = xs[i];
        int a = upper_bound(_all(xs), x + F - T) - begin(xs);
        int b = upper_bound(_all(xs), x + F) - begin(xs) - 1;

        if(b == n + 1) a = b;
        if(a > b) continue;

        ll cur = bit.sum(a, b);
        cur = MUL(cur, pow2(a - i - 1), mod);

        bit.add(i, cur);
    }

    cout << bit.sum(0) << endl;

    return 0;
}
