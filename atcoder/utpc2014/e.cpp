#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

const int insert_max_length=1000100;
const int alphabet_num=10;

const int trie_root=0;
const int trie_nothing=-1;
int size=1;
int ac[insert_max_length]; 
int mov[insert_max_length][alphabet_num];
map<string, int> sc;

void init(){
	memset(ac,0,sizeof(ac));
	memset(mov,trie_nothing,sizeof(mov));
	return;
}

int find(string arg){
	int cur=trie_root;
	for(auto &ch:arg){
		if(mov[cur][ch-'0']==trie_nothing)
            return 0;
		cur=mov[cur][ch-'0'];
	}
	return ac[cur];
}

void ins(string arg){
	int cur=trie_root;
	for(auto &ch:arg){
		if(mov[cur][ch-'0']==trie_nothing)
			mov[cur][ch-'0']=size++;
		cur=mov[cur][ch-'0'];
	}

    ac[cur] = 0;
    if(sc.find(arg) != end(sc)) ac[cur] = sc[arg];

    int diff = 0;
    rep(i, 10){
        string narg = arg;
        narg.push_back(i + '0');
        diff = max(diff, find(narg));
    }
    ac[cur] += diff;
}

signed main(void){
	init();

    int n; cin >> n;
    rep(loop, n){
        string a; int b; cin >> a >> b;
        reverse(begin(a), end(a));
        sc[a] += b;
        rep(i, (int)a.size() + 1){
            ins(a.substr(0, a.size() - i));
        }

        cout << ac[trie_root] << endl;
    }

	return 0;
}
