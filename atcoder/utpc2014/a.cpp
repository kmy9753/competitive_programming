#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    vector<string> words;
    string word;
    while(cin >> word) words.push_back(word);

    bool ok = true;
    while(ok){
        ok = false;

        vector<int> eidx;
        rep(i, (int)words.size() - 2){
            if(words[i] == "not" and words[i + 1] == "not" and words[i + 2] != "not"){
                eidx.push_back(i);
                eidx.push_back(i + 1);
                i += 2;
                ok = true;
            }
        }

        reverse(begin(eidx), end(eidx));
        for(auto & e : eidx){
            words.erase(e + begin(words), e + begin(words) + 1);
        }
    }

    rep(i, (int)words.size()){
        cout << (i ? " ":"") << words[i];
    }
    cout << endl;

	return 0;
}
