#include <bits/stdc++.h>
#define int long long 
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int h, w;
inline bool inside(int x, int y){
    return 0 < x and x < w and 0 < y and y < h;
}
inline bool same(int x1, int y1, int x2, int y2){
    return (x1 == 0 and x2 == 0) or (x1 == w and x2 == w) or
           (y1 == 0 and y2 == 0) or (y1 == h and y2 == h);
}
inline int xy2p(int x, int y){
    int ret = 0;
    if(x == 0){
        ret = y;
    }
    else if(y == h){
        ret = h + x;
    }
    else if(x == w){
        ret = w + h + (h - y);
    }
    else { // y == 0
        ret = w + 2 * h + (w - x);
    }
    return ret;
}

signed main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> h >> w >> n;
    map<int, int> dic;
    vi points;
    rep(i, n){
        int y1, x1, y2, x2; cin >> y1 >> x1 >> y2 >> x2;
        if(inside(x1, y1) or inside(x2, y2)) continue;
        // if(same(x1, y1, x2, y2)) continue;

        int p1 = xy2p(x1, y1),
            p2 = xy2p(x2, y2);
        assert(dic.find(p1) == end(dic) and dic.find(p2) == end(dic));
        dic[p1] = dic[p2] = i;
        points.emplace_back(p1);
        points.emplace_back(p2);
    }
    sort(_all(points));

    int sz = points.size();
    int s = -1;
    rep(i, sz){
        int i1 = dic[points[i]];
        int i2 = dic[points[(i + 1) % sz]];
        if(i1 == i2){
            s = i;
            break;
        }
    }
    if(sz == 0){
        cout << "YES" << endl;
        return 0;
    }
    if(s == -1){
        cout << "NO" << endl;
        return 0;
    }

    bool ok = true;
    int a = s, b = (s + 1) % sz;
    rep(loop, sz / 2){
        int p1 = points[a];
        int p2 = points[b];
        if(dic[p1] != dic[p2]) {
            ok = false;
            break;
        }
        a = (a - 1 + sz) % sz;
        b = (b + 1)      % sz;

        while(loop < sz / 2){
            p1 = points[a];
            p2 = points[(a - 1 + sz) % sz];
            if(dic[p1] != dic[p2]) break;
            a = (a - 2 + sz) % sz;
            loop++;
        }
        while(loop < sz / 2){
            p1 = points[b];
            p2 = points[(b + 1) % sz];
            if(dic[p1] != dic[p2]) break;
            b = (b + 2) % sz;
            loop++;
        }
    }

    cout << (ok ? "YES" : "NO") << endl;

    return 0;
}
