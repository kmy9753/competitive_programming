#include <bits/stdc++.h>
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)

using namespace std;
using ll = long long;

int main(void){
    ll n; cin >> n;

    int res = 10000;
    rep(i, sqrt(n) + 1){
        if(i == 0) continue;
        if(n % i == 0){
            ll x = n / i;
            int cur = 0;
            while(x != 0){
                cur++;
                x /= 10;
            }
            if(cur < res) res = cur;
        }
    }

    cout << res << endl;

    return 0;
}

