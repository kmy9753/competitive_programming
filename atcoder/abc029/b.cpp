#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int res = 0;
    rep(i, 12){
        string s; cin >> s;
        if(s.find('r') != string::npos) res++;
    }
    cout << res << endl;

	return 0;
}
