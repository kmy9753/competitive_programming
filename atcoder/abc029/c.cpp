#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

set<string> s;
int n;

string tt = "abc";

void dfs(string cur){
    if((int)cur.size() == n){
        s.insert(cur);
        return;
    }

    rep(i, 3){
        dfs(cur + tt[i]);
    }
}

int main(void){
    cin >> n;
    dfs("");

    for(auto & e : s){
        cout << e << endl;
    }

	return 0;
}
