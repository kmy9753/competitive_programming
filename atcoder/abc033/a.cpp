#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    string s; cin >> s;

    string res = "SAME";
    rep(i, 3){
        if(s.at(i) != s.at(i + 1)) res = "DIFFERENT";
    }

    cout << res << endl;

	return 0;
}
