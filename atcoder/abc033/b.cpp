#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    int sum = 0, max_p = 0;
    string max_s;
    rep(i, n){
        string s; int p;
        cin >> s >> p;
        sum += p;

        if(p > max_p) max_p = p, max_s = s;
    }

    if(max_p > sum / 2){
        cout << max_s << endl;
    }
    else cout << "atcoder" << endl;

	return 0;
}
