#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    string formula; cin >> formula;
    int len = formula.size();

    int idx = 1, res = 0;
    bool zero = formula.at(0) == '0';
    rep(_, len / 2){
        if(formula.at(idx++) == '*'){
            zero |= (formula.at(idx++) == '0');
        }
        else {
            if(not zero) res++;
            zero = formula.at(idx++) == '0';
        }
    }
    if(not zero) res++;

    cout << res << endl;

	return 0;
}
