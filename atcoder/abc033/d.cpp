#include <bits/stdc++.h>
#define int long long 
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

const double pi = acos(-1);
const double eps = 1e-10;

signed main(void){
    int n; cin >> n;
    vector<int> x(n), y(n);
    rep(i, n) cin >> x[i] >> y[i];
    
    int cnt_90 = 0, cnt_gt90 = 0;
    rep(i, n){
        vector<double> rad;
        rep(j, n){
            if(j == i) continue;

            rad.push_back(atan2(y[j] - y[i], x[j] - x[i]));
        }

        sort(begin(rad), end(rad));
        rep(j, n - 1) rad.push_back(rad[j] + 2.0 * pi);

        rep(j, n - 1){
            double border_s = rad[j] + pi / 2.0, border_z = rad[j] + pi;

            cnt_90   += upper_bound(begin(rad), end(rad), border_s + eps) - lower_bound(begin(rad), end(rad), border_s - eps);
            cnt_gt90 += upper_bound(begin(rad), end(rad), border_z) - upper_bound(begin(rad), end(rad), border_s + eps);
        }
    }

    int cnt_lt90 = n * (n - 1) * (n - 2) / 6 - cnt_gt90 - cnt_90;

    cout << cnt_lt90 << " " << cnt_90 << " " << cnt_gt90 << endl;

	return 0;
}
