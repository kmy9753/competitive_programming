#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    char c;
    int x, y; cin >> x >> c >>  y;

    int gcd = __gcd(x, y);
    x /= gcd, y /= gcd;

    int lb = 0, ub = 1e10 / y;
    int resn = -1, resm;
    rep(loop, 10000){
        int mid = (lb + ub) / 2 * y;
        double diff = (0.5 * (mid + 1) - (double)x / y);

        if(0 < diff && diff <= 1 + 1e-18){
            resn = mid;
            resm = diff * mid;
            break;
        }
        else if(0 < diff){
            ub = mid / y;
        }
        else {
            lb = mid / y;
        }
    }

    if(resn == -1){
        cout << "Impossible" << endl;
    }
    else {
        set<pair<int, int>> res;
        res.insert(make_pair(resn, resm));

        for(int sign = -1; sign <= 1; sign += 2){
            range(i, 1, 1e9){
                int n = resn + y * sign * i;
                int sum = 0.5 * n * (n + 1);
                int xx = x * (n / y);
                if(0 < sum - xx && sum - xx <= n){
                    res.insert(make_pair(n, sum - xx));
                }
                else break;
            }
        }

        for(auto & e : res){
            cout << e.first << " " << e.second << endl;
        }
    }

	return 0;
}
