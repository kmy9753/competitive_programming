#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
#define fr first
#define sc second

int main(void){
    int n; cin >> n;
    vector<pair<double, double>> xy(n);
    for(auto & e : xy){
        cin >> e.fr >> e.sc;
    }

    double res = 0.0;
    for(auto & p1 : xy){
        for(auto & p2 : xy){
            res = max(res, sqrt(pow(p1.fr - p2.fr, 2.0) + pow(p1.sc - p2.sc, 2.0)));
        }
    }

    printf("%.12f\n", res);

	return 0;
}
