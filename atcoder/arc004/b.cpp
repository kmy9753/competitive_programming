#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;

    int res_max = 0, res_min;
    vector<int> d(n);
    for(auto & e : d){
        cin >> e;
        res_max += e;
    }

    sort(d.begin(), d.end());
    res_min = d[n - 1];

    int sum = 0;
    rep(i, n - 1){
        sum += d[i];
    }
    if(sum >= res_min) res_min = 0;
    else res_min -= sum;

    cout << res_max << endl << res_min << endl;

	return 0;
}
