#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int res, n;
vi in;
vvi rel;

void dfs(int idx, int num){
    if(idx == n){
        res = max(res, num);
        return;
    }

    dfs(idx + 1, num);

    bool ok = true;
    rep(i, (int)in.size()){
        if(!in[i]) continue;
        if(!rel[i][idx]) ok = false;
    }

    if(!ok) return;

    in[idx] = true;
    dfs(idx + 1, num + 1);
    in[idx] = false;
}

int main(void){
    int m; cin >> n >> m;

    in = vi(n);
    rel = vvi(n, vi(n));

    rep(i, m){
        int x, y; cin >> x >> y;
        x--, y--;

        rel[x][y] = rel[y][x] = true;
    }

    dfs(0, 0);
    cout << res << endl;

	return 0;
}
