#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
 
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <queue>
#include <stack>
#include <map>
#include <set>
 
#include <functional>
#include <cassert>
 
typedef long long ll;
using namespace std;
 
#define debug(x) cerr << #x << " = " << x << endl;
 
 
#define mod 1000000007 //1e9+7(prime number)
#define INF 1000000000 //1e9
#define LLINF 2000000000000000000LL //2e18
#define SIZE 305
 
 
ll dp[SIZE][SIZE][SIZE];
 
int main(){
  int n,l;
  string s;
 
  cin >> n >> s;
 
  l = s.size();
 
  dp[0][0][0] = 1;
 
  for(int i=0;i<n;i++){
    for(int j=0;j<=l;j++){
      for(int k=0;k<=i-j;k++){
        if(k==0 && j==0){
          dp[i+1][0][0] = (dp[i+1][0][0] + dp[i][0][0])%mod;
        }
        
        if(k==0){
          if(j<l)
            dp[i+1][j+1][0] = (dp[i+1][j+1][0] + dp[i][j][0])%mod; //正しい
 
          if(j<l)
            dp[i+1][j][k+1] = (dp[i+1][j][k+1] + dp[i][j][k])%mod; //間違い
          else            
            dp[i+1][j][k+1] = (dp[i+1][j][k+1] + dp[i][j][k]*2)%mod; //間違い
 
          if(j>0)
            dp[i+1][j-1][k] = (dp[i+1][j-1][k] + dp[i][j][k])%mod; //削除
 
        }else{
          dp[i+1][j][k+1] = (dp[i+1][j][k+1] + dp[i][j][k]*2)%mod; //間違い
          dp[i+1][j][k-1] = (dp[i+1][j][k-1] + dp[i][j][k])%mod; //削除
          
        }
      }
    }
  }
    
  
  printf("%lld\n",dp[n][l][0]);
  
  return 0;
}
