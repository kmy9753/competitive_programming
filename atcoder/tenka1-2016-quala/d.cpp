#include <bits/stdc++.h>
#define int long long 
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

vector<vi> graph;
using P = tuple<ll, ll>;
vector<P> pos;
int in[30];
bool used[30];

void dfs(int v, ll d){
    ll x, y; tie(x, y) = pos[v];

    int di = in[v];
    for(auto & nv : graph[v]){
        if(used[nv]) continue;

        di = (di + 1) % 4;

        in[nv] = (di + 2) % 4;
        P np(x + d * dx[di], y + d * dy[di]);
        pos[nv] = np;
        used[nv] = true;

        dfs(nv, d / 4);
    }
}

void compress(vll const& vec, map<ll, int>& zip, vll& unzip){
    unzip = vec;

    sort(_all(unzip));
    unzip.erase(unique(_all(unzip)), unzip.end());
    unzip.push_back(inf);

    rep(i, unzip.size()) zip[unzip[i]] = i;
}

signed main(void){
    int n; cin >> n;
    graph = vector<vi>(n);
    vi i2c(30, -1);
    map<char, int> c2i;
    int idx = 0;
    rep(i, n - 1){
        char a[2];
        int v[2];
        rep(i, 2){
            cin >> a[i];
            if(c2i.find(a[i]) == end(c2i)){
                c2i[a[i]] = idx;
                i2c[idx] = a[i];
                idx++;
            }
            v[i] = c2i[a[i]];
        }
        graph[v[0]].push_back(v[1]);
        graph[v[1]].push_back(v[0]);
    }

    pos = vector<P>(n);

    in[0] = -1;
    pos[0] = P(0, 0);
    used[0] = true;
    dfs(0, 1LL << 60);

    vll xs, ys;
    rep(i, n){
        ll x, y; tie(x, y) = pos[i];
        rep(k, -1, 2){
            xs.push_back(x + k);
            ys.push_back(y + k);
        }
    }

    vll unzip_x, unzip_y;
    map<ll, int> zip_x, zip_y;

    compress(xs, zip_x, unzip_x);
    compress(ys, zip_y, unzip_y);

    int h = unzip_y.size(), w = unzip_x.size();

    vector<string> res(h);
    for(auto & e : res) e = string(w, '.');

    rep(i, n){
        ll x, y; tie(x, y) = pos[i];
        x = zip_x[x], y = zip_y[y];

        res[y][x] = i2c[i];
    }
    rep(i, n){
        ll x, y; tie(x, y) = pos[i];
        x = zip_x[x], y = zip_y[y];

        cerr << i << "; " << (char)i2c[i] << ": " << x << ", " << y << "; " << in[i] << endl;

        if(i == 0) continue;

        int di = in[i];
        char c = (di % 2 == 1) ? '|':'-';

        int nx = x, ny = y;
        while(1){
            nx += dx[di], ny += dy[di];
            if(res[ny][nx] != '.') break;
            cerr << res[ny][nx] << " ";
            res[ny][nx] = c;
        }
        cerr << endl;
    }

    cout << h << " " << w << endl;
    rep(y, h){
        cout << res[y] << endl;
    }

    return 0;
}
