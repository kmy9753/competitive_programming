#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
#define F first
#define S second

const double eps = 1e-9;

int main(void){
    double a, b; cin >> a >> b;
    if(a > b) swap(a, b);

    int n; cin >> n;
    vector<pair<double, double>> cd(n);
    for(auto && e : cd){
        cin >> e.F >> e.S;
        if(e.F > e.S) swap(e.F, e.S);
    }

    for(auto && e : cd){
        double lb = 0.0, ub = acos(-1) / 4.0;
        rep(loop, 100){
            double mid = (lb + ub) / 2.0;
            double h = a * cos(mid) + b * sin(mid);

            // okay
            if(h <= e.F){
                lb = mid;
            }
            else {
                ub = mid;
            }
        }

        double w = a * sin(lb) + b * cos(lb);
        double h = a * cos(lb) + b * sin(lb);

        if(!(a > e.F) && (b <= e.S || 
            (h <= e.F && w <= e.S))){
            puts("YES");
        }
        else {
            puts("NO");
        }
    }

	return 0;
}
