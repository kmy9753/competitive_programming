#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef pair<int, int> pii;
#define fr first
#define sc second
int main(void){
    int n, m; cin >> n >> m;

    vector<pii> ab(m);
    for(auto & e : ab){
        cin >> e.fr >> e.sc;
        e.fr--, e.sc--;
    }
    ab.push_back(pii(1 << 24, 1 << 24));

    sort(ab.begin(), ab.end());
    int res = 0;
    int ub = 1, nub = -1;
    bool ok = true;
    for(auto & e : ab){
        if(e.fr >= ub){
            if(nub == -1){
                ok = false;
                break;
            }
            if(nub > ub){
                ub = nub;
                nub = -1;
                res++;
            }
        }
        if(e.fr != 1 << 24 && e.fr >= ub){
            ok = false;
            break;
        }
        nub = max(nub, e.sc + 1);
    }
    if(ub != n) ok = false;

    if(!ok) cout << "Impossible" << endl;
    else cout << res << endl;

	return 0;
}
