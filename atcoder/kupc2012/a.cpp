#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    int n, t, E; cin >> n >> t >> E;

    vi x(n);
    for(auto & e : x) cin >> e;

    rep(i, n){
        int e = x[i];
        int diff = min(abs(t - e * (t / e)),
                       abs(t - e * (t / e + 1)));

        if(diff <= E){
            cout << i + 1 << endl;
            return 0;
        }
    }

    cout << -1 << endl;

	return 0;
}
