#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    int n, k; cin >> n >> k;

    vi b(n);
    rep(i, k){
        int m; cin >> m;

        rep(loop, m){
            int idx; cin >> idx;
            idx--;
            b[idx] = i;
        }
    }

    int r; cin >> r;
    set<int> s;
    rep(loop, r){
        int p, q; cin >> p >> q;
        p--, q--;

        if(b[p] == b[q]){
            s.insert(p);
            s.insert(q);
        }
    }

    cout << s.size() << endl;

	return 0;
}
