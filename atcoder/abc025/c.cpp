#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

const int N = 3;

int b[N - 1][N];
int c[N][N - 1];

int board[N][N];
typedef pair<int, int> pii;

#define F first
#define S second

const int inf = 1 << 24;

// turn: choku/nao (1/0)
pii dfs(int num, int turn){
    if(num == N * N){
        pii s;

        rep(i, N - 1){
            rep(j, N){
                if(board[i][j] == board[i + 1][j]){
                    s.F += b[i][j];
                }
                else {
                    s.S += b[i][j];
                }
            }
        }
        rep(i, N){
            rep(j, N - 1){
                if(board[i][j] == board[i][j + 1]){
                    s.F += c[i][j];
                }
                else {
                    s.S += c[i][j];
                }
            }
        }

        return s;
    }

    pii ret;
    int val = turn ? -inf:inf;
    rep(i, N){
        rep(j, N){
            if(board[i][j] != -1) continue;

            board[i][j] = turn;
            pii cur = dfs(num + 1, turn ^ 1);
            int curval = cur.F - cur.S;

            if(turn && curval > val){
                val = curval;
                ret = cur;
            }
            if(!turn&& curval < val){
                val = curval;
                ret = cur;
            }

            board[i][j] = -1;
        }
    }

    return ret;
}

int main(void){
    rep(i, N - 1) rep(j, N) cin >> b[i][j];
    rep(i, N) rep(j, N - 1) cin >> c[i][j];

    rep(i, N) rep(j, N) board[i][j] = -1;

    pii res = dfs(0, 1);
    cout << res.F << endl << res.S << endl;

	return 0;
}
