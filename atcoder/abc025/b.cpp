#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, a, b; cin >> n >> a >> b;

    int res = 0;
    while(n--){
        string s; int d; cin >> s >> d;

        d = max(a, min(b, d));
        if(s == "West") d = -d;

        res += d;
    }
   
    string h;
    if(res > 0) h = "East ";
    if(res < 0) h = "West ";
    cout << h << abs(res) << endl;

	return 0;
}
