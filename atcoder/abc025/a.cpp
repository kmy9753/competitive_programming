#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    string s; cin >> s;
    int n; cin >> n;

    set<string> a;
    rep(i, (int)s.size()){
        rep(j, (int)s.size()){
            a.insert(s.substr(i, 1) + s.substr(j, 1));
        }
    }

    cout << vector<string>(a.begin(), a.end())[n - 1] << endl;

	return 0;
}
