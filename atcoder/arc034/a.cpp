#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;

    double res = 0.0;
    rep(loop, n){
        int a, b, c, d, e; cin >> a >> b >> c >> d >> e;
        double sum = a + b + c + d + 110.0 / 900 * e;
        res = max(res, sum);
    }

    cout.precision(12);
    cout << res << endl;

	return 0;
}
