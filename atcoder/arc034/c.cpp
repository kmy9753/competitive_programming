#include <bits/stdc++.h>
#define int long long
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

const int mod = 1e9 + 7;

map<int, int> cnt_p;
void pf(int p){
    int pp = p;
    range(i, 2, sqrt(p) + 1){
        while(pp % i == 0){
            cnt_p[i]++;
            pp /= i;
        }
    }
    if(pp > 1){
        cnt_p[pp]++;
    }
}

signed main(void){
    int a, b; cin >> a >> b;
    range(i, b + 1, a + 1) {
        pf(i);
    }

    int res = 1;
    for(auto & e : cnt_p){
        (res *= (e.second + 1)) %= mod;
    }

    cout << res << endl;

	return 0;
}
