#include <bits/stdc++.h>
#define int long long
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int n; cin >> n;

    vector<int> res;
    int s = max(0LL, n - 300LL);
    range(i, s, n) {
        int x = i, sum = x;
        while(x > 0){
            sum += x % 10;
            x /= 10;
        }
        if(sum == n) {
            res.push_back(i);
        }
    }

    cout << res.size() << endl;
    for(auto & e : res) {
        cout << e << endl;
    }

	return 0;
}
