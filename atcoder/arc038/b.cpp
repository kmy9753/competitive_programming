#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int h, w;
vector<string> field;
vector<vector<int>> memo;
bool win(int y, int x){
    if(memo[y][x] != -1) return memo[y][x];

    bool ret = false;
    if(y + 1 < h && field[y + 1][x] != '#') ret |= !win(y + 1, x);
    if(x + 1 < w && field[y][x + 1] != '#') ret |= !win(y, x + 1);
    if(x + 1 < w && y + 1 < h && field[y + 1][x + 1] != '#') ret |= !win(y + 1, x + 1);

    return (memo[y][x] = ret);
}
int main(void){
    cin >> h >> w;
    field = vector<string>(h);
    memo = vector<vector<int>>(h, vector<int>(w, -1));
    for(auto & e: field) cin >> e;

    cout << (win(0, 0) ? "First":"Second") << endl;
 
	return 0;
}
