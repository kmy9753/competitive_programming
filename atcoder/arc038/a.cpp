#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e;
    sort(begin(a), end(a));
    reverse(begin(a), end(a));

    int res = 0;
    rep(i, n){
        if(i % 2 == 0){
            res += a[i];
        }
    }
    cout << res << endl;

	return 0;
}
