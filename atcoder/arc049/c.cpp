#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

vector<set<int>> edges;
vector<set<int>> redges;
vi used;

void dfs(int v){
    used[v] = true;

    for(auto & nv : edges[v]){
        if(used[nv]) continue;
        if(redges[nv].find(v) != end(redges[nv])){
            redges[nv].erase(v);
        }
        if(redges[nv].size() == 0) dfs(nv);
    }
}

int main(void){
    int n; cin >> n;
    redges = edges = vector<set<int>>(n);

    int A; cin >> A;
    rep(loop, A){
        int x, y; cin >> x >> y;
        x--, y--;
        edges[y].insert(x);
        redges[x].insert(y);
    }

    int B; cin >> B;
    vector<tuple<int, int>> uv;
    rep(loop, B){
        int u, v; cin >> u >> v;
        u--, v--;
        uv.push_back(make_tuple(u, v));
    }

    vector<set<int>> edges_org = edges, redges_org = redges;

    int res = 0;
    rep(i, 1 << B){
        edges = edges_org;
        redges = redges_org;

        rep(j, B){
            int u, v;
            tie(u, v) = uv[j];

            if((i >> j) & 1){
                edges[u].insert(v);
                redges[v].insert(u);
            }
            else {
                redges[u].insert(-1);
            }
        }
        used = vi(n);

        rep(v, n){
            if(used[v] or redges[v].size() > 0) continue;
            dfs(v);
        }

        int cnt = 0;
        for(auto & e : used){
            if(e) cnt++;
        }
        chmax(res, cnt);
    }

    cout << res << endl;

    return 0;
}
