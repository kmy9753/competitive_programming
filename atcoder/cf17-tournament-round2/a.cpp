#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

struct UnionFind {
    vector<int> data;
    UnionFind(int size) : data(size, -1) { }
    bool unionSet(int x, int y) {
        x = root(x); y = root(y);
        if (x != y) {
            if (data[y] < data[x]) swap(x, y);
            data[x] += data[y]; data[y] = x;
        }
        return x != y;
    }
    bool findSet(int x, int y) {
        return root(x) == root(y);
    }
    int root(int x) {
        return data[x] < 0 ? x : data[x] = root(data[x]);
    }
    int size(int x) {
        return -data[root(x)];
    }
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m, K; cin >> n >> m >> K;
    vi c(n);
    int rest = K;
    vi used(K+1);
    for(auto& e : c){
        cin >> e;
        if(used[e]) continue;
        used[e] = true;
        rest--;
    }
    using Elem = tuple<int, int, int>;
    vector<Elem> edges(m);
    for(auto& e : edges){
        int a, b, w; cin >> a >> b >> w;
        a--, b--;
        e = Elem(w, a, b);
    }
    sort(_all(edges));

    UnionFind uf(n);
    int diff = rest - 1;

    ll res = -1;
    int i = 0;
    ll cur = 0;
    while(i < m){
        auto e = edges[i];
        i++;
        int w, a, b; tie(w, a, b) = e;

        if(uf.findSet(a, b)){
            continue;
        }
        else {
            int r1 = uf.root(a), r2 = uf.root(b);
            if(c[r1] == 0 and c[r2] == 0){
                if(rest >= 1){
                    cur += w;
                    rest--;
                    uf.unionSet(a, b);
                }
            }
            else if(c[r1] == 0 or c[r2] == 0){
                if(rest >= 1){
                    cur += w;
                    rest--;
                    uf.unionSet(a, b);
                    c[r1] = 0, c[r2] = 0;
                }
            }
            else {
                cur += w;
                uf.unionSet(a, b);
            }
        }
    }
    if(rest == 0){
        res = cur;
    }

    cout << res << endl;

    return 0;
}
