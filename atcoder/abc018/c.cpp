#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int w, h, k; 
vs field;

int res;
int used[1000][1000];

void dfs(int x, int y, int cnt){
    if(used[y][x]) return;
    if(cnt == 0) res++;

    used[y][x] = true;

    // yoko
    if(x + k < w){
        int cnt1 = cnt - (field[y][x - (k - 1)] == 'x') + (field[y][x + k] == 'x');
        range(i, 1, k){
            cnt1 += (- (field[y + i][x - (k - 1 - i)] == 'x')
                     - (field[y - i][x - (k - 1 - i)] == 'x')
                     + (field[y + i][x + (k - 1 - i) + 1] == 'x')
                     + (field[y - i][x + (k - 1 - i) + 1] == 'x'));
        }
        dfs(x + 1, y, cnt1);
    }

    // tate
    if(y + k < h){
        int cnt1 = cnt - (field[y - (k - 1)][x] == 'x') + (field[y + k][x] == 'x');
        range(i, 1, k){
            cnt1 += (- (field[y - (k - 1 - i)][x + i] == 'x')
                     - (field[y - (k - 1 - i)][x - i] == 'x')
                     + (field[y + (k - 1 - i) + 1][x + i] == 'x')
                     + (field[y + (k - 1 - i) + 1][x - i] == 'x'));
        }
        dfs(x, y + 1, cnt1);
    }
}

int main(void){
    cin >> h >> w >> k;

    field = vs(h);
    for(auto && e : field) cin >> e;

    int cnt = 0;
    rep(y, 1 + 2 * (k - 1)){
        rep(x, 1 + 2 * (k - 1)){
            if(abs(k - 1 - x) + abs(k - 1 - y) > k - 1) continue;
            if(field[y][x] == 'x') cnt++;
        }
    }

    dfs(k - 1, k - 1, cnt);
    cout << res << endl;

	return 0;
}
