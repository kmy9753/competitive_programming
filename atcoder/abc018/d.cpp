#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 29;

int bitcount(int n) {
    int res = 0;
    for(; n; n /= 2) res += n%2;
    return res;
}

int main(void){
    int n, m, p, q, r; cin >> n >> m >> p >> q >> r;

    vvi z(n, vi(m));
    rep(i, r){
        int x, y, zz; cin >> x >> y >> zz;
        x--, y--;

        z[x][y] = zz;
    }

    vvi sum((1 << m), vi(n));
    int res = 0;
    rep(i, 1 << m){
        if(bitcount(i) > q) continue;

        rep(j, n){
            rep(k, m){
                if((i >> k) & 1){
                    sum[i][j] += z[j][k];
                }
            }
        }

        sort(all(sum[i]), greater<int>());
        int ttl = 0;
        rep(j, p){
            ttl += sum[i][j];
        }

        res = max(res, ttl);
    }

    cout << res << endl;

	return 0;
}
