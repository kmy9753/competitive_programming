#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

const int N = 1e5 + 1;

int cnt_num[N];

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;
    vi x(n); for(auto& e : x) cin >> e, cnt_num[e]++;
    uniq(x);
    for(auto & e : x) cerr << e << " ";
    cerr << endl;

    vector<vi> tbl(m);
    rep(i, x.size()){
        int e = x[i];
        tbl[e % m].push_back(cnt_num[e]);
    }
    rep(i, m){
        sort(_all(tbl[i]));
    }

    int res = 0;
    rep(i, m){
        cerr << i << ": " << endl;
        cerr << "res = " << res << endl;

        int j = (m - i + m) % m;

        if(j == i){
            cerr << "hoge" << endl;
            int sum = 0;
            for(auto e : tbl[i]) sum += e;
            res += sum / 2;
            continue;
        }
        if(j < i) break;

        vi& a = tbl[i];
        vi& b = tbl[j];

        for(auto e : a) {
            cerr << e << " ";
        }
        cerr << endl;
        for(auto e : b) {
            cerr << e << " ";
        }
        cerr << endl;

        int suma = 0, sumb = 0;
        for(auto& e : a) suma += e; 
        for(auto& e : b) sumb += e; 
        if(suma < sumb){
            swap(a, b);
            swap(suma, sumb);
        }

        res += sumb;

        int rest = sumb;
        {
            int ai = 0;

            while(rest >= 1){
                while(ai < a.size() and a[ai] % 2 == 0){
                    ai++;
                }
                if(ai >= a.size()) break;

                a[ai]--;
                rest--;
            }
        }
        rep(ai, a.size()){
            int diff = min(rest, a[ai]);
            a[ai] -= diff;
            rest -= diff;
        }
        assert(rest == 0);

        rep(ai, a.size()){
            cerr << "a[ai] = " << a[ai] << endl;
            res += a[ai] / 2;
        cerr << "! " << res << endl;
        }
    }

    cout << res << endl;

    return 0;
}
