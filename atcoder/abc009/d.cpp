#include <bits/stdc++.h>

typedef long long ll;
#define int ll
using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int k, m; 
vvi mat;

vvi mul(vvi & A, vvi & B){
    vvi C(A.size(), vi(B[0].size()));

    rep(i, (int)A.size()){
        rep(l, (int)B.size()){
            rep(j, (int)B[0].size()){
                C[i][j] = (C[i][j] ^ (A[i][l] & B[l][j]));
            }
        }
    }

    return C;
}

vvi pow(vvi A, ll n){
    vvi B(A.size(), vi(A.size()));

    rep(i, (int)A.size()){
        B[i][i] = -1;
    }

    while(n > 0){
        if(n & 1) B = mul(B, A);
        A = mul(A, A);
        n >>= 1;
    }

    return B;
}

signed main(void){
    cin >> k >> m;

    vvi a(k, vi(1)); vi c(k);
    for(auto && e : a) cin >> e[0];
    for(auto && e : c) cin >> e;
    reverse(a.begin(), a.end());

    if(m <= k){
        cout << a[k - m][0] << endl;
        return 0;
    }

    mat = vvi(k, vi(k));
    mat[0] = c;
    rep(i, k - 1){
        mat[i + 1][i] = -1;
    }

    mat = pow(mat, m - k);
    vvi res = mul(mat, a);

    cout << res[0][0] << endl;

	return 0;
}
