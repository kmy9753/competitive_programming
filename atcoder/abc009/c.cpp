#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

string origin;

string rec(string str, int k, int idx){
    set<char> s;
    for(auto && c : str) s.insert(c);

    for(auto && c : s){
        string t = str;
        t.erase(t.find(c), 1);
        string rest = origin.substr(idx + 1, origin.size() - (idx + 1));

        int kk = k - (c == origin[idx] ? 0:1);
        for(auto && e : t){
            if((int)rest.find(e) != -1){
                rest.erase(rest.find(e), 1);
            }
            else {
                kk--;
            }
        }
        if(kk >= 0){
            str.erase(str.find(c),  1);
            return c + rec(str, k - (c == origin[idx] ? 0:1), idx + 1);
        }
    }

    return str;
}

int main(void){
    int n, k; cin >> n >> k;
    cin >> origin;

    cout << rec(origin, k, 0) << endl;

	return 0;
}
