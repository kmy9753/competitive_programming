#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
int n, start;

// to, cost
using Edge = tuple<int, int>;
vector<vector<Edge>> G;
vi active;
 
vll dist;
 
bool bellman_ford(){
    dist = vll(n, INF);
    dist[start] = 0;

    rep(loop, n){
        rep(v, n){
            if(dist[v] == INF) continue;

            for(auto & e : G[v]){
                int nv, cost;
                tie(nv, cost) = e;
                if(not active[nv]) continue;

                if(dist[nv] > dist[v] + cost){
                    dist[nv] = min(dist[nv], dist[v] + cost);
                    if(loop == n - 1) return true;
                }
            }
        }
    }

    return false;
}

void dfs(vi& used, vector<vi>& T, int v){
    if(used[v]) return;
    used[v] = true;

    for(auto& nv : T[v]){
        dfs(used, T, nv);
    }
}

vi calc(vector<vi>& T, int sv){
    vi used(n);
    dfs(used, T, sv);
    return used;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int m, P;
    cin >> n >> m >> P;
    start = 0;

    G = vector<vector<Edge>>(n);
    vector<vi> T1(n);
    vector<vi> T2(n);

    rep(_, m){
        int a, b, c;
        cin >> a >> b >> c;
        a--, b--;
        G[a].push_back(Edge(b, -(c - P)));
        T1[a].push_back(b);
        T2[b].push_back(a);
    }
    vi used1 = calc(T1, 0);
    vi used2 = calc(T2, n-1);
    active = vi(n);
    rep(i, n){
        active[i] = (used1[i] & used2[i]);
    }

    int res = (bellman_ford() ? -1LL : max(0LL, -dist[n-1]));
    cout << res << endl;

    return 0;
}
