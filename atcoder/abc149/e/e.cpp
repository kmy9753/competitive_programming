#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
int n; ll m;
vll a;
vll sum;

ll cur, cnt;
bool check(ll X){
    cur = 0LL, cnt = 0LL;
    rrep(k, n){
        ll e = a[k];
        ll lb = X - e;
        int i = lower_bound(_all(a), lb) - begin(a);
        int num = n - i;
        cur += num * e + (sum[n] - sum[i]);
        cnt += num;
    }
    return cnt >= m;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> n >> m;
    a = vll(n); for(auto& e : a) cin >> e;
    sort(_all(a));

    sum = vll(n + 1);
    rep(i, n){
        sum[i + 1] = sum[i] + a[i];
    }

    ll lb = 0, ub = (int)(1e6);
    rep(_, 50){
        ll mid = (lb + ub) / 2;

        if(check(mid)){
            lb = mid;
        }
        else {
            ub = mid;
        }
    }
    check(lb);
    ll res = cur - lb * (cnt - m);

    cout << res << endl;

    return 0;
}
