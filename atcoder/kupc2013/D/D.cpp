#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;

int main(void){
    int n; cin >> n;

    vi a(n);
    for(auto && e : a) cin >> e;

    stack<int> stk;
    int res = 0;

    for(auto && e : a){
        while(stk.size() && e < stk.top()){
            stk.pop();
        }
        if(!stk.size() || e > stk.top()){
            stk.push(e);
            res++;
        }
    }

    cout << res << endl;

	return 0;
}
