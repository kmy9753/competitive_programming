#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int n, x, m;
vector<tuple<int, int, int>> lrs;
vi s, res;

void dfs(int num){
    if(num == n){
        bool ok = true;
        rep(i, m){
            int l, r, ss;
            tie(l, r, ss) = lrs[i];

            int sum = 0;
            range(j, l, r){
                sum += s[j];
            }
            if(sum != ss){
                ok = false;
                break;
            }
        }
        if(ok) res = s;
        return;
    }

    rep(i, x + 1){
        s[num] = i;
        dfs(num + 1);
    }
}
int main(void){
    cin >> n >> x >> m;

    lrs = vector<tuple<int, int, int>>(m);
    for(auto && e : lrs){
       int l, r, ss; cin >> l >> r >> ss;
       l--;

       e = make_tuple(l, r, ss);
    }

    s = res = vi(n);
    res[0] = -1;

    dfs(0);

    if(res[0] == -1){
        cout << -1 << endl;
    }
    else {
        rep(i, n){
            cout << (i ? " ":"") << res[i];
        }
        cout << endl;
    }

    return 0;
}
