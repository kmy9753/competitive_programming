#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define X first
#define Y second

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 100;
const int S = 1, B = 0;

const int dx[4] = {-1, 0, 1, 0};
const int dy[4] = {0, 1, 0, -1};

vector<vi> m(N);
int w, h, res = 0;

void dfs(pii, int, bool, vector<vi>);

int main(void){
    cin >> h >> w;
    vector<vi> m(h);
    rep(y, h) rep(x, w){ int in; cin >> in; m[y].pb(in); }

    dfs(mp(0, 0), 0, false, m);

    cout << res << endl;
 
    return 0;
}

void dfs(pii p, int cnt, bool bite, vector<vi> m){
    if(bite){
        if(m[p.Y][p.X] == S) cnt ++;
        rep(i, 4){
            pii curP = mp(p.X + dx[i], p.Y + dy[i]);
            if(curP.X < 0 || w <= curP.X ||
               curP.Y < 0 || h <= curP.Y) continue;
            if(m[curP.Y][curP.X] != -1) m[curP.Y][curP.X] ^= 1;
        }
        m[p.Y][p.X] = -1;
    }

    rep(x, w) rep(y, h)
        if(m[y][x] != -1){
            if((m[y][x - 1] == -1 || m[y][x + 1] == -1) || 
               (x - 1 < 0 || w <= x - 1 || x + 1 < 0 || w <= x + 1))
            dfs(mp(x, y), cnt, true, m);
            break;
        }

    res = max(res, cnt);
}
