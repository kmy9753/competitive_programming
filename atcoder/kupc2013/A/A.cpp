#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
 
#define rep(i, n) for(int i = 0; i < n; i ++)
#define ALL(T) T.begin(), T.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define Y first
#define N second

using namespace std; 

typedef pair<int, int> pii;
typedef pair<int, string> pis;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
 
int main(void){
    int n, q; cin >> n >> q;

    vector<pis> in;

    while(n --){
        int y; string name;
        cin >> y >> name;

        in.push_back(mp(y, name));
    }

    sort(ALL(in));

    string res = "kogakubu10gokan";

    rep(i, in.size()){
        if(in[i].Y <= q) res = in[i].N;
        else{ break; }
    }

    cout << res << endl;
 
    return 0;
}
