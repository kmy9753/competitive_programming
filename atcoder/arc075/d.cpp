#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

map<ll, map<ll, map<bool, ll>>> memo;

ll rec(ll n, ll D, bool fst = true){
    if(memo[n][D].find(fst) != end(memo[n][D])) return memo[n][D][fst];
    ll& ret = memo[n][D][fst] = 0;

    if(n == 0) return ret = (D == 0 ? 1:0);
    if(n == 1) return ret = (abs(D) <= 9 ? 10:0);

    int cur = abs(D) % 10;
    if(D < 0 and cur != 0) cur = abs(cur - 10);

    rep(a, 10){
        if(fst and a == 0) continue;
        rep(b, 10){
            if(a - b == cur or a - b == cur - 10){
                ll nD = D - (a - b) - (b - a) * (ll)pow(10, n - 1);
                assert(abs(nD) % 10 == 0);
                nD /= 10;
                ret += rec(n - 2, nD, false);
                // cerr << a << ", " << b << endl;
            }
        }
    }

    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    ll D; cin >> D;
    ll res = 0;
    int s = 1, t = 18;
    rep(i, s, t + 1){
        res += rec(i, D);
    }
    cout << res << endl;

    return 0;
}
