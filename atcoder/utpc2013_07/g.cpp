#include <algorithm>
#include <bitset>
#include <cctype>
#include <complex>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;
#define s first
#define z second

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =(int)1e9;

int n, m, k;
vector<pii> in;

bool C(int d){
    int kk = k;
    int cur = 0, maxb = 0, cnt = 0;
    priority_queue< int, vi, greater<int> > q;

    while(cur + d < m + 1){
        while(cnt < n && in[cnt].s <= cur + d){
            q.push(in[cnt].z);
            maxb = max(maxb, in[cnt].z);
            cnt++;
        }

        if(q.empty()){
            if(kk <= 0 || maxb <= cur) return false;
            kk--;
            cur = maxb;
        }
        else{
            if(q.top()) cur = min(cur + d, q.top());
            q.pop();
        }
    }
    
    return true;
}

int main(void){
    cin >> n >> m >> k;
    in.resize(n);
    rep(i, n) cin >> in[i].s >> in[i].z;

    sort(all(in));
    int ub = inf, lb = 0;

    rep(i, 100){
        int mid = (lb + ub) / 2;

        if(C(mid)) ub = mid;
        else lb = mid;
    }
    while(C(ub - 1)) ub--;
    while(!C(ub)) ub++;

    cout << ub << endl;

	return 0;
}
