#include <bits/stdc++.h>
#define int long long 
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
vector<int> nico = {2};
map<int, int> memo;

int dfs(int n){
    if(memo.count(n)) return memo[n];

    int ret = n;
    for(auto & e : nico){
        if(e > n) break;
        ret = min(ret, dfs(n / e) +  n % e);
    }

    return memo[n] = ret;
}

signed main(void){
    int a = 2, b = 0;
    rep(i, 20){
        int cur = i % 2 ? 2:5;
        if(not (a / (int)1e17)){
            a = a * 10 + cur;
            nico.push_back(a);
        }
        if(not (b / (int)1e17)){
            b = b * 10 + cur;
            nico.push_back(b);
        }
    }
    sort(begin(nico), end(nico));

    int n; cin >> n;
    
    cout << dfs(n) << endl;

	return 0;
}
