#include <bits/stdc++.h>
#define int long long
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using State = tuple<int, int, int, int>;

vi a = {0}, b = {0};
int alen, blen;
const int inf = 1LL << 50;

double eps = 1e-10;

bool check(double v){
    vector<vvi> min_dist(alen, vvi(blen, vi(2, inf)));
    min_dist[0][0][0] = 0;

    queue<State> q;
    q.push(State(0, 0, 0, 0));

    while(q.size()){
        int dist, ai, bi, in_right;
        tie(dist, ai, bi, in_right) = q.front(); q.pop();

        if(min_dist[ai][bi][in_right] != dist) continue;
        if(ai == alen - 1 and bi == blen - 1) return true;

        if(ai != alen - 1){
            int nai = ai + 1, nbi = bi;

            int ndist = in_right ? dist + b[bi] + a[nai] : dist + (a[nai] - a[ai]);

            if(a[nai] + eps > ndist / v){
                min_dist[nai][nbi][0] = min(min_dist[nai][nbi][0], ndist);
                q.push(State(ndist, nai, nbi, 0));
            }
        }
        if(bi != blen - 1){
            int nai = ai, nbi = bi + 1;

            int ndist = in_right ? dist + (b[nbi] - b[bi]) : dist + a[ai] + b[nbi];

            if(b[nbi] + eps > ndist / v){
                min_dist[nai][nbi][1] = min(min_dist[nai][nbi][1], ndist);
                q.push(State(ndist, nai, nbi, 1));
            }
        }
    }

    return false;
}

signed main(void){
    int n; cin >> n;
    rep(loop, n){
        int x; cin >> x;
        if(x < 0){
            a.push_back(-x);
        }
        else {
            b.push_back(x);
        }
    }
    alen = a.size();
    blen = b.size();
    sort(begin(a), end(a));
    sort(begin(a), end(a));

    double lb = 0.0, ub = 1e10;
    rep(loop, 100){
        double mid = (lb + ub) / 2;

        if(check(mid)){
            ub = mid;
        }
        else {
            lb = mid;
        }
    }

    cout.precision(20);
    cout << fixed << ub << endl;

	return 0;
}
