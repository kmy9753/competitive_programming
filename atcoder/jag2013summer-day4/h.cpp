#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
using namespace std;

template<class T>void reg(vector<T> &ary,const T &elem){ary.emplace_back(elem);}
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using R=long double; // __float128
const R EPS = 1E-12; // [-1000:1000]->EPS=1e-8 [-10000:10000]->EPS=1e-7
const R INF = 1E40;
constexpr R PI = acos(R(-1));
inline int sgn(const R& r){ return (r > EPS) - (r < -EPS);}
inline int sgn(const R& a, const R &b){ return sgn(a-b); }
inline R sq(R x){return sqrt(max<R>(x,0.0));}
 
using P=complex<R>;
using VP=vector<P>;
using L=struct{P s,t;};
using VL=vector<L>;
using C=struct{P c;R r;};
using VC=vector<C>;
 
constexpr P O = P(0,0);
istream& operator >> (istream& is,P& p){ R x,y;is >> x >> y; p=P(x,y); return is;}
ostream& operator << (ostream& os,P& p){ os << real(p) << " " << imag(p); return os;}
 
namespace std{
    bool operator <  (const P& a,const P& b){ return sgn(real(a-b))?real(a-b)<0:sgn(imag(a-b))<0;}
    bool operator == (const P& a,const P& b){ return sgn(real(a-b))==0 && sgn(imag(a-b))==0;}
}
 
inline bool cmp_x(const P& p,const P& q){return sgn(real(p-q))?real(p)<real(q):sgn(imag(p-q));}
inline bool cmp_y(const P& a, const P& b){return sgn(imag(a-b)) ? imag(a-b)<0 : sgn(real(a-b))<0;}
inline bool cmp_a(const P& a, const P& b){return sgn(arg(a)-arg(b)) ? arg(a)-arg(b)<0 : sgn(norm(a)-norm(b))<0;}
bool operator <  (const L& a,const L& b){ return a.s==b.s?a.t<b.t:a.s<b.s;}
bool operator == (const L& a,const L& b){ return a.s==b.s&&a.t==b.t;}

//$BFb@Q(B dot $B30@Q(B det
inline R dot(P o,P a,P b){a-=o,b-=o; return real(conj(a)*b);}
inline R det(P o,P a,P b){a-=o,b-=o; return imag(conj(a)*b);}
inline P vec(L l){return l.t-l.s;}

enum CCW{ LEFT = 1,RIGHT = 2,BACK = 4,FRONT = 8,ON = 16};
inline int ccw(P o,P a, P b) {//$BE@(Ba$B$HE@(Bb$B$,M?$($i$l$?Ld$$$K(B
    if (sgn(det(o,a,b)) > 0) return LEFT;    // counter clockwise
    if (sgn(det(o,a,b)) < 0) return RIGHT;   // clockwise
    if (sgn(dot(o,a,b)) < 0) return BACK;    // b--base--a on line
    if (sgn(norm(a-o)-norm(b-o)) < 0) return FRONT;   // base--a--b on line
    return ON;// base--b--a on line  a$B$H(Bb$B$N@~J,H=Dj$O$3$l(B
}

// $B8rE@(B verify AOJ CGL_2_C
P cross(L a,L b){
    R s1=det(a.s,b.s,b.t);
    R s2=s1+det(a.t,b.t,b.s);
    return a.s+s1/s2*(a.t-a.s);
}

// $BB?3Q7A(B
// $BLL@Q(B Verify AOJ 1100 CGL_3_A
R area(const VP& pol){
    int n=pol.size();
    R sum=0;
    rep(i,n) sum+=det(O,pol[i],pol[(i+1)%n]);
    return abs(sum/2.0);
}

// $BFL%+%C%H(B verify AOJ CGL_4_C
VP convex_cut(const VP& pol,const L& l) {
    VP res;
    int n=pol.size();
    rep(i,n){
        P a = pol[i],b=pol[(i+1)%n];
        if(ccw(l.s,l.t,a)!=RIGHT) reg(res,a);
        if((ccw(l.s,l.t,a)|ccw(l.s,l.t,b))==(LEFT|RIGHT)) reg(res,cross({a,b},l));
    }
    return res;
}

void add_cut(VP& pol, R a, R b, R x, int type){
    a *= pow(-1, type);
    b *= pow(-1, type);
    x *= pow(-1, type);

    if(abs(a) < EPS and abs(b) < EPS){
        if(x < 0) pol = VP();
        return;
    }

    P s, t; s = t = {inf, inf};

    int hoge = 0;
    if(abs(a) < EPS){
        t = P(0, x / b);
        s = t + P(1, 0);
        if(b < 0) swap(s, t);
    }
    else if(abs(b) < EPS){
        hoge = 1;
        s = P(x / a, 0);
        t = s + P(0, 1);
        if(a < 0) swap(s, t);
    }
    else if(abs(x) < EPS){
        hoge = 2;
        t = P(0, 0);
        R coef = -a / b;
        s = P(1, coef);
        if(b < 0) swap(s, t);
    }
    else {
        hoge = 3;
        s = P(x / a, 0);
        t = P(0, x / b);

        if(real(s) > 0){
            if(imag(t) > 0){
            }
            else {
            }
        }
        else {
            if(imag(t) > 0){
                swap(s, t);
            }
            else {
                swap(s, t);
            }
        }
        if(b < 0) swap(s, t);
    }

    L l = { s, t };
    //cerr << hoge << " type: " << "a = " << a << ", b = " << b << ", x = " << x << ", " << s << ", " << t << endl;
    pol = convex_cut(pol, l);
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int h, w; cin >> h >> w;
    R ma1, ma2, mb1, mb2, mx; cin >> ma1 >> ma2 >> mb1 >> mb2 >> mx;

    vector<string> field(h);
    for(auto& e : field) cin >> e;

    P fa, fb, fx; fa = fb = fx = {(R)0, (R)0}; 
    int na, nb, nx; na = nb = nx = 0;
    rep(y, h){
        rep(x, w){
            R xx = x + 0.5;
            R yy = y + 0.5;
            switch(field[y][x]){
                case 'A': { fa += P(xx, yy); na++; break; }
                case 'B': { fb += P(xx, yy); nb++; break; }
                case 'X': { fx += P(xx, yy); nx++; break; }
            }
        }
    }

    R res = (R)0;
    rep(y, h){
        rep(x, w){
            if(field[y][x] == '.') continue;
            VP pol = {
                P(ma1, mb1),
                P(ma2, mb1),
                P(ma2, mb2),
                P(ma1, mb2),
            };
            R base = area(pol);

            rep(i, 2){
                if(pol.size() == 0) break;
                add_cut(pol, na * (x+i) - real(fa), nb * (x+i) - real(fb), mx * real(fx) - mx * nx * (x+i), i);

                if(pol.size() == 0) break;
                add_cut(pol, na * (y+i) - imag(fa), nb * (y+i) - imag(fb), mx * imag(fx) - mx * nx * (y+i), i);
            }
            //cerr << area(pol) << " / " << base << endl;
            //cerr << "----" << endl;

            res += area(pol) / base;
        }
    }

    cout.precision(20);
    cout << fixed << res << endl;

    return 0;
}
