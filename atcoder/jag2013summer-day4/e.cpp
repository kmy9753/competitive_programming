#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int n, m;
vi eval;
vector<vi> G;
vector<vector<vi>> min_num, max_num, memo;
vi scores;
map<int, int> s2i;

int negamax(int v, int alpha, int beta){
    int& mi  = min_num[v][s2i[alpha]][s2i[beta]];
    int& ma  = max_num[v][s2i[alpha]][s2i[beta]];
    int& ret = memo   [v][s2i[alpha]][s2i[beta]];
    if(ma != -1){
        return ret;
    }

    int nc = G[v].size();

    // leaf
    if(nc == 0){
        mi = 1;
        ma = 1;
        return ret = eval[v];
    }

    // internal
    vi perm(nc); rep(i, nc) perm[i] = i;
    do {
        int alpha_org = alpha;
        int beta_org  = beta;
        
        int curmi = 0, curma = 0;
        rep(i, nc){
            int nv = G[v][perm[i]];
            int val = -negamax(nv, -beta, -alpha);
            curmi += min_num[nv][s2i[-beta]][s2i[-alpha]];
            curma += max_num[nv][s2i[-beta]][s2i[-alpha]];

            if(val >= beta){
                ret = val;
                break;
            }
            chmax(alpha, val);
            ret = alpha;
        }
        chmin(mi, curmi);
        chmax(ma, curma);

        alpha = alpha_org;
        beta  = beta_org;
    } while(next_permutation(_all(perm)));

    return ret;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> n;
    eval = vi(n); for(auto& e : eval) cin >> e;
    G = vector<vi>(n);
    scores = {-inf, inf};
    rep(v, n){
        int k; cin >> k;
        rep(loop, k){
            int u; cin >> u; u--;
            G[v].emplace_back(u);
        }
        if(k == 0){
            scores.emplace_back(eval[v]);
            scores.emplace_back(-eval[v]);
        }
    }
    sort(_all(scores));
    scores.erase(unique(_all(scores)), end(scores));
    m = scores.size();
    rep(i, m) s2i[scores[i]] = i;

    min_num = vector<vector<vi>>(n, vector<vi>(m, vi(m, inf)));
    max_num = vector<vector<vi>>(n, vector<vi>(m, vi(m, -1)));
    memo    = vector<vector<vi>>(n, vector<vi>(m, vi(m, -inf)));

    negamax(0, -inf, inf);
    cout << min_num[0][0][m-1] << " " << max_num[0][0][m-1] << endl;

    return 0;
}
