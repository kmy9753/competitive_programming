#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int w, h;
inline int xy2v(int x, int y){
    return w * y + x;
}
inline pair<int, int> v2xy(int v){
    int x = v / h;
    int y = v % h;
    assert(v == xy2v(x, y));
    return make_pair(x, y);
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int m; cin >> w >> h >> m;
    int x1, x2, y1, y2; cin >> x1 >> y1 >> x2 >> y2;
    int s = xy2v(x1, y1), t = xy2v(x2, y2);

    int n = (w + 1) * (h + 1);
    vector<map<int, int>> G(n);
    rep(y, h + 1){
        rep(x, w + 1){
            int a = xy2v(x, y);
            rep(i, 4){
                int ny = y + dy[i];
                int nx = x + dx[i];
                if(ny < 0 or h < ny or
                   nx < 0 or w < nx) continue;
                int b = xy2v(nx, ny);
                G[a][b] = 0;
            }
        }
    }

    rep(i, m){
        int x, y, T; string pat; cin >> x >> y >> T >> pat; x++, y++;
        rep(loop, T){
            for(auto& c : pat){
                int a, b; a = -1;
                switch(c){
                    case 'U': { if(y - 1 >= 1) { a = xy2v(x-1, y-1), b = xy2v(x, y-1); y--; } break; }
                    case 'D': { if(y + 1 <= h) { a = xy2v(x-1, y), b = xy2v(x, y); y++; } break; }
                    case 'L': { if(x - 1 >= 1) { a = xy2v(x-1, y-1), b = xy2v(x-1, y); x--; } break; }
                    case 'R': { if(x + 1 <= w) { a = xy2v(x, y-1), b = xy2v(x, y); x++; } break; }
                }
                if(a != -1){
                    G[a][b]++;
                    G[b][a]++;
                }
            }
        }
    }

    vi min_dist(n, inf);
    min_dist[s] = 0;
    using Elem = tuple<int, int>;
    priority_queue<Elem, vector<Elem>, greater<Elem>> q;
    q.push(Elem(0, s));

    while(q.size()){
        int c, v; tie(c, v) = q.top(); q.pop();
        if(min_dist[v] != c) continue;
        if(v == t) break;

        for(auto& e : G[v]){
            int nv = e.first;
            int dc = e.second;

            int nc = c + dc;
            if(chmin(min_dist[nv], nc)){
                q.push(Elem(nc, nv));
            }
        }
    }

    cout << min_dist[t] << endl;

    return 0;
}
