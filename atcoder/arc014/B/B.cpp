#include <iostream>
#include <set>
#include <string>

using namespace std;

int main(void){
    int n, workN; cin >> n;
    workN = n;
    set<string> usedWords;
    string result = "DRAW";
    string pre;

    while(workN --){
        string in; cin >> in;
        if(result == "DRAW" && n - workN != 1 && !(usedWords.insert(in).second))// || *in.begin() != *pre.end())
            result = ((n - workN % 2)?"WIN":"LOSE");
        pre = in;
    }
    cout << result << endl;

    return 0;
}
