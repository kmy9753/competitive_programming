#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

#define fst first
#define snd second

typedef tuple<int, pii> State;

int main(void){
    int h, w; cin >> h >> w;
    pii s, z; cin >> s.fst >> s.snd >> z.fst >> z.snd;
    s.fst--, s.snd--, z.fst--, z.snd--;

    vs c(h);
    for(auto && e : c) cin >> e;

    priority_queue<State, vector<State>, greater<State>> q;
    q.push(State(0, s));
    vvi used(h, vi(w, false));

    int res = -1;
    while(!q.empty()){
        int cur_c; pii cur_p;
        tie(cur_c, cur_p) = q.top(); q.pop();

        if(used[cur_p.fst][cur_p.snd]) continue;
        if(cur_p == z){
            res = cur_c;
            break;
        }

        used[cur_p.fst][cur_p.snd] = true;

        vi dx = { 1, 0,-1, 0};
        vi dy = { 0,-1, 0, 1};
        rep(i, 4){
            pii next_p = mp(cur_p.fst + dy[i], cur_p.snd + dx[i]);
            int next_c = cur_c + 1;

            if(next_p.fst < 0 || h <= next_p.fst ||
               next_p.snd < 0 || w <= next_p.snd ||
               c[next_p.fst][next_p.snd] == '#') continue;

            q.push(State(next_c, next_p));
        }
    }

    cout << res << endl;

	return 0;
}
