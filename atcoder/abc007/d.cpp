#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

ll f(string x){

    // $B8+=*$o$C$?7e?t!$L$K~3NDj:Q!$(B4or9$BF~$j:Q$_(B
    ll dp[(int)x.size() + 1][2][2];
    clr(dp, 0);

    dp[0][0][0] = 1;

    rep(i, (int)x.size()){
        rep(j, 2){
            rep(k, 2){
                if(dp[i][j][k] == 0) continue;
                
                if(j == 1){
                    if(k == 1){
                        dp[i + 1][j][k] += dp[i][j][k] * 10;
                    }
                    else {
                        dp[i + 1][j][0] += dp[i][j][k] * 8;
                        dp[i + 1][j][1] += dp[i][j][k] * 2;
                    }
                }
                else {
                    int lb = x.at(i) - '0';

                    rep(l, 10){
                        if(j == 0 && l > lb) break;
                        int jj = l < lb ? 1:0;

                        if(k == 1){
                            dp[i + 1][jj][k] += dp[i][j][k];
                        }
                        else {
                            if(l == 4 || l == 9) dp[i + 1][jj][1] += dp[i][j][k];
                            else                 dp[i + 1][jj][0] += dp[i][j][k];
                        }
                    }
                }
            }
        }
    }

    return dp[x.size()][1][1];
}

int main(void){
    string a, b; cin >> a >> b;

    ll fa = f(a);
    ll fb = f(b);

    for(auto && e : b){
        if(e == '4' || e == '9'){
            fb++;
            break;
        }
    }

    cout << fb - fa << endl;

	return 0;
}
