#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, m; cin >> n >> m;
    set<int> s;
    rep(i, n){
        s.insert(i + 1);
    }

    vector<int> a(m);
    for(auto & e : a) cin >> e;
    reverse(begin(a), end(a));
    
    for(auto & e : a){
        if(s.find(e) == end(s)) continue;
        cout << e << endl;
        s.erase(e);
    }

    for(auto & e : s){
        cout << e << endl;
    }

	return 0;
}
