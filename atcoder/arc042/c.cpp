#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
#define fst first
#define snd second
int main(void){
    int n, p; cin >> n >> p;

    vector<pair<int, int>> ab(n);
    for(auto & e: ab) cin >> e.fst >> e.snd, e.snd *= -1;
    sort(begin(ab), end(ab));
    for(auto & e : ab) e.snd *= -1;

    vvi dp(n, vi(p + 1, 0));
    dp[0][0] = ab[0].snd;

    range(i, 1, n){
        dp[i] = dp[i - 1];
        dp[i][0] = max(dp[i][0], ab[i].snd);

        rep(j, p + 1){
            if(j - ab[i].fst < 0 || dp[i - 1][j - ab[i].fst] == 0) continue;

            dp[i][j] = max(dp[i][j], dp[i - 1][j - ab[i].fst] + ab[i].snd);
        }
    }

    cout << *max_element(begin(dp[n - 1]), end(dp[n - 1])) << endl;

	return 0;
}
