#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef bool B;
typedef long double D;
typedef complex<D> P;
typedef vector<P> VP;
typedef struct {P s,t;} L;
typedef vector<L> VL;
typedef struct {P c;D r;} C;
typedef vector <C> VC;

const D eps=1.0e-10;
const D pi=acos(-1.0);
template<class T> bool operator==(T a, T b){return abs(a-b)< eps;}
template<class T> bool operator< (T a, T b){return a < b-eps;}
template<class T> bool operator<=(T a, T b){return a < b+eps;}
//template<class T> int sig(T r) {return (r==0||r==-0) ? 0 : r > 0 ? 1 : -1;}
template<class T> int sig(T a,T b = 0) {return a < b ? -1 : b > a ? 1 : 0;}
#define X real()
#define Y imag()

D ip(P a, P b) {return a.X * b.X + a.Y * b.Y;}
D ep(P a, P b) {return a.X * b.Y - a.Y * b.X;}
D sq(D a) {return sqrt(max(a, (D)0));}
P vec(L l){return l.t-l.s;}
inline P input(){D x,y;cin >> x >> y; return P(x,y);}

// $BE@$HD>@~$N5wN%(B
D dLP(L l,P p){return abs( ep(vec(l),p-l.s) )/abs(vec(l));}
// $BE@$H@~J,$N5wN%(B
D dSP(L s,P p){
	if (sig( ip( vec(s), p - s.s)) < 0) return abs(p - s.s);
	if (sig( ip(-vec(s), p - s.t)) < 0) return abs(p - s.t);
	return dLP(s,p);
}

int main(void){
    P p = input();

    int n; cin >> n;
    VP vp(n);
    for(auto & e : vp) e = input();

    D res = 1e10;
    rep(i, n){
        L l = {vp[i], vp[(i + 1) % n]};
        res = min(res, dSP(l, p));
    }

    printf("%.12Lf\n", res);

	return 0;
}
