#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int h, w, ver, hor; cin >> h >> w >> ver >> hor;
    vector<vi> f(h, vi(w));
    rep(y, h){
        string in; cin >> in;
        rep(x, w){
            if(in[x] == 'S') f[y][x] = 1;
        }
    }

    int cnt4, cnt3, cnt2v, cnt2h;
    bool fst = false;
    cnt4 = cnt3 = cnt2v = cnt2h = 0;
    rep(y, h / 2){
        rep(x, w / 2){
            int oy = h - 1 - y, ox = w - 1 - x;
            int cnt = f[y][x] + f[oy][x] + f[y][ox] + f[oy][ox];
            if(cnt == 0) continue;
            if(cnt == 4) cnt4++;
            else if(cnt == 3) cnt3++, fst = true;
            else if(cnt == 2){
                if((f[y][x] and f[oy][x]) or (f[y][ox] and f[oy][ox])) cnt2v++;
                else if((f[y][x] and f[y][ox]) or (f[oy][x] and f[oy][ox])) cnt2h++;
                else fst = true;
            }
            else fst = true;
        }
    }
    // cerr << cnt4 << " " << cnt3 << " " << cnt2h << " " << cnt2v << endl;
    // cerr << fst << endl;

    if(cnt3 >= 1){
        if((cnt2v + cnt3) * ver > (cnt2h + cnt3) * hor){
            cnt2v += cnt3;
            cnt2h = 0;
        }
        else {
            cnt2h += cnt3;
            cnt2v = 0;
        }
        cnt3 = 0;
    }
    else if(cnt2v >= 1 and cnt2h >= 1){
        if((cnt2v - (1 - fst)) * ver > (cnt2h - (1 - fst)) * hor){
            cnt2h = 0;
        }
        else cnt2v = 0;
        fst = true;
    }

    int res = hor + ver;
    if(cnt2v >= 1 or cnt2h >= 1){
        assert(cnt2v == 0 or cnt2h == 0);
        if(cnt2v >= 1) res += (cnt2v - (1 - fst)) * ver;
        else  res += (cnt2h - (1 - fst)) * hor;
        fst = true;
    }

    if(cnt4 >= 1){
        if(fst) res += hor + ver;
        res += max(ver, hor) * cnt4 + (hor + ver) * (cnt4 - 1);
    }

    cout << res << endl;

    return 0;
}
