#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<string> str(n);
    for(auto & e : str) cin >> e;

    const string T = "monica";
    string res;
    rep(y, n - 1){
        rep(x, n){
            if(T.find(str[y][x]) != string::npos){
                res.push_back(str[y + 1][x]);
            }
        }
    }

    cout << res << endl;

	return 0;
}
