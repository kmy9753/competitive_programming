#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<string> vs;
typedef tuple<string, string, int> Man;
vector<Man> m;
int n;
map<string, int> stat;
set<string> s, res;

void dfs(int idx){
    if(idx == n){
        if(res.size() < s.size()){
            res = s;
        }
        return;
    }

    string ss, u; int v;
    tie(ss, u, v) = m[idx];

    // good
    if(stat[ss] != -1 &&
       stat[u] != v * -1 && (ss != u || v == 1)){

        int preu = stat[u], press = stat[ss];
        stat[ss] = 1;
        stat[u] = v;
        s.insert(ss);

        dfs(idx + 1);

        s.erase(ss);
        stat[ss] = press;
        stat[u] = preu;
    }

    // bad
    if(stat[ss] != 1 &&
       stat[u] != v){

        int preu = stat[u], press = stat[ss];
        stat[ss] = -1;
        stat[u] = v * -1;

        dfs(idx + 1);

        stat[ss] = press;
        stat[u] = preu;
    }
}

int main(void){
    cin >> n;

    m = vector<Man>(n);
    for(auto & e : m) cin >> get<0>(e), stat[get<0>(e)] = 0;

    for(auto & e : m){
        string tmp; string b;
        cin >> get<1>(e) >> tmp >> tmp >> b >> tmp;
        get<2>(e) = (b == "good" ? 1:-1);
    }

    sort(m.begin(), m.end());

    dfs(0);
    if(res.size() == 0){
        cout << "No answers" << endl;
    }
    else {
        for(auto & e : res){
            cout << e << endl;
        }
    }

    return 0;
}
