#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
int S, Z;
int inf = 1 << 28;
pair<int, string> res(inf, "#####");
typedef tuple<int, ll, int, string, int> State;
typedef tuple<int, int> Edge;

int n;
vector<vector<Edge>> edge;
vector<string> str;

void bfs(State cur, int to){
    queue<State> q;
    q.push(cur);

    while(q.size()){
        int cuv, cc, cv;
        ll cue;
        string cs;

        tie(cuv, cue, cc, cs, cv) = q.front();
        q.pop();

        if(cv == to){
            if(to == S){
                cs = cs.substr(0, cs.size() - str[S].size());
                res = min(res, make_pair(cc, cs));
                return;
            }
            else {
                cuv = (1 << cv);
                bfs(State(cuv, cue, cc, cs, cv), S);
                continue;
            }
        }

        int nc = cc + 1;
        for(auto & e : edge[cv]){
            int idx_e, nv;
            tie(nv, idx_e) = e;

            if((cuv >> nv) & 1 || (cue >> idx_e) & 1L){
                continue;
            }

            int nuv = cuv | (1 << nv);
            ll  nue = cue | (1L << idx_e);
            string ns = cs + str[nv];

            q.push(State(nuv, nue, nc, ns, nv));
        }
    }
}

int main(void){
    cin >> n;
    edge = vector<vector<Edge>>(n);

    str = vector<string>(n);
    for(auto & e : str) cin >> e;
    sort(begin(str), end(str));

    int m; cin >> m;
    rep(i, m){
        string as, bs; cin >> as >> bs;
        int a = find(begin(str), end(str), as) - begin(str);
        int b = find(begin(str), end(str), bs) - begin(str);

        edge[a].push_back(Edge(b, i));
        edge[b].push_back(Edge(a, i));
    }
    for(auto & e : edge) sort(begin(e), end(e));
    
    string c, d; cin >> c >> d;
    S = find(begin(str), end(str), c) - begin(str);
    Z = find(begin(str), end(str), d) - begin(str);

    bfs(State(0, 0L, 0, str[S], S), Z);

    cout << res.second << endl;

	return 0;
}
