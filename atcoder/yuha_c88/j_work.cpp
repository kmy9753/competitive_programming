#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    vector<int> ns = { 10000, 252035059, 20, 8, 40000, 5000, 10, 1000 };
    int sum = 0;
    for(auto & e : ns) sum += e;

    stringstream ss;
    ss << sum;
    string s = ss.str();
    
    cout << sum << endl;

    for(auto & c : s) c += 'A' - '0';

    cout << s << endl;

	return 0;
}
