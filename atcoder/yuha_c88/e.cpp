#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

int main(void){
    int n; cin >> n;
    vector<vector<string>> s(n, vector<string>(2));

    for(auto & vec : s){
        string str; cin >> str;

        int idx = 0;
        rep(i, 2){
            if(str[idx] & 128){
                vec[i] = str.substr(idx, 3);
                idx += 3;
            }
            else {
                vec[i] = str.substr(idx, 1);
                idx++;
            }
        }
    }

    vector<vector<string>> l(5, vector<string>(5));
    for(auto & vec: l){
        string str; cin >> str;

        int idx = 0;
        rep(i, 5){
            if(str[idx] & 128){
                vec[i] = str.substr(idx, 3);
                idx += 3;
            }
            else {
                vec[i] = str.substr(idx, 1);
                idx++;
            }
        }
    }

    vector<string> chs = {l[0][2], l[2][0], l[4][2], l[2][4]};
    //for(auto & e: chs) cout << e << endl;
    vector<string> arr = {l[1][2], l[2][1], l[3][2], l[2][3]};
    vector<bool> chIsFront = {arr[0] == "$B",(B", arr[1] == "$B"+(B", arr[2] == "$B"-(B", arr[3] == "$B"*(B"};
    //vector<bool> chIsFront = {0, 1, 1, 1};

    set<string> S;
    map<string, set<string>> m_S;
    //cerr << "s_str: " << endl;
    for(auto & vec : s){
        for(auto & c : vec){
            S.insert(c);
        }
        m_S[vec[0]].insert(vec[1]);
    }

    string res = "aaaaa";
    for(auto & e : S){
        bool ok = true;
        rep(i, 4){
            vector<string> cur = {chs[i], e};
            if(chIsFront[i]){
                swap(cur[0], cur[1]);
            }

            //for(auto & e : cur) cerr << e << " ";
            //cerr << endl;

            if(m_S[cur[0]].find(cur[1]) == end(m_S[cur[0]])){
                ok = false;
                break;
            }
        }
//        cerr << endl;
        if(ok){
            res = e;
            break;
        }
    }

    cout << res << endl;

	return 0;
}
