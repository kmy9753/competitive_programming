#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<vector<string>> str(n, vector<string>(3));
    for(auto & e : str) for(auto & ee : e) cin >> ee;

    string res;
    for(auto & e : str){
        if(e[0] == "BEGINNING"){
            res.push_back(e[2][0]);
        }
        if(e[0] == "MIDDLE"){
            res.push_back(e[2][e[2].size() / 2]);
        }
        if(e[0] == "END"){
            res.push_back(e[2][e[2].size() - 1]);
        }
    }

    cout << res << endl;

	return 0;
}
