#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int n, m;
bool END = false;
typedef pair<int, int> pii;
vector<string> key;
vector<vector<pii>> yoko(30), tate(30);
vector<string> field;

#define fst first
#define snd second

void dfs(int d){
    if(d == n){
        bool ok = true;
        rep(y, m){
            rep(x, m){
                if(field[y][x] == '.'){
                    ok = false;
                    break;
                }
            }
        }
        END |= ok;
        return;
    }

    string ck = key[d];
    int nd = d + 1;

    for(auto & p : tate[ck.size()]){
        int sz = 0;

        // check
        while((int)sz < ck.size() && (field[p.fst + sz][p.snd] == '.' || field[p.fst + sz][p.snd] == ck.at(sz))){
            sz++;
        }
        if((int)sz != ck.size()) continue;

        // fill
        string pre = ck;
        rep(i, (int)ck.size()){
            pre[i] = field[p.fst + i][p.snd];
        }
        rep(i, (int)ck.size()){
            field[p.fst + i][p.snd] = ck[i];
        }

        dfs(nd);
        if(END) return;

        rep(i, (int)ck.size()){
            field[p.fst + i][p.snd] = pre[i];
        }
    }

    for(auto & p : yoko[ck.size()]){
        int sz = 0;

        // check
        while((int)sz < ck.size() && (field[p.fst][p.snd + sz] == '.' || field[p.fst][p.snd + sz] == ck.at(sz))){
            sz++;
        }
        if((int)sz != ck.size()) continue;

        // fill
        string pre = ck;
        rep(i, (int)ck.size()){
            pre[i] = field[p.fst][p.snd + i];
        }
        rep(i, (int)ck.size()){
            field[p.fst][p.snd + i] = ck[i];
        }

        dfs(nd);
        if(END) return;

        rep(i, (int)ck.size()){
            field[p.fst][p.snd + i] = pre[i];
        }
    }
}

int main(void){
    cin >> n;
    key = vector<string>(n);
    for(auto & e : key) cin >> e;

    cin >> m;
    field = vector<string>(m + 2);
    rep(y, m + 2){
        field[y].push_back('#');
        if(y == 0 || y == m + 1){
            rep(x, m){
                field[y].push_back('#');
            }
        }
        else {
            string in; cin >> in;
            field[y] += in;
        }
        field[y].push_back('#');
    }

    range(y, 1, m + 1){
        range(x, 1, m + 1){
            if(field[y][x] == '#') continue;

            if(field[y - 1][x] == '#'){
                int sz = 0;
                while(field[y + sz][x] == '.') sz++;
                tate[sz].push_back(pii(y, x));
            }

            if(field[y][x - 1] == '#'){
                int sz = 0;
                while(field[y][x + sz] == '.') sz++;
                yoko[sz].push_back(pii(y, x));
            }
        }
    }

    dfs(0);

    cout << m << endl;
    range(y, 1, m + 1){
        range(x, 1, m + 1){
            cout << field[y][x];
        }
        cout << endl;
    }

	return 0;
}
