#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> v(n);
    for(auto & e : v) cin >> e;
    vector<int> f(n);
    for(auto & e : f) cin >> e;

    int res = 0;
    rep(i, n){
        if(f[i] > v[i] / 2) res++;
    }

    cout << res << endl;

	return 0;
}
