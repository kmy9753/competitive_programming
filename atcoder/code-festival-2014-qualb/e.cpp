#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<vi> vvi;

int h, w;
vector<string>  field;
const vi dx = { 1, 0,-1, 0};
const vi dy = { 0,-1, 0, 1};

#define fr first
#define sc second

int main(void){
    cin >> h >> w;

    field = vector<string>(h);
    pii s, z;
    cin >> s.fr >> s.sc; s.fr--, s.sc--;
    cin >> z.fr >> z.sc; z.fr--, z.sc--;
    rep(y, h){
        rep(x, w){
            field[y].push_back('.');
        }
    }

    int n; cin >> n;
    rep(loop, n){
        int r, c, hh, ww; cin >> r >> c >> hh >> ww;
        r--, c--;

        range(y, r, r + hh){
            range(x, c, c + ww){
                field[y][x] = '#';
            }
        }
    }

    if(field[s.fr][s.sc] != '#' || field[z.fr][z.sc] != '#'){
        cout << "NO" << endl;
        return 0;
    }

    bool ok = false;
    queue<pii> q;
    q.push(s);
    vvi used(h, vi(w));
    while(q.size()){
        pii p = q.front();
        q.pop();
        
        if(used[p.fr][p.sc]) continue;
        used[p.fr][p.sc] = true;

        if(p == z){
            ok = true;
            break;
        }

        rep(i, 4){
            pii np(p.fr + dy[i], p.sc + dx[i]);

            if(np.fr < 0 || h <= np.fr ||
               np.sc < 0 || w <= np.sc ||
               field[np.fr][np.sc] != '#'){
                continue;
            }

            q.push(np);
        }
    }

    cout << (ok ? "YES":"NO") << endl;

    return 0;
}
