#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    int n, t; cin >> n >> t;

    vi a(n);
    for(auto & e : a) cin >> e;

    vi cnt(t + 1);
    for(auto & e : a){
        range(i, 1, 1000){
            int tt = e * i;
            if(tt > t) break;

            cnt[tt]++;
        }
    }

    cout << *max_element(cnt.begin(), cnt.end()) << endl;

	return 0;
}
