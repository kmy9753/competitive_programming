#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
int main(void){
    int n, p; cin >> n >> p;

    vvi dp(n + 1, vi(n + 1));
    rep(i, n + 1) dp[0][i] = false;

    range(i, 1, n + 1){
        range(j, 1, n + 1){
            range(k, 1, j + 1){
                if(i - k < 0) continue;
                dp[i][j] |= !dp[i - k][min(k + 1, n)];
            }
        }
    }

    cout << (dp[n][p] ? "first":"second") << endl;

	return 0;
}
