#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
const int mod = 1e9 + 7;
int main(void){
    string x; cin >> x;
    vector<string> p(2); rep(i, 2) cin >> p[i];

    vi dp(x.size() + 1);
    dp[0] = 1;

    range(i, 1, (int)x.size() + 1){
        for(auto & e : p){
            if((int)(i - e.size()) >= 0 && x.substr(i - e.size(), e.size()) == e){
                (dp[i] += dp[i - e.size()]) %= mod;
            }
        }
    }

    cout << dp[x.size()] << endl;

	return 0;
}
