#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
vector<set<int>> s(26);
vi used(26);
vi odd(26);

int dfs(int p){
    if(used[p]) return 0;
    used[p] = true;

    int ret = 0;
    if(odd[p]) ret++;

    for(auto & e : s[p]){
        ret += dfs(e);
    }

    return ret;
}

int main(void){
    int n; cin >> n;
    vi occ(26);
    rep(loop, n){
        string a; cin >> a;
        int l = a[0] - 'a', r = a[a.size() - 1] - 'a';
        occ[l] = occ[r] = true;
        odd[l] ^= 1, odd[r] ^= 1;

        s[l].insert(r);
        s[r].insert(l);
    }

    int res = 0;
    bool fst = true;
    rep(i, 26){
        if(!occ[i]) continue;
        if(!fst && !used[i]) res++;
        fst = false;

        int odd_cnt = dfs(i);
        if(odd_cnt == 0 || odd_cnt == 2) continue;

        res += (odd_cnt - 2) / 2;
    }

    cout << res << endl;

	return 0;
}
