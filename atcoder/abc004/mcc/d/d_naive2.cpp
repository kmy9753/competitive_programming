#include <iostream>

using namespace std;

const int offset = 500;

int num_ball[1000] = {0};

int calc(int base, int num){
    base += offset;
    num_ball[base] = num;

    int ret = 0;

    int sign = 1;
    while(num_ball[base] >= 2){
        int k = 1;
        while(num_ball[base + sign * k] != 0){
            k++;
        }
        ret += k;

        num_ball[base + sign * k] = 1;
        num_ball[base]--;
        sign *= -1;
    }

    return ret;
}

int main(void){
    int R, G, B; cin >> R >> G >> B;

    int res = calc(-100, R)
            + calc(   0, G)
            + calc( 100, B);

    cout << res << endl;

    return 0;
}

