#include <iostream>

using namespace std;

const int offset = 500;

int main(void){
    int R, G, B; cin >> R >> G >> B;
    int res = 0;

    int num_ball[1000] = {0};
    num_ball[offset - 100] = R;
    num_ball[offset +   0] = G;
    num_ball[offset + 100] = B;

    int sign = 1;
    while(num_ball[offset - 100] >= 2){
        int k = 1;
        while(num_ball[offset - 100 + sign * k] != 0){
            k++;
        }
        res += k;

        num_ball[offset - 100 + sign * k] = 1;
        num_ball[offset - 100]--;
        sign *= -1;
    }

    sign = 1;
    while(num_ball[offset + 0] >= 2){
        int k = 1;
        while(num_ball[offset + 0 + sign * k] != 0){
            k++;
        }
        res += k;

        num_ball[offset + 0 + sign * k] = 1;
        num_ball[offset + 0]--;
        sign *= -1;
    }

    sign = 1;
    while(num_ball[offset + 100] >= 2){
        int k = 1;
        while(num_ball[offset + 100 + sign * k] != 0){
            k++;
        }
        res += k;

        num_ball[offset + 100 + sign * k] = 1;
        num_ball[offset + 100]--;
        sign *= -1;
    }

    cout << res << endl;

    return 0;
}

