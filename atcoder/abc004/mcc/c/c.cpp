#include <iostream>
#include <utility>

using namespace std;

const int N = 5;

int main(void){
    int n; cin >> n;
    n %= 30;

    int a[N + 1];
    for(int i = 0; i < N + 1; i++){
        a[i] = i + 1;
    }

    for(int i = 0; i < n; i++){
        swap(a[i % N], a[i % N + 1]);
    }

    for(int i = 0; i < N + 1; i++){
        cout << a[i];
    }
    cout << endl;
    
    return 0;
}

