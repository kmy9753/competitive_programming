#include <iostream>

const int N = 4;

using namespace std;

int main(void){
    char field_org[N][N], field[N][N];

    // 入力
    for(int y = 0; y < N; y++){
        for(int x = 0; x < N; x++){
            cin >> field_org[y][x];
        }
    }

    // 処理
    for(int y = 0; y < N; y++){
        for(int x = 0; x < N; x++){
            field[y][x] = field_org[N - 1 - y][N - 1 - x];
        }
    }

    // 出力
    for(int y = 0; y < N; y++){
        for(int x = 0; x < N; x++){
            cout << field[y][x];
            cout << " ";
        }
        cout << endl;
    }

    return 0;
}

