#include <bits/stdc++.h>
typedef long long ll;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> pii;
#define fst first
#define snd second
int main(void){
    int n; cin >> n;
    vector<pii> st(n);
    vi cnte(1e5+2);
    for(auto & e : st){
        cin >> e.fst >> e.snd;
    }

    vi ims(1e5+2);
    for(auto & e : st){
        ims[e.fst]++;
        ims[e.snd]--;
    }
    vi sum(1e5+2);
    int mx = 0, ls, rs;
    range(i, 1, 1e5 + 2){
        sum[i] = sum[i - 1] + ims[i];
        if(sum[i] > mx){
            ls = i;
            rs = i;
            mx = sum[i];
        }
        else if(sum[i] == mx){
            rs = i;
        }
    }

    rep(i, n){
        if(st[i].fst <= ls && rs < st[i].snd){
            cout << mx - 1 << endl;
            return 0;
        }
    }

    cout << mx << endl;
    

	return 0;
}
