#include <bits/stdc++.h>
typedef long long ll;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> pii;
#define fst first
#define snd second
int main(void){
    string s; cin >> s;

    int hcnt = 0, ecnt = 0;
    rep(i, (int)s.size()){
        if(s[i] == '!') break;
        hcnt++;
    }
    for(int i = s.size() - 1; i >= 0; i--){
        if(s[i] == '!') ecnt++;
    }

    hcnt %= 2;
    if(ecnt == 0){
        rep(i, hcnt) {
            cout << "-";
        }
    }
    else {
        rep(i, hcnt){
            cout << "-";
        }
        ecnt %= 2;
        if(ecnt == 0) ecnt = 2;
        rep(i, ecnt){
            cout << "!";
        }
    }
    puts("");


	return 0;
}
