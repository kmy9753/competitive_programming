#include <bits/stdc++.h>
typedef long long ll;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> pii;
#define fst first
#define snd second
int main(void){
    int n; cin >> n;
    string s; cin >> s;
    
    int stks = 0, stkn = 0, res = 0;
    rep(i, (int)s.size()){
        if(s.substr(i, 2) == "10"){
            i++;
        }
        else if(s.substr(i, 2) == "01"){
            i++;
        }
        else if(s[i] == '0'){
            if(stks > 0){
                stks--;
            }
            else {
                res++;
                stkn++;
            }
        }
        else {
            if(stkn > 0){
                stkn--;
            }
            else {
                res++;
                stks++;
            }
        }
    }
    cout << res << endl;

	return 0;
}
