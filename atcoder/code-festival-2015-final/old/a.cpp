#include <bits/stdc++.h>
typedef long long ll;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> pii;
#define fst first
#define snd second
int main(void){
    string s, t, u; cin >> s >> t >> u;

    if(s.size() == 5 && t.size() == 7 && u.size() == 5){
        cout << "valid" << endl;
    }
    else
        cout << "invalid" << endl;

	return 0;
}
