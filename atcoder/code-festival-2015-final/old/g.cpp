#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> pii;
#define fst first
#define snd second
int mod = 1e9+7;
signed main(void){
    int n; cin >> n;

    vi c(n);
    for(auto & e : c){
        cin >> e;
    }

    if(c[0] != 1) {
        cout << 0 << endl;
        return 0;
    }
    if(n == 2) {
        cout << 1 << endl;
        return 0;
    }

    vi dp(n);
    dp[0] = 1, dp[1] = 1;
    range(i, 2, n){
        dp[i] = dp[i - 1];
        int idx = i - 2;
        while(idx >= 0){
            if(c[idx + 1] < c[i]){
                (dp[i] += dp[idx]) %= mod;
            }
            idx--;
        }
    }

    cout << dp[n - 1] << endl;

	return 0;
}
