#include <bits/stdc++.h>
typedef long long ll;
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> pii;
#define fst first
#define snd second
int main(void){
    int n; cin >> n;

    vector<vector<__float128>> dp(n + 1, vector<__float128>(n * 6 + 1, 0.0));
    dp[0][0] = 1.0;
    range(i, 1, n + 1){
        rep(j, n * 6 + 1){
            range(k, 1, 7){
                if(j - k >= 0){
                    dp[i][j] += dp[i - 1][j - k] / 6.0;
                }
            }
        }
    }

    cout <<  max_element(begin(dp[n]), end(dp[n])) - begin(dp[n]) << endl;

	return 0;
}
