#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    vector<double> y(3);
    vector<double> t = {0.0, sqrt(2), 2.0 * sqrt(2)};

    rep(i, 3){
        printf("? %.12f\n", t[i]); fflush(stdout);
        scanf("%lf", &y[i]);
    }

    double h = y[0];
    y[1] -= h, y[2] -= h;

    double g = y[1] - y[2] / 2.0;
    double v = y[1] + g;

    printf("! %.12f %.12f %.12f\n", h, v, g); fflush(stdout);

	return 0;
}
