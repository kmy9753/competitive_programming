#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

#include <sys/time.h>

typedef vector<int> vi;
typedef vector<vi> vvi;

double gett(){
  struct timeval t;
  gettimeofday(&t,NULL);
  return t.tv_sec + t.tv_usec/1e6;
}

vvi mul(vvi & a, vvi & b){
    vvi c(a.size(), vi(b[0].size()));

    rep(i, (int)a.size()){
        rep(j, (int)b[0].size()){
            rep(k, (int)a[0].size()){
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }

    return c;
}

int main(void){
    double st = gett();
    auto myrand = bind(uniform_int_distribution<int>(-1000, 1000), mt19937(static_cast<unsigned int>(time(nullptr))));

    int n; cin >> n;
    vvi a(n, vi(n)), b(n, vi(n)), c(n, vi(n));

    for(auto & ev : a) for(auto & e : ev) cin >> e;
    for(auto & ev : b) for(auto & e : ev) cin >> e;
    for(auto & ev : c) for(auto & e : ev) cin >> e;

    bool ok = true;
    while(gett() - st < 1.8){
        vvi x(n, vi(1));
        for(auto & e : x) e[0] = myrand();

        vvi tmp = mul(b, x);
        vvi lhs = mul(a, tmp);
        vvi rhs = mul(c, x);
        if(lhs != rhs){
            ok = false;
            break;
        }
    }

    cout << (ok ? "YES":"NO") << endl;

	return 0;
}

