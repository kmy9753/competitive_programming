#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, k; cin >> n >> k;
    vector<string> a(n);

    for(auto & e : a){
        cin >> e;
    }

    sort(begin(a), end(a), greater<string>());

    for(auto & e : a){
        cout << e;
    }
    puts("");

	return 0;
}
