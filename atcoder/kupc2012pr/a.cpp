#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;
ll f(int m, int n){
    ll ret;

    if(m == 0){
        ret = n + 1;
    }
    else if(m == 1){
        ret = n + 2;
    }
    else if(m == 2){
        ret = 2 * n + 3;
    }
    else {
        ret = (1L << (n + 3)) - 3;
    }

    return ret;
}

int main(void){
    int m, n; cin >> m >> n;

    cout << f(m, n) << endl;

	return 0;
}
