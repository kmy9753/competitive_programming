#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
bool isPrime(int n){
    if(n == 1) return false;

    for(int i = 2; i <= sqrt(n); i++){
        if(n % i == 0) return false;
    }

    return true;
}

int main(void){
    int n; cin >> n;

    string res = "Not Prime";
    if(isPrime(n) || (n != 1 && n % 2 != 0 && n % 5 != 0 && n % 3 != 0)){
        res = "Prime";
    }

    cout << res << endl;

	return 0;
}
