#include <bits/stdc++.h>

typedef long long ll;
#define int ll

#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int mod = 1e9 + 7;
typedef vector<int> vi;

int p(int a, int n){
    int ret = 1;
    while(n > 0){
        if(n & 1) (ret *= a) %= mod;
        n >>= 1;
        (a *= a) %= mod;
    }

    return ret;
}

signed main(void){
    int n; cin >> n;

    vi a(n);
    vi cnt(n + 1);
    for(auto & e : a){
        cin >> e;
        cnt[e]++;
    }

    if(a[0] != 0 || cnt[0] >= 2){
        cout << 0 << endl;
        return 0;
    }

    int res = 1;
    rep(i, n){
        (res *= p(2, cnt[i] * (cnt[i] - 1) / 2)) %= mod;
        (res *= p((p(2, cnt[i]) - 1 + mod) % mod, cnt[i + 1])) %= mod;
    }

    cout << res << endl;

	return 0;
}
