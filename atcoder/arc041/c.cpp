#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef pair<int, int> pii;
#define fst first
#define snd second
enum {L = 0, R};

signed main(void){
    int n, l; cin >> n >> l;

    vector<pii> xd(n);
    for(auto & e : xd){
        char d;
        cin >> e.fst >> d; e.fst--;
        e.snd = d == 'R' ? R:L;
    }

    int res = 0;
    int tarx = 0;
    for(auto & e : xd){
        if(e.snd == R){
            tarx = -1;
        }
        else {
            if(tarx == -1){
                tarx = e.fst + 1;
            }
            else {
                res += e.fst - tarx;
                e.fst = tarx;
                tarx++;
            }
        }
    }
    reverse(begin(xd), end(xd));
    tarx = l - 1;
    for(auto & e : xd){
        if(e.snd == L){
            tarx = -1;
        }
        else {
            if(tarx == -1){
                tarx = e.fst - 1;
            }
            else {
                res += tarx - e.fst;
                e.fst = tarx;
                tarx--;
            }
        }
    }
    reverse(begin(xd), end(xd));

    int idx = 0;
    while(idx < n){
        int rcnt = 0, lcnt = 0;
        while(idx < n && xd[idx].snd == R) rcnt++, idx++;
        if(rcnt == 0 || idx == n){
            idx++;
            continue;
        }
        int dist = xd[idx].fst - xd[idx - 1].fst - 1;
        while(idx < n && xd[idx].snd == L) lcnt++, idx++;
        
        res += max(rcnt, lcnt) * dist;
    }

    cout << res << endl;

	return 0;
}
