#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int x, y; cin >> x >> y;
    int k; cin >> k;

    if(y - k >= 0){
        y -= k;
        x += k;
    }
    else {
        x += y;
        k -= y;
        x -= k;
    }

    cout << x << endl;

	return 0;
}
