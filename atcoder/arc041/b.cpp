#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
int main(void){
    int n, m; cin >> n >> m;
    vvi b(n, vi(m));
    for(auto & row : b){
        string in; cin >> in;
        rep(i, m){
            row[i] = in.at(i) - '0';
        }
    }

    vvi a(n, vi(m));
    rep(i, n){
        rep(j, m){
            int & bb = b[i][j];
            if(bb != 0){
                a[i + 1][j] += bb;
                b[i + 1][j - 1] -= bb;
                b[i + 1][j + 1] -= bb;
                b[i + 2][j]     -= bb;
                bb = 0;
            }
        }
    }

    for(auto & row : a){
        for(auto & e : row){
            cout << e;
        }
        puts("");
    }

	return 0;
}
