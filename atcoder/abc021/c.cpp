#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

const int mod = 1e9 + 7;

int main(void){
    int n; cin >> n;
    int a, b; cin >> a >> b;
    a--, b--;

    int m; cin >> m;
    vvi adj(n, vi(n));

    rep(loop, m){
        int x, y; cin >> x >> y;
        x--, y--;

        adj[x][y] = adj[y][x] = 1;
    }

    // (cost, v) = num
    vvi dp(n + 1, vi(n));
    dp[0][a] = 1;

    int res = -1;

    range(i, 1, n + 1){
        rep(j, n){
            rep(k, n){
                (dp[i][j] += dp[i - 1][k] * adj[j][k]) %= mod;
            }
            if(j == b && dp[i][j] != 0){
                res = dp[i][j];
                goto END;
            }
        }
    }

END:
    cout << res << endl;

	return 0;
}
