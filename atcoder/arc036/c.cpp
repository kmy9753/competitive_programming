#include <bits/stdc++.h>
#define int long long 
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
int mod = 1e9 + 7;
signed main(void){
	int n, m; cin >> n >> m;
	string s; cin >> s;
 
	vector<vvi> dp(n + 1, vvi(2 * m + 1, vi(2 * m + 1)));
	dp[0][m][m] = 1;
 
	range(i, 1, n + 1){
		rep(j, 2 * m + 1){
			rep(k, 2 * m + 1){
				if(s.at(i - 1) != '1' and j - 1 >= 0 and k + 1 < 2 * m + 1){
                    // case 0
					(dp[i][j][k] += dp[i - 1][j - 1][k + 1]) %= mod;
				}
                if(s.at(i - 1) != '0' and j + 1 < 2 * m + 1 and k - 1 >= 0){
                    // case 1
					(dp[i][j][k] += dp[i - 1][j + 1][k - 1]) %= mod;
                }
			}
		}
	}

    int res = 0;
    for(auto & ee : dp[n]){
        for(auto & e : ee){
            (res += e) %= mod;
        }
    }

    cout << res << endl;
 
	return 0;
}
