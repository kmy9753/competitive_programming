#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> h(n);
    for(auto & e : h) cin >> e;

    bool rise = true;
    int pre = -1, cnt = 0, res = 0;
    rep(i, n){
        if(rise){
            if(h[i] > pre){
                cnt++;
            }
            else if(h[i] < pre){
                cnt++;
                rise = false;
            }
            else {
                res = max(res, cnt);
                cnt = 0;
            }
        }
        else {
            if(h[i] < pre){
                cnt++;
            }
            else {
                res = max(res, cnt);
                pre = -1;
                cnt = 0;
                rise = true;
                continue;
            }
        }
        pre = h[i];
    }
    res = max(res, cnt);

    cout << res << endl;

	return 0;
}
