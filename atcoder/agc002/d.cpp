#include <bits/stdc++.h>
#define int long long 
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

struct UnionFind {
    vector<int> data;
    UnionFind(int size) : data(size, -1) { }
    bool unionSet(int x, int y) {
        x = root(x); y = root(y);
        if (x != y) {
            if (data[y] < data[x]) swap(x, y);
            data[x] += data[y]; data[y] = x;
        }
        return x != y;
    }
    bool findSet(int x, int y) {
        return root(x) == root(y);
    }
    int root(int x) {
        return data[x] < 0 ? x : data[x] = root(data[x]);
    }
    int size(int x) {
        return -data[root(x)];
    }
    int calc(int x, int y){
        if(findSet(x, y)){
            return size(x);
        }
        return size(x) + size(y);
    }
};

using Edge = tuple<int, int>;
using ZXY = tuple<int, int, int>;

signed main(void){
    int n, m; cin >> n >> m;

    vector<Edge> edges(m);
    for(auto & e : edges){
        int a, b; cin >> a >> b;
        a--, b--;
        e = Edge(a, b);
    }

    int Q; cin >> Q;
    vector<ZXY> zxy(Q);
    for(auto & e : zxy){
        int x, y, z; cin >> x >> y >> z;
        x--, y--;
        e = ZXY(z, x, y);
    }

    int sz = 1, depth = 0;
    while(m > sz) sz <<= 1, depth++;
    vector<vi> s(sz * 2);
    rep(i, Q) s[0].push_back(i);

    vi mins(sz), maxs(sz);
    int idx = 0;
    rep(i, depth){
        rep(j, 1 << i){
            mins[idx] = sz / (1 << i) * j;
            maxs[idx] = sz / (1 << i) * (j + 1);
            idx++;
        }
    }

    idx = 0;
    rep(i, depth){
        UnionFind uf(n);

        rep(j, sz){
            if(j < m){
                int a, b; tie(a, b) = edges[j];
                uf.unionSet(a, b);
            }

            int mid = (mins[idx] + maxs[idx]) / 2;

            if(mid == j + 1){
                for(auto & e : s[idx]){
                    int x, y, z; tie(z, x, y) = zxy[e];
                    if(uf.calc(x, y) >= z){
                        s[idx * 2 + 1].push_back(e);
                    }
                    else {
                        s[idx * 2 + 2].push_back(e);
                    }
                }
                idx++;
            }
        }
    }

    vi res(Q);
    rep(i, m){
        for(auto & e : s[sz - 1 + i]){
            res[e] = i + 1;
        }
    }

    for(auto & e : res){
        cout << e << endl;
    }

    return 0;
}
