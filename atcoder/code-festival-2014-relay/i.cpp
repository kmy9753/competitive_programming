#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
constexpr ll inf = 1 << 50;
typedef tuple<int, int> State;
signed main(void){
    int n, m, s, d; cin >> n >> m >> s >> d;
    s--, d--;

    vector<tuple<int, int, int>> abc(n);
    for(auto && e : abc){
        int a, b, c; cin >> a >> b >> c;
        e = make_tuple(a, b, c);
    }

    vector<vector<pair<int, int>>> edge(n);
    rep(loop, m){
        int x, y, t; cin >> x >> y >> t;
        x--, y--;

        edge[x].push_back(make_pair(y, t));
        edge[y].push_back(make_pair(x, t));
    }

    priority_queue<State, vector<State>, greater<State>> q;
    q.push(State(0, s));

    vector<int> min_dist(n, inf);

    while(q.size()){
        int cur_c, cur_v;
        tie(cur_c, cur_v) = q.top(); q.pop();

        if(min_dist[cur_v] != inf) continue;
        min_dist[cur_v] = cur_c;

        int a, b, c;
        tie(a, b, c) = abc[cur_v];
        int diff_c = 0;
        if(cur_c < c){
            diff_c = c - cur_c;
        }
        else {
            int cc = (cur_c - c) % (a + b);

            if(cc >= a){
                diff_c += (a + b) - cc;
            }
        }

        for(auto && e : edge[cur_v]){
            int next_c = cur_c + diff_c + e.second;
            int next_v = e.first;

            if(min_dist[next_v] != inf) continue;

            q.push(State(next_c, next_v));
        }
    }

    cout << min_dist[d] << endl;

	return 0;
}
