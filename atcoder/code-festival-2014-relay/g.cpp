#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
const int inf = 1 << 24;

int main(void){
    int n, m; cin >> n >> m;

    vi a(n);
    for(auto && e : a) cin >> e;

    vi dp(21000, inf);
    dp[0] = 1;

    int res = inf;
    rep(i, n){
        for(int j = 21000 - a[i]; j >= 0; j--){
            if(dp[j] == inf) continue;

            dp[j + a[i]] = 1;
            if(j + a[i] >= m) res = min(res, j + a[i]);
        }
    }

    if(res == inf) res = -1;

    cout << res << endl;

	return 0;
}
