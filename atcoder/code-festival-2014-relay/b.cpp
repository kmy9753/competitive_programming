#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int s, a; cin >> s >> a;

    cout << (a <= s ? "Congratulations!":"Enjoy another semester...") << endl;

	return 0;
}
