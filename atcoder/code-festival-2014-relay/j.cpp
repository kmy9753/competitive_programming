#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, k; cin >> n >> k;

    bool fst = false;
    if(n / 2 <= k || n % 2) fst = true;

    cout << (fst ? "first":"second") << endl;

	return 0;
}
