#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;

int n, res;
vi memo;
vvi edge;

void dfs(int v, int d, int pre){
    if(res != -1) return;
    if(memo[v] != -1){
        res = d - memo[v];
        return;
    }

    memo[v] = d;

    for(auto && u : edge[v]){
        if(u != pre){
            dfs(u, d + 1, v);
        }
    }
}

int main(void){
    cin >> n;
    memo = vi(n, -1);
    res = -1;

    edge = vvi(n);
    rep(loop, n){
        int x, y; cin >> x >> y;
        x--, y--;

        edge[x].push_back(y);
        edge[y].push_back(x);
    }

    dfs(0, 0, -1);
    cout << res << endl;

	return 0;
}
