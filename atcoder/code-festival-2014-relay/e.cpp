#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int h, w; cin >> h >> w;

    int res = 0;
    rep(y, h){
        rep(x, w){
            char c; cin >> c;

            if(c != '.'){
                res += c - '0';
            }
        }
    }

    cout << res << endl;

	return 0;
}
