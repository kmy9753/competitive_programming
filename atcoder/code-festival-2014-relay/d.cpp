#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<string> vs;
int main(void){
    int n; cin >> n;
    vs board(n);
    for(auto && e : board) cin >> e;

    int xcnt = 0, ycnt = 0;
    rep(y, n){
        rep(x, n){
            if(board[y][x] == 'X'){
                xcnt += y;
            }
            if(board[y][x] == 'Y'){
                ycnt += (n - y - 1);
            }
        }
    }

    if(xcnt - ycnt == 1) cout << 'X' << endl;
    else if(xcnt - ycnt ==-1) cout << 'Y' << endl;
    else            cout << "Impossible" << endl;

	return 0;
}
