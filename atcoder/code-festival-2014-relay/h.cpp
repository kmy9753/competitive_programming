#include <bits/stdc++.h>

using namespace std;
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)

signed main(void){
    int n, m; cin >> n >> m;

    vector<int> a(m);
    for(auto && e : a) cin >> e;

    int res = 0;
    rep(i, m){
        res = max(res, (int)(upper_bound(a.begin(), a.end(), a[i] + n) - (a.begin() + i)));
    }

    cout << res << endl;

	return 0;
}
