#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef pair<int, int> pii;
#define fst first
#define snd second
int main(void){
    int n, m; cin >> n >> m;

    vector<pii> yx(n);
    for(auto & e : yx){
        cin >> e.snd >> e.fst;
    }

    set<int> a;
    vector<int> cnta(1e5 + 1);
    rep(i, m){
        int aa; cin >> aa;
        a.insert(aa);
        cnta[aa]++;
    }

    sort(begin(yx), end(yx));

    int res = 0;
    for(auto & e : yx){
        int x = e.snd, y = e.fst;
        auto itr = a.lower_bound(x);
        if(itr != end(a) && *itr <= y){
            res++;
            if(--cnta[*itr] <= 0){
                a.erase(*itr);
            }
        }
    }

    cout << res << endl;

	return 0;
}
