#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;
int main(void){
    long double p; ll n; cin >> p >> n;

    printf("%.12Lf\n", -0.5 * pow(1 - 2 * p, n) + 0.5);

	return 0;
}
