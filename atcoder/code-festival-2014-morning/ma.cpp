#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
#define fst first
#define snd second
typedef pair<int, int> pii;
int main(void){
    int n, m; cin >> n >> m;

    int s, t; cin >> s >> t;
    s--, t--;
    vi st = {s, t};

    vector<vector<pii>> edge(n);
    rep(loop, m){
        int x, y, d; cin >> x >> y >> d;
        x--, y--;
        edge[x].push_back(pii(y, d));
        edge[y].push_back(pii(x, d));
    }

    set<pii> S;

    rep(i, 2){
        priority_queue<pii, vector<pii>, greater<pii>> q;
        q.push(pii(0, st[i]));

        vi minCost(n, -1);
        while(q.size()){
            int cc = q.top().fst, cv = q.top().snd;
            q.pop();

            if(minCost[cv] != -1) continue;
            minCost[cv] = cc;
            if(i == 0) S.insert(pii(cc, cv));
            else {
                if(S.find(pii(cc, cv)) != end(S)){
                    cout << cv + 1 << endl;
                    return 0;
                }
            }

            for(auto & e : edge[cv]){
                int nv = e.fst, nc = cc + e.snd;
                if(minCost[nv] != -1) continue;
                q.push(pii(nc, nv));
            }
        }
    }

    cout << -1 << endl;

	return 0;
}
