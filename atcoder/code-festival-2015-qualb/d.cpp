#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef pair<int, int> pii;
#define fst first
#define snd second
signed main(void){
    int n; cin >> n;

    set<pii> sz = {pii(1, 1e18+1)};
    rep(loop, n){
        int s, c; cin >> s >> c;

        auto itr = sz.lower_bound(pii(s, -1));
        if(itr != begin(sz) && s < prev(itr)->snd){
            itr--;
        }
        else {
            s = itr->fst;
        }

        set<pii> ers, add;
        while(c > 0){
            ers.insert(*itr);

            if(itr->fst < s){
                add.insert(pii(itr->fst, s));
            }

            if(itr->snd - s >= c){
                if(itr->snd - s != c){
                    add.insert(pii(s + c, itr->snd));
                }
                s += c - 1;
                c = 0;
            }
            else {
                c -= (itr->snd - s);
                itr++;
                s = itr->fst;
            }
        }

        for(auto & e : add) sz.insert(e);
        for(auto & e : ers) sz.erase(e);

        cout << s << endl;
    }

	return 0;
}
