#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, m; cin >> n >> m;

    vector<int> a(n), b(m);
    for(auto & e : a) cin >> e; 
    for(auto & e : b) cin >> e; 

    sort(begin(a), end(a));
    sort(begin(b), end(b), greater<int>());

    if(n < m){
        cout << "NO" << endl;
        return 0;
    }

    string res = "YES";
    rep(i, m){
        if(b[i] > a[n - i - 1]){
            res = "NO";
        }
    }

    cout << res << endl;

	return 0;
}
