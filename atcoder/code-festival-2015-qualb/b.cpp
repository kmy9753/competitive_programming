#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, m; cin >> n >> m;
    vector<int> p(m + 1);
    vector<int> a(n);

    for(auto & e : a) cin >> e, p[e]++;

    int res = -1;
    int & aa = *max_element(begin(p), end(p));
    if(aa > n / 2){
        res = max_element(begin(p), end(p)) - begin(p);
        aa = -1;
    }
    if(*max_element(begin(p), end(p)) > n / 2){
        res = -1;
    }

    if(res != -1) cout << res << endl;
    else puts("?");

	return 0;
}
