#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    string s; cin >> s;
    int res = 0;
    bool odd = true;
    for(auto & e : s){
        if(odd){
            res += e - '0';
        }
        else {
            res -= e - '0';
        }
        odd ^= 1;
    }

    cout << res << endl;

	return 0;
}
