#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
#define fst first
#define snd second
int main(void){
    int n; cin >> n;

    vector<int> b(n);
    for(auto & e : b) cin >> e;

    if(n == 1){
        cout << 0 << endl;
        return 0;
    }

    int cnt = 0;
    vector<pair<int, int>> a(n, make_pair(1, 1));

    a[0].fst = b[0];
    a[1].fst = b[0];
    range(i, 1, n){
        if(__gcd(a[i].snd, b[i]) == 1){
            if(i + 1 < n){
                a[i + 1].fst = b[i];
                a[i + 1].snd = b[i] / __gcd(a[i].fst, b[i]);
            }
            else {
                
            }
        }
        else {
            cnt++;
        }
    }

    

	return 0;
}
