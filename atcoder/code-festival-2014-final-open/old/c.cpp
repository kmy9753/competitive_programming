#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int a; cin >> a;

    range(i, 10, 1e4 + 1){
        int cur = i / (int)1e4 * (int)pow(i, 4)
                + i % (int)1e4 / (int)1e3 * (int)pow(i, 3)
                + i % (int)1e3 / (int)1e2 * (int)pow(i, 2)
                + i % (int)1e2 / (int)1e1 * (int)pow(i, 1)
                + i % (int)1e1 / (int)1e0 * (int)pow(i, 0);

        if(a == cur){
            cout << i << endl;
            return 0;
        }
    }
    cout << -1 << endl;

	return 0;
}
