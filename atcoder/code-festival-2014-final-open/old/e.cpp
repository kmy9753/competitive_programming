#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e;
    if(n == 1){
        cout << 0 << endl;
        return 0;
    }
    int pre = a[1], cnt = 2;
    int inc = a[1] - a[0];
    range(i, 2, n){
        if(inc > 0){
            if(a[i] < pre){
                cnt++;
                inc = -1;
            }
        }
        else if(inc < 0){
            if(a[i] > pre){
                cnt++;
                inc = 1;
            }
        }
        else {
            inc = a[i] - pre;
        }
        pre = a[i];
    }

    if(cnt < 3) cnt = 0;
    cout << cnt << endl;

	return 0;
}
