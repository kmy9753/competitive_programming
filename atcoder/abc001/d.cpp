#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

#define fst first
#define snd second

int inc(int & e){
    e++;
    if(e % 100 == 60) e += 40;

    return e;
}

int dec(int & e){
    e--;
    if(e % 100 == 99) e -= 40;

    return e;
}

string f(int e){
    string ret = toString(e);

    while(ret.size() < 4){
        ret = "0" + ret;
    }

    return ret;
}

int main(void){
    int n; cin >> n;

    vector<pii> in(n);
    for(auto && e : in) {
        char c; cin >> e.fst >> c >> e.snd;

        while(e.fst % 5 != 0) dec(e.fst);
        while(e.snd % 5 != 0) inc(e.snd);
    }

    sort(all(in));

    vector<pii> res;
    pii elem = in[0];
    for(auto && e : in){
        if(e.fst <= elem.snd){
            elem.snd = max(elem.snd, e.snd);
        }
        else {
            res.pb(elem);
            elem = e;
        }
    }
    res.pb(elem);

    for(auto && e : res){
        cout << f(e.fst) << "-" << f(e.snd) << endl;
    }

	return 0;
}
