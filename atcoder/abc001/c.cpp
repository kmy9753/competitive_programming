#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

vector<double> W = {0.0, 0.3, 1.6, 3.4, 5.5, 8.0, 10.8, 13.9, 17.2, 20.8, 24.5, 28.5, 32.7, inf};

vector<string> DSTR = {"NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"};

int main(void){
    double deg, dis; cin >> deg >> dis;
    dis /= 60.0;
    dis = (int)(dis * 10 + 0.5) / 10.0;

    //for(auto && e : W) e *= 60;

    string res_d = "N";
    double lb = 112.5, delta = 225;
    for(auto && e : DSTR){
        double ub = lb + delta;

        if(lb <= deg && deg < ub) res_d = e;

        lb = ub;
    }

    int res_w;
    rep(i, (int)W.size() - 1){
        if(W[i] <= dis + eps && dis - eps <= W[i + 1] - 0.1) res_w = i;
    }
    if(res_w == 0) res_d = "C";

    cout << res_d << " " << res_w << endl;

	return 0;
}
