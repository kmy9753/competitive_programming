#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;

    string s; cin >> s;

    vector<int> sum(4);
    for(auto && e : s){
        sum[e - '0' - 1]++;
    }

    cout << *max_element(sum.begin(), sum.end()) << " "
         << *min_element(sum.begin(), sum.end()) << endl;

	return 0;
}
