#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

typedef vector<string> vs;

vs board, res;

const int N = 9;

vector<int> dr = { 1, 1, 0,-1,-1,-1, 0, 1};
vector<int> dc = { 0,-1,-1,-1, 0, 1, 1, 1};

bool putQ(int r, int c){
    bool ret = true;

    rep(i, 8){
        range(j, 1, N){
            int rr = r + dr[i] * j;
            int cc = c + dc[i] * j;

            if(rr < 0 || N <= rr ||
               cc < 0 || N <= cc) break;

            if(board[rr][cc] == 'Q') ret = false;
            if(ret){
                board[rr][cc]++;
            }
        }
    }

    board[r][c] = 'Q';

    return ret;
}

void unputQ(int r, int c){
    bool ret = true;

    rep(i, 8){
        range(j, 1, N){
            int rr = r + dr[i] * j;
            int cc = c + dc[i] * j;

            if(rr < 0 || N <= rr ||
               cc < 0 || N <= cc) break;

            if(board[rr][cc] == 'Q') ret = false;
            if(ret){
                board[rr][cc]--;
            }
        }
    }

    board[r][c] = '.';
}

bool dfs(int num){
    if(num == 5){
        res = board;
        return true;
    }

    bool ret = false;
    rep(i, N){
        rep(j, N){
            if(board[i][j] != '.') continue;
            if(putQ(i, j)) ret |= dfs(num + 1);
            unputQ(i, j);
        }
    }

    return ret;
}

int main(void){
    board = vs(N);

    rep(i, N) cin >> board[i];

    bool ok = true;
    rep(i, N) rep(j, N){
        if(board[i][j] == 'Q'){
            board[i][j] = '.';
            if(!putQ(i, j)){
                ok = false;
            }
        }
    }
    if(ok) ok = dfs(0);

    if(ok){
        rep(i, N) rep(j, N) if(res[i][j] != 'Q') res[i][j] = '.';

        rep(i, N){
            cout << res[i] << endl;
        }
    }
    else {
        cout << "No Answer" << endl;
    }

	return 0;
}
