#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int a, b; cin >> a >> b;
    int diff = abs(a - b);
    int res = 1 << 24;

    range(i, -40, 41){
        range(j, -40, 41){
            range(k, -40, 41){
                if(i + j * 5 + k * 10 == diff){
                    res = min(res, abs(i) + abs(j) + abs(k));
                }
            }
        }
    }

    cout << res << endl;

	return 0;
}
