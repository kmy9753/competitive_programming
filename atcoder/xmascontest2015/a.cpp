#include <bits/stdc++.h>
#define int long long
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using ll = long long;
int mod;
int power(int a, int p) {
	if(p == 0) return 1;
	else if(p == 1) return a;
	else {
		int b = power(a * a % mod, p >> 1);
		if(p & 1) b = b * a % mod;
		return b;
	}
}

ll extgcd(ll a, ll b, ll &x, ll &y) {
  ll d = a;
  if (b) {
    d = extgcd(b, a%b, y, x); 
    y -= (a/b) * x;
  } else {
    x = 1;
    y = 0;
  }
  return d;
}
 
ll mod_inv(ll a, ll m) {
  ll x, y;
  extgcd(a, m, x, y);
  return (m + x % m) % m;
}

int calc(int l, int r){
    if(l <= 1) return 1;

    int m = l / 2;
    if(l % 2){
        return (calc(m, r) * (1 + power(r, m)) % mod + power(r, l - 1)) % mod;
    }
    else {
        return calc(m, r) * (1 + power(r, m)) % mod;
    }
}

signed main(void){
    int n, x, t, a, b, c; cin >> n >> x >> t >> a >> b >> c;
    mod = c;
    
    int at = power(a, t);
    int p = at, q = 1;
    if(a != 1) q = (calc(t, a) * b) % mod;
    else q = b * t % mod;

    int res = 0;
    rep(i, n){
        res += x;
        x = ((p * x) % mod + q) % mod;
    }

    cout << res << endl;

	return 0;
}
