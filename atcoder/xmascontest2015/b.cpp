#include <bits/stdc++.h>
#define int long long 
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e;

    int res = 0, pre = a[0];
    for(auto & e : a){
        res += abs(e - pre);
        pre = e;
    }

    cout << res << endl;
 
	return 0;
}
