#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, m; cin >> n >> m;
    vector<set<int>> cant(n);
    rep(i, n) cant[i].insert(i);

    rep(loop, m){
        int p, q; cin >> p >> q;
        p--, q--;

        cant[p].insert(q);
        cant[q].insert(p);
    }

    rep(i, n){
        if(cant[i].size() == n){
            cout << "No" << endl;
            return 0;
        }
    }

    set<int> cand;
    range(i, 1, n) cand.insert(i);
    queue<int> q;
    q.push(0);

    set<pair<int, int>> res;
    while(q.size()){
        int a = q.front();
        q.pop();

        vector<int> rmv;
        for(auto & b : cand){
            if(cant[a].find(b) == end(cant[a])){
                pair<int, int> cur;
                if(a < b) cur = {a + 1, b + 1};
                else      cur = {b + 1, a + 1};

                if(res.find(cur) == end(res)){
                    res.insert(cur);
                    rmv.push_back(b);
                    q.push(b);
                }
            }
        }
        for(auto & e : rmv) cand.erase(e);
    }

    if(res.size() != n - 1){
        cout << "No" << endl;
    }
    else {
        cout << "Yes" << endl;
        for(auto & e : res){
            cout << e.first << " " << e.second << endl;
        }
    }

	return 0;
}
