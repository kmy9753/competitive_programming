#include <bits/stdc++.h>

typedef long long ll;
#define int ll

#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

signed main(void){
    int n, m; cin >> n >> m;

    vi w(n);
    for(auto && e : w) cin >> e;

    vector<set<int>> edge(n);
    rep(loop, m){
        int u, v; cin >> u >> v;
        u--, v--;

        edge[u].insert(v);
        edge[v].insert(u);
    }

    vi dp(n, 0);
    queue<int> q;
    set<int> uncnt;
    range(u, 1, n){
        if(edge[u].size() == 1){
            q.push(u);
        }
    }

    while(q.size()){
        int v = q.front(); q.pop();
        uncnt.insert(v);

        int u = *edge[v].begin();
        edge[u].erase(v);

        dp[u] = max(dp[u], dp[v] + w[v]);

        if(u == 0 || edge[u].size() != 1) continue;
        q.push(u);
    }

    int res = *max_element(dp.begin(), dp.end());
    rep(u, n){
        if(uncnt.count(u) == 0){
            res += w[u];
        }
    }

    cout << res << endl;

	return 0;
}
