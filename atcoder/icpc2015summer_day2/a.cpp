#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int p, q; cin >> p >> q;

    q /= __gcd(p, q);

    int res = 1, qq = q;
    for(int i = 2; i * i <= q; i++){
        if(qq % i == 0){
            res *= i;

            while(qq % i == 0) qq /= i;
        }
    }

    res *= qq;

    cout << res << endl;

	return 0;
}
