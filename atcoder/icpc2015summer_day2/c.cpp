#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    string s; cin >> s;

    while(s != "ABC"){
        set<char> S;

        vi match(s.size());
        bool ok = false;
        rep(i, (int)s.size()){
            if(s.substr(i, 3) == "ABC"){
                match[i] = true;
                i += 2;
                ok = true;
            }
            else {
                S.insert(s[i]);
            }
        }

        if(!ok || S.size() != 2) break;

        char c;
        rep(i, 3){
            if(S.count('A' + i) == 0) c = 'A' + i;
        }

        string next;
        rep(i, (int)s.size()){
            if(match[i]){
                next.push_back(c);
                i += 2;
            }
            else {
                next.push_back(s[i]);
            }
        }
        s = next;
    }

    cout << (s == "ABC" ? "Yes":"No") << endl;

	return 0;
}
