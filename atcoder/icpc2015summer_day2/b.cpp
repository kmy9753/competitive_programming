#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define int long long
using namespace std;

int n, k; 

signed main(void){
    cin >> n >> k;

    int x = 0;
    rep(i, n - 1){
        int lb = 0, ub = (int)1e18;

        rep(loop, 100){
            int mid = (lb + ub) / 2;
            if(mid - x - 1 >= mid / k){
                ub = mid;
            }
            else {
                lb = mid;
            }
        }

        x = ub;
    }

    cout << x << endl;

	return 0;
}
