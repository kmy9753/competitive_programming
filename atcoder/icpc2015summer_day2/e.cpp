#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

#define pb push_back
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

const int inf = 1 << 24;

int main(void){
    string s; cin >> s;

    vi num, br_l, br_r;
    string op;
    rep(i, (int)s.size()){
        if(isdigit(s[i])){
            num.pb(s[i] - '0');
            if(0 <= i - 1 && s[i - 1] == '(')            br_l.pb(true);
            else                                         br_l.pb(false);
            if(i + 1 < (int)s.size() && s[i + 1] == ')') br_r.pb(true);
            else                                         br_r.pb(false);
        }
        if(s[i] == '+' || s[i] == '-'){
            op.pb(s[i]);
        }
    }

    vvi dp_max(num.size(), vi(num.size(), -inf));
    vvi dp_min(num.size(), vi(num.size(), inf));
    rep(i, (int)num.size()){
        dp_max[i][i] = dp_min[i][i] = num[i];
    }

    range(i, 2, (int)num.size() + 1){    // len 
        rep(j, (int)num.size() - i + 1){ // l such [l, l + len)
            int l = j, r = j + i - 1;
            if(br_r[l] || br_l[r]) continue;

            range(k, l + 1, r + 1){
                //if(br_l[k - 1] || br_r[k]) continue;
                
                if(op[k - 1] == '+'){
                    if(dp_min[l][k - 1] != inf && dp_min[k][r] != inf)
                        dp_min[l][r] = min(dp_min[l][r], dp_min[l][k - 1] + dp_min[k][r]);
                    if(dp_max[l][k - 1] != -inf && dp_max[k][r] != -inf)
                        dp_max[l][r] = max(dp_max[l][r], dp_max[l][k - 1] + dp_max[k][r]);
                }
                else {
                    if(dp_min[l][k - 1] != inf && dp_max[k][r] != -inf)
                        dp_min[l][r] = min(dp_min[l][r], dp_min[l][k - 1] - dp_max[k][r]);
                    if(dp_max[l][k - 1] != -inf && dp_min[k][r] != inf)
                        dp_max[l][r] = max(dp_max[l][r], dp_max[l][k - 1] - dp_min[k][r]);
                }
            }
        }
    }

    cout << dp_max[0][(int)num.size() - 1] << endl;

    return 0;
}
