#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;

int main(void){
    string s; cin >> s;

    ll numP = 0, numN = 0, numMove = 0;
    for(auto && e : s){
        if(e == '+'){
            numP++;
        }
        else if(e == '-'){
            numN++;
        }
        else {
            numMove++;
        }
    }

    vector<ll> cntP(s.size(), (ll)1 << 60);

    rep(i, (int)s.size()){
        if(s[i] == '+'){
            numP--;
        }
        else if(s[i] == '-'){
            numN--;
        }
        else {
            cntP[i] = numP - numN;
        }
    }

    sort(cntP.begin(), cntP.end());

    ll res = 0;
    rep(i, numMove / 2){
        res -= cntP[i];
    }
    range(i, numMove / 2, numMove){
        res += cntP[i];
    }

    cout << res << endl;

	return 0;
}
