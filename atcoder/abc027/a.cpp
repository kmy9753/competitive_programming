#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int a, b, c; cin >> a >> b >> c;

    if(a == b) cout << c;
    else if(a == c) cout << b;
    else if(b == c) cout << a;

    puts("");

	return 0;
}
