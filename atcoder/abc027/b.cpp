#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;

int main(void){
    int n; cin >> n;

    vi a(n); int TTL;
    for(auto && e : a) cin >> e, TTL += e;
    if(TTL % n != 0){
        cout << -1 << endl;
        return 0;
    }
    int num = TTL / n;

    int res = 0, cnt = 0, sum = 0;
    for(auto && e : a){
        cnt++;
        sum += e;

        if(sum % cnt == 0 && sum / cnt == num){
            cnt = 0;
            sum = 0;
        }
        else {
            res++;
        }
    }

    cout << res << endl;

	return 0;
}
