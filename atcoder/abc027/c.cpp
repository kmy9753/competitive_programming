#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;

int main(void){
    ll n; cin >> n;

    ll lb = n + 1, ub = 2 * n;
    bool won = true;

    while(lb > 1){
        won = !won;

        if(!won){
            lb = (lb + 1) / 2;
            ub /= 2;
        }
        else {
            lb /= 2;
            ub /= 2;
        }
    }

    cout << (won ? "Takahashi":"Aoki") << endl;

	return 0;
}
