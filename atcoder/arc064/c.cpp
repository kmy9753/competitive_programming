#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using R = long double;
using C = tuple<R, R, R>;

inline R calc(C& ci, C& cj){
    R xi, yi, ri; tie(xi, yi, ri) = ci;
    R xj, yj, rj; tie(xj, yj, rj) = cj; 
    R ret = sqrt(pow(xi - xj, 2) + pow(yi - yj, 2)) - (ri + rj);
    return max((R)0, ret);
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    R xs, ys, xt, yt; cin >> xs >> ys >> xt >> yt;
    int n; cin >> n;
    vector<C> in(n);
    for(auto& e : in){
        R x, y, r;
        cin >> x >> y >> r;
        e = C(x, y, r);
    }
    in.emplace_back(C(xs, ys, 0));
    in.emplace_back(C(xt, yt, 0));
    n += 2;
    int s = n - 2, t = n - 1;

    using Edge = tuple<R, int>;
    vector<vector<Edge>> G(n);
    rep(i, n){
        rep(j, i + 1, n){
            R cost = calc(in[i], in[j]);
            G[i].emplace_back(Edge(cost, j));
            G[j].emplace_back(Edge(cost, i));
        }
    }

    using Elem = tuple<R, int>;
    priority_queue<Elem, vector<Elem>, greater<Elem>> q;
    vector<R> min_dist(n, 1e10);
    q.push(Elem(0, s));
    min_dist[s] = 0;

    R res = -11;
    while(q.size()){
        R cost; int v; tie(cost, v) = q.top(); q.pop();
        if(min_dist[v] != cost) continue;
        if(v == t){
            res = cost;
            break;
        }

        for(auto& e : G[v]){
            R dcost; int nv; tie(dcost, nv) = e;
            R ncost = cost + dcost;
            if(chmin(min_dist[nv], ncost)){
                q.push(Elem(ncost, nv));
            }
        }
    }

    assert(res >= -1);
    cout.precision(20);
    cout << fixed << res << endl;

    return 0;
}
