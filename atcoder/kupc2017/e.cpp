#include <bits/stdc++.h>
#define int long long
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

struct UnionFind {
    vector<int> data;
    UnionFind(int size) : data(size, -1) { }
    bool unionSet(int x, int y) {
        x = root(x); y = root(y);
        if (x != y) {
            if (data[y] < data[x]) swap(x, y);
            data[x] += data[y]; data[y] = x;
        }
        return x != y;
    }
    bool findSet(int x, int y) {
        return root(x) == root(y);
    }
    int root(int x) {
        return data[x] < 0 ? x : data[x] = root(data[x]);
    }
    int size(int x) {
        return -data[root(x)];
    }
};

int n;
vector<set<int>> G;
vi value;
vi used;

pair<int, int> dfs(int v){
    assert(not used[v]);
    used[v] = true;

    int sum = 0, minv = value[v];
    for(auto& nv : G[v]){
        auto cur = dfs(nv);
        sum += cur.first;
        chmin(minv, value[nv]);
    }

    return make_pair(sum + value[v], sum + value[v] - minv);
}

signed main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int N, M; cin >> N >> M;
    vi V(N); for(auto& e : V) cin >> e;

    vi rem(N);
    vi xs(M), ys(M);
    map<int, map<int, int>> dic;
    UnionFind uf(N);
    rep(i, M){
        int x, y; cin >> x >> y; x--, y--;
        xs[i] = x, ys[i] = y;

        // if(x == y){ // self
        //     rem[x] = true;
        // }
        // else if(dic[x][y]){ // multi
        //     rem[x] = rem[y] = true;
        // }
        // else {
        //     dic[x][y] = true;
        //     dic[y][x] = true;
        // }
    }
    rep(i, M){
        int x, y; x = xs[i], y = ys[i];
        if(rem[x] or rem[y]) continue;
        uf.unionSet(x, y);
    }

    vi ecnt(N);
    vi vcnt(N);
    vi mini(N, inf);
    vi sum(N);
    rep(i, M){
        int x, y; x = xs[i], y = ys[i];
        if(rem[x] or rem[y]) continue;
        ecnt[uf.root(x)]++;
    }

    rep(i, N){
        if(rem[i]) continue;
        int r = uf.root(i);
        vcnt[r]++;
        chmin(mini[r], V[i]);
        sum[r] += V[i];
    }

    ll res = 0LL;
    rep(i, N){
        if(rem[i]) res += V[i];
        if(rem[i] or uf.root(i) != i) continue;
        if(vcnt[i] <= ecnt[i]){
            res += sum[i];
        }
        else {
            res += sum[i] - mini[i];
        }
    }

    cout << res << endl;

    return 0;
}
