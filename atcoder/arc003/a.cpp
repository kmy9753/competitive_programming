#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    int sum = 0;
    rep(i, n){
        char r; cin >> r;
        if(r != 'F') sum += 4 - (r - 'A');
    }

    printf("%.12f\n", 1.0 * sum / n);

	return 0;
}
