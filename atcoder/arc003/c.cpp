#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef vector<int> vi;
const vi dx = { 1, 0,-1, 0};
const vi dy = { 0,-1, 0, 1};
const double eps = 1e-11;
#define F first
#define S second
 
int main(void){
    int h, w; cin >> h >> w;
    vs field(h);
    pii s, z;
    rep(y, h){
        cin >> field[y];
        rep(x, w){
            if(field[y][x] == 's') s = pii(y, x);
        }
    }
 
    double lb = -1.0, ub = 10.0;
    bool arrived = false;
    rep(loop, 50){
        double mid = (lb + ub) / 2.0;
 
        queue<pair<int, pii>> q;
        q.push(make_pair(0, s));
        vector<vi> used(h, vi(w)); 
 
        bool ok = false;
        while(q.size()){
            int cur_t = q.front().F;
            pii cur_p = q.front().S;
            q.pop();
 
            if(used[cur_p.F][cur_p.S]) continue;
            used[cur_p.F][cur_p.S] = true;
 
            if(cur_t != 0){
                if(field[cur_p.F][cur_p.S] == 'g'){
                    ok = true;
                    arrived = true;
                    break;
                }
                if(field[cur_p.F][cur_p.S] == '#' ||
                   (field[cur_p.F][cur_p.S] - '0') * pow(0.99, cur_t) - eps < mid + eps){
                    continue;
                }
            }
 
            rep(i, 4){
                int next_t = cur_t + 1;
                pii next_p = pii(cur_p.F + dy[i], cur_p.S + dx[i]);
 
                if(next_p.F < 0 || h <= next_p.F ||
                   next_p.S < 0 || w <= next_p.S) continue;
 
                if(!used[next_p.F][next_p.S]) q.push(make_pair(next_t, next_p));
            }
        }
 
        if(ok){
            lb = mid;
        }
        else {
            ub = mid;
        }
    }
 
    if(!arrived) cout << -1 << endl;
    else printf("%.12f\n", lb);
 
	return 0;
}
