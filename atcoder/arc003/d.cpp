#include <bits/stdc++.h>
#include <sys/time.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
double gett(){
  struct timeval t;
  gettimeofday(&t,NULL);
  return t.tv_sec + t.tv_usec/1e6;
}
int main(void){
    double st = gett();

    int n, m, k; cin >> n >> m >> k;
    auto myrand = bind(uniform_int_distribution<int>(0, n-1), mt19937(static_cast<unsigned int>(time(nullptr))));

    vvi f(n, vi(n, true));
    rep(loop, m){
        int a, b; cin >> a >> b;
        f[a][b] = f[b][a] = false;
    }

    vi A(n);
    rep(i, n) A[i] = i;

    long long scnt = 0, cnt = 0;
    while(gett() - st < 1.8){
        vi idx = A;
        rep(loop, k){
            int a = myrand() % n, b;
            while((b = myrand() % n) == a);
            swap(idx[a], idx[b]);
        }

        bool ok = true;
        rep(i, n){
            if(!f[idx[i]][idx[(i + 1) % n]]) ok = false;
        }

        if(ok) scnt++;
        cnt++;
    }

    printf("%.12f\n", 1.0 * scnt / cnt);

	return 0;
}
