#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    set<string> s;
    rep(i, n){
        string t; cin >> t;
        reverse(t.begin(), t.end());
        s.insert(t);
    }

    for(auto e : s){
        reverse(e.begin(), e.end());
        cout << e << endl;
    }

	return 0;
}
