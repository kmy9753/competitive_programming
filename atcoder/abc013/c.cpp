#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
#define int ll

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int A, B, C, D, E, H, N;

bool check(int x, int y){
    return (B + E) * x + (D + E) * y + H - E * N > 0;
}

signed main(void){
    cin >> N >> H >> A >> B >> C >> D >> E;

    int res = INF;
    rep(x, N + 1){
        int lb = 0, ub = N - x;

        rep(loop, 50){
            int mid = (lb + ub) / 2;

            if(check(x, mid)){
                ub = mid;
            }
            else {
                lb = mid;
            }
        }

        int y = ub;

        res = min(res, A * x + C * y);
    }

    cout << res << endl;

	return 0;
}
