#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int main(void){
    int n, m, d; cin >> n >> m >> d;

    vi a(m);
    for(auto && e : a) cin >> e, e--;

    vvi dp(40, vi(n));
    rep(i, n){
        dp[0][i] = i;
    }
    rep(i, m){
        int idx = a[m - i - 1];
        swap(dp[0][idx], dp[0][idx + 1]);
    }

    range(i, 1, 40){
        rep(j, n){
            dp[i][j] = dp[i - 1][dp[i - 1][j]];
        }
    }

    vi res(n);
    rep(i, n) res[i] = i + 1;

    rep(x, 40){
        if(d % 2 == 1){
            vi next(n);

            rep(i, n){
                next[i] = res[dp[x][i]];
            }
            res = next;
        }

        d /= 2;
    }

    for(auto && e : res){
        cout << e << endl;
    }

	return 0;
}
