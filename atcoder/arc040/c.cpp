#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
int main(void){
    int n; cin >> n;
    
    vector<string> field(n);
    for(auto & e : field) cin >> e;

    int res = 0;
    rep(y, n){
        int c = -1;
        rep(x, n){
            if(field[y][x] == '.'){
                c = x;
            }
        }
        if(c == -1) continue;
        rep(x, c + 1) field[y][x] = 'o';
        if(y + 1 < n) range(x, c, n) field[y + 1][x] = 'o';
        res++;
    }

    cout << res << endl;

	return 0;
}
