#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef tuple<int, int, int> State;
typedef vector<string> vs;
int main(void){
    int n; cin >> n;
    vs field(n);
    int sx, sy;
    rep(y, n){
        cin >> field[y];
        rep(x, n){
            if(field[y][x] == 's'){
                sx = x, sy = y;
                field[y][x] = '.';
            }
        }
    }

	return 0;
}
