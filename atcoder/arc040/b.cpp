#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, r; cin >> n >> r;
    string s; cin >> s;
    int sum = count(begin(s), end(s), '.');

    int res = 0, pos = 0;
    while(pos < n && sum != 0){
        if(s.at(pos) == 'o'){
            int d = 0;
            rep(k, r){
                if(pos + k < n && s.at(pos + k) == '.'){
                    d++;
                }
            }
            if(sum == d){
                sum = 0;
            }
            pos++;
        }
        else {
            rep(k, r){
                if(pos + k < n && s.at(pos + k) == '.'){
                    s[pos + k] = 'o';
                    sum--;
                }
            }
        }
        res++;
    }

    cout << res << endl;

	return 0;
}
