#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
int main(void){
    int n; cin >> n;

    int rcnt = 0, bcnt = 0;
    rep(i, n){
        string in; cin >> in;

        for(auto & e : in){
            if(e == 'R') rcnt++;
            if(e == 'B') bcnt++;
        }
    }

    string res = "DRAW";
    if(rcnt > bcnt) res = "TAKAHASHI";
    if(rcnt < bcnt) res = "AOKI";

    cout << res << endl;

	return 0;
}
