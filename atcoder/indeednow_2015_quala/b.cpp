#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;

    map<char, int> sample;
    string T = "indeednow";
    for(auto & e : T) sample[e]++;

    while(n--){
        string s; cin >> s;

        map<char, int> cnt;
        for(auto & e : s) cnt[e]++;

        string res = "YES";
        if(s.size() == 9){
            for(auto & e : sample){
                if(e.second != cnt[e.first]){
                    res = "NO";
                }
            }
        }
        else res = "NO";

        cout << res << endl;
    }

	return 0;
}
