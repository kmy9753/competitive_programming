#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;

int main(void){
    int n; cin >> n;
    vi score;
    rep(i, n){
        int s; cin >> s;
        if(s != 0) score.push_back(s);
    }
    sort(begin(score), end(score));

    int q; cin >> q;
    while(q--){
        int cap; cin >> cap;

        int lb = 0, ub = 1e6+10;
        rep(loop, 100){
            int mid = (lb + ub) / 2;

            int num = end(score) - lower_bound(begin(score), end(score), mid);
            if(num <= cap){
                ub = mid;
            }
            else {
                lb = mid;
            }
        }

        cout << ub << endl;
     }

	return 0;
}
