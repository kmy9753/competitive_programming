#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using pii = pair<int, int>;

map<vvi, int> memo1, memo2;
vvi field;
int h, w;
int res = 10000;

vi dx = { 1, 0,-1, 0};
vi dy = { 0,-1, 0, 1};

void bfs(pii pos, bool is_fst){
    queue<tuple<vvi, pii, int>> q;
    q.push(make_tuple(field, pos, 0));

    while(q.size()){
        vvi f; pii pos; int depth;
        tie(f, pos, depth) = q.front();
        q.pop();

        if(is_fst){
            if(memo1.find(f) != end(memo1)) continue;
            memo1[f] = depth;
        }
        if(not is_fst){
            if(memo2.find(f) != end(memo2)) continue;
            memo2[f] = depth;

            if(memo1.find(f) != end(memo1)){
                res = min(res, depth + memo1[f]);
            }
        }
        if(depth == 12) continue;

        rep(i, 4){
            pii next_pos(pos.first + dy[i], pos.second + dx[i]);
            if(next_pos.first < 0 or h <= next_pos.first or
               next_pos.second< 0 or w <= next_pos.second){
                continue;
            }

            vvi nf = f;
            swap(nf[pos.first][pos.second], nf[next_pos.first][next_pos.second]);
            q.push(make_tuple(nf, next_pos, depth + 1));
        }
    }
}

int main(void){
    cin >> h >> w;
    field = vvi(h, vi(w));

    pii start;
    rep(y, h){
        rep(x, w){
            cin >> field[y][x];
            if(field[y][x] == 0) start = pii(y, x);
        }
    }

    bfs(start, true);

    int num = 1;
    rep(y, h){
        rep(x, w){
            field[y][x] = num++;
        }
    }
    field[h - 1][w - 1] = 0;
    bfs(pii(h - 1, w - 1), false);

    cout << res << endl;

    return 0;
}
