#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
const int N = 1e6; 
const ll mod = 1e9 + 7;
ll fact[N+1];
 
void set_fact() {
  fact[0] = 1;
  rep(i,N) fact[i+1] = ((((ll)i+1) % mod) * fact[i]) % mod;
}
 
ll extgcd(ll a, ll b, ll &x, ll &y) {
  ll d = a;
  if (b) {
    d = extgcd(b, a%b, y, x); 
    y -= (a/b) * x;
  } else {
    x = 1;
    y = 0;
  }
  return d;
}
 
ll mod_inv(ll a, ll m) {
  ll x, y;
  extgcd(a, m, x, y);
  return (m + x % m) % m;
}
 
// n! mod p 
ll mod_fact(ll _n, ll p, ll &e) {
  e = 0;
  if (!_n) return 1;
  ll res = mod_fact(_n/p, p, e);
  e += _n/p;
 
  if ((_n/p)%2) 
    return res * (p - fact[_n%p]) %p;
  else
    return res * fact[_n%p] % p;
}
 
// nCk mod p ; O(log_p n)
ll mod_comb(ll _n, ll k, ll p) {
  if (_n<0 || k<0 || _n<k) return 0;
  ll e1, e2, e3;
  ll a1 = mod_fact(_n, p, e1), a2 = mod_fact(k, p, e2), a3 = mod_fact(_n-k, p, e3);
  if (e1 > e2+e3) return 0;
  return a1 * mod_inv(a2*a3%p, p) % p;
}

// nHk mod p ;
ll mod_h_comb(ll n, ll k, ll p) {
  return mod_comb(n+k-1, k, p);
}

signed main(void){
    set_fact();

    ll n, k;
    cin >> n >> k;

    ll res = 0;
    if(n <= k){
        k %= n;
        res = mod_comb(n, k, mod);
    }
    else {
        res = mod_h_comb(n, k, mod);
    }

    cout << res << endl;

	return 0;
}
