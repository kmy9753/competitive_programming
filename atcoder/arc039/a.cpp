#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int s2i(string s){
    stringstream ss(s);
    int ret;
    ss >> ret;
    return ret;
}
int main(void){
    string a, b; cin >> a >> b;
    string aa = a, bb = b;
    int idx = 0;
    while(idx < 3 && aa[idx] == '9') idx++;
    if(idx < 3) aa[idx] = '9';
    idx = 0;
    while((idx < 3 && bb[idx] == '0') || (idx == 0 && bb[idx] == '1')) idx++;
    if(idx < 3){
        bb[idx] = '1';
        if(idx != 0) bb[idx] = '0';
    }

    int na = s2i(a), nb = s2i(b);
    int naa = s2i(aa), nbb = s2i(bb);

    cout << max(na - nbb, naa - nb) << endl;

	return 0;
}
