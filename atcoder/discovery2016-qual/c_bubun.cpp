#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

string res;

void dfs(string str, int K){
    if(K == 0){
        res = min(res, str);
        return;
    }

    rep(i, str.size()){
        string nstr = str;
        nstr[i] = 'a';
        dfs(nstr, K - 1);
    }
    dfs("a" + str, K - 1);
    rep(i, str.size()){
        string nstr = str.substr(0, i) + str.substr(i + 1);
        dfs(nstr, K - 1);
    }
}

int main(void){
    string s; cin >> s;
    if(s.size() > 10){
        cout << "0" << endl;
        return 0;
    }
    int K; cin >> K;
    res.push_back('z' + 1);

    dfs(s, K);

    cout << res << endl;

	return 0;
}
