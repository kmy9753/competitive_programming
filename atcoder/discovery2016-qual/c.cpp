#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

string res;

int main(void){
    string s; cin >> s;
    int K; cin >> K;

    s.push_back('a');
    int n = s.size();

    vector<int> memo1(n);
    vector<int> memo2(n);

    int pos;
    int cnt = 0;
    for(int i = n - 1; i >= 0; i--){
        if(s[i] == 'a'){
            pos = i;
        }
        else cnt++;

        memo1[i] = pos - i;
        memo2[i] = cnt;
    }

    if(cnt <= K){
        rep(i, n - K - 1){
            cout << "a";
        }
        cout << endl;

        return 0;
    }
    
    rep(i, n){
        if(s[i] == 'a') continue;

        if(memo1[i] <= K){
            s[i] = 'a';
            K--;
        }
        int num = 0; char c = s[i];
        else {
            range(j, i + 1, i + K){
                if(s[j] < c){
                    c = s[j];
                    num = j - i;
                }
            }
            break;
        }
    }
    rep(i, K) s = "a" + s;
    s = s.substr(0, s.size() - 1);

    cout << s << endl;

	return 0;
}
