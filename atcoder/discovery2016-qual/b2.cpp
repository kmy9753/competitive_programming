#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e, e = 1e5 - e;

    vector<int> cnt(1e5 + 1);
    set<int> s;

    for(auto & e : a){
        if(s.lower_bound(e) != end(s)){
            int tar = *s.lower_bound(e);
            s.erase(tar);
            s.insert(e);
        }
        else {
            s.insert(e);
        }
    }

    vector<int> b = a;
    sort(begin(b) + 1, end(b));

    if(a == b){
        cout << 1 << endl;
    }
    else {
        cout << s.size() << endl;
    }

	return 0;
}
