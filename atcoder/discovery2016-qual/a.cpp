#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    string s = "DiscoPresentsDiscoveryChannelProgrammingContest2016";
    int n; cin >> n;
    
    while(s != ""){
        cout << s.substr(0, n) << endl;
        if(s.size() <= n) break;

        s = s.substr(n);
    }

	return 0;
}
