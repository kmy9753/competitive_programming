#include <bits/stdc++.h>
#define int long long 
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int n, m, K; cin >> n >> m >> K;

    if(K != 1){
        puts("0");
        return 0;
    }

    vector<int> a(n), b(m);
    for(auto & e: a) cin >> e;
    for(auto & e: b) cin >> e;
    int res = 0;
    rep(i, n){
        rep(j, m){
            swap(a[i], b[j]);

            int sa = 0, sb = 0;
            for(auto & e : a) sa += e;
            for(auto & e : b) sb += e;

            res = max(res, sa * sb);

            swap(a[i], b[j]);
        }
    }

    cout << res << endl;

	return 0;
}
