#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e;

    vector<set<int>> place(1e5 + 1);
    rep(i, n) place[a[i]].insert(i);

    int pos = 0;
    int res = 1;
    for(auto & s : place){
        while(s.size()){
            if(s.lower_bound(pos) == end(s)){
                pos = 0;
                res++;
            }
            else {
                pos = *s.lower_bound(pos);
                s.erase(pos);
            }
        }
    }

    if(pos == 0) res--;

    cout << res << endl;

	return 0;
}
