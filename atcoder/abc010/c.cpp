#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

#define F first
#define S second

int main(void){
    pii ta, tb; cin >> ta.S >> ta.F >> tb.S >> tb.F;
    int t, v; cin >> t >> v;
    
    int n; cin >> n;
    vector<pii> p(n);

    for(auto && e : p){
        cin >> e.S >> e.F;
    }

    string res = "NO";
    for(auto && e : p){
        double d1 = sqrt(sqr(e.F - ta.F) + sqr(e.S - ta.S));
        double d2 = sqrt(sqr(e.F - tb.F) + sqr(e.S - tb.S));

        double d = d1 + d2;

        if(t * v + eps >= d){
            res = "YES";
        }
    }

    cout << res << endl;

	return 0;
}
