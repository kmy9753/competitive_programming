#include <bits/stdc++.h>
#define int long long
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

signed main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; ll K; cin >> n >> K;
    vll a(n); for(auto& e : a) cin >> e;

    const int N = 1e5;
    vi is_p(N, true);
    vi primes;
    is_p[0] = is_p[1] = false;
    rep(i, N){
        if(not is_p[i]) continue;
        primes.emplace_back(i);
        for(int j = i * 2; j < N; j += i){
            is_p[j] = false;
        }
    }

    vi pcnt; vi ps;
    int sz = 0;
    for(auto e : primes){
        if(K % e != 0) continue;
        if(K == 1) break;

        pcnt.emplace_back(0);
        ps.emplace_back(e);
        while(K % e == 0){
            K /= e;
            pcnt[sz]++;
        }
        sz++;
    }
    if(K > 1){
        pcnt.emplace_back(1);
        ps.emplace_back(K);
        sz++;
    }

    map<vi, int> dic;
    rep(i, n){
        vi cur(sz);
        rep(j, sz){
            if(a[i] == 1) break;
            while(a[i] % ps[j] == 0){
                a[i] /= ps[j];
                cur[j]++;
            }
            chmin(cur[j], pcnt[j]);
        }
        dic[cur]++;
    }
    vector<vi> vecs;
    for(auto& e : dic) vecs.emplace_back(e.first);
    int len = (int)vecs.size();

    ll res = 0;
    rep(i, len){
        vi& pa = vecs[i]; int cnta = dic[pa];
        rep(j, i, len){
            vi& pb = vecs[j]; int cntb = dic[pb];

            bool ok = true;
            rep(k, sz){
                if(pa[k] + pb[k] < pcnt[k]){
                    ok = false;
                    break;
                }
            }
            if(ok){
                if(i == j) res += cnta * (cntb - 1) / 2;
                else       res += cnta * cntb;
            }
        }
    }

    cout << res << endl;

    return 0;
}
