#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

int main(void){
    int n; cin >> n;
    vvi d(n + 1, vi(n + 1));
    rep(y, n + 1){
        rep(x, n + 1){
            if(y == 0 || x == 0) d[y][x] = 0;
            else cin >> d[y][x];
        }
    }
    range(y, 1, n + 1){
        range(x, 1, n + 1){
            d[y][x] += d[y][x - 1] + d[y - 1][x] - d[y - 1][x - 1];
        }
    }

    int q; cin >> q;
    while(q--){
        int p; cin >> p;

        int sum = 0;
        range(h, 1, n + 1){
            int w = min(n, p / h);
            if(w == 0) break;

            rep(y, n - h + 1){
                rep(x, n - w + 1){
                    sum = max(sum, d[y + h][x + w] - d[y + h][x] - d[y][x + w] + d[y][x]);
                }
            }
        }

        cout << sum << endl;
    }

    return 0;
}
