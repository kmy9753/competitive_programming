#include <bits/stdc++.h>
using namespace std;
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)

int main(void){
    int n; cin >> n;
    vector<int> c(n);
    for(auto & e : c) cin >> e;

    long double res = 0.0;
    rep(i, n){
        int cnt = 0;
        rep(j, n){
            if(i == j) continue;

            if(c[i] % c[j] == 0){
                cnt++;
            }
        }

        long double diff;
        if(cnt % 2 == 1){
            diff = 0.5;
        }
        else {
            diff = 1.0 * (cnt + 2) / (2 * cnt + 2);
        }

        res += diff;
    }

    printf("%.12Lf\n", res);

	return 0;
}
