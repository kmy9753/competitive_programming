#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

#define F first
#define S second

int n;
vector<pii> mac;

map<pair<pii, pii>, int> memo;

int rec(pii p1, pii p2){
    if(memo.find(mp(p1, p2)) != memo.end()){
        return memo[mp(p1, p2)];
    }

    int ret = 0;
    for(auto && e : mac){
        if(e.S < p1.S || p2.S < e.S ||
           e.F < p1.F || p2.F < e.F) continue;

        int sum = (p2.F - p1.F) + (p2.S - p1.S) + 1;

        sum += rec(p1, mp(e.F - 1, e.S - 1));
        sum += rec(mp(e.F + 1, p1.S), mp(p2.F, e.S - 1));
        sum += rec(mp(p1.F, e.S + 1), mp(e.F - 1, p2.S));
        sum += rec(mp(e.F + 1, e.S + 1), p2);

        ret = max(ret, sum);
    }

    return memo[mp(p1, p2)] = ret;
}

int main(void){
    int w, h; cin >> w >> h;

    cin >> n;
    mac = vector<pii>(n);
    for(auto && e : mac) cin >> e.S >> e.F;

    cout << rec(mp(1, 1), mp(h, w)) << endl;

	return 0;
}
