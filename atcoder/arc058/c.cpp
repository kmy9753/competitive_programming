#include <bits/stdc++.h>
#define int long long
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

<<<<<<< HEAD
///////////////////////
const int N = 50; 
ll fact[N+1];
ll p10[N+1];

void set_pow10() {
    p10[0] = 1;
    rep(i,N) p10[i+1] = MUL((ll)10, p10[i], mod);
}

void set_fact() {
    fact[0] = 1;
    rep(i,N) fact[i+1] = ((((ll)i+1) % mod) * fact[i]) % mod;
}

ll mod_inv(ll a, ll m) {
    ll x, y;
    extgcd(a, m, x, y);
    return (m + x % m) % m;
}

// n! mod p 
ll mod_fact(ll n, ll p, ll &e) {
    e = 0;
    if (!n) return 1;
    ll res = mod_fact(n/p, p, e);
    e += n/p;

    if ((n/p)%2) 
        return res * (p - fact[n%p]) %p;
    else
        return res * fact[n%p] % p;
}

// nCk mod p ; O(log_p n)
ll mod_comb(ll n, ll k, ll p) {
    if (n<0 || k<0 || n<k) return 0;
    ll e1, e2, e3;
    ll a1 = mod_fact(n, p, e1), a2 = mod_fact(k, p, e2), a3 = mod_fact(n-k, p, e3);
    if (e1 > e2+e3) return 0;
    return a1 * mod_inv(a2*a3%p, p) % p;
}

// nHk mod p ;
ll mod_h_comb(ll n, ll k, ll p) {
    return mod_comb(n+k-1, k, p);
}
///////////////////////

int n;
int x[3];
map<string, int> occ;
map<int, vector<string>> dic;
int sum = 0;

void dfs(int idx, int d, string str){
    if(x[idx] == 0){
        idx++;
    }

    if(idx == 3){
        int restn = n - d;
        if(restn < 0){
            return;
        }
//        int num = MUL(pow10[restn], )
//        dic[n].push_back(pow10[restn]);
        sum++;
    }

    rep(next, 1, x[idx] + 1){
        x[idx] -= next;
        dfs(idx, d + 1, str + (char)('a' + next - 1));
        x[idx] += next;
    }
}

signed main(void){
    int x[3];
    int n; cin >> n >> x[0] >> x[1] >> x[2];

    int xyz = x[0] + x[1] + x[2];
    int mask = (1 << (xyz + 1)) - 1;

    vector<vi> dp(n + 1, vi(mask + 1));
    dp[0][1] = 1;

    int nmask = 0;
    {
        int sum = 0;
        rrep(i, 3){
            sum += x[i];
            nmask |= (1 << sum);
        }
    }

    rep(i, n){
        rep(j, mask + 1){
            if(dp[i][j] == 0) continue;

            rep(k, 1, 11){
                int nj = (((j << k) + 1) & mask);
                if((nj & nmask) == nmask) continue;

                dp[i + 1][nj] = ADD(dp[i + 1][nj], dp[i][j], mod);
            }
        }
    }
    
    int res = 1;
    rep(i, n){
        res = MUL(res, 10, mod);
    }
    for(auto & e : dp[n]){
        res = SUB(res, e, mod);
    }

    cout << res << endl;

    return 0;
}
