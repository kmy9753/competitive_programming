#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using R = long double;
const R EPS = 1e-10;

const int N = 11;
R memo[N][N][N][N][N][N][2];

R rec(int a1, int a2, int b1, int b2, int c1, int c2, int turn){
    if(a1 < 0 or a2 < 0 or b1 < 0 or b2 < 0 or c1 < 0 or c2 < 0) return 0.;
    R& cur = memo[a1][a2][b1][b2][c1][c2][turn];
    if(cur > -EPS) return cur;
    cur = -1e30;

    bool fst = (turn == 0);
    int sgn = (fst ? 1:-1);

    // A
    if(a1 + a2 > 0){
        R s1 = rec(a1-1,a2,b1,b2,c1,c2,turn^1);
        R s2 = rec(a1,a2-1,b1,b2,c1,c2,turn^1);
        R nx = 1. * a1/(a1+a2) * ((fst ? 100.:0.) + s1) + 1. * a2/(a1+a2) * ((fst ? 50.:0.) + s2);
        chmax(cur, nx * sgn);
    }
    // B
    if(b1 + b2 > 0){
        R s1 = rec(a1,a2,b1-1,b2,c1,c2,turn^1);
        R s2 = rec(a1,a2,b1,b2-1,c1,c2,turn^1);
        R nx = 1. * b1/(b1+b2) * ((fst ? 100.:0.) + s1) + 1. * b2/(b1+b2) * ((fst ? 50.:0.) + s2);
        chmax(cur, nx * sgn);
    }
    // C
    if(c1 + c2 > 0){
        R s1 = rec(a1,a2,b1,b2,c1-1,c2,turn^1);
        R s2 = rec(a1,a2,b1,b2,c1,c2-1,turn^1);
        R nx = 1. * c1/(c1+c2) * ((fst ? 100.:0.) + s1) + 1. * c2/(c1+c2) * ((fst ? 50.:0.) + s2);
        chmax(cur, nx * sgn);
    }
    if(cur < -1e29) cur = 0.;
    cur *= sgn;

    return cur;
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int a1, a2, b1, b2, c1, c2; cin >> a1 >> a2 >> b1 >> b2 >> c1 >> c2;
    rep(i1, a1+1) rep(i2, a2+1) rep(j1, b1+1) rep(j2, b2+1) rep(k1, c1+1) rep(k2, c2+1) rep(l, 2) memo[i1][i2][j1][j2][k1][k2][l] = -1.;
    cout.precision(20);
    cout << fixed << rec(a1, a2, b1, b2, c1, c2, 0) << endl;

    return 0;
}
