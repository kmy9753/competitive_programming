#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf =1 << 30;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

string f;
int n;

using pii = pair<int, int>;
const int N = 52;
pii memo[N][N][N];

const int UND = inf + 1;
const int NG  = UND + 1;

pii rec(int l, int r, int rest){
    if(rest < 0) return make_pair(NG, NG);

    int& mi = memo[l][r][rest].first;
    int& ma = memo[l][r][rest].second;
    if(mi != UND) return make_pair(mi, ma);

    if((r - l) % 2 == 0){
        mi = ma = NG;
        return make_pair(mi, ma);
    }

    char bak = f[r - 1];
    if(r - l == 1){
        if(not isdigit(bak)){
            if(rest == 0) mi = ma = NG; 
            else {
                mi = 0, ma = 9;
            }
        }
        else {
            mi = ma = bak - '0';
            assert(0 <= mi and mi <= 9);
            if(rest >= 1) mi = 0, ma = 9;
        }
        return make_pair(mi, ma);
    }

    mi = inf, ma = -inf;

    for(auto c : {'+', '-'}){
        rep(nr, l + 1, r - 1){
            rep(nrest, rest + 1){
                pii lhs = rec(l, nr, nrest - (bak == c ? 0:1));
                pii rhs = rec(nr, r - 1, rest - nrest);
                if(lhs.first == NG or rhs.first == NG) continue;

                if(c == '+'){
                    chmin(mi, lhs.first + rhs.first);
                    chmax(ma, lhs.second + rhs.second);
                }
                else {
                    chmin(mi, lhs.first - rhs.second);
                    chmax(ma, lhs.second - rhs.first);
                }
            }
        }
    }
    if(mi == inf) mi = ma = NG;

    return make_pair(mi, ma);
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int K; cin >> K;
    cin >> f;
    n = (int)f.size();
    rep(i, N) rep(j, N) rep(k, N) memo[i][j][k] = make_pair(UND, UND);

    pii res = rec(0, n, K);
    if(res.first == NG){
        cout << "NG" << endl;
    }
    else {
        cout << "OK" << endl;
        cout << res.second << endl;
    }

    return 0;
}
