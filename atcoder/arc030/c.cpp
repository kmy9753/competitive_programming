#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,x,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);
 
// ----------------------------
using vi = vector<int>;
using vvi = vector<vi>;

int n;             // |V|
vvi edge;          // 元のグラフ
vvi r_edge;        // 辺の向きを逆にしたグラフ
vi vs;             // 帰りがけ順
vector<bool> used; // 既に調べたか
vi cmp;            // 属する強連結成分のトポロジカル順序

vector<char> C;

void add_edge(int from, int to){
    edge[from].push_back(to);
    r_edge[to].push_back(from);
}

void dfs(int v){
    used[v] = true;
    for(auto & nv : edge[v]){
        if(not used[nv]) dfs(nv);
    }
    vs.push_back(v);
}

void rdfs(int v, int k){
    used[v] = true;
    cmp[v] = k;
    for(auto & nv : r_edge[v]){
        if(not used[nv]) rdfs(nv, k);
    }
}

int scc(){
    used = vector<bool>(n);
    vs.clear();
    rep(v, n){
        if(not used[v]) dfs(v);
    }

    used = vector<bool>(n);
    int k = 0;
    rrep(i, vs.size()){
        if(not used[vs[i]]) rdfs(vs[i], k++);
    }

    return k;
}
// ----------------------------

int main(void){
    int m, K; cin >> n >> m >> K;

    edge = r_edge = vvi(n);
    cmp = vi(n);
    C = vector<char>(n);

    for(auto & e : C) cin >> e;

    rep(loop, m){
        int a, b; cin >> a >> b;
        a--, b--;

        add_edge(a, b);
    }

    int len = scc();

    vector<string> C_sc = vector<string>(len);
    rep(v, n){
        int k = cmp[v];
        C_sc[k] += C[v];
    }
    for(auto & e : C_sc) sort(_all(e));

    vector<set<int>> edge_sc(len);
    rep(v, n){
        int k = cmp[v];

        for(auto & nv : edge[v]){
            int nk = cmp[nv];
            if(k == nk) continue;

            edge_sc[k].insert(nk);
        }
    }

    vector<vector<string>> dp(len, vector<string>(K + 1, "-1"));

    rep(i, len){
        rep(j, (int)C_sc[i].size() + 1){
            if(j > K) break;

            string ns = C_sc[i].substr(0, j);
            if(dp[i][j] == "-1") dp[i][j] = ns;
            chmin(dp[i][j], ns);
        }

        rep(j, K + 1){
            if(dp[i][j] == "-1") continue;

            for(auto & e : edge_sc[i]){
                rep(k, (int)C_sc[e].size() + 1){
                    if(j + k > K) break;

                    string ns = dp[i][j] + C_sc[e].substr(0, k);
                    if(dp[e][j + k] == "-1") dp[e][j + k] = ns;
                    chmin(dp[e][j + k], ns);
                }
            }
        }
    }

    string res = "-1";

    rep(i, len){
        if(dp[i][K] == "-1") continue;

        if(res == "-1") res = dp[i][K];
        chmin(res, dp[i][K]);
    }
    cout << res << endl;

    return 0;
}
