#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;

vi h;
vvi edge;

int dfs(int v, int prev){
    int ret = 0;
    for(auto & u : edge[v]){
        if(u == prev) continue;
        int cur = dfs(u, v);
        if(cur != 0){
            ret += cur;
        }
    }

    if(ret != 0) ret += 2;
    if(ret == 0 and h[v]){
        ret = 2;
    }

    return ret;
}

int main(void){
    int n, x; cin >> n >> x;
    x--;
    h = vi(n);
    for(auto & e : h) cin >> e;

    edge = vvi(n);
    rep(loop, n - 1){
        int a, b; cin >> a >> b;
        a--, b--;

        edge[a].push_back(b);
        edge[b].push_back(a);
    }

    int res = 0;
    for(auto & u : edge[x]){
        res += dfs(u, x);
    }

    cout << res << endl;

	return 0;
}
