#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, k; cin >> n >> k;

    string res = "YES";
    if(n / 2 < k) res = "NO";

    cout << res << endl;

	return 0;
}
