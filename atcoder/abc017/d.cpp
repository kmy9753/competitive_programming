#include <bits/stdc++.h>

using namespace std;

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef long long ll;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

const int mod = 1000000007;

int main(void){
    int n, m; cin >> n >> m;

    vi f(n);
    for(auto && e : f) cin >> e;

    vi dp(n + 1);
    dp[0] = 1;
    vi cnt(m + 1);

    int l = 0, sum = 0;
    rep(i, n){
        cnt[f[i]]++;
        (sum += dp[i]) %= mod;

        while(cnt[f[i]] > 1){
            cnt[f[l]]--;
            (sum += (mod - dp[l])) %= mod;

            l++;
        }

        dp[i + 1] = sum;
    }

    cout << dp[n] << endl;

	return 0;
}
