#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;

    const int M = 4;
    string T = "ACGT";
    map<char, int> dic;
    rep(i, M) dic[T[i]] = i;

    vector<vll> dp(n+1, vll(bit(2*M)));
    dp[0][0] = 1;
    const int mask = bit(2*M) - 1;
    rep(i, n){
        rep(j, bit(2*M)){
            ll cur = dp[i][j];
            if(cur == 0LL) continue;

            rep(k, M){
                int nj = (((j << 2) | k) & mask);

                string S(M, '#');
                rep(l, M){
                    if(i - l >= 0){
                        S[M-l-1] = T[((nj >> (2*l)) & 3)];
                    }
                }
                if(S.substr(1) == "AGC" or 
                   S.substr(1) == "GAC" or
                   S.substr(1) == "ACG" or
                   (S[0] == 'A' and S.substr(2) == "GC") or
                   (S.substr(0, 2) == "AG" and S[3] == 'C')){
                    continue;
                }
                ll& nxt = dp[i+1][nj];
                nxt = ADD(nxt, cur);
            }
        }
    }

    ll res = 0LL;
    for(auto& e : dp[n]){
        res = ADD(res, e);
    }
    cout << res << endl;

    return 0;
}
