#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

ll solve(int m, vi& kill, int n){
    vector<vll> dp(m + 1, vll(n + 1));
    dp[0][0] = 1;
    rep(i, 1, m + 1){
        rep(j, n + 1){
            ll& cur = dp[i][j];
            if(j - i >= 0){
                cur = ADD(dp[i - 1][j], dp[i][j - i], mod);
            }
            else {
                cur = dp[i - 1][j];
            }
        }
    }
    vi zip = {1};
    rep(i, 1, m){
        if(kill[i] == kill[i-1]) zip.back()++;
        else zip.emplace_back(1);
    }
    m = zip.size();

    vector<vll> dp2(m+1, vll(n+1));
    dp2[0][0] = 1;
    rep(i, m){
        rep(j, n+1){
            ll cur = dp2[i][j];
            if(cur == 0) continue;
            rep(nj, j, n+1){
                int d = nj - j;
                dp2[i+1][nj] = ADD(dp2[i+1][nj], MUL(dp[zip[i]][d], cur, mod), mod);
            }
        }
    }

    return dp2[m][n];
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;
    vector<vi> kill(2);
    kill[0] = vi(n);
    kill[1] = vi(m);
    vi dsum(2);
    rep(i, n){
        int a; cin >> a;
        kill[0][i] = a;
        dsum[1] += a;
    }
    rep(i, m){
        int a; cin >> a;
        kill[1][i] = a;
        dsum[0] += a;
    }

    ll A = solve(n, kill[0], dsum[0]);
    ll B = solve(m, kill[1], dsum[1]);
    cout << MUL(A, B, mod) << endl;

    return 0;
}
