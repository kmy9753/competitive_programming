#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
class TSort {
public:
	int n;
	vector<vi> G;
	vi deg,res;
	TSort(int n_) : n(n_), G(n), deg(n, 0) {}
	void add_edge(int from,int to){
		G[from].push_back(to);
		deg[to]++;
	}
	bool solve() {
		queue<int> q;
		for(int i = 0; i < n; i++){
			if(deg[i] == 0){
				q.push(i);
			}
		}
		while(!q.empty()){
			int p = q.front();
			q.pop();
			res.push_back(p);
			for(int v : G[p]){
				if(--deg[v] == 0){
					q.push(v);
				}
			}
		}
		return (*max_element(deg.begin(),deg.end()) == 0);
	}
};

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int m; cin >> m;
    vector<vi> A(m, vi(m-1));
    rep(i, m) rep(j, m-1) cin >> A[i][j], A[i][j]--;

    using Pa = tuple<int, int>;
    map<Pa, int> dic;
    int n = 0;
    rep(i, m){
        rep(j, i+1, m){
            dic[Pa(i, j)] = n;
            dic[Pa(j, i)] = n++;
        }
    }
    assert(n == m * (m - 1) / 2);
    
    TSort ts(n);
    rep(i, m){
        rep(j, 1, m-1){
            int u = dic[Pa(i, A[i][j-1])];
            int v = dic[Pa(i, A[i][j])];
            ts.add_edge(u, v);
        }
    }
    if(not ts.solve()){
        cout << -1 << endl;
        return 0;
    }

    vi dp(n, 1);
    vector<vi>& G = ts.G;

    for(auto& v : ts.res){
        for(auto& nv : G[v]){
            chmax(dp[nv], dp[v] + 1);
        }
    }
    int res = *max_element(_all(dp));

    cout << res << endl;

    return 0;
}
