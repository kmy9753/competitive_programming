#include <bits/stdc++.h>
using ll = long long;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int n, m; cin >> n >> m;

    vector<int> a(n);
    for(auto & e : a) cin >> e;

    int cur = 1, res = 0;
    int ls = 0;
    rep(i, n){
        cur *= a[i];
        while(m < cur and ls < i){
            cur /= a[ls];
            ls++;
        }

        if(m >= cur) res = max(res, i - ls + 1);
    }

    if(cur == 0){
        res = n;
    }
    cout << res << endl;

	return 0;
}
