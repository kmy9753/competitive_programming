#include <bits/stdc++.h>
using ll = long long;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
const int inf = 1LL<<60;

int n, W;
vi v, w;

int res;

void solve1(){
    int n1 = n / 2, n2 = n - n1;
    map<int, int> memo;

    rep(i, 1 << n1){
        int sumw, sumv;
        sumw = sumv = 0;

        rep(j, n1){
            if(((i >> j) & 1) == 1){
                sumw += w[j];
                sumv += v[j];
            }
        }

        if(sumw <= W) memo[sumw] = max(memo[sumw], sumv); 
    }
    
    int maxv = 0;
    for(auto & e : memo){
        maxv = max(maxv, e.second);
        e.second = maxv;
    }

    rep(i, 1 << n2){
        int sumw, sumv;
        sumw = sumv = 0;

        rep(j, n2){
            if(((i >> j) & 1) == 1){
                sumw += w[n1 + j];
                sumv += v[n1 + j];
            }
        }

        if(sumw > W) continue;

        auto itr = memo.upper_bound(W - sumw);
        itr--;
        res = max(res, sumv + itr->second);
    }
}

void solve2(){
    vvi dp(2, vi(1000 * n + 1, inf));
    dp[0][0] = 0;

    rep(i, n){
        int di = (i+1) % 2, pdi = di ^ 1;
        dp[di] = dp[di ^ 1];
        range(j, v[i], 1000 * n + 1){
            dp[di][j] = min(dp[di][j], dp[pdi][j - v[i]] + w[i]);
        }
    }

    rep(i, 2){
        rep(j, 1000 * n + 1){
            if(dp[i][j] <= W){
                res = max(res, j);
            }
        }
    }
}

void solve3(){
    vvi dp(2, vi(1000 * n + 1, 0));
    dp[0][0] = 0;

    rep(i, n){
        int di = (i+1) % 2, pdi = di ^ 1;
        dp[di] = dp[di ^ 1];
        range(j, w[i], 1000 * n + 1){
            dp[di][j] = max(dp[di][j], dp[pdi][j - w[i]] + v[i]);
        }
    }

    rep(i, 2){
        rep(j, 1000 * n + 1){
            if(j <= W){
                res = max(res, dp[i][j]);
            }
        }
    }
}

signed main(void){
    cin >> n >> W;
    v = w = vi(n);

    rep(i, n){
        cin >> v[i] >> w[i];
    }

    if(n <= 30){
        solve1();
    }
    else if(*max_element(begin(v), end(v)) <= 1000){
        solve2();
    }
    else {
        solve3();
    }

    cout << res << endl;

	return 0;
}
