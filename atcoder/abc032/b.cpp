#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    string S;
    int K; cin >> S >> K;

    set<string> s;
    rep(i, (int)S.size()){
        string cur = S.substr(i, K);
        if((int)cur.size() != K) continue;

        s.insert(cur);
    }

    cout << s.size() << endl;

	return 0;
}
