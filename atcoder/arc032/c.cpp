#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
#define fst first
#define snd second
int main(void){
    int n; cin >> n;

    vector<pair<int, int>> ab(n), _ab;
    vector<pair<pair<int, int>, int>> abi(n);
    for(auto & e : ab){
        cin >> e.fst >> e.snd;
    }
    rep(i, n) abi[i].fst = ab[i], abi[i].snd = i + 1;
    _ab = ab;
    sort(begin(ab), end(ab));
    sort(begin(abi), end(abi));

    vector<pair<int, int>> dp(1e6 + 3);
    int idx = n - 1;
    for(int i = 1e6; i >= 0; i--){
        dp[i] = dp[i + 1];
        while(idx >= 0 && ab[idx].fst == i){
            int nn = dp[i + ab[idx].snd - ab[idx].fst].fst;
            if(dp[i].fst == nn + 1){
                dp[i].snd = min(dp[i].snd, abi[idx].snd);
            }
            else if(dp[i].fst < nn + 1){
                dp[i].fst = nn + 1;
                dp[i].snd = abi[idx].snd;
            }
            idx--;
        }
    }

    cout << dp[0].fst << endl;
    idx = 0;
    while(dp[idx].snd != 0){
        cout << (idx != 0 ? " ":"") << dp[idx].snd;
        idx = _ab[dp[idx].snd - 1].snd;
    }
    puts("");

	return 0;
}
