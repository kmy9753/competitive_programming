#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)

using namespace std;

const int inf = 1000000;

int calc_cost(int n, int x){
    
    // [l, r)
    int l = x, r = x + n;

    if(r <= 0){
        return (2 * (-(r - 1)) + n - 1) * n / 2;
    }
    if(0 <= l){
        return (2 * l + n - 1) * n / 2;
    }

    int nl = -l, nr = r - 1;
    return nl * (nl + 1) / 2 + nr * (nr + 1) / 2;
}

int main(void){
    int r, g, b; cin >> r >> g >> b;

    int res = inf;
    rep(xg, -500, 500){
        int xr = min(-100 - r / 2, xg - r),
            xb = max( 100 - b / 2, xg + g);

        res = min(res, calc_cost(r, xr + 100) + calc_cost(g, xg) + calc_cost(b, xb - 100));
    }

    cout << res << endl;

    return 0;
}
