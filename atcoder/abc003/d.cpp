#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
#define int ll 

inline int toInt(string s) {int v; istringstream sin(s);sin>>v;return v;}
template<class T> inline string toString(T x) {ostringstream sout;sout<<x;return sout.str();}
template<class T> inline T sqr(T x) {return x*x;}

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;
typedef pair<int, int> pii;

#define all(a)  (a).begin(),(a).end()
#define rall(a) (a).rbegin(), (a).rend()
#define pb push_back
#define mp make_pair
#define each(i,c) for(typeof((c).begin()) i=(c).begin(); i!=(c).end(); ++i)
#define exist(s,e) ((s).find(e)!=(s).end())
#define range(i,a,b) for(int i=(a);i<(b);++i)
#define rep(i,n)  range(i,0,n)
#define clr(a,b) memset((a), (b) ,sizeof(a))
#define dump(x)  cerr << #x << " = " << (x) << endl;
#define debug(x) cerr << #x << " = " << (x) << " (L" << __LINE__ << ")" << " " << __FILE__ << endl;

const double eps = 1e-10;
const double pi  = acos(-1.0);
const ll INF =1LL << 62;
const int inf =1 << 24;

const int mod = 1000000007;

signed main(void){
    int r, c, x, y, d, l; cin >> r >> c >> x >> y >> d >> l;

    vi p, q;
    range(i, 1, d + l + 1) p.pb(i);
    range(i, 1, d + 1) q.pb(i);
    range(i, 1, l + 1) q.pb(i);

    p.pb(((r - x + 1) * (c - y + 1)));

    int res = 1;
    for(auto && pp : p){
        rep(i, (int)q.size()){
            int qmax = q[i];

            range(j, 1, qmax + 1){
                if(q[i] % j == 0 && pp % j == 0){
                    q[i] /= j;
                    pp   /= j;
                }
            }
        }

        (res *= pp) %= mod;
    }

    cout << res << endl;

	return 0;
}
