#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    int n, t; cin >> n >> t;

    vi a(n);
    for(auto && e : a) cin >> e;

    a.push_back(1e9);

    int st = -1, res = 0;
    for(auto && e : a){
        if(st == -1){
            st = e;
            continue;
        }
        
        res += min(e - st, t);
        st = e;
    }

    cout << res << endl;

	return 0;
}
