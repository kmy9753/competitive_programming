#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
#define F first
#define S second
int main(void){
    int n, d, k; cin >> n >> d >> k;
    
    vector<pair<int, int>> lr(d);
    for(auto && e : lr) cin >> e.F >> e.S;
    vector<pair<int, int>> r(k);
    vector<int> t(k);
    rep(i, k){
        cin >> r[i].F >> t[i];
        r[i].S = r[i].F;
    }

    vector<int> res(k, -1);
    rep(j, d){
        auto && e = lr[j];
        rep(i, k){
            if(res[i] != -1) continue;

            if(e.F <= r[i].S && r[i].F <= e.S){
                r[i].F = min(r[i].F, e.F);
                r[i].S = max(r[i].S, e.S);
            }

            if(r[i].F <= t[i] && t[i] <= r[i].S){
                res[i] = j + 1;
            }
        }
    }

    for(auto && e : res) cout << e << endl;

	return 0;
}
