#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef long long ll;
///////////////////////
const int N = 1e6; 
const ll mod = 1e9 + 7;
ll fact[N+1];
 
ll extgcd(ll a, ll b, ll &x, ll &y) {
  ll d = a;
  if (b) {
    d = extgcd(b, a%b, y, x); 
    y -= (a/b) * x;
  } else {
    x = 1;
    y = 0;
  }
  return d;
}
 
ll mod_inv(ll a, ll m) {
  ll x, y;
  extgcd(a, m, x, y);
  return (m + x % m) % m;
}
///////////////////////
int main(void){
    ll a, b, c; cin >> a >> b >> c;

    ll rr = (((b * c) % mod - (a * c) % mod + mod) % mod) * mod_inv(((a * b) % mod - (b * c) % mod + mod) % mod + (a * c) % mod, mod);
    ll cc = (((b * c) % mod - (a * b) % mod + mod) % mod) * mod_inv(((a * b) % mod - (b * c) % mod + mod) % mod + (a * c) % mod, mod);

    cout << rr % mod << " " << cc % mod << endl;

	return 0;
}
