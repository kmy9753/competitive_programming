#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int a, b, c, k; cin >> a >> b >> c >> k;
    int s, t; cin >> s >> t;

    int res = a * s + b * t;
    if((s + t) >= k){
        res -= (s + t) * c;
    }

    cout << res << endl;

	return 0;
}
