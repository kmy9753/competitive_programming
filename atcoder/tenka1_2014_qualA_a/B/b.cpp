#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <sstream>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define rep(i, n) for(int i = 0; i < n; i ++)
#define COUNT(i, n) for(int i = 1; i <= n; i ++)
#define ITER(c) __typeof((c).begin())
#define each(c, it) for(ITER(c) it =(c).begin(); it!=(c).end(); it++)
#define ALL(c) c.begin(), c.end()
#define mp make_pair
#define pb push_back
#define MEMSET(v, h) memset((v), h, sizeof(v))
#define S first
#define T second.first
#define C second.second

using namespace std; 

typedef pair<int, int> pii;
typedef long long ll;
typedef vector<int> vi;
typedef vector<string> vs;

const int INF = 1 << 24;
const int N = 5;

enum Type{NONE, NORMAL, HARD,};
enum Turn{THROW, ATTACK, BACK,};

ll res = 0;
int comb = 0;
const int TURN[3][3] = {{0, 0, 0}, {1, 3, 13}, {5, 7, 17}};
const int DAMAGE[3] = {0, 10, 50};
const int KABU[3] = {0, 1, 3};

bool able = true;
vector< pair<int, pii> > kabu(N);

void update();

void update();
void init(int);
    
int main(void){

    rep(i, N) init(i);

    string in; cin >> in;

    rep(i, in.size() + 40){
        update();
        update();

        char cur = i < in.size() ? in.at(i):'-';
        vi next;
        rep(j, N) if(kabu[j].S == NONE){ next.pb(j); };

        if(!able || cur == '-' || (cur == 'C' && next.size() < 3) 
                 || next.size() == 0) continue;

        if(cur == 'N') kabu[next[0]].S = NORMAL, kabu[next[0]].C = comb;
        else{
            rep(j, 3) kabu[next[j]].S = HARD, kabu[next[j]].C = comb;
            able = false;
        }
    }
    
    cout << res << endl;

    return 0;
}

void update(){
    bool c = false;

    rep(i, N){
        int s = kabu[i].S;
        if(s == NONE) continue;

        kabu[i].T++;

        if(kabu[i].T == TURN[s][BACK]){
            init(i);
        }

        else if(kabu[i].T == TURN[s][ATTACK]){
            if(s == NORMAL || !c){
                res += (int)(DAMAGE[s] * (1 + (int)(kabu[i].C / 10) * 0.1));
                comb++;
            }

            if(s == HARD) c = true;
        }

        else if(s == HARD && kabu[i].T == TURN[s][THROW]) able = true;
    }
}

void init(int ind){
    kabu[ind].S = NONE;
    kabu[ind].T = 0;
    kabu[ind].C = 0;
}
