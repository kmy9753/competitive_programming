#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> a(n-1);
    for(auto & e : a) cin >> e;
    a.push_back(1<<30);

    vector<int> res(n);
    res[0] = a[0];
    rep(i, n-1){
        res[i+1] = min(a[i], a[i+1]);
    }

    rep(i, n){
        cout << (i ? " ":"") << res[i];
    }
    cout << endl;

	return 0;
}
