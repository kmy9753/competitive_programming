#include <bits/stdc++.h>
#define int long long
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define fst first
#define snd second
using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using pii = pair<int, int>;

vvi sum;

inline int calc(pii p1, pii p2){
    return sum[p2.fst+1][p2.snd+1] - sum[p2.fst+1][p1.snd] - sum[p1.fst][p2.snd+1] + sum[p1.fst][p1.snd];
}

inline bool cross(pii l1, pii r1, pii l2, pii r2){
    return l1.snd <= r2.snd and l1.snd <= r1.snd and
           l1.fst <= r2.fst and l1.fst <= r1.fst;
}

signed main(void){
    int h, w; cin >> h >> w;
    vvi b(h, vi(w));
    rep(y, h) rep(x, w) cin >> b[y][x];

    sum = vvi(h+1, vi(w+1));
    rep(y, h){
        rep(x, w){
            sum[y + 1][x + 1] = b[y][x] + sum[y+1][x] + sum[y][x+1] - sum[y][x];
        }
    }

    int max_v = -(int)1e9;
    pii p1, p2;
    rep(y1, h){
        rep(x1, w){
            range(y2, y1, h){
                range(x2, x1, w){
                    int s = calc(pii(y1, x1), pii(y2, x2));
                    if(s >= max_v){
                        p1 = pii(y1, x1);
                        p2 = pii(y2, x2);
                        max_v = s;
                    }
                }
            }
        }
    }

    int res = max_v;

    // add
    rep(y1, h){
        rep(x1, w){
            range(y2, y1, h){
                range(x2, x1, w){
                    if(cross(pii(y1, x1), pii(y2, x2), p1, p2)) continue;

                    int s = calc(pii(y1, x1), pii(y2, x2));
                    res = max(res, max_v + s);
                }
            }
        }
    }

    // erase
    vector<pii> ps = {p1, p2, pii(p1.fst, p2.snd), pii(p2.fst, p1.snd)};
    range(y1, p1.fst, p2.fst+1){
        rep(x1, p1.snd, p2.snd+1){
            range(y2, p1.fst, p2.fst+1){
                range(x2, p1.snd, p2.snd+1){
                    pii p3 = pii(y1, x1);
                    pii p4 = pii(y2, x2);

                    rep(i, 4){
                        rep(j, 4){
                            pii t1(min(p3.fst, ps[i].fst), min(p3.snd, ps[i].snd));
                            pii t2(max(p3.fst, ps[i].fst), max(p3.snd, ps[i].snd));
                            pii t3(min(p4.fst, ps[j].fst), min(p4.snd, ps[j].snd));
                            pii t4(max(p4.fst, ps[j].fst), max(p4.snd, ps[j].snd));

                            if(cross(t1, t2, t3, t4)) continue;
                        }
                    }
                    if(not cross(p1, p3, p4, p2)){
                        
                    }

                    p1d = pii(p3.fst, p1.snd);
                    p2d = pii(p2.fst, p3.snd);
                    p3d =
                    if(not cross())
                }
            }
        }
    }

    cout << res << endl;

	return 0;
}
