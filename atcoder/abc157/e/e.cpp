#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
// -----------------------
namespace SegmentTrees{
    int inf = 0;
 
    template<typename T>
    class RMQ{
    private:
        int Pow2Fit(int _n){
            int d = 1;
            while((d << 1) <= _n) d <<= 1;
            return d;
        }
 
    public:
        vector<T> dat;
        int n, size;
 
        RMQ(int _n){
            n = Pow2Fit(_n) << 1;
            size = 2 * n - 1;
            dat = vector<T>(size, inf);
        }
        RMQ(){}
 
        // node v := a (0-indexed)
        void set(int v, T a){
 
            // leaf
            v += n - 1;
            dat[v]=a;
 
            // update toward root
            while(v > 0){
                int parent = v = (v - 1) / 2;
                int chl = parent * 2 + 1, chr = parent * 2 + 2;
                dat[parent] = min(dat[chl], dat[chr]);
            }
        }
 
        T get(int v){ // v (0-indexed)
            return dat[v + n - 1];
        }
 
        T query(int a, int b){ // [a,b)
            return query(0, a, b, 0, n);
        }
 
    private:
        T query(int v, int a, int b, int l, int r){ // [a,b)
            if(r <= a || b <= l) return inf; // out range
            if(a <= l && r <= b) return dat[v]; // covered
 
            T vl = query(v * 2 + 1, a, b, l, (l + r) / 2),
              vr = query(v * 2 + 2, a, b, (l + r) / 2, r);

            return min(vl, vr);
        }
    };
}
 
using namespace SegmentTrees;
// -----------------------

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n;
    string in; cin >> in;

    const int M = 'z' - 'a' + 1;
    vector<RMQ<int>> trees(M);
    for(auto& tree : trees){
        tree = RMQ<int>(n);
    }
    rep(i, n){
        trees[in[i] - 'a'].set(i, -1);
    }

    int Q; cin >> Q;
    rep(_, Q){
        int op; cin >> op;
        if(op == 1){
            int i; char c2; cin >> i >> c2;
            i--;
            char c1 = in[i];
            trees[c1 - 'a'].set(i, 0);
            trees[c2 - 'a'].set(i, -1);
            in[i] = c2;
        }
        else {
            int l, r; cin >> l >> r;
            l--;
            int res = 0;
            for(auto& tree : trees){
                int cur = -tree.query(l, r);
                res += cur;
            }
            cout << res << endl;
        }
    }

    return 0;
}
