#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

const int inf = 1 << 29;

int main(void){
    int n, m; cin >> n >> m;

    vvi min_dist(n, vi(n, inf));
    rep(i, n) min_dist[i][i] = 0;
    rep(i, m){
        int u, v, l; cin >> u >> v >> l;
        u--, v--;

        min_dist[u][v] = min_dist[v][u] = l;
    }

    range(k, 1, n) range(i, 1, n) range(j, 1, n)
        min_dist[i][j] = min(min_dist[i][j], min_dist[i][k] + min_dist[k][j]);

    int res = inf;
    range(i, 1, n){
        range(j, i + 1, n){
            res = min(res, min_dist[0][i] + min_dist[i][j] + min_dist[j][0]);
        }
    }

    if(res == inf) res = -1;
    cout << res << endl;

	return 0;
}
