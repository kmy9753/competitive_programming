#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

typedef bool B;
typedef long double D;
typedef complex<D> P;
typedef vector<P> VP;
typedef struct {P s,t;} L;
typedef vector<L> VL;
typedef struct {P c;D r;} C;
typedef vector <C> VC;

const D eps=1.0e-10;
const D pi=acos(-1.0);
template<class T> bool operator==(T a, T b){return abs(a-b)< eps;}
template<class T> bool operator< (T a, T b){return a < b-eps;}
template<class T> bool operator<=(T a, T b){return a < b+eps;}
//template<class T> int sig(T r) {return (r==0||r==-0) ? 0 : r > 0 ? 1 : -1;}
template<class T> int sig(T a,T b = 0) {return a < b ? -1 : b > a ? 1 : 0;}
#define X real()
#define Y imag()

D ip(P a, P b) {return a.X * b.X + a.Y * b.Y;}
D ep(P a, P b) {return a.X * b.Y - a.Y * b.X;}
D sq(D a) {return sqrt(max(a, (D)0));}
P vec(L l){return l.t-l.s;}
inline P input(){D x,y;cin >> x >> y; return P(x,y);}

// $B%=!<%H$N%-!<(B
inline B cmp_x(const P &a,const P &b){
	return (abs(a.X-b.X)<eps ) ?  a.Y<b.Y : a.X<b.X;
}  // base x
inline B cmp_y(const P &a,const P &b){
	return (abs(a.Y-b.Y)<eps ) ?  a.X<b.X : a.Y<b.Y;
}  // base y
inline B cmp_a(const P &a,const P &b){
	return (abs(arg(a)-arg(b))<eps ) ?  norm(a) < norm(b) : arg(a)<arg(b);
} // base arg

// convex_hull
// Verify AOJ 0063
// Verify AOJ CGL_4_A
VP convex_hull(VP pol){
	int n=pol.size(),k=0;
	sort(pol.begin(),pol.end(),cmp_x);
	VP res(2*n);

	//$B0J2<$N(Bwhile$BH=Dj<0$K$D$$$F(B
	//$BFLJq$N@~J,>e$ND:E@$r=|5n$9$k>l9g$O(B<=0
	//$BFLJq$N@~J,>e$ND:E@$r=|5n$7$J$$>l9g$O(B<0

	// down
	rep(i,n){
		while( k>1 && ep(res[k-1]-res[k-2],pol[i]-res[k-1])<0) k--;
		res[k++]=pol[i];
	}
	// up
	for(int i=n-2,t=k;i>=0;i--){
		while( k>t && ep(res[k-1]-res[k-2],pol[i]-res[k-1])<0) k--;
		res[k++]=pol[i];
	}
	res.resize(k-1);
	return res;
}

D convex_len(VP p){
    int n = p.size();

    D ret = 0.0;
    rep(i, n){
        D d = pow(p[(i + 1) % n].X - p[i].X, 2.0)
            + pow(p[(i + 1) % n].Y - p[i].Y, 2.0);
        d = sqrt(d);

        ret += d;
    }

    return ret;
}

int main(void){
    int n; cin >> n;

    VP a(n);
    for(auto && e : a) e = input();

    VP b(n);
    for(auto && e : b) e = input();

    VP conv_a = convex_hull(a);
    VP conv_b = convex_hull(b);

    D l1 = convex_len(conv_a);
    D l2 = convex_len(conv_b);

    printf("%.12Le\n", l2 / l1);

	return 0;
}
