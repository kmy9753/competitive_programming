#include <bits/stdc++.h>

#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)

using namespace std;

int main(void){
    int n, s, t; cin >> n >> s >> t;

    int w; cin >> w;
    vector<int> a(n); a[0] = 0;

    rep(i, n - 1) cin >> a[i + 1];

    int res = 0;
    for(auto && e : a){
        w += e;

        if(s <= w && w <= t) res++;
    }

    cout << res << endl;

	return 0;
}
