#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main(void){
    int r, c, m; cin >> r >> c >> m;
    int n; cin >> n;
    vi ra(n), rb(n), ca(n), cb(n);
    rep(i, n) cin >> ra[i] >> rb[i] >> ca[i] >> cb[i];

    vvi field(r, vi(c));
    rep(i, n){
        range(y, ra[i] - 1, rb[i]){
            range(x, ca[i] - 1, cb[i]){
                (++field[y][x]) %= 4;
            }
        }
    }

    vi res;
    rep(i, n){
        int cnt = 0;
        rep(y, r){
            rep(x, c){
                if(ra[i] - 1 <= y && y < rb[i] &&
                   ca[i] - 1 <= x && x < cb[i]){
                    if(field[y][x] == 1) cnt++;
                }
                else {
                    if(field[y][x] == 0) cnt++;
                }
            }
        }
        if(cnt == m) res.push_back(i + 1);
    }

    for(auto & e : res){
        cout << e << endl;
    }

	return 0;
}
