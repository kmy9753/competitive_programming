#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, q; cin >> n >> q;

    rep(loop, q){
        int a, b, s, t; cin >> a >> b >> s >> t;

        int res = (t - s) * 100;

        a = max(a, s), b = min(b, t);
        if(a < b){
            res -= (b - a) * 100;
        }

        cout << res << endl;
    }

	return 0;
}
