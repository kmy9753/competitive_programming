#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main(void){
    int n, m; cin >> n >> m;
    vector<double> p(n);
    for(auto & e : p) cin >> e, e /= 100.0;

    vector<vector<vector<double>>> dp(n, vector<vector<double>>(m, vector<double>(m)));
    dp[0][0][0] = 1.0;

    range(i, 1, n){
        range(j, 0, m){
            rep(k, m - 1){
                
                // + quick
                dp[i][j][k] += dp[i - 1][j][k + 1] * p[i];
                if(k == 0) {

                    // - full
                    if(j == m - 1) dp[i][j][k] += dp[i - 1][j][k] * p[i];

                    if(j != 0) dp[i][j][k] += dp[i - 1][j - 1][k] * p[i];
                }

                // + slow
                if(j > 1 && k > 0){
                    dp[i][j][k] += dp[i - 1][j - 2][k - 1] * (1.0 - p[i]);
                }

                // - full
                if(j == m - 2 || j == m - 1){
                    dp[i][j][k] += dp[i - 1][j][k] * (1.0 - p[i]);
                }
            }
        }
    }

    double res = 0.0;
    rep(j, m){
        rep(k, m){
            res += (k + (m - (j + 1))) * dp[n - 1][j][k];
        }
    }

    printf("%.12f\n", res);

	return 0;
}
