#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<int> a(3); rep(i, 3) cin >> a[i];

    sort(a.begin(), a.end(), greater<int>());

    int res = 0;
    int ind = 0;
    while(n > 0){
        n -= a[ind];
        (++ind) %= 3;

        res++;
    }

    cout << res << endl;

	return 0;
}
