#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int n, m; 
vector<set<int>> high;
vector<int> used;

void dfs(int ind){
    if(used[ind]) return;

    used[ind]++;

    for(auto & e : high[ind]){
        high[0].insert(e);
        dfs(e);
    }
}

int main(void){
    cin >> n >> m;
    high = vector<set<int>>(n);
    used = vector<int>(n);

    rep(i, m){
        int a, b; cin >> a >> b;
        a--, b--;
        high[b].insert(a);
    }

    dfs(0);

    cout << 1 + high[0].size() << endl;

	return 0;
}
