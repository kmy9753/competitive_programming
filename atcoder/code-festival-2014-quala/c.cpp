#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    int n, m; cin >> n >> m;
    vi p(n), s(m);
    for(auto & e : p) cin >> e;
    for(auto & e : s) cin >> e, e--;

    int res = 0;
    for(auto & e : s){
        res += p[e];
    }

    cout << res << endl;

	return 0;
}
