#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int l, h; cin >> l >> h;
    int n; cin >> n;

    vector<int> a(n);
    for(auto & e : a) cin >> e;

    for(auto & e : a){
        int res = -1;
        if(e <= h){
            if(e >= l){
                res = 0;
            }
            else {
                res = l - e;
            }
        }

        cout << res << endl;
    }

	return 0;
}
