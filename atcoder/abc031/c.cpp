#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int inf = 1 << 20;
int main(void){
    int n; cin >> n;
    vector<int> a(n);
    for(auto & e : a) cin >> e;

    int res = -inf;
    rep(i, n){
        int cur = -inf;
        int maxa = -inf;
        rep(j, n){
            if(i == j) continue;
            int l = min(i, j), r = max(i, j);
            int st = 0, sa = 0;
            bool f = true;
            range(k, l, r + 1){
                if(f) st += a[k];
                else  sa += a[k];
                f ^= 1;
            }
            if(sa > maxa){
                maxa = sa;
                cur = st;
            }
        }
        res = max(res, cur);
    }

    cout << res << endl;

	return 0;
}
