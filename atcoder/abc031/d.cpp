#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
#define fst first
#define snd second
using namespace std;
using VW = pair<string, string>;
int n, m;
bool ok;
vector<string> res;
vector<VW> vw;
vector<int> len;

bool check(){
    for(auto & _ : vw){
        string v = _.fst, w = _.snd;

        int wi = 0;
        for(auto & e : v){
            int idx = e - '1';
            if(wi + len[idx] > (int)w.size()){
                return false;
            }
            string cw = w.substr(wi, len[idx]);
            if(res[idx] == ""){
                res[idx] = cw;
            }
            else {
                if(res[idx] != cw){
                    return false;
                }
            }
            wi += len[idx];
        }
        if(wi != (int)w.size()){
            return false;
        }
    }
    ok = true;

    return true;
}

bool solve(int ci){
    if(ok){
        return true;
    }
    if(ci == m){
        fill(begin(res), end(res), "");
        return check();
    }

    bool ret = false;
    range(i, 1, 4){
        len[ci] = i;
        ret |= solve(ci + 1);
    }

    return ret;
}

int main(void){
    cin >> m >> n;

    vw = vector<VW>(n);
    for(auto & e : vw){
        cin >> e.fst >> e.snd;
    }

    res = vector<string>(m);
    len = vector<int>(m);

    if(solve(0)){
        rep(i, m){
            if(res[i] == ""){
                res[i] = "a";
            }
            cout << res[i] << endl;
        }
    }

	return 0;
}
