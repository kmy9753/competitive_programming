#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> P;
#define mp make_pair
#define fst first
#define snd second
int nn;
vvi edge;

#include <sys/time.h>
double gett(){
  struct timeval t;
  gettimeofday(&t,NULL);
  return t.tv_sec + t.tv_usec/1e6;
}

bool dfs(int v, vi & visited, int c){
    if(visited[v] == c){
        return false;
    }
    visited[v] = c;

    bool ret = true;
    for(auto & u : edge[v]){
        ret &= dfs(u, visited, c);
    }

    return ret;
}
int main(void){
    double st = gett();
    int n; cin >> n;
    vi a(n);
    for(auto & e : a) cin >> e;
    int h, w; cin >> h >> w;
    vvi field(h, vi(w));
    for(auto & ee : field) for(auto & e : ee) cin >> e;

    vi idx(n, -1);
    vector<pair<P, P>> rect;
    rep(y, h){
        rep(x, w){
            int c = field[y][x];
            if(idx[c] == -1){
                rect.push_back(mp(P(y, x), P(y + 1, x + 1)));
                idx[c] = rect.size() - 1;
            }
            else {
                int i = idx[c];
                P & p1 = rect[i].fst;
                P & p2 = rect[i].snd;

                p1.fst = min(p1.fst, y);
                p1.snd = min(p1.snd, x);
                p2.fst = max(p2.fst, y + 1);
                p2.snd = max(p2.snd, x + 1);
            }
            field[y][x] = idx[c];
        }
    }

    nn = (int)rect.size();
    //cerr << nn << endl;
    //rep(i, nn){
    //    cerr << rect[i].fst.fst << " " << rect[i].fst.snd << " " <<
    //            rect[i].snd.fst << " " << rect[i].snd.snd << endl;
    //}
    edge = vvi(nn);
    rep(i, nn){
        range(j, i + 1, nn){
            P pi1 = rect[i].fst, pi2 = rect[i].snd;
            P pj1 = rect[j].fst, pj2 = rect[j].snd;

            bool i2j = false, j2i = false;
            range(y, max(pi1.fst, pj1.fst), min(pi2.fst, pj2.fst)){
                range(x, max(pi1.snd, pj1.snd), min(pi2.snd, pj2.snd)){
                    if(field[y][x] == j){
                        if(!i2j) edge[i].push_back(j);
                        i2j = true;
                    }
                    else {
                        if(!j2i) edge[j].push_back(i);
                        j2i = true; 
                    }
                }
            }
        }
    }

    //rep(v, nn){
    //    for(auto & u : edge[v]){
    //        cerr << u << " ";
    //    }
    //    cerr << endl;
    //}

    vi visited(nn, -1);
    rep(i, nn){
        if(visited[i] != -1) continue;
        if(!dfs(i, visited, i)){
            cout << 0 << endl;
            return 0;
        }
    }

    auto myrand = bind(uniform_int_distribution<int>(0, 1<<30), mt19937(static_cast<unsigned int>(time(nullptr))));
    
    vi cs(nn);
    while(gett() - st < 2.9){
        vi aa = a;
        rep(i, nn){
            int rest = n - i, ii = myrand() % rest;
            int c = aa[ii];
            cs[i] = c;
            aa[ii] = -1;
            swap(aa[ii], aa[rest - 1]);
        }
        bool ok = true;
        rep(v, nn){
            for(auto & u : edge[v]){
                if(cs[u] <= cs[v]){
                    ok = false;
                    break;
                }
            }
            if(!ok) break;
        }
        if(ok){
            cout << 1 << endl;
            return 0;
        }
    }
    cout << 0 << endl;

	return 0;
}
