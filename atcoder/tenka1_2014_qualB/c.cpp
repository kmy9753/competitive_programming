#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    vector<string> field(n);
    for(auto & e : field) cin >> e;

    vector<string> res = field;
    range(y, 1, n){
        rep(x, n){
            int cnt = 0;
            if(x - 1 >= 0 && res[y - 1][x - 1] == '#') cnt++;
            if(x + 1 <  n && res[y - 1][x + 1] == '#') cnt++;
            if(y - 2 >= 0 && res[y - 2][x]     == '#') cnt++;

            if(cnt % 2 == 1){
                res[y][x] = field[y - 1][x] == '#' ? '.':'#';
            }
            else {
                res[y][x] = field[y - 1][x] == '#' ? '#':'.';
            }
        }
    }

    for(auto & e : res){
        cout << e << endl;
    }

	return 0;
}
