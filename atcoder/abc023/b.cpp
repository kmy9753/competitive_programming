#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    string s; cin >> s;

    string t = "b";
    int res = -1;

    range(i, 1, 100){
        if(s == t) res = i - 1;

        if(i % 3 == 1){
            t = "a" + t + "c";
        }
        else if(i % 3 == 2){
            t = "c" + t + "a";
        }
        else if(i % 3 == 0){
            t = "b" + t + "b";
        }
    }

    cout << res << endl;

	return 0;
}
