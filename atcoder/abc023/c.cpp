#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define int ll

#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)

#define F first
#define S second

signed main(void){
    int r, c, k; cin >> r >> c >> k;
    int n; cin >> n;

    vector<int> rc(r), cc(c);
    set<pair<int, int>> s;

    rep(loop, n){
        int ri, ci; cin >> ri >> ci;
        ri--, ci--;

        rc[ri]++, cc[ci]++;
        s.insert(make_pair(ri, ci));
    }

    vector<int> rc_orig = rc, cc_orig = cc;

    sort(cc.begin(), cc.end());

    int res = 0;
    for(auto && e : rc){
        int kk = k - e;

        res += upper_bound(cc.begin(), cc.end(), kk)
            -  lower_bound(cc.begin(), cc.end(), kk);
    }

    for(auto && e : s){
        int ri = e.F, ci = e.S;

        if(rc_orig[ri] + cc_orig[ci]     == k) res--;
        if(rc_orig[ri] + cc_orig[ci] - 1 == k) res++;
    }

    cout << res << endl;

	return 0;
}
