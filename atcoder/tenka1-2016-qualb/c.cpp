#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

using R = long double;

const int N = 31;
int A[N][N];
R dp[N][N];
R dp1[N][N];

using WI = tuple<int, int>;

int main(void){
    int n; cin >> n;
    string pq; cin >> pq;
    pq[pq.find('/')] = ' ';
    stringstream ss(pq);
    int p, q; ss >> p >> q;
    R prob = 1.0 * p / q;

    rep(i, n) rep(j, n) cin >> A[i][j];

    vector<WI> tbl(n);
    rep(i, n){
        int woncnt = 0;
        rep(j, n){
            woncnt += A[i][j];
        }
        tbl[i] = WI(woncnt, n - i);
    }
    sort(_all(tbl), greater<WI>());

    vi r2i(n);
    vi i2r(n);
    rep(i, n){
        int idx;
        tie(ignore, idx) = tbl[i];
        idx = n - idx;

        r2i[i] = idx;
        i2r[idx] = i;
    }

    rep(r, n){
        dp1[r][0] = 1.0;

        int i = r2i[r];
        rep(j, n){
            if(i == j) continue;
            int cur = A[i][j];
            R wonp = cur ? prob : (1.0 - prob);

            vector<R> next(n + 1);
            rep(k, n){
                next[k + 1] += dp1[r][k] * wonp;
                next[k] += dp1[r][k] * (1.0 - wonp);
            }
            rep(k, n + 1){
                dp1[r][k] = next[k];
            }
        }
    }

    int preidx = -1;
    dp[0][n] = 1.0;
    rep(r, n){
        int idx = r2i[r];

        rep(pw, n + 1){
            rep(w, pw + 1){
                if(pw == w and preidx > idx){
                    continue;
                }
                dp[r + 1][w] += dp[r][pw] * dp1[r][w];
            }
        }

        preidx = idx;
    }

    R res = 0.0;
    rep(i, n + 1){
        res += dp[n][i];
    }
    cout.precision(20);
    cout << fixed << res << endl;

    return 0;
}
