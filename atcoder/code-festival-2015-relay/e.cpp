#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int h1, h2, m1, m2; cin >> h1 >> m1 >> h2 >> m2;

    while(h2 != 12){
        if( (h2 + 6) % 12  < h1 || 
           ((h2 + 6) % 12 == h1 && (m2 + 30) % 60 <= m1)){
            cout << "Yes" << endl;
            return 0;
        }

        m2++;
        if(m2 == 60){
            h2++;
            m2 = 0;
        }
    }

    cout << "No" << endl;

	return 0;
}
