#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[6]={1,0,-1,0,1,-1};
const int dy[6]={0,1,0,-1,1,1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, X, Y; cin >> n >> X >> Y;
    const int M = 200;
    map<int, map<int, int>> wall;
    rep(_, n){
        int x, y; cin >> x >> y;
        wall[x][y] = true;
    }
    using Elem = tuple<int, int>;
    queue<Elem> q;
    q.push(Elem(0, 0));

    map<int, map<int, int>> min_dist;
    min_dist[0][0] = 0;

    while(q.size()){
        int x, y; tie(x, y) = q.front();
        q.pop();

        int cur = min_dist[x][y];
        if(x == X and y == Y){
            cout << cur << endl;
            return 0;
        }

        rep(i, 6){
            int nx = x + dx[i];
            int ny = y + dy[i];
            if(nx < -2*M-1 or 2*M+1 < nx or
               ny < -2*M-1 or 2*M+1 < ny) continue;

            if(wall[nx][ny]) continue;

            if(min_dist.find(nx) == end(min_dist) or
               min_dist[nx].find(ny) == end(min_dist[nx])){
                min_dist[nx][ny] = cur + 1;
                q.push(Elem(nx, ny));
            }
        }
    }

    cout << -1 << endl;

    return 0;
}
