#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n, m; cin >> n >> m;
    vector<vi> G(n);
    rep(_, m){
        int a, b; cin >> a >> b;
        a--, b--;
        G[a].emplace_back(b);
        G[b].emplace_back(a);
    }

    int sv; cin >> sv;
    sv--;
    int K; cin >> K;
    vi t(K); for(auto& e : t) cin >> e, e--;
    t.emplace_back(sv);

    vector<vi> D(K + 1, vi(K + 1, inf));
    {
        vector<vi> DD(K + 1, vi(n, inf));
        rep(i, K+1){
            auto& dp = DD[i];
            int s = (i == K ? sv : t[i]);
            dp[s] = 0;
            queue<int> q; q.push(s);
            while(q.size()){
                int v = q.front(); q.pop();
                for(auto& nv : G[v]){
                    if(chmin(dp[nv], dp[v] + 1)){
                        q.push(nv);
                    }
                }
            }
            rep(j, K+1){
                int k = (j == K ? sv : t[j]);
                D[i][j] = dp[k];
            }
        }
    }

    vector<vi> dp(K, vi(bit(K), inf));

    using Elem = tuple<int, int, int>;
    priority_queue<Elem, vector<Elem>, greater<Elem>> q;
    rep(i, K){
        int d = D[K][i];
        q.push(Elem(d, i, 1 << i));
        dp[i][1 << i] = d;
    }

    while(q.size()){
        int cost, v, stat; tie(cost, v, stat) = q.top();
        q.pop();

        rep(nv, K){
            int nstat = stat | (1 << nv);
            int& nxt = dp[nv][nstat];
            int ncost = cost + D[v][nv];
            if(chmin(nxt, ncost)){
                q.push(Elem(ncost, nv, nstat));
            }
        }
    }

    int res = inf;
    rep(i, K){
        chmin(res, dp[i][bit(K)-1]);
    }
    cout << res << endl;

    return 0;
}
