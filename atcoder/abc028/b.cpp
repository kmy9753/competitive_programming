#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
const string T = "ABCDEF";
int main(void){
    string s; cin >> s;

    rep(i, 6){
        cout << (i ? " ":"") << count(s.begin(), s.end(), T[i]);
    }

    cout << endl;

	return 0;
}
