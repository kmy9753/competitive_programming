#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;

    string res = "Perfect";
    if(n <= 59) res = "Bad";
    else if(n <= 89) res = "Good";
    else if(n <= 99) res = "Great";

    cout << res << endl;

	return 0;
}
