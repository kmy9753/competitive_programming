#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    double n, k; cin >> n >> k;

    double o1 = (k - 1.0) * (n - k) * 6.0;
    double o2 = (n - 1.0) * 3.0;
    double o3 = 1.0;

    printf("%.12f\n", (o1 + o2 + o3) / pow(n, 3.0));

	return 0;
}
