#include <bits/stdc++.h>
#define int long long 
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const int inf = INT_MAX;
const ll INF = LLONG_MAX;
const ll MOD = 1000000007LL;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b, const ll mod=MOD) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b, const ll mod=MOD) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b, const ll mod=MOD) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b, const ll mod=MOD) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
ll K;
int nz;
vll ap, an;

bool check(ll x){
    int np = ap.size(),
        nn = an.size();

    ll cnt = 0LL;

    // neg
    if(x < 0LL){
        for(auto& a : ap){
            ll b = (-x - 1) / a;
            cnt += nn - (int)(upper_bound(_all(an), b) - begin(an));
        }
    }
    else {
        cnt += 1LL * np * nn;
    }
    
    if(x >= 0LL){
        // zero
        cnt += nz * (np + nn);
        cnt += nz * (nz - 1) / 2;

        // pos
        for(auto& vec : {ap, an}){
            int n = vec.size();
            rep(i, n){
                auto a = vec[i];
                ll b = x / a;
                cnt += max(0LL, (int)(upper_bound(_all(vec), b) - (begin(vec) + i+1)));
            }
        }
    }

    return cnt >= K;
}

signed main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n; cin >> n >> K;
    vi a(n); for(auto& e : a) cin >> e;
    for(auto& e : a){
        if(e > 0) ap.emplace_back(e);
        else if(e < 0) an.emplace_back(-e);
        else nz++;
    }
    sort(_all(ap));
    sort(_all(an));

    const ll M = (ll)(1e18);
    ll lb = -M, ub = M;
    rep(loop, 70){
        ll mid = (lb + ub) / 2;
        if(check(mid)){
            ub = mid;
        }
        else {
            lb = mid;
        }
    }

    cout << ub << endl;

    return 0;
}
