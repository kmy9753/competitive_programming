#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, l; cin >> n >> l;

    string S; cin >> S;
    int cur = 1, res = 0;
    for(auto & e : S){
        cur += (e == '+' ? 1:-1);
        if(cur > l){
            res++;
            cur = 1;
        }
    }

    cout << res << endl;

	return 0;
}
