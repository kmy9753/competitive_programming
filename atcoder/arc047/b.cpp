#include <bits/stdc++.h>
#define int long long 
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
const int inf = 1LL << 40;
int lx = inf, rx = -inf, ly = inf, ry = -inf;

int resx, resy;

void dfs(int d){
}

signed main(void){
    int n; cin >> n;

    vector<int> xs(n), ys(n);
    vector<int> xs_org(n), ys_org(n);
    
    rep(i, n){
        int x, y; cin >> x >> y;
        int xx = x + y, yy = x - y;
        xs[i] = xx, ys[i] = yy;
        xs_org[i] = x, ys_org[i] = y;

        lx = min(lx, xx), rx = max(rx, xx);
        ly = min(ly, yy), ry = max(ry, yy);
    }

    int sz = max(rx - lx, ry - ly) + 1;

    vector<int> candx = {lx + sz / 2, rx - sz / 2}, candy = {ly + sz / 2, ry - sz / 2};

    rep(i, 2){
        rep(j, 2){
            int py = (candx[i] - candy[j]) / 2, px = candx[i] - py;
            //+cerr << px << " "  << py << endl;

            int D = abs(px - xs_org[0]) + abs(py - ys_org[0]);
            [&]{
                rep(k, n){
                    if(abs(px - xs_org[k]) + abs(py - ys_org[k]) != D)
                        return;
                }
                resx = px, resy = py;
            }();
        }
    }

    cout << resx << " " << resy << endl;

	return 0;
}
