#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, m; cin >> n >> m;
    int x, y; cin >> x >> y;
    vector<int> a(n), b(m);
    for(auto & e : a) cin >> e;
    for(auto & e : b) cin >> e;
    
    int ct = 0, cidx = 0, res = 0;
    for(auto & e : a){
        if(e < ct){
            continue;
        }

        ct = e + x;
        while(cidx < m && b[cidx] < ct) cidx++;
        if(cidx < m){
            ct = b[cidx] + y;
            res++;
        }
    }

    cout << res << endl;

	return 0;
}
