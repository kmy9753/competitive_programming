#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
inline int s2i(string k){
    stringstream ss(k);
    int ret;
    ss >> ret;
    return ret;
}

signed main(void){
    int n, a; cin >> n >> a; a--;
    string k; cin >> k;
    vector<int> b(n), v2i(n);
    for(auto & e : b) cin >> e, e--;
    rep(i, n) v2i[b[i]] = i;

    int idx = a;
    vector<int> ord(n, -1);
    int cnt = 0;
    while(ord[idx] == -1){
        if((int)k.size() < 8 && cnt == s2i(k)){
            cout << idx + 1 << endl;
            return 0;
        }
        ord[idx] = cnt++;
        idx = b[idx];
    }
    int p = cnt - ord[idx];

    int kk = 0;
    for(auto & e : k){
        kk = (kk * 10 + e - '0') % p;
    }

    while(kk < ord[idx]) kk += p;
    idx = a;
    while(kk > 0){
        idx = b[idx];
        kk--;
    }

    cout << idx + 1 << endl;


	return 0;
}
