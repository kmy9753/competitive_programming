#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n, m; cin >> n >> m;

    double tm = 360.0 * m / 60.0;
    double tn = 360.0 * (n % 12) / 12.0 + 30.0 * (m / 60.0);

    printf("%.12f\n", min(fabs(tn - tm), 360.0 - fabs(tn - tm)));
 
	return 0;
}
