#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
inline bool check(string & s){
    rep(i, (int)s.size() / 2){
        if(s[i] != s[i + s.size() / 2]) return false;
    }
    return true;
}
int main(void){
    int n; cin >> n;
    string s; cin >> s;

    int res = 1e5;
    rep(_, n){
        string s1 = s.substr(0, _);
        string s2 = s.substr(_);

        cerr << s1 << " " << s2 << endl;

        vector<int> cnt(s2.size());
        for(auto & e : s1){
            for(int i = (int)s2.size() - 1; i >= 0; i--){
                if(e != s2[i]) continue;

                int cur = cnt[i];
                range(j, i, (int)s2.size()){
                    if(cnt[j] > cur) break;
                    cnt[j]++;
                }
            }
        }
        int me = *max_element(begin(cnt), end(cnt));

        res = min(res, n - 2 * me);
    }

    if(res == 1e5) res = -1;
    cout << res << endl;

	return 0;
}
