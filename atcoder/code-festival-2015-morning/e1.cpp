#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    rep(i, 1e7){
        int nn = n + i;
        if((int)sqrt(nn) * (int)sqrt(nn) == nn){
            cout << i << endl;
            return 0;
        }
    }

	return 0;
}
