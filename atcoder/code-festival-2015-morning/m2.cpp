#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
inline bool check(string & s){
    rep(i, (int)s.size() / 2){
        if(s[i] != s[i + s.size() / 2]) return false;
    }
    return true;
}
int main(void){
    int n; cin >> n;
    string s; cin >> s;

    int res = 1e5;
    range(_, 0, n){
        string s1 = s.substr(0, _);
        string s2 = s.substr(_);

        vector<vector<int>> dp(s1.size() + 1, vector<int>(s2.size() + 1, -1));
        dp[0][0] = 0;

        rep(i, (int)s1.size()){
            rep(j, (int)s2.size()){
                if(dp[i][j] == -1){
                    continue;
                }

                dp[i + 1][j] = max(dp[i + 1][j], dp[i][j]);

                range(k, j, (int)s2.size()){
                    if(s1[i] == s2[k]){
                        dp[i + 1][k + 1] = max(dp[i + 1][k + 1], dp[i][j] + 1);
                    }
                }
            }
        }

        res = min(res, n - *max_element(begin(dp[s1.size()]), end(dp[s1.size()])) * 2);
    }

    if(res == 1e5) res = -1;
    cout << res << endl;

	return 0;
}
