#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int n; cin >> n;
    string s; cin >> s;

    if(n % 2 != 0){
        cout << -1 << endl;
        return 0;
    }

    int len = s.size();
    int res = 0;
    rep(i, len / 2){
        if(s[i] != s[i + len/2]){
            res++;
        }
    }

    cout << res << endl;

	return 0;
}
