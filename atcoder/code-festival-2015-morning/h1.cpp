#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int n; cin >> n;

    vector<int> a(n);
    int ls = 0, rs = n - 1;
    int lcnt = 0, rcnt = 0;
    for(auto & e : a){
        cin >> e;
    }

    int res = 0;
    while(n >= 3){
        int ldiff = 2 * a[ls] + a[ls + 1] + 2 * lcnt + 1;
        int rdiff = 2 * a[rs] + a[rs - 1] + 2 * rcnt + 1;
        if(ldiff < rdiff){
            res += ldiff;
            lcnt += a[ls] + a[ls + 1] + 2;
            ls += 2;
        }
        else {
            res += rdiff;
            rcnt += a[rs] + a[rs - 1] + 2;
            rs -= 2;
        }
        n -= 2;
    }

    cout << res << endl;

	return 0;
}
