#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
signed main(void){
    int n, k, m, r; cin >> n >> k >> m >> r;

    vi s(n - 1);
    for(auto & e : s){
        cin >> e;
    }
    sort(begin(s), end(s), greater<int>());
    s.push_back(0);

    int sum = 0;
    rep(i, k){
        sum += s[i];
    }

    if(sum >= r * k){
        cout << 0 << endl;
        return 0;
    }
    
    int rest = r * k - (sum - s[k - 1]);
    if(rest > m){
        cout << -1 << endl;
    }
    else {
        cout << rest << endl;
    }

	return 0;
}
