#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

const int N = 1e6 + 1;
const int mod = 1e9 + 7;

// [1, n]
int bit[4][N * 2];

int sum(int i, int d){
    int s = 0;
    while(i > 0){
        (s += bit[d][i]) %= mod;
        i -= i & -i;
    }
    return s;
}
void add(int i, int x, int d){
    while(i <= N){
        (bit[d][i] += x) %= mod;
        i += i & -i;
    }
}

signed main(void){
    int n; cin >> n;
    vector<int> cnt(N);
    rep(loop, n){
        int e; cin >> e;
        add(e, 1, 0);
        cnt[e]++;
    }

    range(i, 1, 4){
        range(j, 1, N / 2){
            add(j, ((sum(N, i - 1) - sum(j * 2 - 1, i - 1) + mod) % mod) * cnt[j] % mod, i);
        }
    }

    cout << sum(N, 3) << endl;

	return 0;
}
