#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
signed main(void){
    int n, a, b; cin >> n >> a >> b;

    vector<int> s(n);
    for(auto & e : s) cin >> e;

    int smax = *max_element(begin(s), end(s)),
        smin = *min_element(begin(s), end(s));

    if(smax == smin){
        cout << -1 << endl;
        return 0;
    }

    double p = 1.0 * b / (smax - smin);

    int sum = 0;
    for(auto & e : s) sum += e;

    double q = 1.0 * a - (1.0 / n * p * sum);

    printf("%.12f %.12f\n", p, q);

	return 0;
}
