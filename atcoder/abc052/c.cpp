#include <iostream>

using namespace std;

const int N_MAX = 1000;
const int MOD = 1e9 + 7; // = 1000000007;

int main(void){
    int N;
    cin >> N;

    int primeCnt[N_MAX + 1];
    for(int i = 0; i <= N; i++){
        primeCnt[i] = 0;
    }

    for(int i = 1; i <= N; i++){
        int num = i;
        for(int j = 2; j <= i; j++){
            while(num % j == 0){
                primeCnt[j]++;
                num /= j;
            }
        }
    }

    long long res = 1LL;
    for(int i = 2; i <= N; i++){
        res *= (primeCnt[i] + 1);
        res %= MOD;
    }

    cout << res << endl;
    
    return 0;
}
