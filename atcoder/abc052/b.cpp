#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

int main(void){
    int N; cin >> N;
    string S; cin >> S;

    int cur, res;
    cur = res = 0;

    for(int i = 0; i < N; i++){
        char c = S[i];
        if(c == 'I'){
            cur++;
        }
        else {
            cur--;
        }
        res = max(res, cur);
    }

    cout << res << endl;
    
    return 0;
}
