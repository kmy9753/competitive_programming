#include <iostream>
#include <algorithm>

using namespace std;

const int N_MAX = 1e5; // = 100000

int main(void){
    int N, A, B;
    cin >> N >> A >> B;

    int x[N_MAX];
    for(int i = 0; i < N; i++){
        cin >> x[i];
    }

    long long res = 0;
    for(int i = 1; i < N; i++){
        long long len = x[i] - x[i - 1];
        long long diff = min(len * A, 1LL * B);

        res += diff;
    }

    cout << res << endl;
    
    return 0;
}
