#include <iostream>
#include <algorithm>

using namespace std;

int main(void){
    int A, B, C, D;
    cin >> A >> B >> C >> D;

    int res = max(A * B, C * D);
    cout << res << endl;
    
    return 0;
}
