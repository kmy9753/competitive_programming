#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    int n; cin >> n;
    vi w(n); for(auto & e : w) cin >> e;

    vi b(n, 1e7);
    for(auto & e : w){
        b[lower_bound(begin(b), end(b), e) - begin(b)] = e;
    }

    cout << find(begin(b), end(b), 1e7) - begin(b) << endl;

	return 0;
}
