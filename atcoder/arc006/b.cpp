#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
int main(void){
    int n, l; cin >> n >> l;
    cin.ignore();

    vi to(n);
    rep(i, n) to[i] = i;

    rep(h, l){
        string row;
        getline(cin, row);

        rep(i, (int)row.size()){
            if(row[i] != '-') continue;

            int i1 = i / 2, i2 = i1 + 1;
            swap(to[i1], to[i2]);
        }
    }

    string b; getline(cin, b);
    int res;
    rep(i, (int)b.size()){
        if(b[i] == 'o'){
            res = to[i / 2] + 1;
        }
    }

    cout << res << endl;

	return 0;
}
