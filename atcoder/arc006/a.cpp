#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    set<int> s;
    rep(i, 6){
        int e; cin >> e;
        s.insert(e);
    }

    int b; cin >> b;
    int cnt = 0, B = 0;
    rep(i, 6){
        int e; cin >> e;
        if(s.find(e) != s.end()) cnt++;
        if(e == b) B = 1;
    }

    int res = 0;
    switch(cnt){
        case 6: res = 1; break;
        case 5: res = B ? 2:3; break;
        case 4: res = 4; break;
        case 3: res = 5; break;
    }

    cout << res << endl;

	return 0;
}
