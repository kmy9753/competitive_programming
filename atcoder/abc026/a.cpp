#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int a; cin >> a;

    int res = 0;
    rep(x, 1000){
        rep(y, 1000){
            if(x + y == a){
                res = max(res, x * y);
            }
        }
    }

    cout << res << endl;

	return 0;
}
