#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;

int n;
vector<int> edge[33];

int dfs(int v){
    int ret = 1;
    int lb = 1 << 24, ub = 0;

    for(auto && u : edge[v]){
        int cur = dfs(u);
        lb = min(lb, cur);
        ub = max(ub, cur);
    }

    if(ub != 0){
        ret += lb + ub;
    }

    return ret;
}

int main(void){
    cin >> n;

    range(i, 1, n){
        int p; cin >> p;
        p--;

        edge[p].push_back(i);
    }

    cout << dfs(0) << endl;

	return 0;
}
