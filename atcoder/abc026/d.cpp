#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
double eps = 1e-9;
const double pi = acos(-1);

double a, b, c;

double f(double t){
    return a * t + b * sin(c * t * pi);
}

// f(t) >= 100 ?
bool check(double t){
    return f(t) + eps >= 100;
}

int main(void){
    cin >> a >> b >> c;

    double ub = 1e6, lb = 0.0;
    rep(i, 1000){
        double mid = (ub + lb) / 2.0;

        if(check(mid)){
            ub = mid;
        }
        else {
            lb = mid;
        }
    }

    printf("%.12f\n", ub);

	return 0;
}
