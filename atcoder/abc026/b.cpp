#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
const double pi = acos(-1);

int main(void){
    int n; cin >> n;
    
    vi r(n);
    for(auto && e : r) cin >> e;
    sort(r.begin(), r.end(), greater<int>());

    double res = 0.0;
    rep(i, n){
        double s = r[i] * r[i];
        if(i % 2) res -= s;
        else      res += s;
    }
    res *= pi;

    printf("%.12f\n", res);

	return 0;
}
