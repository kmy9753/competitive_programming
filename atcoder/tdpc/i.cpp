#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
#define pb push_back
typedef vector<int> vi;
typedef vector<vi> vvi;

const string T = "iwi";

int main(void){
    string s; cin >> s;

    int res = 0;
    string cur_s = s;

    rep(j, 3){
        rep(loop, 400){
            string next_s;
            rep(i, (int)cur_s.size()){
                if(cur_s.substr(i, j == 2 ? 3:4) == (j == 1 ? "i":"") + T + (j == 0 ? "i":"")){
                    res++;
                    if(j != 2) next_s.pb('i');
                    i += (j == 2 ? 2:3);
                    continue;
                }
                next_s.pb(cur_s[i]);
            }

            cur_s = next_s;
        }
    }

    cout << res << endl;

	return 0;
}
