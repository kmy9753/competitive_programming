#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
int main(void){
    int k; cin >> k;

    vector<double> r(1 << k);
    for(auto && e : r) cin >> e;

    vector<vector<double>> dp(k + 1, vector<double>(1 << k));
    rep(i, 1 << k) dp[0][i] = 1.0;

    range(i, 1, k + 1){
        rep(j, 1 << k){
            int idx_block = j / (1 << i);
            bool fwd = j % (1 << i) / (1 << (i - 1)) == 0;

            int start = idx_block * (1 << i) + (fwd ? (1 << (i - 1)):0);
            range(jj, start, start + (1 << (i - 1))){
                double p = 1.0 / (1.0 + pow(10.0, (r[jj] - r[j]) / 400.0));
                dp[i][j] += dp[i - 1][j] * dp[i - 1][jj] * p;
            }
        }
    }

    for(auto && e : dp[k]){
        printf("%.12f\n", e);
    }

	return 0;
}
