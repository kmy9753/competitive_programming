#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
#define pb push_back
typedef vector<int> vi;
typedef vector<vi> vvi;

const string T = "iwi";

int main(void){
    string s; cin >> s;
    int len = s.size();
    rep(i, 400) s.pb('#');

    vvi dp(len / 3 + 1, vi(len));
    rep(i, len){
        if(s.substr(i, 3) == "iwi"){
            dp[1][i] = 1;
        }
    }

    range(i, 2, len / 3 + 1){
        rep(j, len){
            if(s[j] != 'i') continue;

            rep(k, 3){
                if(s.substr(j, k) == T.substr(0, k) && dp[i - 1][j + k] && s.substr(j + k + 3 * (i - 1), 3 - k) == T.substr(k, 3 - k)){
                    dp[i][j] = 1;
                }
            }
        }
    }

    int res = 0;

    rep(i, len){
        int diffi = 0, diffres = 0;
        range(j, 1, len / 3 + 1){
            if(dp[j][i] == 0 || diffres >= j) continue;
            
            diffres = j;
            diffi = 3 * j - 1;
        }

        res += diffres;
        i += diffi;
    }

    cout << res << endl;

	return 0;
}
