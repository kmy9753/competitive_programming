#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main(void){
    int na, nb; cin >> na >> nb;

    vi a(na), b(nb);
    for(auto && e : a) cin >> e;
    for(auto && e : b) cin >> e;

    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());

    int even = (na + nb) % 2 == 0 ? 1:0;
    vvi dp(na + 1, vi(nb + 1));

    range(i, 1, na + 1){
        dp[i][0] = dp[i - 1][0];
        if((i + even) % 2) dp[i][0] += a[i - 1];
    }
    range(i, 1, nb + 1){
        dp[0][i] = dp[0][i - 1];
        if((i + even) % 2) dp[0][i] += b[i - 1];
    }

    range(i, 1, na + 1){
        range(j, 1, nb + 1){
            if(((i + j) + even) % 2){
                dp[i][j] = max(dp[i - 1][j] + a[i - 1], dp[i][j - 1] + b[j - 1]);
            }
            else {
                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]);
            }
        }
    }

    cout << dp[na][nb] << endl;

	return 0;
}
