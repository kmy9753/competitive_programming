#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
int main(void){
    int n; cin >> n;

    vi p(n);
    for(auto && e : p) cin >> e;

    vi dp(10001, 0);
    dp[0] = 1;
    rep(i, n){
        for(int j = 10000 - p[i]; j >= 0; j--){
            if(dp[j] == 0) continue;
            dp[j + p[i]] = 1;
        }
    }

    int sum = 0;
    for(auto && e : dp){
        sum += e;
    }

    cout << sum << endl;

	return 0;
}
