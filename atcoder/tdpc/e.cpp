#include <bits/stdc++.h>
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

const int mod = 1e9 + 7;

int main(void){
    int d; cin >> d;
    string n; cin >> n;
    int nn = n.size();

    vector<vvi> dp(nn + 1, vvi(d, vi(2)));
    dp[0][0][0] = 1;

    range(i, 1, nn + 1){
        rep(j, d){
            int ub = n[i - 1] - '0';
            rep(k, ub){
                (dp[i][j][1] += dp[i - 1][(j + k) % d][0]) %= mod;
            }
            (dp[i][j][0] += dp[i - 1][(j + ub) % d][0]) %= mod;

            rep(k, 10){
                (dp[i][j][1] += dp[i - 1][(j + k) % d][1]) %= mod;
            }
        }
    }

    cout << (dp[nn][0][0] + dp[nn][0][1] - 1) % mod << endl;

	return 0;
}
