#include <bits/stdc++.h>
typedef long long ll;
#define int ll
#define range(i, a, n) for(int (i) = (a); (i) < (n); (i)++)
#define rep(i, n) for(int (i) = 0; (i) < (n); (i)++)
using namespace std;
typedef vector<double> vi;
typedef vector<vi> vvi;

signed main(void){
    int n, d; cin >> n >> d;

    int A, B, C; A = B = C = 0;
    while(d % 2 == 0 || d % 3 == 0 || d % 5 == 0){
        if(d % 2 == 0) d /= 2, A++;
        if(d % 3 == 0) d /= 3, B++;
        if(d % 5 == 0) d /= 5, C++;
    }
    if(d != 1){
        cout << -1 << endl;
        return 0;
    }

    vvi dpA(n + 1, vi(n * 2 + 1));
    vvi dpB(n + 1, vi(n * 2 + 1));
    vvi dpC(n + 1, vi(n * 2 + 1));

    dpA[0][0] = dpB[0][0] = dpC[0][0] = 1.0;

    range(i, 1, n + 1){
        rep(j, n * 2 + 1){
            dpA[i][j] = dpA[i - 1][j] * 3.0 / 6.0;
            if(j - 1 >= 0) dpA[i][j] += dpA[i - 1][j - 1] * 2.0 / 6.0;
            if(j - 2 >= 0) dpA[i][j] += dpA[i - 1][j - 2] * 1.0 / 6.0;

            dpB[i][j] = dpB[i - 1][j] * 4.0 / 6.0;
            if(j - 1 >= 0) dpB[i][j] += dpB[i - 1][j - 1] * 2.0 / 6.0;

            dpC[i][j] = dpC[i - 1][j] * 5.0 / 6.0;
            if(j - 1 >= 0) dpC[i][j] += dpC[i - 1][j - 1] * 1.0 / 6.0;
        }
    }

    double sumA, sumB, sumC; sumA = sumB = sumC = 0.0;
    range(i, A, n * 2 + 1) sumA += dpA[n][i];
    range(i, B, n * 2 + 1) sumB += dpB[n][i];
    range(i, C, n * 2 + 1) sumC += dpC[n][i];

    double res = sumA * sumB * sumC;
    printf("%.12f\n", res);

	return 0;
}
