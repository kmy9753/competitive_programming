#include <bits/stdc++.h>
 
#define _overload(_1,_2,_3,name,...) name
#define _rep(i,n) _range(i,0,n)
#define _range(i,a,b) for(int i=(int)(a);i<(int)(b);++i)
#define rep(...) _overload(__VA_ARGS__,_range,_rep,)(__VA_ARGS__)
 
#define _rrep(i,n) _rrange(i,n,0)
#define _rrange(i,a,b) for(int i=(int)(a)-1;i>=(int)(b);--i)
#define rrep(...) _overload(__VA_ARGS__,_rrange,_rrep,)(__VA_ARGS__)
 
#define _all(arg) begin(arg),end(arg)
#define uniq(arg) sort(_all(arg)),(arg).erase(unique(_all(arg)),end(arg))
#define getidx(ary,key) lower_bound(_all(ary),key)-begin(ary)
#define clr(a,b) memset((a),(b),sizeof(a))
#define bit(n) (1LL<<(n))
 
// #define DEBUG
 
#ifdef DEBUG
    #define dump(...) fprintf(stderr, __VA_ARGS__)
#else
    #define dump(...)
#endif
 
template<class T>bool chmax(T &a, const T &b) { return (a<b)?(a=b,1):0;}
template<class T>bool chmin(T &a, const T &b) { return (b<a)?(a=b,1):0;}
 
using namespace std;
using ll=long long;
using vi=vector<int>;
using vll=vector<ll>;
 
const double EPS = 1e-10;
const double PI = acos(-1.0);
const ll inf =1LL << 62;
const ll mod=1000000007LL;
 
ll extgcd(ll a,ll b,ll& x,ll& y){x=1,y=0;ll g=a;if(b!=0) g=extgcd(b,a%b,y,x),y-=a/b*x;return g;}
ll ADD(const ll &a, const ll &b,const ll &mod) { return (a+b)%mod;}
ll SUB(const ll &a, const ll &b,const ll &mod) { return (a-b+mod)%mod;}
ll MUL(const ll &a, const ll &b,const ll &mod) { return (1LL*a*b)%mod;}
ll DIV(const ll &a, const ll &b,const ll &mod) {ll x,y; extgcd(b,mod,x,y);return MUL(a,(x+mod)%mod,mod);}
 
random_device rd;
mt19937 mt(rd());
uniform_int_distribution<int> dice(1,6);
uniform_real_distribution<double> score(0.0,10.0);

int res, n;
vector<vi> field;
vector<vi> resfield;

vi dx = {-1, 1};
vi dy = {-1,-1};
void dfs(int x, int y, int cnt){
    if(x == n){
        x = 0;
        y++;
    }
    if(y == n){
        if(chmax(res, cnt)){
            resfield = field;
        }
        return;
    }

    rep(i, 1, 4){
        bool ok = true;
        auto check_diag = [&]{
            rep(j, 2){
                int ny = y, nx = x;
                while(ny >= 0 and nx >= 0 and nx < n){
                    ny += dy[j], nx += dx[j];
                    if(ny < 0 or nx < 0 or nx >= n) break;

                    if(field[ny][nx] == 1 or field[ny][nx] == 3){
                        ok = false;
                        return;
                    }
                }
            }
        };
        auto check_hv = [&]{
            rep(xx, x){
                if(field[y][xx] == 2 or field[y][xx] == 3){
                    ok = false;
                    return;
                }
            }
            rep(yy, y){
                if(field[yy][x] == 2 or field[yy][x] == 3){
                    ok = false;
                    return;
                }
            }
        };
        if(i == 1 or i == 3){
            check_diag();
        }
        if(ok and (i == 2 or i == 3)){
            check_hv();
        }

        if(ok){
            field[y][x] = i;
            dfs(x + 1, y, cnt + (i == 3 ? 2 : 1));
            field[y][x] = 0;
        }
    }
    dfs(x + 1, y, cnt);
}

int main(void){
    cin.tie(0);
    ios::sync_with_stdio(false);

    // cin >> n;
    n = 6;
    field = vector<vi>(n, vi(n));
    field[0] = {1, 1, 1, 3, 1, 1};
    dfs(0, 1, 7);
    cout << res << endl;

    rep(y, n){
        rep(x, n){
            cout << resfield[y][x] << " ";
        }
        cout << endl;
    }

    return 0;
}
